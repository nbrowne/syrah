
-- Problems and settings are stored in this table
create table problems (
	id int identity(1, 1) primary key,
	title varchar(1024) not null,
	notes text not null default(''),
	settings xml not null,
	test_cases xml not null
)

-- Create the client table
create table clients (
	name varchar(255) primary key,
	last_seen datetime not null default(getdate()),
	islands tinyint not null default(1),
	version varchar(32) not null,
	ip_address varchar(16) not null default '',
	processor_count int not null default(0),
	processor_details varchar(2048) not null default(''),
	operating_system varchar(1024) not null default(''),
	computer_name varchar(1024) not null default(''),
	os_memory int not null,
	current_problem_id int references problems(id)
)

-- Store the results from the clients here
create table problem_results (
	id int identity(1, 1) primary key,
	problem_id int not null references problems(id),
	run_number int not null,
	client_name varchar(255) references clients(name) not null,
	max_possible_fitness float not null,
	best_fitness float not null,	
	best_fitness_generation int not null,
	run_time int not null,					-- in seconds
	average_generation_time int not null,	-- in seconds
	max_generation_time int not null,		-- in seconds
	best_chromosome varbinary(max) not null,
	best_chromosome_text varchar(max) not null default(''),
	results_text varchar(max) not null default(''),
	run_date datetime not null default(getdate()),
	run_statistics xml not null
)

