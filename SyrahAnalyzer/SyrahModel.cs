﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SyrahAnalyzer {
    public class SyrahModel {
        protected readonly double HitRoundingThreshold = 0.5;

        #region RNC Arrays for Syrah Model
        List<List<double>> _rncConstants = new List<List<double>> {
            new List<double> {
                -8.17090329628945, 5.59230034965663, 8.44869906010511, 9.20084257572929, -3.43182601660109, -0.0729301711883998, 3.6849241022416, 8.59369297446389, -1.62086996790993, 0.703560090951417
            },
            new List<double> {
                -1.6093684693842, -3.87471792002894, 1.18374293259519, 3.57221855482656, -2.76658935135537, 7.35881144057904, 6.64416210569635, 9.83336645170737, 5.16545986531557, -9.5948497204086
            },
            new List<double> {
                -1.90363021190447, 9.87524149002286, -6.76239617018141, -3.43107426233174, 9.46915263285355, -5.19704763553899, -0.667233360310659, 5.14258077141949, -7.97751616592403, 1.86966959939788
            },
            new List<double> {
                3.17029538246351, 5.11350005637552, 2.01444693469184, -5.0734410691417, -0.855072229567483, 7.54017099623576, 7.5566341995991, 0.241594365910437, -0.336782788083323, -6.53525281536172
            }
        }; //End of constant definition
        #endregion

        #region Support Methods for Syrah Model
        protected double Sqrt(double p0) {
            return Math.Sqrt(p0);
        }

        protected double Exp(double p0) {
            return Math.Exp(p0);
        }

        protected double Sin(double p0) {
            return Math.Sin(p0);
        }

        protected double Cos(double p0) {
            return Math.Cos(p0);
        }

        protected double Tan(double p0) {
            return Math.Tan(p0);
        }

        protected double Log(double p0) {
            return Math.Log(p0);
        }

        protected double Floor(double p0) {
            return Math.Floor(p0);
        }

        protected double Ceiling(double p0) {
            return Math.Ceiling(p0);
        }

        protected double Not(double p0) {
            return p0 <= 0 ? 1 : 0;
        }

        protected double Div(double p0, double p1) {
            return p1 != 0 ? p0 / p1 : 1;
        }

        protected double Or(double p0, double p1) {
            return p0 > 0 || p1 > 0 ? 1 : 0;
        }

        protected double And(double p0, double p1) {
            return p0 > 0 && p1 > 0 ? 1 : 0;
        }

        protected double Lt(double p0, double p1) {
            return p0 < p1 ? p0 : p1;
        }

        protected double Gt(double p0, double p1) {
            return p0 > p1 ? p0 : p1;
        }

        protected double Lte(double p0, double p1) {
            return p0 <= p1 ? p0 : p1;
        }

        protected double Gte(double p0, double p1) {
            return p0 >= p1 ? p0 : p1;
        }

        protected double Eq(double p0, double p1) {
            return p0 == p1 ? p0 : p1;
        }

        protected double Ne(double p0, double p1) {
            return p0 != p1 ? p0 : p1;
        }

        #endregion

        //Automatically generated method by Syrah.
        public double EvaluateModel(double sd, double avg, double ent, double zcab, double zcls, double zcle) {
            double returnValue =
                //Gene 1 of 4
                Floor(Sqrt(((ent - Ceiling((Lte(zcle, zcab) + Log((Ceiling((zcab + zcls)) * ent))))) * Lt(sd, zcle))))
                + // Static linking function

                //Gene 2 of 4
                Lt((Or(((Tan(zcle) - ent) + zcab), Ceiling(zcle)) * (zcle - Floor(Ne((zcls * Eq(Gte(ent, zcle), (Floor(zcab) * And(ent, zcab)))), sd)))), Lte(ent, Lte(zcab, Div(Gt(zcab, Log(zcab)), Lte(sd, _rncConstants[1][9])))))
                + // Static linking function

                //Gene 3 of 4
                Lte(Gt(ent, ent), Lte(ent, (Cos(Log(Sqrt(avg))) * sd)))
                + // Static linking function

                //Gene 4 of 4
                Lte((Or(Lte(zcab, ent), Ceiling(_rncConstants[3][3])) - (zcls - Floor((avg + sd)))), Lte(ent, Lte(avg, Div(Gt(zcab, Log(Or(Lte(sd, zcab), Log(ent)))), Lt((Lte((zcab * ent), Sin(zcab)) + Sin((_rncConstants[3][6] * zcab))), _rncConstants[3][9])))))
            ;

            returnValue = returnValue >= this.HitRoundingThreshold ? 1.0 : 0.0;

            return returnValue;
        } //End of evaluate method.
    } //End of Class
}
