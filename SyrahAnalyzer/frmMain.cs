﻿/***********************************************************************
 * frmMain.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.Data;
using NigelBrowne.Syrah.Common.ProblemBuilder;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace SyrahAnalyzer {
    public partial class frmMain : Form {
        public frmMain() {
            InitializeComponent();

            //Instantiate a data access object to set the connection string for later
            DataAccess dataAccess = new DataAccess("Persist Security Info='False';User ID='SyrahUser';Password='syrah!@#';Initial Catalog='Syrah.ICEC';Data Source='127.0.0.1'");
            dataAccess = null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void btnLoadIndividual_Click(object sender, EventArgs e) {            
            //Get the run id from the user
            int runId = Int32.Parse(txtRunNumber.Text);

            //Get the results and the problem
            ProblemResult runResults = new ProblemResult(runId);
            Problem runProblem = new Problem(runResults.ProblemId);

            //Load the settings and best chromosome
            Chromosome individual = runResults.BestChromosome;
            GepSettings runSettings = runProblem.Settings;

            //Build the code for the indivdual
            string codeBuffer  = ChromosomeTranslator.ToCSharp(individual, runSettings).Replace("\t", "    ");

            //Display the code
            txtIndividualCode.Text = codeBuffer;
        }

        private void btnLdTest_Click(object sender, EventArgs e) {
            SyrahModel model = new SyrahModel();
            int index = 0;
            int hits = 0, misses = 0;

            Problem testProblem = new Problem(14);
            ProblemResult results = new ProblemResult(2154);

            DataTable data = new DataTable("results");
            data.Columns.Add("index", typeof(Int32));
            data.Columns.Add("expected", typeof(double));
            data.Columns.Add("evolved", typeof(double));
            data.BeginLoadData();

            foreach (TestCase test in testProblem.TestCases) {
                DataRow newRow = data.NewRow();

                double evolvedValue = model.EvaluateModel(test.TerminalValues["sd"], test.TerminalValues["avg"], test.TerminalValues["ent"], test.TerminalValues["zcab"], test.TerminalValues["zcls"], test.TerminalValues["zcle"]);

                newRow["index"] = index;
                newRow["expected"] = test.ExpectedValue;
                newRow["evolved"] = evolvedValue;

                if (test.ExpectedValue == evolvedValue)
                    hits++;
                else
                    misses++;

                data.Rows.Add(newRow);

                index++;
            }

            data.EndLoadData();

            dataGridView1.DataSource = data;

            label1.Text = "Hits :"+ hits.ToString() + " - " + ((double)hits / (double)testProblem.TestCases.Count * 100.0).ToString("0.00") + " %";
            label2.Text = "Misses :" + misses.ToString() + " - " + ((double)misses / (double)testProblem.TestCases.Count * 100.0).ToString("0.00") + " %";

            foreach(DataGridViewRow row in dataGridView1.Rows) {
                if (row.Cells["expected"].Value != null) {
                    if (row.Cells["expected"].Value.ToString() != row.Cells["evolved"].Value.ToString()) {
                        row.DefaultCellStyle.BackColor = Color.Tomato;
                    }
                }
            }
        }
    }
}
