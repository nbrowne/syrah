﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace SyrahClient {
    public partial class SyrahClientService : ServiceBase {
        public SyrahClientService() {
            //Set the name of the service
            this.ServiceName = "Syrah Evolution Client";

            //Enable autologging to the event log
            this.AutoLog = true;
            
            //Set what notifications the service supports
            this.CanStop = true;
            this.CanShutdown = false;
            this.CanPauseAndContinue = false;

            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
        }

        protected override void OnStop() {
        }
    }
}
