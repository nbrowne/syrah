﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SyrahClassBuilder {
    /// <summary>
    /// Summary description for CimRegEx.
    /// </summary>
    public class RegEx {
        public RegEx() {
        }


        /// <summary>
        /// Regular expression match evaluator to capitalize the first letter of a word
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        static string CapText(Match MatchItem) {
            string sBuffer = MatchItem.ToString();

            //Modify the buffer if the first letter isn't a capital
            if (char.IsLower(sBuffer[0])) {
                //Cap the first letter and then tag on the remaining bits
                sBuffer = char.ToUpper(sBuffer[0]) + sBuffer.Substring(1, sBuffer.Length - 1);
            }

            //Return the buffer
            return sBuffer;
        }

        public string CapitalizeFirsts(string Text) {
            //Capitalize all the words using a regular expression match
            Text = Regex.Replace(Text, @"\w+", new MatchEvaluator(CapText));

            //Return the updated buffer
            return Text;
        }
    }
}
