﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace SyrahClassBuilder {

	/// <summary>
	/// Summary description for MssqlDataAccess.
	/// </summary>
	public class DataAccess {
		//Define the member vars that hold our connection object
		protected SqlConnection _oConnection;
		protected SqlCommand _oCommand;

		protected string _sDbConnectionString;		
		protected string _sLastSQLQuery = "";

		protected string _sDatabaseServer = "";
		protected string _sDatabaseName = "";
		protected string _sDatabaseUser = "";
		protected string _sDatabasePassword = "";

		/// <summary>
		/// Default constructor
		/// </summary>
		public DataAccess(string server, string name, string user, string password) {
			//Populate our member variables
			_sDatabaseServer = server;
			_sDatabaseName = name;
			_sDatabaseUser = user;
			_sDatabasePassword = password;

			//Load the application database settings from the web.config file
			this._sDbConnectionString = this.BuildConnectionString();
	
			//Open the connection to the database
			this.OpenConnection();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="timestamp"></param>
		/// <returns></returns>
		public static string GetNativeDateTimeFormat(DateTime timestamp) {
			return timestamp.ToString("MMM dd, yyyy HH:mm:ss");
		}


		/// <summary>
		/// Returns the last identity value used by the specific table for an insert
		/// </summary>
		/// <param name="table"></param>
		/// <returns></returns>
		public int GetLastIdentity(string table) {
			//MySql returns per-connection identity values
			string sSql = "select IDENT_CURRENT('"+ table +"') as value";
			return Int32.Parse(this.ExecuteScalar(sSql).ToString());
		}


		/// <summary>
		/// Executes an SQL query and returns the results in a datatable
		/// </summary>
		/// <param name="SQL">SQL query statement.</param>
		/// <returns>DataTable containing the results of the query.</returns>
		public DataTable Query(string SQL) {
			//Update the last queried var
			this._sLastSQLQuery = SQL;

			//Instantiate our adapter and return objects
			SqlDataAdapter oAdapter = new SqlDataAdapter();
			DataTable oData = new DataTable();
		
			//Configure the command object
			_oCommand.CommandType = CommandType.Text;
			_oCommand.CommandText = SQL;

			//Attach the command object to the adapter
			oAdapter.SelectCommand = (SqlCommand)_oCommand;

			//Fill the dataset
			oAdapter.Fill(oData);

			//Return the datatable that we've built
			return oData;
		}


		/// <summary>
		/// Worker method to open the connection to the database. This is called by the base classes constructor.
		/// </summary>
		protected void OpenConnection() {
			//instantiate new connection and command objects
			this._oConnection = new SqlConnection(this._sDbConnectionString);
			this._oCommand = new SqlCommand();

			//Assign the connection to the command
			_oCommand.Connection = (SqlConnection)this._oConnection;

			_oCommand.CommandTimeout = 300;

			//Open the connection
			_oCommand.Connection.Open();
		}


		/// <summary>
		/// Build the connection string. In this case, we just pull it from the config file.
		/// </summary>
		/// <returns></returns>
		protected string BuildConnectionString() {	
			return "Persist Security Info='False';User ID='"+ _sDatabaseUser +"';Password='"+ _sDatabasePassword +"';Initial Catalog='"+ _sDatabaseName +"';Data Source='"+ _sDatabaseServer +"'";
		}

		public void Close() {
			_oConnection.Close();
		}

		/// <summary>
		/// Default destructor, makes sure that the connection object is closed
		/// </summary>
		~DataAccess() {
			if (this._oConnection!=null && _oConnection.State != ConnectionState.Closed) {
				//_oConnection.Close();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		public string CleanSql(string sql) {
			return sql.Replace("\r", " ").Replace("\n", " ").Replace("\t", "");
		}

		/// <summary>
		/// Retrieve the last SQL statement
		/// </summary>
		public string LastQuery {
			get { return this._sLastSQLQuery; }
		}

		/// <summary>
		/// Convert boolean values into integers for use in sql statements
		/// </summary>
		/// <param name="Value"></param>
		/// <returns></returns>
		public int BoolToInt(bool Value) {
			return (Value ? 1 : 0);
		}

			
		public void BeginTransaction() {
			_oCommand.Transaction = _oConnection.BeginTransaction(IsolationLevel.ReadUncommitted);
		}

		public void RollbackTransaction() {
			_oCommand.Transaction.Rollback();
		}

		public void CommitTransaction() {
			_oCommand.Transaction.Commit();
		}

		/// <summary>
		/// Queries the table/field combination for the next availbe key value
		/// </summary>
		/// <param name="TableName">Name of the table to query.</param>
		/// <param name="FieldName">Name of the field that contains the id list.</param>
		/// <returns>Integer of the next available ID</returns>
		public int GetNextID(string TableName, string FieldName) {
			//Build the sql query
			string sSQL = "select isNull(max("+ FieldName +")+1, 0) as next_id from "+ TableName;

			//Return the new id
			return (Int32)this.ExecuteScalar(sSQL);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="SQL"></param>
		/// <returns></returns>
		public Object ExecuteScalar(string SQL) {
			//Update the last queried var
			this._sLastSQLQuery = SQL;		

			//Configure the command object
			_oCommand.CommandType = CommandType.Text;
			_oCommand.CommandText = SQL;

			//Return our object
			return _oCommand.ExecuteScalar();
		}


		/// <summary>
		/// Executes an SQL Statement
		/// </summary>
		/// <param name="SQL">SQL statement to execute.</param>
		/// <returns>Records affected by the statement.</returns>
		public int ExecuteSql(string sql) {
			int iReturn = -1;

			//Update the last queried var
			this._sLastSQLQuery = sql;

			try {
				//Configure the command object
				_oCommand.CommandType = CommandType.Text;
				_oCommand.CommandText = sql;
				iReturn = _oCommand.ExecuteNonQuery();
			} catch (Exception e) {
				throw new Exception("Error executing sql statement: "+ sql, e);
			} 

			//Execute the statement and return the number of records affected
			return iReturn;
		}
	}
}

