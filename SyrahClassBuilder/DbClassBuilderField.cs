﻿using System;
using System.Data;
using System.Diagnostics;
using System.Collections;


namespace SyrahClassBuilder {
    /// <summary>
    /// Summary description for DbClassBuilderField.
    /// </summary>
    public class DbClassBuilderField {
        #region Member Variables
        protected string _sName;
        protected string _sCSharpType;
        protected string _sCSharpPrefix;
        protected string _sDbType;
        protected string _sFieldName;
        protected string _sMemberVar;
        protected string _sTable;
        protected int _iSize;
        protected bool _bNullable;
        protected bool _bBuildGet;
        protected bool _bBuildSet;
        protected bool _bBuildSave;
        protected bool _bIdentity;
        protected bool _bPrimaryKey;
        protected bool _bBuildFilter;
        #endregion

        #region Properties
        public string Name {
            get { return _sName; }
            set { _sName = value; }
        }

        public string CSharpType {
            get { return _sCSharpType; }
            set { _sCSharpType = value; }
        }

        public string CSharpPrefix {
            get { return _sCSharpPrefix; }
            set { _sCSharpPrefix = value; }
        }

        public string DbType {
            get { return _sDbType; }
            set { _sDbType = value; }
        }

        public string FieldName {
            get { return _sFieldName; }
            set { _sFieldName = value; }
        }

        public string MemberVar {
            get { return _sMemberVar; }
            set { _sMemberVar = value; }
        }

        public string Table {
            get { return _sTable; }
            set { _sTable = value; }
        }

        public int Size {
            get { return _iSize; }
            set { _iSize = value; }
        }

        public bool Nullable {
            get { return _bNullable; }
            set { _bNullable = value; }
        }

        public bool BuildGet {
            get { return _bBuildGet; }
            set { _bBuildGet = value; }
        }

        public bool BuildSet {
            get { return _bBuildSet; }
            set { _bBuildSet = value; }
        }

        public bool BuildSave {
            get { return _bBuildSave; }
            set { _bBuildSave = value; }
        }

        public bool Identity {
            get { return _bIdentity; }
            set { _bIdentity = value; }
        }

        public bool PrimaryKey {
            get { return _bPrimaryKey; }
            set { _bPrimaryKey = value; }
        }

        public bool BuildFilter {
            get { return _bBuildFilter; }
            set { _bBuildFilter = value; }
        }
        #endregion

        public DbClassBuilderField(string table, string name, string dbType, int size, bool nullable,
            bool identity, bool buildGet, bool buildSet, bool buildSave, bool buildFilter, bool isPrimaryKey) {

            string[] sTypeData = GetCSharpTypeData(dbType);

            this.Table = table;
            this.Name = name;
            this.CSharpType = sTypeData[0];
            this.CSharpPrefix = sTypeData[1];
            this.DbType = dbType;
            this.Size = size;
            this.Nullable = nullable;
            this.BuildGet = buildGet;
            this.BuildSet = buildSet;
            this.BuildSave = buildSave;
            this.BuildFilter = buildFilter;
            this.Identity = identity;
            this.PrimaryKey = isPrimaryKey;

            //Format the fieldName
            FieldName = "";
            foreach (string sWord in name.ToLower().Split('_')) {
                FieldName += sWord[0].ToString().ToUpper() + sWord.Substring(1);
            }


            MemberVar = "_" + this.FieldName[0].ToString().ToLower() + this.FieldName.Substring(1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <returns></returns>
        protected string[] GetCSharpTypeData(string dbType) {
            string[] sReturn = { "string", "s" };

            switch (dbType.ToLower()) {
                case "int": sReturn[0] = "int"; sReturn[1] = "i"; break;
                case "tinyint": sReturn[0] = "int"; sReturn[1] = "i"; break;
                case "smallint": sReturn[0] = "int"; sReturn[1] = "i"; break;
                case "real": sReturn[0] = "float"; sReturn[1] = "f"; break;
                case "decimal": sReturn[0] = "Decimal"; sReturn[1] = "d"; break;
                case "datetime": sReturn[0] = "DateTime"; sReturn[1] = "dt"; break;
                case "bit": sReturn[0] = "bool"; sReturn[1] = "b"; break;
                case "varbinary": sReturn[0] = "byte[]"; sReturn[1] = "yield"; break;
                case "float": sReturn[0] = "float"; sReturn[1] = "f"; break;
            }

            return sReturn;
        }
    }
}
