﻿using System;
using System.Data;
using System.Diagnostics;
using System.Collections;


namespace SyrahClassBuilder {
    /// <summary>
    /// Summary description for DbClassBuilder.
    /// </summary>
    public class DbClassBuilder {
        protected static string DEFAULT_NAMESPACE = "NigelBrowne.Syrah.Common";

        #region DetailTableItem - This protected class is used by the DbBuilderClass to track the detail tables
        protected class DetailTableItem {
            protected string _sDetailTable = "";
            protected string _sDetailIdColumn = "";
            protected string _sParentIdColumn = "";
            protected int _iIndex = -1;

            public string DetailTable { get { return _sDetailTable; } set { _sDetailTable = value; } }
            public string DetailIdColumn { get { return _sDetailIdColumn; } set { _sDetailIdColumn = value; } }
            public string ParentIdColumn { get { return _sParentIdColumn; } set { _sParentIdColumn = value; } }
            public int Index { get { return _iIndex; } set { _iIndex = value; } }

            public DetailTableItem(string table, string detailIdColumn, string parentIdColumn, int index) {
                this.DetailTable = table.Trim();
                this.DetailIdColumn = detailIdColumn.Trim();
                this.ParentIdColumn = parentIdColumn.Trim();
                this.Index = index;
            }
        }

        #endregion

        protected string _sNamespace = "";
        protected string _sClassName = "";
        protected string _sTableName = "";
        protected ArrayList _alDetailTables = null;
        protected string _sDataAccessClass = "";

        #region Properties
        public string Namespace {
            get { return _sNamespace; }
            set { _sNamespace = value; }
        }

        public string ClassName {
            get { return _sClassName; }
            set { _sClassName = value; }
        }

        public string DataAccessClass {
            get { return _sDataAccessClass; }
            set { _sDataAccessClass = value; }
        }

        public string TableName {
            get { return _sTableName; }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        public DbClassBuilder()
            : base() {
        }      

        #region Routines for building the class after all the user options have been set.

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnData"></param>
        public string BuildClass(DataTable columnData) {
            ArrayList oFieldList = new ArrayList();
            RegEx oRegEx = new RegEx();

            //Initialize the main entity buffers
            string sMembers = "\n";
            string sProperties = "\n";
            string sLoadFromDatabaseBlock = "\n";
            string sNfcInitialization = "\n";
            string sNfcList = "\n";

            //Initialize the filter buffers
            string sBuildFilterWhere = "\n";
            string sFilterMemberVariables = "\n";
            string sFilterProperties = "\n";

            //Initialize the primary key data
            string sPrimaryKeyMemberVariable = "";
            string sPrimaryKeyFieldName = "";
            string sPrimaryKeyMemberType = "";
            DbClassBuilderField oPrimaryKeyField = null;

            //Load the template
            string sTemplate = this.GetClassTemplate();

            //Set the default namespace
            if (_sNamespace.Trim().Length == 0) {
                _sNamespace = DEFAULT_NAMESPACE;
            }

            //Set the default data access class
            if (_sDataAccessClass.Trim().Length == 0) {
                _sDataAccessClass = "DataAccess";
            }

            //Set the default classname
            if (_sClassName.Trim().Length == 0) {
                _sClassName = oRegEx.CapitalizeFirsts(_sTableName.Replace("_", " ")).Replace(" ", "").TrimEnd('s');
            }

            //Iterate over the fields items, adding them to our field list
            foreach (DataRow oItem in columnData.Rows) {
                //Get the data for the new field from the database row
                string sName = (string)oItem["field_name"];
                string sDataType = (string)oItem["type_name"];
                string sFieldTable = (string)oItem["table_name"];
                int iSize = Int32.Parse(oItem["length"].ToString());
                bool bIsNullable = (string)oItem["nullable"] == "Yes";
                bool bIsIdentity = (string)oItem["is_identity"] == "Yes";
                bool bIsPrimaryKey = (bool)oItem["is_primarykey"];
                bool bBuildGet = (bool)oItem["build_get"];
                bool bBuildSet = (bool)oItem["build_set"];
                bool bBuildSave = (bool)oItem["build_save"];
                bool bBuildFilter = (bool)oItem["build_filter"];

                //Create a new Field object and add it to the list
                DbClassBuilderField oField = new DbClassBuilderField(sFieldTable, sName, sDataType, iSize, bIsNullable, bIsIdentity,
                    bBuildGet, bBuildSet, bBuildSave, bBuildFilter, bIsPrimaryKey);
                oFieldList.Add(oField);

                //If the field is the primary key, then load it into the primary key variables
                if (bIsPrimaryKey) {
                    sPrimaryKeyMemberVariable = oField.MemberVar;
                    sPrimaryKeyFieldName = oField.Name;
                    sPrimaryKeyMemberType = oField.CSharpType;
                    oPrimaryKeyField = oField;
                }
            }

            //Process the field list and create the properties and member variables
            foreach (DbClassBuilderField oField in oFieldList) {
                //Check if the current field needs a get or set property or if it is to be saved
                if (oField.BuildGet || oField.BuildSave || oField.BuildSet) {
                    sMembers += BuildMemberVariable(oField);
                    sProperties += BuildProperty(oField);
                    sLoadFromDatabaseBlock += BuildLoadStatement(oField);

                    //Check if we have an nullable field that the user can update.
                    //If we do, we need to add it to our list of nullable fields
                    if (oField.Nullable && oField.BuildSet) {
                        sNfcInitialization += BuildNullablePropertyInitialization(oField);
                        sNfcList += BuildNullablePropertyList(oField);
                    }
                } //End of get/set/save check

                //If the current field will be a filter item, add it to the filter buffers
                if (oField.BuildFilter) {
                    sBuildFilterWhere += BuildFilterWhere(oField);
                    sFilterMemberVariables += BuildFilterMemberVariable(oField);
                    sFilterProperties += BuildFilterProperty(oField);
                }
            }

            //Cleanup items
            sLoadFromDatabaseBlock = sLoadFromDatabaseBlock.TrimEnd('\n').TrimEnd('\r').TrimStart();

            //Build the sql select statements
            string sSqlSelectById = BuildRecordSelectStatement(_sTableName, oPrimaryKeyField, oFieldList);
            string sSqlSelectAll = sSqlSelectById.Substring(0, sSqlSelectById.LastIndexOf("where")).TrimEnd();

            //Replace the parts of the template
            sTemplate = sTemplate.Replace("$Namespace$", _sNamespace);
            sTemplate = sTemplate.Replace("$ClassName$", _sClassName);
            sTemplate = sTemplate.Replace("$TableName$", _sTableName);
            sTemplate = sTemplate.Replace("$Properties$", sProperties);
            sTemplate = sTemplate.Replace("$MemberVariables$", sMembers);
            sTemplate = sTemplate.Replace("$BuildWhereFromFilter$", sBuildFilterWhere);
            sTemplate = sTemplate.Replace("$FilterMemberVariables$", sFilterMemberVariables);
            sTemplate = sTemplate.Replace("$FilterProperties$", sFilterProperties);
            sTemplate = sTemplate.Replace("$LoadFromDatabaseRow$", sLoadFromDatabaseBlock);
            sTemplate = sTemplate.Replace("$NullableFieldsCollectionInitialization$", sNfcInitialization);
            sTemplate = sTemplate.Replace("$NullableFieldsCollectionList$", sNfcList);
            sTemplate = sTemplate.Replace("$PrimaryKeyMemberVariable$", sPrimaryKeyMemberVariable);
            sTemplate = sTemplate.Replace("$sPrimaryKeyMemberType$", sPrimaryKeyMemberType);
            sTemplate = sTemplate.Replace("$SqlStatement_SelectAll$", sSqlSelectAll);
            sTemplate = sTemplate.Replace("$SqlStatement_Select$", sSqlSelectById);
            sTemplate = sTemplate.Replace("$SqlStatement_Insert$", BuildRecordInsertStatement(_sTableName, oFieldList));
            sTemplate = sTemplate.Replace("$SqlStatement_Update$", BuildRecordUpdateStatement(_sTableName, oPrimaryKeyField, oFieldList));
            sTemplate = sTemplate.Replace("$ParameterValidation$", "");
            sTemplate = sTemplate.Replace("$MainTablePrimaryKey$", sPrimaryKeyFieldName);
            sTemplate = sTemplate.Replace("$DataAccessClass$", _sDataAccessClass);
            
            if (oPrimaryKeyField.CSharpType=="int")
                sTemplate = sTemplate.Replace("$UpdateIdentityOnInsert$", "this." + sPrimaryKeyMemberVariable + " = dataAccess.GetLastIdentity(\""+ _sTableName +"\");");
            else
                sTemplate = sTemplate.Replace("$UpdateIdentityOnInsert$", "");

            //Output the new class to a new document
            //this.OutputClassToNewDocument(sTemplate);
            return sTemplate;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string BuildFilterWhere(DbClassBuilderField field) {
            string sReturn = "";
            string sType = field.CSharpType.ToLower().Trim();

            //Build the sFieldValueReference
            if (sType != "datetime") {
                sReturn += this.BuildFilterWhereWorker(sType, field.Name, field.FieldName, field.MemberVar);

            } else {
                sReturn += this.BuildFilterWhereWorker(sType, field.Name, field.FieldName + "Start", field.MemberVar + "Start");
                sReturn += this.BuildFilterWhereWorker(sType, field.Name, field.FieldName + "End", field.MemberVar + "End");
            }

            return sReturn;
        }


        protected string BuildFilterWhereWorker(string type, string colName, string fieldName, string memberName) {
            const string sTabs = "\t\t\t";
            string sReturn = "";
            string sOperator = "=";

            //Build the sFieldValueReference
            string sFieldValueReference = "filter." + fieldName;

            //If it's a number type, we need to "ToString()" it
            if (type == "int" || type == "float" || type == "decimal") {
                sFieldValueReference += ".ToString()";
            } else if (type == "datetime") {
                sFieldValueReference += ".ToString(DataAccess.NativeDateFormat)";
            }

            //Add the surrounding quotes and string concats
            sFieldValueReference = "\"+ " + sFieldValueReference + " +\"";

            //If it's a date or string type, then we need to surround it in single quotes for the db.
            if (type == "datetime" || type == "string") {
                sFieldValueReference = "'" + sFieldValueReference + "'";
            }

            //Update operator for datetime types
            if (type == "datetime") {
                //If this is the start, then we need >=.
                //If this is the end, the we need <=
                if (fieldName.EndsWith("Start")) {
                    sOperator = ">=";

                } else {
                    sOperator = "<=";
                }
            }

            sReturn += sTabs + "if (filter.Is" + fieldName + "Set) {\n";
            sReturn += sTabs + "\twhereStatement += \" and " + colName + sOperator + sFieldValueReference + " \";\n";
            sReturn += sTabs + "}\n";

            return sReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string BuildFilterMemberVariable(DbClassBuilderField field) {
            string sInitialValue = "";
            string sReturn = "";
            string sType = field.CSharpType.ToLower().Trim();

            //If the field is the primary key, we'll initialize it to -1 so we know later if we need an insert or an update            
            if (sType == "int") sInitialValue = " = int.MinValue";
            if (sType == "byte") sInitialValue = " = byte.MinValue";
            if (sType == "float") sInitialValue = " = float.MinValue";
            if (sType == "decimal") sInitialValue = " = decimal.MinValue";
            if (sType == "string") sInitialValue = " = \"\"";
            if (sType == "datetime") sInitialValue = " = DateTime.MinValue";

            //Handle a normal member var build
            if (sType != "datetime") {
                //Build the member variable and it's set flag
                sReturn += "\t\tprotected " + field.CSharpType + " " + field.MemberVar + sInitialValue + ";\n";
                sReturn += "\t\tprotected bool _is" + field.FieldName + "Set = false;\n";

            } else {
                //We're handling a date so make start and end members
                //Build the start
                sReturn += "\t\tprotected " + field.CSharpType + " " + field.MemberVar + "Start" + sInitialValue + ";\n";
                sReturn += "\t\tprotected bool _is" + field.FieldName + "StartSet = false;\n";
                //Build the end
                sReturn += "\t\tprotected " + field.CSharpType + " " + field.MemberVar + "End" + sInitialValue + ";\n";
                sReturn += "\t\tprotected bool _is" + field.FieldName + "EndSet = false;\n";

            }
            //Return the member variable declaration
            return sReturn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string BuildFilterProperty(DbClassBuilderField field) {
            string sReturn = "";

            //Handle the non-date fields first, dates need start and end properties
            if (field.CSharpType.ToLower() != "datetime") {
                sReturn += this.BuildFilterPropertyWorker(field.CSharpType, field.FieldName, field.MemberVar);

            } else {
                sReturn += this.BuildFilterPropertyWorker(field.CSharpType, field.FieldName + "Start", field.MemberVar + "Start");
                sReturn += this.BuildFilterPropertyWorker(field.CSharpType, field.FieldName + "End", field.MemberVar + "End");
            }

            return sReturn;
        }

        protected string BuildFilterPropertyWorker(string type, string fieldName, string memberName) {
            string sReturn = "", sTabs = "\t\t";

            //Build the get and set for the field
            sReturn += sTabs + "public " + type + " " + fieldName + " {\n";
            sReturn += sTabs + "\tget { return " + memberName + "; }\n";
            sReturn += sTabs + "\tset { " + memberName + "=value; _is" + fieldName + "Set=true; }\n";
            sReturn += sTabs + "}\n";

            //Build the get for the IsSet property for the field
            sReturn += sTabs + "public bool Is" + fieldName + "Set {\n";
            sReturn += sTabs + "\tget { return _is" + fieldName + "Set; }\n";
            sReturn += sTabs + "}\n\n";

            return sReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string GetFieldValueSaveText(DbClassBuilderField field) {
            bool bNeedsQuotes = field.CSharpPrefix == "s" || field.CSharpPrefix == "dt";
            string sValue = field.MemberVar;
            string sReturn = "";

            //Check if the field is a date modified field, in which case override any value supplied with the 
            //current date and time
            if (field.Name.Trim().ToLower() == "date_modified" || field.Name.Trim().ToLower() == "modified_date") {
                sValue = "DateTime.Now";
            }

            switch (field.CSharpPrefix) {
                case "s":
                    sValue += ".Replace(\"'\", \"''\")";
                    break;
                case "dt":
                    sValue += ".ToString(DataAccess.NativeDateFormat)";
                    break;
                case "b":
                    sValue = "dataAccess.BoolToInt(" + sValue + ").ToString()";
                    break;
                default:
                    sValue += ".ToString()";
                    break;
            }

            sValue = "\"+ " + sValue + " +@\"";
            if (bNeedsQuotes) sValue = "'" + sValue + "'";

            if (field.Nullable) {
                //Reformat the value for the nullable field processing
                if (bNeedsQuotes) {
                    sValue = "\"" + sValue + "\"";
                } else {
                    sValue = sValue.Substring(3).Substring(0, sValue.Length - 7);
                }
                sReturn = "\"+ this.GetNullablePropertyValue(\"" + field.FieldName + "\", " + sValue + ") +@\"";
            } else {
                sReturn = sValue;
            }

            return sReturn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string BuildRecordSelectStatement(string tableName, DbClassBuilderField primaryKey, ArrayList fieldList) {
            string sFieldList = "";
            string sJoinList = "";
            string sReturn = "";
            string sInitialIndent = "\t\t\t\t";
            
            //Build the id text for the where statement
            string sIdText = "\"+ recordId.ToString() +@\"";
            if (primaryKey.CSharpType.ToLower().Trim() == "string") sIdText = "'\"+ recordId +@\"'";


            //Iterate over the fields in the field list
            foreach (DbClassBuilderField oField in fieldList) {
                //Add the db field to the lists, unless it's an identity field
                //if (!oField.Identity) {
                if (oField.BuildSet || oField.BuildSave || oField.BuildGet) {
                    sFieldList += sInitialIndent + "\t";

                    //Get the correct table prefix
                    sFieldList += GetSelectTablePrefix(oField.Table);

                    //Append the field name
                    sFieldList += oField.Name + ",\n";
                }
                //}
            }

            //Build the join list
            foreach (DetailTableItem oItem in _alDetailTables) {
                sJoinList += sInitialIndent;
                sJoinList += "left join " + oItem.DetailTable + " detail_" + oItem.Index + " on detail_" + oItem.Index + "." + oItem.DetailIdColumn + "=main." + oItem.ParentIdColumn + "\n";
            }
            //Trim the trailing comma (and extra cruft) that isn't needed for the last item
            sFieldList = sFieldList.TrimEnd('\n').TrimEnd(' ').TrimEnd(',') + "\n";

            //Put it all together for the insert statement
            sReturn += "\n";
            sReturn += sInitialIndent + "select \n";
            sReturn += sFieldList;
            sReturn += sInitialIndent + "from " + tableName + " main \n";
            sReturn += sJoinList;
            sReturn += sInitialIndent + "where main." + primaryKey.Name + "="+ sIdText +"";
            sReturn += "\n" + sInitialIndent.Substring(0, sInitialIndent.Length - 1);

            return sReturn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <returns></returns>
        protected string GetSelectTablePrefix(string fieldTable) {
            string sPrefix = "main.";
            string sTable = fieldTable.Trim().ToLower();

            //If the table isn't the main table, then iterate over the details
            //tables until we find it
            if (sTable != this._sTableName.Trim().ToLower()) {
                foreach (DetailTableItem oItem in _alDetailTables) {
                    if (oItem.DetailTable.Trim().ToLower() == sTable) {
                        sPrefix = "detail_" + oItem.Index + ".";
                    }
                }
            }

            return sPrefix;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string BuildRecordInsertStatement(string tableName, ArrayList fieldList) {
            string sFieldList = "\t\t\t\t\t\t";
            string sValueList = "";
            string sReturn = "";

            //Iterate over the fields in the field list
            foreach (DbClassBuilderField oField in fieldList) {
                //Add the db field to the lists, unless it's an identity field or it's not 
                //part of the main table
                if (oField.BuildSave && !oField.Identity && oField.Table.Trim().ToLower() == tableName.Trim().ToLower()) {
                    sFieldList += "" + oField.Name + ", ";

                    sValueList += "\t\t\t\t\t\t";
                    sValueList += GetFieldValueSaveText(oField) + ", \n";
                }
            }

            //Trim the trailing comma (and extra cruft) that isn't needed for the last item
            sFieldList = sFieldList.TrimEnd('\n').TrimEnd(' ').TrimEnd(',') + "\n";
            sValueList = sValueList.TrimEnd('\n').TrimEnd(' ').TrimEnd(',') + "\n";

            //Put it all together for the insert statement
            sReturn += "\n\t\t\t\t\t";
            sReturn += "insert into " + tableName + "(\n";
            sReturn += sFieldList;
            sReturn += "\t\t\t\t\t) values (\n";
            sReturn += sValueList;
            sReturn += "\t\t\t\t\t)\n";
            sReturn += "\t\t\t\t";

            return sReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string BuildRecordUpdateStatement(string tableName, DbClassBuilderField primaryKey, ArrayList fieldList) {
            string sReturn = "";
            string sUpdates = "";
            
            //Build the id text for the where statement
            string sIdText = "\"+ this." + primaryKey.MemberVar + ".ToString() +@\"";
            if (primaryKey.CSharpType.ToLower().Trim() == "string") sIdText = "'\"+ this." + primaryKey.MemberVar + " +@\"'";

            //Iterate over each of the fields in the field list, adding them to the update statement
            foreach (DbClassBuilderField oField in fieldList) {
                //Check if the field is supposed to be saved
                if (oField.BuildSave && !oField.Identity && oField.Table.Trim().ToLower() == tableName.Trim().ToLower()) {
                    sUpdates += "\t\t\t\t\t\t" + oField.Name + " = ";
                    sUpdates += GetFieldValueSaveText(oField) + ", \n";
                }
            }

            //Trim the trailing comma (and extra cruft) that isn't needed for the last item
            sUpdates = sUpdates.TrimEnd('\n').TrimEnd(' ').TrimEnd(',') + "\n";

            //Put it all together to build the update statement
            sReturn += "\n\t\t\t\t\t";
            sReturn += "update " + tableName + " set\n";
            sReturn += sUpdates;
            sReturn += "\t\t\t\t\t";
            sReturn += "where " + primaryKey.FieldName + "=" + sIdText + "\n\t\t\t\t";

            //Return the update statement
            return sReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string BuildNullablePropertyInitialization(DbClassBuilderField field) {
            string sReturn = "";

            sReturn += "\t\t\t";
            sReturn += "_propertyNullStateDictionary.Add(\"" + field.FieldName + "\", true);";
            sReturn += "\n";

            return sReturn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string BuildNullablePropertyList(DbClassBuilderField field) {
            string sReturn = "";

            sReturn += "\t\t\t";
            sReturn += "isNullable |= propertyName.Trim().ToLower()==\"" + field.FieldName.Trim().ToLower() + "\";";
            sReturn += "\n";

            return sReturn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        protected string BuildMemberVariable(DbClassBuilderField field) {
            string sInitialValue = "";

            //Add a comment indicating if the field is nullable in the table
            string sComment = (field.Nullable ? "\t\t\t//Nullable" : "");
        
            //Return the member variable declaration
            return "\t\t\tprotected " + field.CSharpType + " " + field.MemberVar + sInitialValue + "; " + sComment + "\n";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected string BuildLoadStatement(DbClassBuilderField field) {
            string sReturn = "\t\t\t";

            if (field.Nullable) {
                sReturn += "if (!dataItem[\"" + field.Name + "\"].Equals(System.DBNull.Value)) this." + field.MemberVar + " = ";
            } else {
                sReturn += "this." + field.MemberVar + " = ";
            }

            switch (field.CSharpType.ToLower()) {
                case "datetime":
                    sReturn += "DateTime.Parse(dataItem[\"" + field.Name + "\"].ToString());";
                    break;

                case "float":
                    sReturn += "(float)dataItem[\"" + field.Name + "\"];";
                    break;

                case "decimal":
                    sReturn += "Decimal.Parse(dataItem[\"" + field.Name + "\"].ToString());";
                    break;

                case "int":
                    if (field.DbType=="int")
                        sReturn += "(int)dataItem[\"" + field.Name + "\"];";
                    if (field.DbType == "smallint")
                        sReturn += "(int)dataItem[\"" + field.Name + "\"];";
                    if (field.DbType == "tinyint")
                        sReturn += "(byte)dataItem[\"" + field.Name + "\"];";
                    break;

                case "string":
                    sReturn += "(string)dataItem[\"" + field.Name + "\"];";
                    break;

                case "bool":
                    sReturn += "(bool)dataItem[\"" + field.Name + "\"];";
                    break;

                case "byte[]":
                    sReturn += "(byte[])dataItem[\"" + field.Name + "\"];";
                    break;
            }

            sReturn += "\n";

            return sReturn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string BuildProperty(DbClassBuilderField field) {
            string sReturn = "", sTabs = "\t\t\t";

            if (field.BuildGet || field.BuildSet) {
                sReturn += sTabs + "public " + field.CSharpType + " " + field.FieldName + " {\n";

                //Build the get property
                if (field.BuildGet)
                    sReturn += sTabs + "\tget { return " + field.MemberVar + "; }\n";

                //Build the set property
                if (field.BuildSet) {
                    sReturn += sTabs + "\tset { ";
                    if (field.Nullable) sReturn += "\n" + sTabs + "\t\t";
                    sReturn += field.MemberVar + "=value; ";
                    if (field.Nullable) sReturn += "\n" + sTabs + "\t\t_propertyNullStateDictionary[\"" + field.FieldName + "\"] = false;\n" + sTabs + "\t";
                    sReturn += "}\n";
                }

                sReturn += sTabs + "}\n";
            }

            return sReturn;
        }
        #endregion

        #region Routines for getting the data for the user to make the field selections.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        /// <param name="database"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="table"></param>
        /// <param name="detailTables"></param>
        /// <returns></returns>
        public DataTable LoadColumnData(string server, string database, string user, string password, string table, string detailTables) {
            DataTable oColumnData = null;

            //Open the connection to the database
            DataAccess oDa = new DataAccess(server, database, user, password);

            //Set the main table
            _sTableName = table;

            //Get the main table primary keys
            Hashtable htKeys = this.GetMainTablePrimaryKeys(oDa);

            //Build the table list
            string sTableList = "'" + table + "'";

            //Instantiate a new arraylist for storing our detail tables
            _alDetailTables = new ArrayList();
            int iIndex = 1;

            //Add any detail tables to the list
            foreach (string sLine in detailTables.Split('\n')) {
                if (sLine.Trim().Length > 0) {
                    string[] a_sDetails = sLine.Split(',');
                    sTableList += ", '" + a_sDetails[0] + "'";

                    if (a_sDetails.Length == 3) {
                        //Add the detail item to our list
                        _alDetailTables.Add(new DetailTableItem(a_sDetails[0], a_sDetails[1], a_sDetails[2], iIndex++));
                    }
                }
            }

            //Query the column data from the mssql database
            string sSql = oDa.CleanSql(@"
				select 
					convert(varchar, tables.name +'.'+ fields.name) as id,
					convert(varchar, tables.name +' - '+ fields.name) as label,
					tables.name as table_name, 
					fields.name as field_name, 
					types.name as type_name, 
					fields.length,
					nullable = case fields.isnullable when 1 then 'Yes' else 'No' end,
					is_identity = case IsNull(colstat, 0) when 1 then 'Yes' else 'No' end,
					is_primarykey = convert(bit, 0),
					build_get = convert(bit, case when tables.name='" + table + @"' then 1 else 0 end),
					build_set = 
						convert(bit, case IsNull(colstat, 0) when 1 then 0 else 1 end) 
						& convert(bit, case when tables.name='" + table + @"' then 1 else 0 end),
					build_save = 
						convert(bit, case IsNull(colstat, 0) when 1 then 0 else 1 end)
						& convert(bit, case when tables.name='" + table + @"' then 1 else 0 end),
					build_filter = convert(bit, 1)
				from 
				syscolumns fields
				left join sysobjects tables on fields.id=tables.id
				left join systypes types on fields.xtype=types.xtype
				where tables.type='U' and tables.name in (" + sTableList + @")
				order by tables.name, fields.colorder
			");
            oColumnData = oDa.Query(sSql);

            //Close the connection to the database
            oDa.Close();

            //Iterate over the list and set the primary keys
            foreach (DataRow oItem in oColumnData.Rows) {
                string sTableName = ((string)oItem["table_name"]).Trim().ToLower();
                string sFieldName = ((string)oItem["field_name"]).Trim().ToLower();
                if (sTableName == _sTableName.Trim().ToLower() && htKeys.Contains(sFieldName)) {
                    oItem["is_primarykey"] = true;  
                    oItem.AcceptChanges();
                }
            }

            return oColumnData;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected Hashtable GetMainTablePrimaryKeys(DataAccess dataAccess) {
            //Instantiate the return value
            Hashtable htReturn = new Hashtable();

            //Use the MS stored procedure to get the pk names
            string sSql = "sp_pkeys @table_name=" + _sTableName;
            DataTable oKeys = dataAccess.Query(sSql);

            //Get the primary keys and stick them in our return hashtable
            foreach (DataRow oItem in oKeys.Rows) {
                string sColumName = (string)oItem["COLUMN_NAME"];
                htReturn.Add(sColumName, sColumName);
            }

            return htReturn;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string GetClassTemplate() {
            //Load the template
            string template = System.IO.File.ReadAllText("ClassTemplate.txt");           

            //Return the template
            return template;
        } //End of GetClassTemplate
    }
}
