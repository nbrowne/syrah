﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SyrahClassBuilder {
    public partial class Viewer : Form {
        public Viewer(string buffer, string filename) {
            InitializeComponent();

            txtViewer.Text = buffer;
            txtViewer.Select(0, 0);

            this.Text = "Save Syrah Class: " + filename;
            saveFileDialog.FileName = filename;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.saveFileDialog.ShowDialog() == DialogResult.OK) {
                StreamWriter writer = File.CreateText(this.saveFileDialog.FileName);
                writer.Write(txtViewer.Text);
                writer.Flush();
                writer.Close();
            }
        }
    }
}
