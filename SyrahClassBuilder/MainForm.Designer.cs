﻿namespace SyrahClassBuilder {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDatabasePassword;
        private System.Windows.Forms.TextBox txtDatabaseUser;
        private System.Windows.Forms.TextBox txtDatabaseName;
        private System.Windows.Forms.TextBox txtDatabaseServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnGetColumnData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDatabaseTable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDatabaseDetailTables;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGrid grdColumnData;
        private System.Windows.Forms.DataGridTableStyle MainGridStyle;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn3;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn4;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn5;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn1;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn2;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn3;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn4;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNamespace;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNamespace = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDatabaseDetailTables = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDatabaseTable = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnGetColumnData = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDatabasePassword = new System.Windows.Forms.TextBox();
            this.txtDatabaseUser = new System.Windows.Forms.TextBox();
            this.txtDatabaseName = new System.Windows.Forms.TextBox();
            this.txtDatabaseServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grdColumnData = new System.Windows.Forms.DataGrid();
            this.MainGridStyle = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn3 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn4 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn5 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridBoolColumn1 = new System.Windows.Forms.DataGridBoolColumn();
            this.dataGridBoolColumn2 = new System.Windows.Forms.DataGridBoolColumn();
            this.dataGridBoolColumn3 = new System.Windows.Forms.DataGridBoolColumn();
            this.dataGridBoolColumn4 = new System.Windows.Forms.DataGridBoolColumn();
            this.dataGridBoolColumn5 = new System.Windows.Forms.DataGridBoolColumn();
            this.btnCreate = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdColumnData)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNamespace);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDatabaseDetailTables);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtDatabaseTable);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnGetColumnData);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDatabasePassword);
            this.groupBox1.Controls.Add(this.txtDatabaseUser);
            this.groupBox1.Controls.Add(this.txtDatabaseName);
            this.groupBox1.Controls.Add(this.txtDatabaseServer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(912, 200);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(8, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 23);
            this.label8.TabIndex = 18;
            this.label8.Text = "Namespace:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNamespace
            // 
            this.txtNamespace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtNamespace.Location = new System.Drawing.Point(120, 160);
            this.txtNamespace.Name = "txtNamespace";
            this.txtNamespace.Size = new System.Drawing.Size(176, 26);
            this.txtNamespace.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(344, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(416, 48);
            this.label7.TabIndex = 16;
            this.label7.Text = "Note: Each detail table should be in the following format: DetailtableName, Detai" +
                "lColumn, ParentColumn";
            // 
            // txtDatabaseDetailTables
            // 
            this.txtDatabaseDetailTables.AcceptsReturn = true;
            this.txtDatabaseDetailTables.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDatabaseDetailTables.Location = new System.Drawing.Point(416, 64);
            this.txtDatabaseDetailTables.Multiline = true;
            this.txtDatabaseDetailTables.Name = "txtDatabaseDetailTables";
            this.txtDatabaseDetailTables.Size = new System.Drawing.Size(480, 72);
            this.txtDatabaseDetailTables.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(296, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 23);
            this.label6.TabIndex = 15;
            this.label6.Text = "Detail Tables:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDatabaseTable
            // 
            this.txtDatabaseTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDatabaseTable.Location = new System.Drawing.Point(416, 32);
            this.txtDatabaseTable.Name = "txtDatabaseTable";
            this.txtDatabaseTable.Size = new System.Drawing.Size(176, 26);
            this.txtDatabaseTable.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(320, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 23);
            this.label5.TabIndex = 14;
            this.label5.Text = "Table:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnGetColumnData
            // 
            this.btnGetColumnData.Location = new System.Drawing.Point(776, 144);
            this.btnGetColumnData.Name = "btnGetColumnData";
            this.btnGetColumnData.Size = new System.Drawing.Size(120, 40);
            this.btnGetColumnData.TabIndex = 8;
            this.btnGetColumnData.Text = "Get Columns";
            this.btnGetColumnData.Click += new System.EventHandler(this.btnGetColumnData_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(56, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 23);
            this.label4.TabIndex = 12;
            this.label4.Text = "Name:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(48, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 23);
            this.label3.TabIndex = 11;
            this.label3.Text = "User:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(24, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 23);
            this.label2.TabIndex = 10;
            this.label2.Text = "Password:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDatabasePassword
            // 
            this.txtDatabasePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDatabasePassword.Location = new System.Drawing.Point(120, 128);
            this.txtDatabasePassword.Name = "txtDatabasePassword";
            this.txtDatabasePassword.PasswordChar = '*';
            this.txtDatabasePassword.Size = new System.Drawing.Size(176, 26);
            this.txtDatabasePassword.TabIndex = 4;
            // 
            // txtDatabaseUser
            // 
            this.txtDatabaseUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDatabaseUser.Location = new System.Drawing.Point(120, 96);
            this.txtDatabaseUser.Name = "txtDatabaseUser";
            this.txtDatabaseUser.Size = new System.Drawing.Size(176, 26);
            this.txtDatabaseUser.TabIndex = 3;
            // 
            // txtDatabaseName
            // 
            this.txtDatabaseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDatabaseName.Location = new System.Drawing.Point(120, 64);
            this.txtDatabaseName.Name = "txtDatabaseName";
            this.txtDatabaseName.Size = new System.Drawing.Size(176, 26);
            this.txtDatabaseName.TabIndex = 2;
            // 
            // txtDatabaseServer
            // 
            this.txtDatabaseServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDatabaseServer.Location = new System.Drawing.Point(120, 32);
            this.txtDatabaseServer.Name = "txtDatabaseServer";
            this.txtDatabaseServer.Size = new System.Drawing.Size(176, 26);
            this.txtDatabaseServer.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(48, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Server:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grdColumnData);
            this.groupBox2.Location = new System.Drawing.Point(8, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(912, 288);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Columns to Include";
            // 
            // grdColumnData
            // 
            this.grdColumnData.CaptionVisible = false;
            this.grdColumnData.DataMember = "";
            this.grdColumnData.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grdColumnData.Location = new System.Drawing.Point(8, 24);
            this.grdColumnData.Name = "grdColumnData";
            this.grdColumnData.Size = new System.Drawing.Size(896, 256);
            this.grdColumnData.TabIndex = 9;
            this.grdColumnData.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.MainGridStyle});
            this.grdColumnData.Navigate += new System.Windows.Forms.NavigateEventHandler(this.grdColumnData_Navigate);
            // 
            // MainGridStyle
            // 
            this.MainGridStyle.AlternatingBackColor = System.Drawing.Color.LightBlue;
            this.MainGridStyle.DataGrid = this.grdColumnData;
            this.MainGridStyle.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.dataGridTextBoxColumn1,
            this.dataGridTextBoxColumn2,
            this.dataGridTextBoxColumn3,
            this.dataGridTextBoxColumn4,
            this.dataGridTextBoxColumn5,
            this.dataGridBoolColumn1,
            this.dataGridBoolColumn2,
            this.dataGridBoolColumn3,
            this.dataGridBoolColumn4,
            this.dataGridBoolColumn5});
            this.MainGridStyle.HeaderBackColor = System.Drawing.Color.Navy;
            this.MainGridStyle.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainGridStyle.HeaderForeColor = System.Drawing.Color.White;
            this.MainGridStyle.RowHeadersVisible = false;
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            this.dataGridTextBoxColumn1.HeaderText = "Field";
            this.dataGridTextBoxColumn1.MappingName = "label";
            this.dataGridTextBoxColumn1.ReadOnly = true;
            this.dataGridTextBoxColumn1.Width = 275;
            // 
            // dataGridTextBoxColumn2
            // 
            this.dataGridTextBoxColumn2.Format = "";
            this.dataGridTextBoxColumn2.FormatInfo = null;
            this.dataGridTextBoxColumn2.HeaderText = "Type";
            this.dataGridTextBoxColumn2.MappingName = "type_name";
            this.dataGridTextBoxColumn2.ReadOnly = true;
            this.dataGridTextBoxColumn2.Width = 75;
            // 
            // dataGridTextBoxColumn3
            // 
            this.dataGridTextBoxColumn3.Alignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.dataGridTextBoxColumn3.Format = "";
            this.dataGridTextBoxColumn3.FormatInfo = null;
            this.dataGridTextBoxColumn3.HeaderText = "Length";
            this.dataGridTextBoxColumn3.MappingName = "length";
            this.dataGridTextBoxColumn3.ReadOnly = true;
            this.dataGridTextBoxColumn3.Width = 75;
            // 
            // dataGridTextBoxColumn4
            // 
            this.dataGridTextBoxColumn4.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridTextBoxColumn4.Format = "";
            this.dataGridTextBoxColumn4.FormatInfo = null;
            this.dataGridTextBoxColumn4.HeaderText = "Nullable";
            this.dataGridTextBoxColumn4.MappingName = "nullable";
            this.dataGridTextBoxColumn4.ReadOnly = true;
            this.dataGridTextBoxColumn4.Width = 75;
            // 
            // dataGridTextBoxColumn5
            // 
            this.dataGridTextBoxColumn5.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridTextBoxColumn5.Format = "";
            this.dataGridTextBoxColumn5.FormatInfo = null;
            this.dataGridTextBoxColumn5.HeaderText = "Identity";
            this.dataGridTextBoxColumn5.MappingName = "is_identity";
            this.dataGridTextBoxColumn5.ReadOnly = true;
            this.dataGridTextBoxColumn5.Width = 75;
            // 
            // dataGridBoolColumn1
            // 
            this.dataGridBoolColumn1.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridBoolColumn1.AllowNull = false;
            this.dataGridBoolColumn1.HeaderText = "Primary";
            this.dataGridBoolColumn1.MappingName = "is_primarykey";
            this.dataGridBoolColumn1.NullValue = null;
            this.dataGridBoolColumn1.Width = 75;
            // 
            // dataGridBoolColumn2
            // 
            this.dataGridBoolColumn2.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridBoolColumn2.AllowNull = false;
            this.dataGridBoolColumn2.HeaderText = "Get";
            this.dataGridBoolColumn2.MappingName = "build_get";
            this.dataGridBoolColumn2.Width = 50;
            // 
            // dataGridBoolColumn3
            // 
            this.dataGridBoolColumn3.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridBoolColumn3.AllowNull = false;
            this.dataGridBoolColumn3.HeaderText = "Set";
            this.dataGridBoolColumn3.MappingName = "build_set";
            this.dataGridBoolColumn3.Width = 50;
            // 
            // dataGridBoolColumn4
            // 
            this.dataGridBoolColumn4.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridBoolColumn4.AllowNull = false;
            this.dataGridBoolColumn4.HeaderText = "Save";
            this.dataGridBoolColumn4.MappingName = "build_save";
            this.dataGridBoolColumn4.Width = 50;
            // 
            // dataGridBoolColumn5
            // 
            this.dataGridBoolColumn5.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridBoolColumn5.AllowNull = false;
            this.dataGridBoolColumn5.HeaderText = "Filter";
            this.dataGridBoolColumn5.MappingName = "build_filter";
            this.dataGridBoolColumn5.Width = 75;
            // 
            // btnCreate
            // 
            this.btnCreate.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCreate.Enabled = false;
            this.btnCreate.Location = new System.Drawing.Point(799, 528);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(120, 40);
            this.btnCreate.TabIndex = 10;
            this.btnCreate.Text = "Build Class";
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(8, 19);
            this.ClientSize = new System.Drawing.Size(930, 584);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCreate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Build Class from Database";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdColumnData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion


    }
}

