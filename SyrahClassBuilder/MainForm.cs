﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SyrahClassBuilder {
    public partial class MainForm : Form {
        DbClassBuilder _oBuilder;

        public MainForm() {
            InitializeComponent();

            //Associate the builder object
            _oBuilder = new DbClassBuilder();

            //Load testing defaults
            txtDatabaseServer.Text = "orion";
            txtDatabaseName.Text = "Syrah";
            txtDatabaseUser.Text = "sa";
            txtDatabasePassword.Text = "sql!@#";
            txtDatabaseTable.Text = "";
            txtNamespace.Text = "NigelBrowne.Syrah.Common.Data";
            txtDatabaseTable.Text = "clients";

            //Disable the create until the columns are loaded
            btnCreate.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetColumnData_Click(object sender, System.EventArgs e) {
            btnCreate.Enabled = true;

            DataTable oColumnData = _oBuilder.LoadColumnData(txtDatabaseServer.Text, txtDatabaseName.Text,
                txtDatabaseUser.Text, txtDatabasePassword.Text, txtDatabaseTable.Text, txtDatabaseDetailTables.Text);

            grdColumnData.DataSource = oColumnData;
        }

        private void grdColumnData_Navigate(object sender, System.Windows.Forms.NavigateEventArgs ne) {

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Click(object sender, System.EventArgs e) {
            //if the user set a namespace, set the builder to use it
            string sUserNamespace = txtNamespace.Text.Trim();
            if (sUserNamespace.Length > 0) _oBuilder.Namespace = sUserNamespace;

            //run the build method
            string buffer = _oBuilder.BuildClass((DataTable)grdColumnData.DataSource);

            Viewer viewer = new Viewer(buffer, _oBuilder.ClassName +".cs");
            viewer.ShowDialog();
        }
    }
}
