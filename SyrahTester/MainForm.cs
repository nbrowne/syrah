﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Tester {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
            
        }

        private void btnTest1_Click(object sender, EventArgs e) {
            GepAlgorithmTestHarness testHarness = new GepAlgorithmTestHarness { OutputListBox = this.lstLogger };

            testHarness.RunTest();
        }
    }
}
