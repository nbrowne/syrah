﻿namespace NigelBrowne.Syrah.Tester {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnTest1 = new System.Windows.Forms.Button();
            this.TestControlsContainer = new System.Windows.Forms.GroupBox();
            this.lstLogger = new System.Windows.Forms.ListBox();
            this.TestControlsContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTest1
            // 
            this.btnTest1.Location = new System.Drawing.Point(12, 19);
            this.btnTest1.Name = "btnTest1";
            this.btnTest1.Size = new System.Drawing.Size(122, 23);
            this.btnTest1.TabIndex = 0;
            this.btnTest1.Text = "Test 1";
            this.btnTest1.UseVisualStyleBackColor = true;
            this.btnTest1.Click += new System.EventHandler(this.btnTest1_Click);
            // 
            // TestControlsContainer
            // 
            this.TestControlsContainer.Controls.Add(this.btnTest1);
            this.TestControlsContainer.Dock = System.Windows.Forms.DockStyle.Left;
            this.TestControlsContainer.Location = new System.Drawing.Point(0, 0);
            this.TestControlsContainer.Name = "TestControlsContainer";
            this.TestControlsContainer.Size = new System.Drawing.Size(149, 396);
            this.TestControlsContainer.TabIndex = 2;
            this.TestControlsContainer.TabStop = false;
            this.TestControlsContainer.Text = " Tests ";
            // 
            // lstLogger
            // 
            this.lstLogger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLogger.FormattingEnabled = true;
            this.lstLogger.Location = new System.Drawing.Point(149, 0);
            this.lstLogger.Name = "lstLogger";
            this.lstLogger.ScrollAlwaysVisible = true;
            this.lstLogger.Size = new System.Drawing.Size(1056, 394);
            this.lstLogger.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 396);
            this.Controls.Add(this.lstLogger);
            this.Controls.Add(this.TestControlsContainer);
            this.Name = "MainForm";
            this.Text = "Syrah Tester";
            this.TestControlsContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTest1;
        private System.Windows.Forms.GroupBox TestControlsContainer;
        private System.Windows.Forms.ListBox lstLogger;
    }
}

