﻿/***********************************************************************
 * MainForm.SyrahLogic.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Syrah specific methods that are a part of the master contorl form,
 * but don't logically belong in the Syrah.Common namespace.
 ***********************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.Data;
using NigelBrowne.Syrah.Common.ProblemBuilder;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Master {
    partial class MainForm {
        private string _lastSelectedClient = "";
        private LogDetailLevel _displayedLogDetail = LogDetailLevel.Information;
        private string _defaultFormTitle = "Syrah Master";
        private Common.Data.Problem _problemBuilderProblem = null;
        private DateTime _runStartTime = DateTime.Now;
        private int _runCompletedCounter = 0;

        private Syrah.Common.Master.SyrahMaster _syrahMaster = null;
        private SortedList<string, Common.Data.Client> _clientCollection = new SortedList<string, NigelBrowne.Syrah.Common.Data.Client>();

        private ProblemResult _bestProblemResults = null;
        private ProblemResult _bestSessionProblemResults = null;
        private int _maxRuns = 0;
        private ProblemResult DisplayedRunResults { get; set; }

        #region General Methods
        /// <summary>
        /// 
        /// </summary>
        private void BindDataToControls() {
            /*
            Common.Data.Problem dummyProblem = new Common.Data.Problem();
            dummyProblem.Title = "None";

            lstCurrentProblem.Items.Clear();
            lstCurrentProblem.Items.Add(dummyProblem);
            lstCurrentProblem.SelectedIndex = 0;
            */

            this.RefreshControlTabProblemList();

            //Set the control page to be displayed by default
            this.MainTabControl.SelectedIndex = 0;

            //Load the data for the problem builder
            this.LoadProblemBuilder();

            //GepSettings testSettings = new GepSettings();
            //testSettings.UseStaticLinkingFunction = true;
            //testSettings.StaticLinkingFunction = '+';
            //testSettings.TerminalList = new CodonCollection();
            //testSettings.TerminalList.Add(new Codon("a"));
            //testSettings.TerminalList.Add(new Codon("b"));
            //testSettings.TerminalList.Add(new Codon("c"));
            //testSettings.TerminalList.Add(new Codon("e"));
            //testSettings.TerminalList.Add(new Codon("f"));
            //testSettings.FunctionList.Add(new Codon("+"));
            //testSettings.FunctionList.Add(new Codon("-"));
            //testSettings.FunctionList.Add(new Codon("*"));
            //testSettings.FunctionList.Add(new Codon("/"));
            //testSettings.FunctionList.Add(new Codon("E"));
            //testSettings.FunctionList.Add(new Codon("S"));

            //Chromosome test = new Chromosome(1, 0, 2, 0, false, RncConstantsType.Rational, 0, 0, 0);
            //test[1][0] = new Codon("*");
            //test[1][1] = new Codon("-");
            //test[1][2] = new Codon("+");
            //test[1][3] = new Codon("a");
            //test[1][4] = new Codon("/");
            //test[1][5] = new Codon("b");
            //test[1][6] = new Codon("c");
            //test[1][7] = new Codon("e");
            //test[1][8] = new Codon("S");
            //test[1][9] = new Codon("f");     

            //txtResultsCSharp.Text = ChromosomeTranslator.ToCSharp(test, testSettings).Replace("\t", "    ");
        }
        #endregion

        #region ProblemBuilder Methods


        /// <summary>
        /// Import the test cases
        /// </summary>
        /// <param name="testCases"></param>
        private void ImportTestCases(string testCaseText) {
            //Verify that we have an active problem before attempting to import
            if (_problemBuilderProblem == null) throw new ArgumentNullException("No problem is currently active in the problem builder. Please select a problem and then retry.");

            //Hand off to the problem builder worker
            _problemBuilderProblem.TestCases = ProblemBuilder.ImportTestCasesFromText(testCaseText);

            //Set the terminals inthe problem settings
            CodonCollection terminalCollection = new CodonCollection();
            if (_problemBuilderProblem.TestCases.Count > 0) {
                foreach (Codon terminalValue in _problemBuilderProblem.TestCases[0].TerminalValues.Keys) {
                    terminalCollection.Add(new Codon(terminalValue.ValueAsString));
                }
            }
            ((GepSettings)problemSettingsGrid.SelectedObject).TerminalList = terminalCollection;

            //Display the information about the test cases
            lblProblemBuilderTestCaseDetail.Text = ProblemBuilder.ExportTestCasesToText(_problemBuilderProblem.TestCases);
        }



        /// <summary>
        /// Load the problems from the database and populate the combobox.
        /// </summary>
        private void LoadProblemBuilder() {
            _problemBuilderProblem = null;

            //Reset
            problemSettingsGrid.SelectedObject = null;
            txtProblemBuilderTitle.Text = "";
            txtProblemBuilderComments.Text = "";
            lblProblemBuilderTestCaseDetail.Text = "";
            lstProblemBuilderProblems.Text = "";
            lstProblemBuilderProblems.Items.Clear();

            ProblemBuilder pb = new ProblemBuilder();

            foreach (Common.Data.Problem item in pb.GetProblemList(new Common.Data.DataAccess(_syrahMaster.Configuration.DbConnection))) {
                lstProblemBuilderProblems.Items.Add(item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void NewProblemBuilderProblem() {
            lstProblemBuilderProblems.SelectedItem = null;
            lstProblemBuilderProblems.Text = "";

            this.LoadProblemBuilderProblem(new Common.Data.Problem());
        }


        /// <summary>
        /// 
        /// </summary>
        protected void LoadProblemBuilderProblem(Common.Data.Problem problem) {
            if (problem != null) {
                _problemBuilderProblem = problem;
                problemSettingsGrid.SelectedObject = _problemBuilderProblem.Settings;
                txtProblemBuilderTitle.Text = _problemBuilderProblem.Title;
                txtProblemBuilderComments.Text = _problemBuilderProblem.Notes;
                lblProblemBuilderTestCaseDetail.Text = ProblemBuilder.ExportTestCasesToText(_problemBuilderProblem.TestCases);
            }
        }

        /// <summary>
        /// Saves the current problem configuration to a new record.
        /// </summary>
        protected void SaveNewProblemBuilderProblem() {
            if (_problemBuilderProblem != null) {
                try {
                    Problem newProblem = new Problem();

                    //Configure the new problem
                    newProblem.Title = txtProblemBuilderTitle.Text;
                    _problemBuilderProblem.Notes = txtProblemBuilderComments.Text;
                    newProblem.Settings = (Syrah.Common.EvolutionaryAlgorithms.GEP.GepSettings)problemSettingsGrid.SelectedObject;
                    newProblem.TestCases = _problemBuilderProblem.TestCases;

                    //Save the new problem
                    newProblem.Save();

                    this.LoadProblemBuilder();
                    this.RefreshControlTabProblemList();
                    this.UpdateApplicationStatus("Saved problem as a new problem.");
                } catch (Exception err) {
                    this.DisplayErrorDialog("An error occurred while saving the problem. " + err.Message);
                }

            } else {
                this.DisplayErrorDialog("Cannot save problem, because no problem is currently active in the problem builder.");
            }
        }

        /// <summary>
        /// Saves any changes to the currently loaded problem.
        /// </summary>
        protected void SaveCurrentProblemBuilderProblem() {
            if (_problemBuilderProblem != null) {
                try {
                    _problemBuilderProblem.Title = txtProblemBuilderTitle.Text;
                    _problemBuilderProblem.Notes = txtProblemBuilderComments.Text;
                    _problemBuilderProblem.Settings = (Syrah.Common.EvolutionaryAlgorithms.GEP.GepSettings)problemSettingsGrid.SelectedObject;
                    //_problemBuilderProblem.TestCases = ProblemBuilder.ImportTestCasesFromText(txtProblemBuilderTestCases.Text);
                    _problemBuilderProblem.Save();

                    this.LoadProblemBuilder();
                    this.RefreshControlTabProblemList();
                    this.UpdateApplicationStatus("Saved problem.");

                } catch (Exception err) {
                    this.DisplayErrorDialog("An error occurred while saving the problem. " + err.Message);
                }

            } else {
                this.DisplayErrorDialog("Cannot save problem, because no problem is currently active in the problem builder.");
            }
        }
        #endregion


        #region Logging and Status methods
        /// <summary>
        /// Updates the application status bar.
        /// </summary>
        /// <param name="buffer"></param>
        private void UpdateApplicationStatus(string buffer) {
            this.ApplicationToolStripStatus.Text = DateTime.Now.ToString("MMM dd - HH:mm:ss") + ": " + buffer;
        }

        /// <summary>
        /// Append a message to the event log
        /// </summary>
        /// <param name="text"></param>
        private void AppendToOutputLog(LogDetailLevel level, string text) {
            if (level >= _displayedLogDetail) {
                //Reformat
                string sBuffer = DateTime.Now.ToString("MMM dd - HH:mm:ss") + "> " + text;

                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (this.lstLog.InvokeRequired) {
                    MessageHandlerDelegate delHandler = new MessageHandlerDelegate(AppendToOutputLog);
                    this.Invoke(delHandler, new object[] { level, text });

                } else {
                    //Add to the log
                    lstLog.Items.Add(sBuffer);

                    //Scroll
                    lstLog.SelectedIndex = lstLog.Items.Count - 1;

                }
            }
        }
        #endregion


        #region Client Tab specific methods
        /// <summary>
        /// Refreshes the client viewer list on the Clients tab.
        /// </summary>
        protected void RefreshClientViewer() {
            //ClientViewerRefreshTimer.Stop();
            _clientCollection.Clear();
            clientListBox.Items.Clear();

            foreach (KeyValuePair<string, Common.Data.Client> item in _syrahMaster.GetClientList()) {
                clientListBox.Items.Add(item.Key);
                _clientCollection.Add(item.Key, item.Value);
            }

            if (clientListBox.Items.Count > 0) {
                if (_lastSelectedClient.Length > 0 && clientListBox.Items.Contains(_lastSelectedClient)) {
                    clientListBox.SelectedIndex = clientListBox.Items.IndexOf(_lastSelectedClient);

                } else {
                    clientListBox.SelectedIndex = 0;
                }
            }
            //ClientViewerRefreshTimer.Start();
        }
        #endregion

        #region Control Tab specific methods
        /// <summary>
        /// Update the control tab
        /// </summary>
        private void UpdateControlTab() {
            TimeSpan elapsedTime = DateTime.Now.Subtract(_runStartTime);
            int days = (int)elapsedTime.Days;
            int hours = (int)elapsedTime.Hours;
            int minutes = (int)elapsedTime.Minutes;
            int seconds = (int)elapsedTime.Seconds;

            //Update the elapsed time for this run
            this.lblControlRuntime.Text = String.Format("{0} days, {1:00}:{2:00}:{3:00}", days, hours, minutes, seconds);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="problemId"></param>
        /// <param name="notes"></param>
        private void SaveNotesToProblem(int problemId, string notes) {
            Syrah.Common.Data.Problem problem = new NigelBrowne.Syrah.Common.Data.Problem(problemId);
            problem.Notes = notes;
            problem.Save();
        }


        /// <summary>
        /// 
        /// </summary>
        private void RefreshControlTabProblemList() {
            //Clear any existing problems
            lstCurrentProblem.Items.Clear();

            List<Problem> problems = _syrahMaster.GetProblemList();

            //Populate the list of problems for the control page active problem selector
            for (int problemIndex = problems.Count-1; problemIndex >=0; problemIndex--) {
                lstCurrentProblem.Items.Add(problems[problemIndex]);
            }
        }

        /// <summary>
        /// Set the currently selected problem on the control tab to be the active problem
        /// </summary>
        private void ActivateProblem() {
            int totalRuns = -1;
            
            if (lstCurrentProblem.SelectedItem != null) {
                _syrahMaster.KFoldEnabled = useKFoldValidationToolStripMenuItem.Checked;

                Common.Data.Problem problem = (Common.Data.Problem)lstCurrentProblem.SelectedItem;
                _syrahMaster.ActiveProblemId = problem.Id;
                
                _maxRuns = problem.Settings.NumberOfRuns;

                //Set the start time
                _runStartTime = DateTime.Now;

                _bestSessionProblemResults = null;

                //Update the application title
                this.Text = _defaultFormTitle + ": " + problem.Title;

                //Update the notes
                this.txtControlProblemNotes.Text = problem.Notes;

                //Update the watching controls
                this.lblControlProblemId.Text = problem.Id.ToString();
                this.lblActiveProblemTitle.Text = problem.Title;
                this.txtControlProblemNotes.Text = problem.Notes;
                this.lblControlRunStarted.Text = _runStartTime.ToString("MMM dd, yyyy hh:mm:ss");
                this.lblControlClientCount.Text = "0";
                this.lblControlRuns.Text = "0";
                this.lblControlBestSessionFitness.Text = "?";

                this.UpdateControlTab();

                //Enable the update timer
                tmrControlTabUpdate.Enabled = true;

                //Load the best results overall
                _bestProblemResults = _syrahMaster.GetBestProblemResults(problem.Id, out totalRuns);

                if (totalRuns != -1) lblControlTotalRuns.Text = totalRuns.ToString();

                this.DisplayBestProblemResults(_bestProblemResults);

                _runCompletedCounter = 0;

                //this.LoadProblemRuns();

                //Update the status bar
                this.UpdateApplicationStatus("Activated problem '" + problem.Title + "'");
            }
        }


        protected void DisplayBestSessionResults(ProblemResult results) {
            if (results != null) {
                Problem problem = new Problem(results.ProblemId);
                if (GepEvaluator.IsParamterOptimizationProblem(problem.Settings.FitnessMethod)) {
                    lblControlBestSessionFitness.Text = String.Format("#{0} - {1}", results.Id, results.BestFitness);

                } else {
                    lblControlBestSessionFitness.Text = String.Format("#{2} - {0:0.0} - {3:0.000}%", results.BestFitness, results.MaxPossibleFitness, results.Id, results.BestFitness / results.MaxPossibleFitness * 100);
                }
            } else {
                lblControlBestSessionFitness.Text = "?";

            }
        }

        protected void DisplayBestProblemResults(ProblemResult results) {
            if (results != null) {
                Problem problem = new Problem(results.ProblemId);
                if (GepEvaluator.IsParamterOptimizationProblem(problem.Settings.FitnessMethod)) {
                    lblControlBestFitness.Text = String.Format("#{0} - {1}", results.Id, results.BestFitness);
                    lblControlMaxFitness.Text = "N/A";

                } else {
                    lblControlBestFitness.Text = String.Format("#{2} - {0:0.0} - {3:0.000}%", results.BestFitness, results.MaxPossibleFitness, results.Id, results.BestFitness / results.MaxPossibleFitness * 100);
                    lblControlMaxFitness.Text = results.MaxPossibleFitness.ToString("0.0");
                }
            } else {
                lblControlBestFitness.Text = "?";
                lblControlMaxFitness.Text = "?";

            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void LoadTopProblemRuns(int topN) {

            if (_syrahMaster.ActiveProblemId >= 0) {
                ProblemResultCollectionFilter filter = new ProblemResultCollectionFilter();
                filter.ProblemId = _syrahMaster.ActiveProblemId;
                filter.TopN = " top " + topN.ToString() + " ";
                filter.OrderBy = "best_fitness desc";
                List<ProblemResult> runs = _syrahMaster.GetProblemRuns(filter);

                lstRuns.Items.Clear();

                //Iterate and add the runsto hte list
                foreach (ProblemResult run in runs) {
                    lstRuns.Items.Add(run);
                }
            }
        }
       

        /// <summary>
        /// 
        /// </summary>
        protected void LoadAllProblemRuns() {
            if (_syrahMaster.ActiveProblemId >= 0) {
                ProblemResultCollectionFilter filter = new ProblemResultCollectionFilter();
                filter.Id = _syrahMaster.ActiveProblemId;
                //filter.OrderBy = "best_fitness desc ";
                filter.OrderBy = "id desc";
                List<ProblemResult> runs = _syrahMaster.GetProblemRuns(filter);

                lstRuns.Items.Clear();
                                
                //Iterate and add the runsto hte list
                foreach(ProblemResult run in runs) {
                    lstRuns.Items.Add(run);
                }
            }
        }

        /// <summary>
        /// Deactivate the currently active problem.
        /// </summary>
        private void DeactivateProblem() {
            //Disable the update timer
            tmrControlTabUpdate.Enabled = false;

            lstCurrentProblem.ClearSelected();
            _syrahMaster.ActiveProblemId = -1;
            this.Text = _defaultFormTitle;

            
            this.lblControlProblemId.Text = "";
            this.lblActiveProblemTitle.Text = "";
            this.txtControlProblemNotes.Text = "";

            this.UpdateApplicationStatus("Deactivated currently running problem.");
        }


        private void UpdateResults() {

        }


        /// <summary>
        /// A client finished a run
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="results"></param>
        private void ClientRunCompletedHandler(string clientName, Common.Data.ProblemResult results) {
            //If we need to invoke because we're running in a different thread, then do it 
            if (this.InvokeRequired) {
                ClientRunCompletedDelegate d = new ClientRunCompletedDelegate(ClientRunCompletedHandler);
                this.Invoke(d, new object[] { clientName, results });

            } else {
                this.lblControlRuns.Text = (Int32.Parse(this.lblControlRuns.Text) + 1).ToString();
                this.lblControlTotalRuns.Text = (Int32.Parse(this.lblControlTotalRuns.Text) + 1).ToString();
                this.lblControlAverageTimePerRun.Text = String.Format("{0:0.0} minutes", DateTime.Now.Subtract(_runStartTime).TotalMinutes / double.Parse(this.lblControlRuns.Text));
                this.lblControlClientCount.Text = (Int32.Parse(this.lblControlClientCount.Text) - 1).ToString();

                if (_bestSessionProblemResults == null || results.BestFitness > _bestSessionProblemResults.BestFitness) {
                    _bestSessionProblemResults = results;
                    this.DisplayBestSessionResults(results);
                }
                
                if (_bestProblemResults==null || (_bestSessionProblemResults != null && _bestSessionProblemResults.BestFitness > _bestProblemResults.BestFitness)) {                    
                    _bestProblemResults = results;
                    this.DisplayBestProblemResults(results);
                }

                _runCompletedCounter++;

                //Deactivate if we have all of our runs
                if (_runCompletedCounter >= _maxRuns) {
                    if (_syrahMaster.ActiveProblemId != -1) {
                        this.DeactivateProblem();
                        UpdateApplicationStatus("Evolution session ended because all runs were executed.");
                        //DisplayInformationDialog("Evolution session ended because 100 runs were executed.");
                    }
                }
            }
        }


        /// <summary>
        /// A client started a run
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="problem"></param>
        private void ClientRunStartedHandler(string clientName, Common.Data.Problem problem) {
            //If we need to invoke because we're running in a different thread, then do it 
            if (this.InvokeRequired) {
                ClientRunStartedDelegate d = new ClientRunStartedDelegate(ClientRunStartedHandler);
                this.Invoke(d, new object[] { clientName, problem });

            } else {
                this.lblControlClientCount.Text = (Int32.Parse(this.lblControlClientCount.Text) + 1).ToString();

            }
        }


        private DataTable TextResultsToTable(string textResults) {
            DataTable data = new DataTable("problem_results");
            data.Columns.Add("Point", typeof(double));
            data.Columns.Add("Expected", typeof(double));
            data.Columns.Add("Evolved", typeof(double));
            int rowIndex = 0;

            foreach (string line in textResults.Split('\n')) {
                if (line.Trim().Length > 0 && rowIndex>0) {
                    string[] items = line.Trim().Split(',');

                    DataRow newRow = data.NewRow();
                    newRow["Point"] = double.Parse(items[0]);
                    newRow["Expected"] = double.Parse(items[1]);
                    newRow["Evolved"] = double.Parse(items[2]);

                    data.Rows.InsertAt(newRow, 0);
                }

                rowIndex++;
            }

            return data;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="results"></param>
        private void DisplayRunResultDetails(ProblemResult results) {
            if (results != null) {
                this.DisplayedRunResults = results;

                //Display the text version of the results
                txtResultsChromosome.Text = results.RunStatistics.BestChromosome.ToString();

                //Display the c# code of the best chromosome
                txtResultsCSharp.Text = "";

                //Load hte results grid with the results data
                //gridResultsData.DataSource = this.TextResultsToTable(results.ResultsText);
                //txtResults.Text = results.ResultsText;

                //Display the size data
                //txtResultsSizeData.Text = this.GenerateChromosomeSizeData<int>(results.ProblemId, results.Id, results.RunStatistics.ChromosomeGenerationGeneCount, "Occurrences");
                txtResultsSizeData.Text = this.GenerateChromosomeSizeData<int>(results.ProblemId, results.Id, results.RunStatistics.ChromosomeGenerationTotalLengthCount, "Occurrences");

                //Display the size data
                //this.txtResultsSizeVsFit.Text = this.GenerateChromosomeSizeData<double>(results.ProblemId, results.Id, results.RunStatistics.ChromosomeGenerationGeneFitness, "Avg Fitness");
                this.txtResultsSizeVsFit.Text = this.GenerateChromosomeSizeData<double>(results.ProblemId, results.Id, results.RunStatistics.ChromosomeGenerationTotalLengthFitness, "Avg Fitness");

                //Display the size data
                this.txtResultsSizeVsMutations.Text = this.GenerateChromosomeSizeData<int>(results.ProblemId, results.Id, results.RunStatistics.GenerationGeneCountMutations, "Mutations");

                //this.txtResultsMinMax.Text = this.GenerateChromosomeMinMaxSizeData<int>(results.ProblemId, results.Id, results.RunStatistics.ChromosomeGenerationGeneCount);
                this.txtResultsMinMax.Text = this.GenerateChromosomeMinMaxSizeData<int>(results.ProblemId, results.Id, results.RunStatistics.ChromosomeGenerationTotalLengthCount, results.RunStatistics.BestGenerationFitness);

                this.txtDetails.Text = this.GenerateResultDetails(results);

               
            } else {
                this.DisplayErrorDialog("No run results are available.");
            }
        }

        private string GenerateResultDetails(ProblemResult results) {
            StringBuilder buffer = new StringBuilder();
            Problem problem = new Problem(results.ProblemId);
            TimeSpan runtime = TimeSpan.FromMilliseconds(Convert.ToDouble(results.RunTime));
            bool isPoProblem = GepEvaluator.IsParamterOptimizationProblem(problem.Settings.FitnessMethod);

            buffer.AppendLine("Run details");
            buffer.AppendLine("--------------------------------------");
            buffer.AppendFormat("Problem: {0} - {1}" + Environment.NewLine, problem.Id, problem.Title);
            buffer.AppendFormat("Run Id: {0}" + Environment.NewLine, results.Id);
            buffer.AppendFormat("Total generations: {0}" + Environment.NewLine, problem.Settings.MaximumGenerations);
            buffer.AppendFormat("Population size: {0}" + Environment.NewLine, problem.Settings.PopulationSize);
            buffer.AppendFormat("Acs enabled: {0}" + Environment.NewLine, problem.Settings.IsAcsGeneEnabled ? "yes" : "no");
            buffer.AppendFormat("HIS Enabled: {0}" + Environment.NewLine, problem.Settings.IsHisTranspositionEnabled ? "yes" : "no");
            buffer.AppendFormat("RNC Enabled: {0}" + Environment.NewLine, problem.Settings.IsRncEnabled ? "yes" : "no");
            buffer.AppendFormat("Fitness Method : {0}" + Environment.NewLine, problem.Settings.FitnessMethod);
            buffer.AppendFormat("Max fitness: {0}" + Environment.NewLine, results.MaxPossibleFitness);
            buffer.AppendFormat("Best fitness generation: {0}" + Environment.NewLine, results.BestFitnessGeneration);
            buffer.AppendFormat("Run time: {0}h {1}m {2}s" + Environment.NewLine, runtime.Hours, runtime.Minutes, runtime.Seconds);
            buffer.AppendLine();

            buffer.AppendLine("Best Chromosome details");
            buffer.AppendLine("--------------------------------------");
            //Display the fitness differently for PO problems
            if (isPoProblem) {
                buffer.AppendFormat("Max/Min: {0}" + Environment.NewLine, results.BestChromosome.Fitness);
                buffer.AppendFormat("Results: {0}" + Environment.NewLine, results.ResultsText);
            } else {
                buffer.AppendFormat("Fitness: {0:P}" + Environment.NewLine, results.BestChromosome.Fitness / results.MaxPossibleFitness);
                buffer.AppendFormat("         {0} of {1}" + Environment.NewLine, results.BestChromosome.Fitness, results.MaxPossibleFitness);
            }
            buffer.AppendFormat("Number of Genes: {0}" + Environment.NewLine, results.BestChromosome.GeneCount);
            buffer.AppendFormat("Total Chromosome Length: {0}" + Environment.NewLine, results.BestChromosome.Length);
            buffer.AppendFormat("Number of RNC constants: {0}" + Environment.NewLine, results.BestChromosome.RncConstantsPerGene);            
            buffer.AppendLine();

            foreach (Gene gene in results.BestChromosome.Genes) {
                buffer.AppendFormat("   Gene {0}" + Environment.NewLine, results.BestChromosome.Genes.IndexOf(gene));
                buffer.AppendFormat("      Head length: {0}" + Environment.NewLine, gene.HeadLength);
                buffer.AppendFormat("      Tail length: {0}" + Environment.NewLine, gene.TailLength);

            }

            return buffer.ToString();
        }


        private string GenerateChromosomeSizeDataOld<T>(int problemId, int runId, List<SerializableDictionary<int, T>> geneCountData, string fieldName) {
            StringBuilder buffer = new StringBuilder();
            if (geneCountData != null) {
                buffer.AppendLine("#Autogenerate chromosome size data from run id " + runId + ".");
                buffer.AppendLine("#Problem ID: " + problemId.ToString());
                buffer.AppendLine("#Generations: " + geneCountData.Count.ToString());
                buffer.AppendLine("#Generation Number, Genes in Chromosome, " + fieldName);

                for (int generationIndex = 0; generationIndex < geneCountData.Count; generationIndex++) {
                    foreach (int size in geneCountData[generationIndex].Keys) {
                        T data = geneCountData[generationIndex][size];
                        string formattedData = data.ToString();
                        buffer.AppendLine(generationIndex.ToString() + "\t" + size.ToString() + "\t" + formattedData);
                    }
                    buffer.AppendLine();
                }
            } else {
                buffer.AppendLine("No data present.");
            }
            return buffer.ToString();
        }


        private string GenerateChromosomeMinMaxSizeData<T>(int problemId, int runId, List<SerializableDictionary<int, T>> geneCountData, List<double> bestGenerationFitness) {
            StringBuilder buffer = new StringBuilder();
            int maxGeneCount = 0;

            if (geneCountData != null) {

                //Get the max gene count
                foreach (SerializableDictionary<int, T> dict in geneCountData) {
                    foreach (int size in dict.Keys) {
                        if (size > maxGeneCount) maxGeneCount = size;
                    }
                }

                buffer.AppendLine("#Autogenerate chromosome size min max data from run id " + runId + ".");
                buffer.AppendLine("#Problem ID: " + problemId.ToString());
                buffer.AppendLine("#Generations: " + geneCountData.Count.ToString());
                buffer.AppendLine("#Generation Number, Min, Max, Avg, Best Fitness");

                for (int generationIndex = 0; generationIndex < geneCountData.Count; generationIndex++) {
                    int max = 0;
                    int min = maxGeneCount;
                    int sum = 0;

                    foreach (int size in geneCountData[generationIndex].Keys) {
                        if (size > max) max = size;
                        if (size < min) min = size;
                        sum += size;
                    }

                    buffer.AppendLine(generationIndex.ToString() + "\t" + min.ToString() + "\t" + max.ToString() + "\t" + (sum / geneCountData[generationIndex].Keys.Count).ToString("0.0") +"\t"+ bestGenerationFitness[generationIndex].ToString("0.0000"));
                }

            }
            return buffer.ToString();
        }

        private string GenerateChromosomeSizeData<T>(int problemId, int runId, List<SerializableDictionary<int, T>> geneCountData, string fieldName) {
            StringBuilder buffer = new StringBuilder();
            int maxGeneCount = 0;

            if (geneCountData != null) {

                //Get the max gene count
                foreach (SerializableDictionary<int, T> dict in geneCountData) {
                    foreach (int size in dict.Keys) {
                        if (size > maxGeneCount) maxGeneCount = size;
                    }
                }

                buffer.AppendLine("#Autogenerate chromosome size data from run id " + runId + ".");
                buffer.AppendLine("#Problem ID: " + problemId.ToString());
                buffer.AppendLine("#Generations: " + geneCountData.Count.ToString());
                buffer.AppendLine("#Generation Number, Chromosome Size (Codons), " + fieldName);

                for (int generationIndex = 0; generationIndex < geneCountData.Count; generationIndex++) {
                    for (int sizeIndex = 1; sizeIndex <= maxGeneCount; sizeIndex++) {
                        string formattedData = "0";

                        if (geneCountData[generationIndex].ContainsKey(sizeIndex)) {
                            T data = geneCountData[generationIndex][sizeIndex];
                            formattedData = data.ToString();
                        }

                        buffer.AppendLine(generationIndex.ToString() + "\t" + sizeIndex.ToString() + "\t" + formattedData.ToString());
                    }
                    buffer.AppendLine();
                }

            }
            return buffer.ToString();
        }
        #endregion

    } //End of partial class MainForm.SyrahLogic.cs
}
