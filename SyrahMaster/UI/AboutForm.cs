﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace NigelBrowne.Syrah.Master {
    partial class AboutForm : Form {
        public AboutForm() {
            InitializeComponent();
            this.Text = String.Format("About {0} - {1}", Syrah.Common.About.PackageTitle, Syrah.Common.About.MasterTitle);
            this.labelProductName.Text = Syrah.Common.About.PackageTitle + " - " + Syrah.Common.About.MasterTitle + " - " + AssemblyProduct;
            this.labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
            this.labelCopyright.Text = Syrah.Common.About.Copyright;
            this.labelCompanyName.Text = Syrah.Common.About.Company;
            this.textBoxDescription.Text = Syrah.Common.About.MasterDescription;
        }

        #region Assembly Attribute Accessors
        public string AssemblyVersion {
            get {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyProduct {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }
        #endregion
    }
}
