﻿namespace NigelBrowne.Syrah.Master {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.ApplicationStatusStrip = new System.Windows.Forms.StatusStrip();
            this.ApplicationToolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useKFoldValidationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.tabControl = new System.Windows.Forms.TabPage();
            this.grpControlSession = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblControlClientCount = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblControlRuns = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblControlAverageTimePerRun = new System.Windows.Forms.Label();
            this.lblControlRuntime = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblControlRunStarted = new System.Windows.Forms.Label();
            this.grpControlResults = new System.Windows.Forms.GroupBox();
            this.tabResults = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtDetails = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtResultsCSharp = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btnLoadAllRuns = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNBest = new System.Windows.Forms.TextBox();
            this.btnLoadBestRuns = new System.Windows.Forms.Button();
            this.lstRuns = new System.Windows.Forms.ListBox();
            this.btnLoadRunData = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtResultsChromosome = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtResults = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnLoadStoredResults = new System.Windows.Forms.Button();
            this.btnResultsLoadData = new System.Windows.Forms.Button();
            this.btnSaveResults = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.txtResultsSizeData = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnResultsSaveSize = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.txtResultsSizeVsFit = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnResultsSizeVsFitSave = new System.Windows.Forms.Button();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.txtResultsSizeVsMutations = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.txtResultsMinMax = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.lblControlTotalRuns = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblControlBestSessionFitness = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblControlBestFitness = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblControlMaxFitness = new System.Windows.Forms.Label();
            this.grpControlActiveProblem = new System.Windows.Forms.GroupBox();
            this.btnControlSaveNotes = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblControlProblemId = new System.Windows.Forms.Label();
            this.lblActiveProblemTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtControlProblemNotes = new System.Windows.Forms.TextBox();
            this.grpControlProblemSelector = new System.Windows.Forms.GroupBox();
            this.lstCurrentProblem = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabProblemEditor = new System.Windows.Forms.TabPage();
            this.problemSettingsGrid = new System.Windows.Forms.PropertyGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnClearTestCases = new System.Windows.Forms.Button();
            this.lblProblemBuilderTestCaseDetail = new System.Windows.Forms.Label();
            this.btnProblemBuilderImportTestCases = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtProblemBuilderComments = new System.Windows.Forms.TextBox();
            this.txtProblemBuilderTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnProblemBuilderNew = new System.Windows.Forms.Button();
            this.btnProblemBuilderSave = new System.Windows.Forms.Button();
            this.btnProblemBuilderSaveNew = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lstProblemBuilderProblems = new System.Windows.Forms.ComboBox();
            this.tabClientList = new System.Windows.Forms.TabPage();
            this.ClientProperties = new System.Windows.Forms.PropertyGrid();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.clientListBox = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkAutoRefresh = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tabLog = new System.Windows.Forms.TabPage();
            this.lstLog = new System.Windows.Forms.ListBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtTestBed2 = new System.Windows.Forms.TextBox();
            this.txtTestBed1 = new System.Windows.Forms.TextBox();
            this.tmrClientViewerRefresh = new System.Windows.Forms.Timer(this.components);
            this.openTestCasesDialog = new System.Windows.Forms.OpenFileDialog();
            this.tmrControlTabUpdate = new System.Windows.Forms.Timer(this.components);
            this.tmrResultsUpdater = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.button7 = new System.Windows.Forms.Button();
            this.txtRunIdToLoad = new System.Windows.Forms.TextBox();
            this.ApplicationStatusStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.grpControlSession.SuspendLayout();
            this.grpControlResults.SuspendLayout();
            this.tabResults.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.grpControlActiveProblem.SuspendLayout();
            this.grpControlProblemSelector.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabProblemEditor.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabClientList.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabLog.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplicationStatusStrip
            // 
            this.ApplicationStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ApplicationToolStripStatus});
            this.ApplicationStatusStrip.Location = new System.Drawing.Point(0, 729);
            this.ApplicationStatusStrip.Name = "ApplicationStatusStrip";
            this.ApplicationStatusStrip.Size = new System.Drawing.Size(1205, 22);
            this.ApplicationStatusStrip.TabIndex = 1;
            this.ApplicationStatusStrip.Text = "statusStrip1";
            // 
            // ApplicationToolStripStatus
            // 
            this.ApplicationToolStripStatus.Name = "ApplicationToolStripStatus";
            this.ApplicationToolStripStatus.Size = new System.Drawing.Size(109, 17);
            this.ApplicationToolStripStatus.Text = "toolStripStatusLabel1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(100, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(142, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.aboutToolStripMenuItem.Text = "&About Syrah";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.toolStripMenuItem2,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1205, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.useKFoldValidationToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // useKFoldValidationToolStripMenuItem
            // 
            this.useKFoldValidationToolStripMenuItem.Checked = true;
            this.useKFoldValidationToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useKFoldValidationToolStripMenuItem.Name = "useKFoldValidationToolStripMenuItem";
            this.useKFoldValidationToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.useKFoldValidationToolStripMenuItem.Text = "Use K-Fold Validation";
            this.useKFoldValidationToolStripMenuItem.Click += new System.EventHandler(this.useKFoldValidationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalToolStripMenuItem,
            this.detailedToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem2.Text = "&Log Detail";
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Checked = true;
            this.normalToolStripMenuItem.CheckOnClick = true;
            this.normalToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.normalToolStripMenuItem.Text = "Normal";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // detailedToolStripMenuItem
            // 
            this.detailedToolStripMenuItem.CheckOnClick = true;
            this.detailedToolStripMenuItem.Name = "detailedToolStripMenuItem";
            this.detailedToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.detailedToolStripMenuItem.Text = "Detailed";
            this.detailedToolStripMenuItem.Click += new System.EventHandler(this.detailedToolStripMenuItem_Click);
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.tabControl);
            this.MainTabControl.Controls.Add(this.tabProblemEditor);
            this.MainTabControl.Controls.Add(this.tabClientList);
            this.MainTabControl.Controls.Add(this.tabLog);
            this.MainTabControl.Controls.Add(this.tabPage5);
            this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.MainTabControl.Location = new System.Drawing.Point(0, 24);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(1205, 705);
            this.MainTabControl.TabIndex = 2;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.grpControlSession);
            this.tabControl.Controls.Add(this.grpControlResults);
            this.tabControl.Controls.Add(this.grpControlActiveProblem);
            this.tabControl.Controls.Add(this.grpControlProblemSelector);
            this.tabControl.Location = new System.Drawing.Point(4, 27);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Windows.Forms.Padding(3);
            this.tabControl.Size = new System.Drawing.Size(1197, 674);
            this.tabControl.TabIndex = 0;
            this.tabControl.Text = "Control";
            this.tabControl.UseVisualStyleBackColor = true;
            // 
            // grpControlSession
            // 
            this.grpControlSession.Controls.Add(this.label11);
            this.grpControlSession.Controls.Add(this.lblControlClientCount);
            this.grpControlSession.Controls.Add(this.label13);
            this.grpControlSession.Controls.Add(this.lblControlRuns);
            this.grpControlSession.Controls.Add(this.label10);
            this.grpControlSession.Controls.Add(this.label6);
            this.grpControlSession.Controls.Add(this.lblControlAverageTimePerRun);
            this.grpControlSession.Controls.Add(this.lblControlRuntime);
            this.grpControlSession.Controls.Add(this.label8);
            this.grpControlSession.Controls.Add(this.lblControlRunStarted);
            this.grpControlSession.Location = new System.Drawing.Point(521, 4);
            this.grpControlSession.Name = "grpControlSession";
            this.grpControlSession.Size = new System.Drawing.Size(664, 82);
            this.grpControlSession.TabIndex = 12;
            this.grpControlSession.TabStop = false;
            this.grpControlSession.Text = "Session";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(301, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(131, 18);
            this.label11.TabIndex = 15;
            this.label11.Text = "Number of Clients:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlClientCount
            // 
            this.lblControlClientCount.AutoSize = true;
            this.lblControlClientCount.Location = new System.Drawing.Point(432, 55);
            this.lblControlClientCount.Name = "lblControlClientCount";
            this.lblControlClientCount.Size = new System.Drawing.Size(16, 18);
            this.lblControlClientCount.TabIndex = 16;
            this.lblControlClientCount.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(311, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 18);
            this.label13.TabIndex = 15;
            this.label13.Text = "Number of Runs:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlRuns
            // 
            this.lblControlRuns.AutoSize = true;
            this.lblControlRuns.Location = new System.Drawing.Point(432, 36);
            this.lblControlRuns.Name = "lblControlRuns";
            this.lblControlRuns.Size = new System.Drawing.Size(16, 18);
            this.lblControlRuns.TabIndex = 16;
            this.lblControlRuns.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(299, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 18);
            this.label10.TabIndex = 13;
            this.label10.Text = "Average Time/Run:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Session Started:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlAverageTimePerRun
            // 
            this.lblControlAverageTimePerRun.AutoSize = true;
            this.lblControlAverageTimePerRun.Location = new System.Drawing.Point(432, 18);
            this.lblControlAverageTimePerRun.Name = "lblControlAverageTimePerRun";
            this.lblControlAverageTimePerRun.Size = new System.Drawing.Size(16, 18);
            this.lblControlAverageTimePerRun.TabIndex = 14;
            this.lblControlAverageTimePerRun.Text = "?";
            // 
            // lblControlRuntime
            // 
            this.lblControlRuntime.AutoSize = true;
            this.lblControlRuntime.Location = new System.Drawing.Point(130, 38);
            this.lblControlRuntime.Name = "lblControlRuntime";
            this.lblControlRuntime.Size = new System.Drawing.Size(16, 18);
            this.lblControlRuntime.TabIndex = 10;
            this.lblControlRuntime.Text = "?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "Session time:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlRunStarted
            // 
            this.lblControlRunStarted.AutoSize = true;
            this.lblControlRunStarted.Location = new System.Drawing.Point(129, 18);
            this.lblControlRunStarted.Name = "lblControlRunStarted";
            this.lblControlRunStarted.Size = new System.Drawing.Size(16, 18);
            this.lblControlRunStarted.TabIndex = 12;
            this.lblControlRunStarted.Text = "?";
            // 
            // grpControlResults
            // 
            this.grpControlResults.Controls.Add(this.txtRunIdToLoad);
            this.grpControlResults.Controls.Add(this.button7);
            this.grpControlResults.Controls.Add(this.tabResults);
            this.grpControlResults.Controls.Add(this.label20);
            this.grpControlResults.Controls.Add(this.lblControlTotalRuns);
            this.grpControlResults.Controls.Add(this.label18);
            this.grpControlResults.Controls.Add(this.lblControlBestSessionFitness);
            this.grpControlResults.Controls.Add(this.label16);
            this.grpControlResults.Controls.Add(this.lblControlBestFitness);
            this.grpControlResults.Controls.Add(this.label12);
            this.grpControlResults.Controls.Add(this.lblControlMaxFitness);
            this.grpControlResults.Location = new System.Drawing.Point(521, 94);
            this.grpControlResults.Name = "grpControlResults";
            this.grpControlResults.Size = new System.Drawing.Size(662, 574);
            this.grpControlResults.TabIndex = 14;
            this.grpControlResults.TabStop = false;
            this.grpControlResults.Text = "Results";
            this.grpControlResults.Enter += new System.EventHandler(this.grpControlResults_Enter);
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.tabPage1);
            this.tabResults.Controls.Add(this.tabPage2);
            this.tabResults.Controls.Add(this.tabPage7);
            this.tabResults.Controls.Add(this.tabPage4);
            this.tabResults.Controls.Add(this.tabPage3);
            this.tabResults.Controls.Add(this.tabPage6);
            this.tabResults.Controls.Add(this.tabPage8);
            this.tabResults.Controls.Add(this.tabPage9);
            this.tabResults.Controls.Add(this.tabPage10);
            this.tabResults.Location = new System.Drawing.Point(10, 63);
            this.tabResults.Multiline = true;
            this.tabResults.Name = "tabResults";
            this.tabResults.SelectedIndex = 0;
            this.tabResults.Size = new System.Drawing.Size(646, 505);
            this.tabResults.TabIndex = 28;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtDetails);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(638, 474);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtDetails
            // 
            this.txtDetails.AcceptsReturn = true;
            this.txtDetails.AcceptsTab = true;
            this.txtDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDetails.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDetails.Location = new System.Drawing.Point(0, 0);
            this.txtDetails.Multiline = true;
            this.txtDetails.Name = "txtDetails";
            this.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDetails.Size = new System.Drawing.Size(638, 474);
            this.txtDetails.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtResultsCSharp);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(638, 474);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "C#";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtResultsCSharp
            // 
            this.txtResultsCSharp.AcceptsReturn = true;
            this.txtResultsCSharp.AcceptsTab = true;
            this.txtResultsCSharp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultsCSharp.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultsCSharp.Location = new System.Drawing.Point(3, 3);
            this.txtResultsCSharp.Multiline = true;
            this.txtResultsCSharp.Name = "txtResultsCSharp";
            this.txtResultsCSharp.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResultsCSharp.Size = new System.Drawing.Size(632, 468);
            this.txtResultsCSharp.TabIndex = 1;
            this.txtResultsCSharp.WordWrap = false;
            this.txtResultsCSharp.TextChanged += new System.EventHandler(this.txtResultsCSharp_TextChanged);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.btnLoadAllRuns);
            this.tabPage7.Controls.Add(this.label15);
            this.tabPage7.Controls.Add(this.txtNBest);
            this.tabPage7.Controls.Add(this.btnLoadBestRuns);
            this.tabPage7.Controls.Add(this.lstRuns);
            this.tabPage7.Controls.Add(this.btnLoadRunData);
            this.tabPage7.Location = new System.Drawing.Point(4, 27);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(638, 474);
            this.tabPage7.TabIndex = 4;
            this.tabPage7.Text = "Runs";
            this.tabPage7.UseVisualStyleBackColor = true;
            this.tabPage7.Click += new System.EventHandler(this.tabPage7_Click);
            // 
            // btnLoadAllRuns
            // 
            this.btnLoadAllRuns.Location = new System.Drawing.Point(254, 9);
            this.btnLoadAllRuns.Name = "btnLoadAllRuns";
            this.btnLoadAllRuns.Size = new System.Drawing.Size(139, 40);
            this.btnLoadAllRuns.TabIndex = 7;
            this.btnLoadAllRuns.Text = "Load All Runs";
            this.btnLoadAllRuns.UseVisualStyleBackColor = true;
            this.btnLoadAllRuns.Click += new System.EventHandler(this.btnLoadAllRuns_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 18);
            this.label15.TabIndex = 6;
            this.label15.Text = "Top:";
            // 
            // txtNBest
            // 
            this.txtNBest.Location = new System.Drawing.Point(50, 17);
            this.txtNBest.Name = "txtNBest";
            this.txtNBest.Size = new System.Drawing.Size(53, 24);
            this.txtNBest.TabIndex = 4;
            this.txtNBest.Text = "10";
            // 
            // btnLoadBestRuns
            // 
            this.btnLoadBestRuns.Location = new System.Drawing.Point(109, 9);
            this.btnLoadBestRuns.Name = "btnLoadBestRuns";
            this.btnLoadBestRuns.Size = new System.Drawing.Size(139, 40);
            this.btnLoadBestRuns.TabIndex = 3;
            this.btnLoadBestRuns.Text = "Load Best Runs";
            this.btnLoadBestRuns.UseVisualStyleBackColor = true;
            this.btnLoadBestRuns.Click += new System.EventHandler(this.btnLoadBestRuns_Click);
            // 
            // lstRuns
            // 
            this.lstRuns.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lstRuns.FormattingEnabled = true;
            this.lstRuns.ItemHeight = 18;
            this.lstRuns.Location = new System.Drawing.Point(3, 71);
            this.lstRuns.Name = "lstRuns";
            this.lstRuns.Size = new System.Drawing.Size(632, 400);
            this.lstRuns.TabIndex = 2;
            // 
            // btnLoadRunData
            // 
            this.btnLoadRunData.Location = new System.Drawing.Point(500, 9);
            this.btnLoadRunData.Name = "btnLoadRunData";
            this.btnLoadRunData.Size = new System.Drawing.Size(132, 40);
            this.btnLoadRunData.TabIndex = 1;
            this.btnLoadRunData.Text = "Load Run Data";
            this.btnLoadRunData.UseVisualStyleBackColor = true;
            this.btnLoadRunData.Click += new System.EventHandler(this.btnLoadRunData_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtResultsChromosome);
            this.tabPage4.Location = new System.Drawing.Point(4, 27);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(638, 474);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Chromosome";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtResultsChromosome
            // 
            this.txtResultsChromosome.AcceptsReturn = true;
            this.txtResultsChromosome.AcceptsTab = true;
            this.txtResultsChromosome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultsChromosome.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultsChromosome.Location = new System.Drawing.Point(0, 0);
            this.txtResultsChromosome.Multiline = true;
            this.txtResultsChromosome.Name = "txtResultsChromosome";
            this.txtResultsChromosome.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultsChromosome.Size = new System.Drawing.Size(638, 474);
            this.txtResultsChromosome.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtResults);
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Location = new System.Drawing.Point(4, 27);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(638, 474);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Results";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtResults
            // 
            this.txtResults.AcceptsReturn = true;
            this.txtResults.AcceptsTab = true;
            this.txtResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResults.Location = new System.Drawing.Point(0, 43);
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResults.Size = new System.Drawing.Size(638, 431);
            this.txtResults.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btnLoadStoredResults);
            this.panel9.Controls.Add(this.btnResultsLoadData);
            this.panel9.Controls.Add(this.btnSaveResults);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(638, 43);
            this.panel9.TabIndex = 2;
            // 
            // btnLoadStoredResults
            // 
            this.btnLoadStoredResults.Location = new System.Drawing.Point(138, 2);
            this.btnLoadStoredResults.Name = "btnLoadStoredResults";
            this.btnLoadStoredResults.Size = new System.Drawing.Size(162, 38);
            this.btnLoadStoredResults.TabIndex = 2;
            this.btnLoadStoredResults.Text = "Show Stored Results";
            this.btnLoadStoredResults.UseVisualStyleBackColor = true;
            this.btnLoadStoredResults.Click += new System.EventHandler(this.btnLoadStoredResults_Click);
            // 
            // btnResultsLoadData
            // 
            this.btnResultsLoadData.Location = new System.Drawing.Point(3, 2);
            this.btnResultsLoadData.Name = "btnResultsLoadData";
            this.btnResultsLoadData.Size = new System.Drawing.Size(129, 38);
            this.btnResultsLoadData.TabIndex = 1;
            this.btnResultsLoadData.Text = "Load Data";
            this.btnResultsLoadData.UseVisualStyleBackColor = true;
            this.btnResultsLoadData.Click += new System.EventHandler(this.btnResultsLoadData_Click);
            // 
            // btnSaveResults
            // 
            this.btnSaveResults.Location = new System.Drawing.Point(560, 2);
            this.btnSaveResults.Name = "btnSaveResults";
            this.btnSaveResults.Size = new System.Drawing.Size(75, 38);
            this.btnSaveResults.TabIndex = 0;
            this.btnSaveResults.Text = "Save";
            this.btnSaveResults.UseVisualStyleBackColor = true;
            this.btnSaveResults.Click += new System.EventHandler(this.btnSaveResults_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.txtResultsSizeData);
            this.tabPage6.Controls.Add(this.panel5);
            this.tabPage6.Location = new System.Drawing.Point(4, 27);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(638, 474);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Size";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // txtResultsSizeData
            // 
            this.txtResultsSizeData.AcceptsReturn = true;
            this.txtResultsSizeData.AcceptsTab = true;
            this.txtResultsSizeData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultsSizeData.Location = new System.Drawing.Point(3, 46);
            this.txtResultsSizeData.Multiline = true;
            this.txtResultsSizeData.Name = "txtResultsSizeData";
            this.txtResultsSizeData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResultsSizeData.Size = new System.Drawing.Size(632, 425);
            this.txtResultsSizeData.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnResultsSaveSize);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(632, 43);
            this.panel5.TabIndex = 0;
            // 
            // btnResultsSaveSize
            // 
            this.btnResultsSaveSize.Location = new System.Drawing.Point(3, 3);
            this.btnResultsSaveSize.Name = "btnResultsSaveSize";
            this.btnResultsSaveSize.Size = new System.Drawing.Size(75, 38);
            this.btnResultsSaveSize.TabIndex = 0;
            this.btnResultsSaveSize.Text = "Save";
            this.btnResultsSaveSize.UseVisualStyleBackColor = true;
            this.btnResultsSaveSize.Click += new System.EventHandler(this.btnResultsSaveSize_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.txtResultsSizeVsFit);
            this.tabPage8.Controls.Add(this.panel6);
            this.tabPage8.Location = new System.Drawing.Point(4, 27);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(638, 474);
            this.tabPage8.TabIndex = 5;
            this.tabPage8.Text = "Size vs Fit";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // txtResultsSizeVsFit
            // 
            this.txtResultsSizeVsFit.AcceptsReturn = true;
            this.txtResultsSizeVsFit.AcceptsTab = true;
            this.txtResultsSizeVsFit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultsSizeVsFit.Location = new System.Drawing.Point(0, 43);
            this.txtResultsSizeVsFit.Multiline = true;
            this.txtResultsSizeVsFit.Name = "txtResultsSizeVsFit";
            this.txtResultsSizeVsFit.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResultsSizeVsFit.Size = new System.Drawing.Size(638, 431);
            this.txtResultsSizeVsFit.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnResultsSizeVsFitSave);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(638, 43);
            this.panel6.TabIndex = 1;
            // 
            // btnResultsSizeVsFitSave
            // 
            this.btnResultsSizeVsFitSave.Location = new System.Drawing.Point(3, 3);
            this.btnResultsSizeVsFitSave.Name = "btnResultsSizeVsFitSave";
            this.btnResultsSizeVsFitSave.Size = new System.Drawing.Size(75, 38);
            this.btnResultsSizeVsFitSave.TabIndex = 0;
            this.btnResultsSizeVsFitSave.Text = "Save";
            this.btnResultsSizeVsFitSave.UseVisualStyleBackColor = true;
            this.btnResultsSizeVsFitSave.Click += new System.EventHandler(this.btnResultsSizeVsFitSave_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.txtResultsSizeVsMutations);
            this.tabPage9.Controls.Add(this.panel7);
            this.tabPage9.Location = new System.Drawing.Point(4, 27);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(638, 474);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Size vs Muts";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // txtResultsSizeVsMutations
            // 
            this.txtResultsSizeVsMutations.AcceptsReturn = true;
            this.txtResultsSizeVsMutations.AcceptsTab = true;
            this.txtResultsSizeVsMutations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultsSizeVsMutations.Location = new System.Drawing.Point(0, 43);
            this.txtResultsSizeVsMutations.Multiline = true;
            this.txtResultsSizeVsMutations.Name = "txtResultsSizeVsMutations";
            this.txtResultsSizeVsMutations.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResultsSizeVsMutations.Size = new System.Drawing.Size(638, 431);
            this.txtResultsSizeVsMutations.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.button3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(638, 43);
            this.panel7.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 38);
            this.button3.TabIndex = 0;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.txtResultsMinMax);
            this.tabPage10.Controls.Add(this.panel8);
            this.tabPage10.Location = new System.Drawing.Point(4, 27);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(638, 474);
            this.tabPage10.TabIndex = 6;
            this.tabPage10.Text = "MinMax";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // txtResultsMinMax
            // 
            this.txtResultsMinMax.AcceptsTab = true;
            this.txtResultsMinMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultsMinMax.Location = new System.Drawing.Point(0, 43);
            this.txtResultsMinMax.Multiline = true;
            this.txtResultsMinMax.Name = "txtResultsMinMax";
            this.txtResultsMinMax.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResultsMinMax.Size = new System.Drawing.Size(638, 431);
            this.txtResultsMinMax.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button6);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(638, 43);
            this.panel8.TabIndex = 4;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(3, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 38);
            this.button6.TabIndex = 0;
            this.button6.Text = "Save";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(75, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 18);
            this.label20.TabIndex = 23;
            this.label20.Text = "Total Runs:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlTotalRuns
            // 
            this.lblControlTotalRuns.AutoSize = true;
            this.lblControlTotalRuns.Location = new System.Drawing.Point(165, 38);
            this.lblControlTotalRuns.Name = "lblControlTotalRuns";
            this.lblControlTotalRuns.Size = new System.Drawing.Size(16, 18);
            this.lblControlTotalRuns.TabIndex = 24;
            this.lblControlTotalRuns.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(217, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(152, 18);
            this.label18.TabIndex = 21;
            this.label18.Text = "Best Session Fitness:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlBestSessionFitness
            // 
            this.lblControlBestSessionFitness.AutoSize = true;
            this.lblControlBestSessionFitness.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlBestSessionFitness.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline);
            this.lblControlBestSessionFitness.ForeColor = System.Drawing.Color.Blue;
            this.lblControlBestSessionFitness.Location = new System.Drawing.Point(375, 38);
            this.lblControlBestSessionFitness.Name = "lblControlBestSessionFitness";
            this.lblControlBestSessionFitness.Size = new System.Drawing.Size(16, 18);
            this.lblControlBestSessionFitness.TabIndex = 22;
            this.lblControlBestSessionFitness.Text = "?";
            this.lblControlBestSessionFitness.Click += new System.EventHandler(this.lblControlBestSessionFitness_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(275, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 18);
            this.label16.TabIndex = 19;
            this.label16.Text = "Best Fitness:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlBestFitness
            // 
            this.lblControlBestFitness.AutoSize = true;
            this.lblControlBestFitness.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlBestFitness.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline);
            this.lblControlBestFitness.ForeColor = System.Drawing.Color.Blue;
            this.lblControlBestFitness.Location = new System.Drawing.Point(375, 20);
            this.lblControlBestFitness.Name = "lblControlBestFitness";
            this.lblControlBestFitness.Size = new System.Drawing.Size(16, 18);
            this.lblControlBestFitness.TabIndex = 20;
            this.lblControlBestFitness.Text = "?";
            this.lblControlBestFitness.Click += new System.EventHandler(this.lblControlBestFitness_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(153, 18);
            this.label12.TabIndex = 17;
            this.label12.Text = "Max Possible Fitness:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblControlMaxFitness
            // 
            this.lblControlMaxFitness.AutoSize = true;
            this.lblControlMaxFitness.Location = new System.Drawing.Point(165, 20);
            this.lblControlMaxFitness.Name = "lblControlMaxFitness";
            this.lblControlMaxFitness.Size = new System.Drawing.Size(16, 18);
            this.lblControlMaxFitness.TabIndex = 18;
            this.lblControlMaxFitness.Text = "0";
            // 
            // grpControlActiveProblem
            // 
            this.grpControlActiveProblem.Controls.Add(this.btnControlSaveNotes);
            this.grpControlActiveProblem.Controls.Add(this.label7);
            this.grpControlActiveProblem.Controls.Add(this.lblControlProblemId);
            this.grpControlActiveProblem.Controls.Add(this.lblActiveProblemTitle);
            this.grpControlActiveProblem.Controls.Add(this.label1);
            this.grpControlActiveProblem.Controls.Add(this.label9);
            this.grpControlActiveProblem.Controls.Add(this.txtControlProblemNotes);
            this.grpControlActiveProblem.Location = new System.Drawing.Point(6, 3);
            this.grpControlActiveProblem.Name = "grpControlActiveProblem";
            this.grpControlActiveProblem.Size = new System.Drawing.Size(509, 203);
            this.grpControlActiveProblem.TabIndex = 13;
            this.grpControlActiveProblem.TabStop = false;
            this.grpControlActiveProblem.Text = "Problem";
            // 
            // btnControlSaveNotes
            // 
            this.btnControlSaveNotes.Location = new System.Drawing.Point(9, 143);
            this.btnControlSaveNotes.Name = "btnControlSaveNotes";
            this.btnControlSaveNotes.Size = new System.Drawing.Size(62, 51);
            this.btnControlSaveNotes.TabIndex = 6;
            this.btnControlSaveNotes.Text = "Save Notes";
            this.btnControlSaveNotes.UseVisualStyleBackColor = true;
            this.btnControlSaveNotes.Click += new System.EventHandler(this.btnControlSaveNotes_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 18);
            this.label7.TabIndex = 2;
            this.label7.Text = "Problem:";
            // 
            // lblControlProblemId
            // 
            this.lblControlProblemId.AutoSize = true;
            this.lblControlProblemId.Location = new System.Drawing.Point(93, 22);
            this.lblControlProblemId.Name = "lblControlProblemId";
            this.lblControlProblemId.Size = new System.Drawing.Size(0, 18);
            this.lblControlProblemId.TabIndex = 8;
            // 
            // lblActiveProblemTitle
            // 
            this.lblActiveProblemTitle.AutoSize = true;
            this.lblActiveProblemTitle.Location = new System.Drawing.Point(85, 47);
            this.lblActiveProblemTitle.Name = "lblActiveProblemTitle";
            this.lblActiveProblemTitle.Size = new System.Drawing.Size(0, 18);
            this.lblActiveProblemTitle.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 18);
            this.label9.TabIndex = 4;
            this.label9.Text = "Notes:";
            // 
            // txtControlProblemNotes
            // 
            this.txtControlProblemNotes.AcceptsReturn = true;
            this.txtControlProblemNotes.AcceptsTab = true;
            this.txtControlProblemNotes.Location = new System.Drawing.Point(96, 71);
            this.txtControlProblemNotes.Multiline = true;
            this.txtControlProblemNotes.Name = "txtControlProblemNotes";
            this.txtControlProblemNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtControlProblemNotes.Size = new System.Drawing.Size(407, 126);
            this.txtControlProblemNotes.TabIndex = 5;
            // 
            // grpControlProblemSelector
            // 
            this.grpControlProblemSelector.Controls.Add(this.lstCurrentProblem);
            this.grpControlProblemSelector.Controls.Add(this.panel4);
            this.grpControlProblemSelector.Location = new System.Drawing.Point(6, 212);
            this.grpControlProblemSelector.MinimumSize = new System.Drawing.Size(450, 250);
            this.grpControlProblemSelector.Name = "grpControlProblemSelector";
            this.grpControlProblemSelector.Size = new System.Drawing.Size(509, 459);
            this.grpControlProblemSelector.TabIndex = 4;
            this.grpControlProblemSelector.TabStop = false;
            this.grpControlProblemSelector.Text = " Problem Selector ";
            // 
            // lstCurrentProblem
            // 
            this.lstCurrentProblem.FormattingEnabled = true;
            this.lstCurrentProblem.ItemHeight = 18;
            this.lstCurrentProblem.Location = new System.Drawing.Point(9, 23);
            this.lstCurrentProblem.Name = "lstCurrentProblem";
            this.lstCurrentProblem.ScrollAlwaysVisible = true;
            this.lstCurrentProblem.Size = new System.Drawing.Size(494, 382);
            this.lstCurrentProblem.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 416);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(503, 40);
            this.panel4.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Left;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(215, 40);
            this.button2.TabIndex = 4;
            this.button2.Text = "End Evolution";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Location = new System.Drawing.Point(288, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(215, 40);
            this.button1.TabIndex = 3;
            this.button1.Text = "Activate Selected Problem";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabProblemEditor
            // 
            this.tabProblemEditor.Controls.Add(this.problemSettingsGrid);
            this.tabProblemEditor.Controls.Add(this.panel3);
            this.tabProblemEditor.Location = new System.Drawing.Point(4, 27);
            this.tabProblemEditor.Name = "tabProblemEditor";
            this.tabProblemEditor.Size = new System.Drawing.Size(1197, 674);
            this.tabProblemEditor.TabIndex = 3;
            this.tabProblemEditor.Text = "Problems";
            this.tabProblemEditor.UseVisualStyleBackColor = true;
            // 
            // problemSettingsGrid
            // 
            this.problemSettingsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.problemSettingsGrid.Location = new System.Drawing.Point(630, 0);
            this.problemSettingsGrid.Name = "problemSettingsGrid";
            this.problemSettingsGrid.Size = new System.Drawing.Size(567, 674);
            this.problemSettingsGrid.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitter2);
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(630, 674);
            this.panel3.TabIndex = 5;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(620, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(10, 674);
            this.splitter2.TabIndex = 9;
            this.splitter2.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnClearTestCases);
            this.groupBox2.Controls.Add(this.lblProblemBuilderTestCaseDetail);
            this.groupBox2.Controls.Add(this.btnProblemBuilderImportTestCases);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtProblemBuilderComments);
            this.groupBox2.Controls.Add(this.txtProblemBuilderTitle);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(6, 109);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(611, 562);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Problem Details ";
            // 
            // btnClearTestCases
            // 
            this.btnClearTestCases.Location = new System.Drawing.Point(86, 345);
            this.btnClearTestCases.Name = "btnClearTestCases";
            this.btnClearTestCases.Size = new System.Drawing.Size(190, 31);
            this.btnClearTestCases.TabIndex = 13;
            this.btnClearTestCases.Text = "Clear Test Cases";
            this.btnClearTestCases.UseVisualStyleBackColor = true;
            this.btnClearTestCases.Click += new System.EventHandler(this.btnClearTestCases_Click);
            // 
            // lblProblemBuilderTestCaseDetail
            // 
            this.lblProblemBuilderTestCaseDetail.AutoEllipsis = true;
            this.lblProblemBuilderTestCaseDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProblemBuilderTestCaseDetail.Location = new System.Drawing.Point(85, 233);
            this.lblProblemBuilderTestCaseDetail.Name = "lblProblemBuilderTestCaseDetail";
            this.lblProblemBuilderTestCaseDetail.Size = new System.Drawing.Size(507, 98);
            this.lblProblemBuilderTestCaseDetail.TabIndex = 12;
            this.lblProblemBuilderTestCaseDetail.Text = "None";
            // 
            // btnProblemBuilderImportTestCases
            // 
            this.btnProblemBuilderImportTestCases.Location = new System.Drawing.Point(402, 345);
            this.btnProblemBuilderImportTestCases.Name = "btnProblemBuilderImportTestCases";
            this.btnProblemBuilderImportTestCases.Size = new System.Drawing.Size(190, 31);
            this.btnProblemBuilderImportTestCases.TabIndex = 11;
            this.btnProblemBuilderImportTestCases.Text = "Import Test Cases";
            this.btnProblemBuilderImportTestCases.UseVisualStyleBackColor = true;
            this.btnProblemBuilderImportTestCases.Click += new System.EventHandler(this.btnProblemBuilderImportTestCases_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-3, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Test Cases:";
            // 
            // txtProblemBuilderComments
            // 
            this.txtProblemBuilderComments.AcceptsReturn = true;
            this.txtProblemBuilderComments.AcceptsTab = true;
            this.txtProblemBuilderComments.Location = new System.Drawing.Point(85, 48);
            this.txtProblemBuilderComments.Multiline = true;
            this.txtProblemBuilderComments.Name = "txtProblemBuilderComments";
            this.txtProblemBuilderComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProblemBuilderComments.Size = new System.Drawing.Size(507, 179);
            this.txtProblemBuilderComments.TabIndex = 3;
            // 
            // txtProblemBuilderTitle
            // 
            this.txtProblemBuilderTitle.Location = new System.Drawing.Point(85, 22);
            this.txtProblemBuilderTitle.Name = "txtProblemBuilderTitle";
            this.txtProblemBuilderTitle.Size = new System.Drawing.Size(507, 24);
            this.txtProblemBuilderTitle.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Notes:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Title:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnProblemBuilderNew);
            this.groupBox1.Controls.Add(this.btnProblemBuilderSave);
            this.groupBox1.Controls.Add(this.btnProblemBuilderSaveNew);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lstProblemBuilderProblems);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(612, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // btnProblemBuilderNew
            // 
            this.btnProblemBuilderNew.Location = new System.Drawing.Point(11, 58);
            this.btnProblemBuilderNew.Name = "btnProblemBuilderNew";
            this.btnProblemBuilderNew.Size = new System.Drawing.Size(120, 31);
            this.btnProblemBuilderNew.TabIndex = 12;
            this.btnProblemBuilderNew.Text = "New Problem";
            this.btnProblemBuilderNew.UseVisualStyleBackColor = true;
            this.btnProblemBuilderNew.Click += new System.EventHandler(this.btnProblemBuilderNew_Click);
            // 
            // btnProblemBuilderSave
            // 
            this.btnProblemBuilderSave.Location = new System.Drawing.Point(330, 58);
            this.btnProblemBuilderSave.Name = "btnProblemBuilderSave";
            this.btnProblemBuilderSave.Size = new System.Drawing.Size(75, 31);
            this.btnProblemBuilderSave.TabIndex = 11;
            this.btnProblemBuilderSave.Text = "Save";
            this.btnProblemBuilderSave.UseVisualStyleBackColor = true;
            this.btnProblemBuilderSave.Click += new System.EventHandler(this.btnProblemBuilderSave_Click);
            // 
            // btnProblemBuilderSaveNew
            // 
            this.btnProblemBuilderSaveNew.Location = new System.Drawing.Point(416, 58);
            this.btnProblemBuilderSaveNew.Name = "btnProblemBuilderSaveNew";
            this.btnProblemBuilderSaveNew.Size = new System.Drawing.Size(190, 31);
            this.btnProblemBuilderSaveNew.TabIndex = 10;
            this.btnProblemBuilderSaveNew.Text = "Save Problem as New";
            this.btnProblemBuilderSaveNew.UseVisualStyleBackColor = true;
            this.btnProblemBuilderSaveNew.Click += new System.EventHandler(this.btnSaveProblemAsNew_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Problem: ";
            // 
            // lstProblemBuilderProblems
            // 
            this.lstProblemBuilderProblems.DisplayMember = "(none)";
            this.lstProblemBuilderProblems.Location = new System.Drawing.Point(86, 19);
            this.lstProblemBuilderProblems.Name = "lstProblemBuilderProblems";
            this.lstProblemBuilderProblems.Size = new System.Drawing.Size(520, 26);
            this.lstProblemBuilderProblems.TabIndex = 7;
            this.lstProblemBuilderProblems.SelectedIndexChanged += new System.EventHandler(this.lstProblemBuilderProblems_SelectedIndexChanged);
            // 
            // tabClientList
            // 
            this.tabClientList.Controls.Add(this.ClientProperties);
            this.tabClientList.Controls.Add(this.splitter1);
            this.tabClientList.Controls.Add(this.panel1);
            this.tabClientList.Location = new System.Drawing.Point(4, 27);
            this.tabClientList.Name = "tabClientList";
            this.tabClientList.Padding = new System.Windows.Forms.Padding(3);
            this.tabClientList.Size = new System.Drawing.Size(1197, 674);
            this.tabClientList.TabIndex = 1;
            this.tabClientList.Text = "Clients";
            this.tabClientList.UseVisualStyleBackColor = true;
            // 
            // ClientProperties
            // 
            this.ClientProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientProperties.Location = new System.Drawing.Point(316, 3);
            this.ClientProperties.Name = "ClientProperties";
            this.ClientProperties.Size = new System.Drawing.Size(878, 668);
            this.ClientProperties.TabIndex = 7;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(313, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 668);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.clientListBox);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(310, 668);
            this.panel1.TabIndex = 3;
            // 
            // clientListBox
            // 
            this.clientListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientListBox.FormattingEnabled = true;
            this.clientListBox.ItemHeight = 18;
            this.clientListBox.Location = new System.Drawing.Point(0, 0);
            this.clientListBox.Name = "clientListBox";
            this.clientListBox.Size = new System.Drawing.Size(310, 616);
            this.clientListBox.TabIndex = 3;
            this.clientListBox.SelectedIndexChanged += new System.EventHandler(this.clientListBox_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkAutoRefresh);
            this.panel2.Controls.Add(this.btnRefresh);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 618);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(310, 50);
            this.panel2.TabIndex = 2;
            // 
            // chkAutoRefresh
            // 
            this.chkAutoRefresh.AutoSize = true;
            this.chkAutoRefresh.Location = new System.Drawing.Point(190, 14);
            this.chkAutoRefresh.Name = "chkAutoRefresh";
            this.chkAutoRefresh.Size = new System.Drawing.Size(113, 22);
            this.chkAutoRefresh.TabIndex = 1;
            this.chkAutoRefresh.Text = "Auto Refresh";
            this.chkAutoRefresh.UseVisualStyleBackColor = true;
            this.chkAutoRefresh.CheckedChanged += new System.EventHandler(this.chkAutoRefresh_CheckedChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(17, 8);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(140, 33);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // tabLog
            // 
            this.tabLog.Controls.Add(this.lstLog);
            this.tabLog.Location = new System.Drawing.Point(4, 27);
            this.tabLog.Name = "tabLog";
            this.tabLog.Size = new System.Drawing.Size(1197, 674);
            this.tabLog.TabIndex = 2;
            this.tabLog.Text = "Log";
            this.tabLog.UseVisualStyleBackColor = true;
            // 
            // lstLog
            // 
            this.lstLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLog.FormattingEnabled = true;
            this.lstLog.ItemHeight = 18;
            this.lstLog.Location = new System.Drawing.Point(0, 0);
            this.lstLog.Name = "lstLog";
            this.lstLog.Size = new System.Drawing.Size(1197, 670);
            this.lstLog.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button5);
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.txtTestBed2);
            this.tabPage5.Controls.Add(this.txtTestBed1);
            this.tabPage5.Location = new System.Drawing.Point(4, 27);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1197, 674);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Test Bed";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(116, 332);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(164, 36);
            this.button5.TabIndex = 4;
            this.button5.Text = "Deserialize Object";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(116, 17);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(164, 34);
            this.button4.TabIndex = 3;
            this.button4.Text = "Serialize Object";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtTestBed2
            // 
            this.txtTestBed2.AcceptsReturn = true;
            this.txtTestBed2.AcceptsTab = true;
            this.txtTestBed2.Location = new System.Drawing.Point(315, 332);
            this.txtTestBed2.Multiline = true;
            this.txtTestBed2.Name = "txtTestBed2";
            this.txtTestBed2.Size = new System.Drawing.Size(868, 309);
            this.txtTestBed2.TabIndex = 1;
            // 
            // txtTestBed1
            // 
            this.txtTestBed1.AcceptsReturn = true;
            this.txtTestBed1.AcceptsTab = true;
            this.txtTestBed1.Location = new System.Drawing.Point(315, 6);
            this.txtTestBed1.Multiline = true;
            this.txtTestBed1.Name = "txtTestBed1";
            this.txtTestBed1.Size = new System.Drawing.Size(868, 309);
            this.txtTestBed1.TabIndex = 0;
            // 
            // tmrClientViewerRefresh
            // 
            this.tmrClientViewerRefresh.Interval = 60000;
            this.tmrClientViewerRefresh.Tick += new System.EventHandler(this.ClientViewerRefreshTimer_Tick);
            // 
            // openTestCasesDialog
            // 
            this.openTestCasesDialog.Filter = "CSV Data Files|*.csv";
            this.openTestCasesDialog.Title = "Select test case data file";
            // 
            // tmrControlTabUpdate
            // 
            this.tmrControlTabUpdate.Interval = 1000;
            this.tmrControlTabUpdate.Tick += new System.EventHandler(this.tmrControlTabUpdate_Tick);
            // 
            // tmrResultsUpdater
            // 
            this.tmrResultsUpdater.Interval = 10000;
            this.tmrResultsUpdater.Tick += new System.EventHandler(this.tmrResultsUpdater_Tick);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.CreatePrompt = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(599, 18);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 23);
            this.button7.TabIndex = 29;
            this.button7.Text = "ld";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // txtRunIdToLoad
            // 
            this.txtRunIdToLoad.Location = new System.Drawing.Point(548, 18);
            this.txtRunIdToLoad.Name = "txtRunIdToLoad";
            this.txtRunIdToLoad.Size = new System.Drawing.Size(45, 24);
            this.txtRunIdToLoad.TabIndex = 30;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 751);
            this.Controls.Add(this.MainTabControl);
            this.Controls.Add(this.ApplicationStatusStrip);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Syrah Master";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.ApplicationStatusStrip.ResumeLayout(false);
            this.ApplicationStatusStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.MainTabControl.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.grpControlSession.ResumeLayout(false);
            this.grpControlSession.PerformLayout();
            this.grpControlResults.ResumeLayout(false);
            this.grpControlResults.PerformLayout();
            this.tabResults.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.grpControlActiveProblem.ResumeLayout(false);
            this.grpControlActiveProblem.PerformLayout();
            this.grpControlProblemSelector.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabProblemEditor.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabClientList.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabLog.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip ApplicationStatusStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage tabControl;
        private System.Windows.Forms.TabPage tabClientList;
        private System.Windows.Forms.TabPage tabLog;
        private System.Windows.Forms.ListBox lstLog;
        private System.Windows.Forms.Timer tmrClientViewerRefresh;        
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkAutoRefresh;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListBox clientListBox;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detailedToolStripMenuItem;
        private System.Windows.Forms.TabPage tabProblemEditor;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtProblemBuilderComments;
        private System.Windows.Forms.TextBox txtProblemBuilderTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox lstProblemBuilderProblems;
        private System.Windows.Forms.PropertyGrid problemSettingsGrid;
        private System.Windows.Forms.Button btnProblemBuilderNew;
        private System.Windows.Forms.Button btnProblemBuilderSave;
        private System.Windows.Forms.Button btnProblemBuilderSaveNew;
        private System.Windows.Forms.Button btnProblemBuilderImportTestCases;
        private System.Windows.Forms.OpenFileDialog openTestCasesDialog;
        private System.Windows.Forms.GroupBox grpControlProblemSelector;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListBox lstCurrentProblem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblActiveProblemTitle;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnControlSaveNotes;
        private System.Windows.Forms.TextBox txtControlProblemNotes;
        private System.Windows.Forms.Label lblControlProblemId;
        private System.Windows.Forms.ToolStripStatusLabel ApplicationToolStripStatus;
        private System.Windows.Forms.Timer tmrControlTabUpdate;
        private System.Windows.Forms.GroupBox grpControlActiveProblem;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.PropertyGrid ClientProperties;
        private System.Windows.Forms.GroupBox grpControlResults;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblControlTotalRuns;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblControlBestSessionFitness;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblControlBestFitness;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblControlMaxFitness;
        private System.Windows.Forms.GroupBox grpControlSession;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblControlClientCount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblControlRuns;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblControlAverageTimePerRun;
        private System.Windows.Forms.Label lblControlRuntime;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblControlRunStarted;
        private System.Windows.Forms.TabControl tabResults;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtResultsChromosome;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtResultsCSharp;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Timer tmrResultsUpdater;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lblProblemBuilderTestCaseDetail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox txtTestBed2;
        private System.Windows.Forms.TextBox txtTestBed1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox txtResultsSizeData;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnResultsSaveSize;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ListBox lstRuns;
        private System.Windows.Forms.Button btnLoadRunData;
        private System.Windows.Forms.Button btnLoadBestRuns;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnResultsSizeVsFitSave;
        private System.Windows.Forms.TextBox txtResultsSizeVsFit;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TextBox txtResultsSizeVsMutations;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtResultsMinMax;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtNBest;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnLoadAllRuns;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem useKFoldValidationToolStripMenuItem;
        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnSaveResults;
        private System.Windows.Forms.Button btnResultsLoadData;
        private System.Windows.Forms.TextBox txtDetails;
        private System.Windows.Forms.Button btnLoadStoredResults;
        private System.Windows.Forms.Button btnClearTestCases;
        private System.Windows.Forms.TextBox txtRunIdToLoad;
        private System.Windows.Forms.Button button7;
    }
}

