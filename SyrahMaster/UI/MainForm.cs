﻿/***********************************************************************
 * MainForm.cs (Syrah Master)
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * This is the code behind and UI code for the Syrah master.
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.Data;
using NigelBrowne.Syrah.Common.ProblemBuilder;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Master {
    public partial class MainForm : Form {
        /// <summary>
        /// 
        /// </summary>
        public MainForm() {
            InitializeComponent();
            this.Text = _defaultFormTitle;

            _syrahMaster = new Syrah.Common.Master.SyrahMaster(
                new MessageHandlerDelegate(this.AppendToOutputLog),
                new ClientRunStartedDelegate(this.ClientRunStartedHandler),
                new ClientRunCompletedDelegate(this.ClientRunCompletedHandler));
            _syrahMaster.Start();

            try {
                //Load the client list
                this.RefreshClientViewer();
                this.BindDataToControls();
                            
            } catch (Exception err) {
                DisplayErrorDialog("Could not initialize the Syrah Master application. " + err.Message);
            }


            this.ResizeControls();    
            UpdateApplicationStatus("Syrah master application started.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void DisplayErrorDialog(string message) {
            MessageBox.Show(message, "Syrah Master Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void DisplayInformationDialog(string message) {
            MessageBox.Show(message, "Syrah Master Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            AboutForm frmAbout = new AboutForm();
            frmAbout.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            _syrahMaster.Stop();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientViewerRefreshTimer_Tick(object sender, EventArgs e) {
            try {
                this.RefreshClientViewer();
            } catch (Exception err) {
                tmrClientViewerRefresh.Stop();
                tmrClientViewerRefresh.Enabled = false;
                chkAutoRefresh.Checked = false;

                this.DisplayErrorDialog("And error occurred while trying to automatically refresh the client list. " + err.Message);
            }
        }


      

        private void clientListBox_SelectedIndexChanged(object sender, EventArgs e) {
            string clientName = (string)clientListBox.SelectedItem;
            ClientProperties.SelectedObject = _clientCollection[clientName];
            _lastSelectedClient = clientName;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e) {
            try {
                this.RefreshClientViewer();

            } catch (Exception err) {
                this.DisplayErrorDialog("An error occurred while trying to refresh the client list. "+ err.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAutoRefresh_CheckedChanged(object sender, EventArgs e) {
            if (chkAutoRefresh.Checked) {
                tmrClientViewerRefresh.Enabled = true;
                tmrClientViewerRefresh.Start();

            } else {
                tmrClientViewerRefresh.Stop();
                tmrClientViewerRefresh.Enabled = false;
            }
        }

        private void detailedToolStripMenuItem_Click(object sender, EventArgs e) {
            _displayedLogDetail = LogDetailLevel.Detail;
            normalToolStripMenuItem.Checked = false;            
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e) {
            _displayedLogDetail = LogDetailLevel.Information;
            detailedToolStripMenuItem.Checked = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e) {
            this.ActivateProblem();
        }


        /// <summary>
        /// This handler fires whenever the selected problem on the ProblemBuilder tab is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstProblemBuilderProblems_SelectedIndexChanged(object sender, EventArgs e) {
            this.LoadProblemBuilderProblem((Common.Data.Problem)lstProblemBuilderProblems.SelectedItem);
        }


        /// <summary>
        /// Onclick handler for the save as new button on the problem builder tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveProblemAsNew_Click(object sender, EventArgs e) {
            this.SaveNewProblemBuilderProblem();
        }


        /// <summary>
        /// Onclick handler for the new problem button on the problem builder tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void btnProblemBuilderNew_Click(object sender, EventArgs e) {
            this.NewProblemBuilderProblem();
        }

        /// <summary>
        /// Onclick handler for the save button on the problem builder tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProblemBuilderSave_Click(object sender, EventArgs e) {
            this.SaveCurrentProblemBuilderProblem();
        }


        /// <summary>
        /// Onclick handler for the import test cases button on the problem builder tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProblemBuilderImportTestCases_Click(object sender, EventArgs e) {
            if (_problemBuilderProblem != null) {
                if (this.openTestCasesDialog.ShowDialog() == DialogResult.OK) {
                    try {
                        this.ImportTestCases(System.IO.File.ReadAllText(openTestCasesDialog.FileName));
                    } catch (Exception err) {
                        this.DisplayErrorDialog("An error occurred while importing the test cases. " + err.Message);
                    }
                }
            } else {
                this.DisplayErrorDialog("Cannot import test cases because no problem is currently loaded in the problem builder.");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e) {
            this.DeactivateProblem();
        }       


        /// <summary>
        /// The button click handler for the Save Notes button on the control page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnControlSaveNotes_Click(object sender, EventArgs e) {
            if (_syrahMaster.ActiveProblemId != -1) {
                SaveNotesToProblem(_syrahMaster.ActiveProblemId, txtControlProblemNotes.Text);
                this.UpdateApplicationStatus("Successfully update the notes for the active problem.");

            } else {
                this.DisplayErrorDialog("Cannot save active problem notes because there is no problem active.");
            }
        }

        private void tmrControlTabUpdate_Tick(object sender, EventArgs e) {
            this.UpdateControlTab();
        }

        /// <summary>
        /// Resize the evolution monitor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResizeControls() {
            int leftWidthOffset = 540;

            //Resize the problem selector
            this.grpControlProblemSelector.Height = MainTabControl.Height - 258;
            this.lstCurrentProblem.Height = MainTabControl.Height - 325;

            //Resize teh session group box
            this.grpControlSession.Width = MainTabControl.Width - leftWidthOffset;

            //Resize the results group box
            this.grpControlResults.Width = MainTabControl.Width - leftWidthOffset;
            this.grpControlResults.Height = MainTabControl.Height - 140;

            //Resize the tab group in the results group box
            this.tabResults.Width = grpControlResults.Width - 20;
            this.tabResults.Height = grpControlResults.Height - 80;
        }

        private void tmrResultsUpdater_Tick(object sender, EventArgs e) {
            this.UpdateResults();
        }

        private void MainForm_Resize(object sender, EventArgs e) {
            this.ResizeControls();
        }

        private void lblControlBestFitness_Click(object sender, EventArgs e) {
            this.DisplayRunResultDetails(_bestProblemResults);
        }

        private void lblControlBestSessionFitness_Click(object sender, EventArgs e) {
            this.DisplayRunResultDetails(_bestSessionProblemResults);
        }

        #region TestBed Methods
        private void button4_Click(object sender, EventArgs e) {
            Common.EvolutionaryAlgorithms.GEP.Chromosome obj = new NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP.Chromosome(3, 0, 2, 0, false, RncConstantsType.Rational, 1, 2, 3);
            StringBuilder sb = new StringBuilder();
            System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(sb);
            
            Common.XmlUtilities.GenericXmlSerializer(writer, "Chromosome", obj);

            writer.Flush();
            txtTestBed1.Text = sb.ToString();
            writer.Close();
        }

        private void button5_Click(object sender, EventArgs e) {
            System.IO.StringReader sr = new System.IO.StringReader(txtTestBed1.Text);
            System.Xml.XmlReader reader = System.Xml.XmlReader.Create(sr);

            Common.XmlUtilities.GenericXmlDeserializer<Common.EvolutionaryAlgorithms.GEP.Chromosome>(reader);
            reader.Close();
        }
        #endregion

        private void btnResultsSaveSize_Click(object sender, EventArgs e) {
            saveFileDialog.Title = "Save Chromosome Size Results";
            saveFileDialog.DefaultExt = ".dat";
            saveFileDialog.Filter = "GnuPlot Data|*.dat";
            saveFileDialog.FileName = "Run Results - Size by generation.dat";
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(saveFileDialog.FileName, txtResultsSizeData.Text);
            }
        }

        private void btnLoadRunData_Click(object sender, EventArgs e) {
            //Get the displayed id
            ProblemResult problemResults = (ProblemResult)lstRuns.SelectedItem;

            this.DisplayRunResultDetails(problemResults);
        }

        private void grpControlResults_Enter(object sender, EventArgs e) {

        }

        private void button3_Click(object sender, EventArgs e) {
            saveFileDialog.Title = "Save Chromosome Size Vs Mutations Results";
            saveFileDialog.DefaultExt = ".dat";
            saveFileDialog.Filter = "GnuPlot Data|*.dat";
            saveFileDialog.FileName = "Run Results - Size vs Mutations.dat";
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(saveFileDialog.FileName, this.txtResultsSizeVsFit.Text);
            }
        }

        private void btnResultsSizeVsFitSave_Click(object sender, EventArgs e) {
            saveFileDialog.Title = "Save Chromosome Size Vs Fitness Results";
            saveFileDialog.DefaultExt = ".dat";
            saveFileDialog.Filter = "GnuPlot Data|*.dat";
            saveFileDialog.FileName = "Run Results - Size vs Fitness.dat";
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(saveFileDialog.FileName, this.txtResultsSizeVsFit.Text);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

        }

        private void panel8_Paint(object sender, PaintEventArgs e) {

        }

        private void button6_Click(object sender, EventArgs e) {
            saveFileDialog.Title = "Save Chromosome Size MinMax Results";
            saveFileDialog.DefaultExt = ".dat";
            saveFileDialog.Filter = "GnuPlot Data|*.dat";
            saveFileDialog.FileName = "Run Results - Size MinMax.dat";
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(saveFileDialog.FileName, this.txtResultsMinMax.Text);
            }
        }

        private void MainForm_Load(object sender, EventArgs e) {

        }

        private void txtResultsCSharp_TextChanged(object sender, EventArgs e) {

        }

        private void btnLoadRunRange_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented");
        }

        private void btnLoadBestRuns_Click(object sender, EventArgs e) {
            this.LoadTopProblemRuns(Int32.Parse(txtNBest.Text));            
        }

        private void tabPage7_Click(object sender, EventArgs e) {

        }

        private void btnLoadAllRuns_Click(object sender, EventArgs e) {
            this.LoadAllProblemRuns();
        }

        private void useKFoldValidationToolStripMenuItem_Click(object sender, EventArgs e) {
            useKFoldValidationToolStripMenuItem.Checked = !useKFoldValidationToolStripMenuItem.Checked;
        }

        private void btnResultsLoadData_Click(object sender, EventArgs e) {
            if (this.DisplayedRunResults != null) {
                if (this.openTestCasesDialog.ShowDialog() == DialogResult.OK) {
                    try {
                        List<TestCase> testCases = ProblemBuilder.ImportTestCasesFromText(System.IO.File.ReadAllText(openTestCasesDialog.FileName));
                        Problem runProblem = new Problem(this.DisplayedRunResults.ProblemId);
                        GepEvaluator eval = new GepEvaluator(runProblem.Settings);
                        StringBuilder buffer = new StringBuilder();

                        buffer.AppendLine("#Autogenerate results from run id " + this.DisplayedRunResults.Id.ToString() + ".");
                        buffer.AppendLine("#Problem ID: " + runProblem.Id.ToString());
                        buffer.AppendLine("#Tests: " + testCases.Count.ToString());
                        buffer.AppendLine("#Test Number, Expected, Evolved");


                        foreach (TestCase testCase in testCases) {
                            double result = eval.EvaluateSingleTestCase(this.DisplayedRunResults.BestChromosome, testCase, runProblem.Settings);
                            buffer.AppendLine(testCases.IndexOf(testCase).ToString() + "\t"+ testCase.ExpectedValue.ToString("0.00000") +"\t"+ result.ToString("0.00000"));
                        }

                        txtResults.Text = buffer.ToString();

                    } catch (Exception err) {
                        this.DisplayErrorDialog("An error occurred while importing the test cases. " + err.Message);
                    }
                }
            } else {
                this.DisplayErrorDialog("Cannot import test cases because run results are currently displayed.");
            }
        }

        private void btnSaveResults_Click(object sender, EventArgs e) {
            saveFileDialog.Title = "Save Run Results";
            saveFileDialog.DefaultExt = ".dat";
            saveFileDialog.Filter = "GnuPlot Data|*.dat";
            saveFileDialog.FileName = "Run Results.dat";
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(saveFileDialog.FileName, this.txtResults.Text);
            }
        }

        private void btnLoadStoredResults_Click(object sender, EventArgs e) {
            if (this.DisplayedRunResults != null) {
                Problem runProblem = new Problem(this.DisplayedRunResults.ProblemId);
                StringBuilder buffer = new StringBuilder();

                buffer.AppendLine("#Autogenerate results from run id " + this.DisplayedRunResults.Id.ToString() + ".");
                buffer.AppendLine("#Problem ID: " + runProblem.Id.ToString());
                buffer.AppendLine("#Fitness Method: "+ runProblem.Settings.FitnessMethod);
                buffer.AppendLine(this.DisplayedRunResults.ResultsText);

                txtResults.Text = buffer.ToString();
            }
        }

        private void btnClearTestCases_Click(object sender, EventArgs e) {
            this.ImportTestCases("");
        }

        private void button7_Click(object sender, EventArgs e) {
            int id = Convert.ToInt32(txtRunIdToLoad.Text);
            this.DisplayRunResultDetails(new ProblemResult(id));
        }
    }
}
