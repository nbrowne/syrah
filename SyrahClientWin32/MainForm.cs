﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.Client;


namespace SyrahClientWin32 {
    public partial class MainForm : Form {
        protected SyrahClient _syrahClient = null;
        protected Thread _syrahRunThread = null;
        /// <summary>
        /// Default form constructor
        /// </summary>
        public MainForm() {
            InitializeComponent();

            //Initialize the Syrah client
            InitializeSyrah();
        }


        /// <summary>
        /// Worker process to handle the initialization of the Syrah client object
        /// </summary>
        private void InitializeSyrah() {            
            _syrahClient = new SyrahClient(new MessageHandlerDelegate(AppendToLog));

            UpdateStatus("Initialized");

        }


        private void StartSyrah() {
            try {
                this.UpdateStatus("Starting Syrah Client...");

                _syrahRunThread = new Thread(new ThreadStart(_syrahClient.Run));
                _syrahRunThread.Start();

                this.UpdateStatus("Syrah Client is running...");

            } catch (Exception err) {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Worker process to handle the shutdown of the Syrah client object
        /// </summary>
        private void ShutdownSyrah() {
            _syrahClient.Stop();
            this.UpdateStatus("Stopping Syrah Client...");  
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        private void AppendToLog(LogDetailLevel level, string text) {
            //Reformat
            string sBuffer = DateTime.Now.ToString("MMM dd - HH:mm:ss") + "> " + text;

            //Check if the form is not disposed before we attempt to update the logger. This is
            //nessessary because of the multithreading of the logger.
            if (!this.lstResponseLog.IsDisposed ) {
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (this.lstResponseLog.InvokeRequired) {
                    MessageHandlerDelegate delHandler = new MessageHandlerDelegate(AppendToLog);
                    this.lstResponseLog.Invoke(delHandler, new object[] { level, text });

                } else {
                    //Add to the log
                    lstResponseLog.Items.Add(sBuffer);

                    //Scroll
                    lstResponseLog.SelectedIndex = lstResponseLog.Items.Count - 1;

                }
            } //End of isDisposed check
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        private void UpdateStatus(string text) {
            statusLabel.Text = text;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestForm_FormClosing(object sender, FormClosingEventArgs e) {
            //Notify that we're shutting down
            AppendToLog(LogDetailLevel.Detail, "Shutting down the Syrah Client...");
            UpdateStatus("Starting to shutdown client.");

            //Pass the syrah shutdown to the worker
            this.ShutdownSyrah();

            //Give the thread a chance to stop normally
            for (int i = 0; i < 3 * 10; i++) {
                if (!_syrahClient.IsRunning) break;

                System.Threading.Thread.Sleep(100);
            }

            _syrahRunThread.Abort();
        }

        private void TestForm_Load(object sender, EventArgs e) {
            this.StartSyrah();
        }

        private void lstResponseLog_DoubleClick(object sender, EventArgs e) {
            MessageBox.Show((string)lstResponseLog.SelectedItem, "Syrah Master Response", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
