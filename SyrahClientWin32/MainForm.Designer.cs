﻿namespace SyrahClientWin32 {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.statusTimer = new System.Windows.Forms.Timer(this.components);
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lstResponseLog = new System.Windows.Forms.ListBox();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusTimer
            // 
            this.statusTimer.Interval = 500;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 514);
            this.statusBar.Name = "statusBar";
            this.statusBar.ShowItemToolTips = true;
            this.statusBar.Size = new System.Drawing.Size(1172, 22);
            this.statusBar.TabIndex = 9;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(1157, 17);
            this.statusLabel.Spring = true;
            this.statusLabel.Text = "Starting...";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lstResponseLog
            // 
            this.lstResponseLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstResponseLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lstResponseLog.FormattingEnabled = true;
            this.lstResponseLog.ItemHeight = 15;
            this.lstResponseLog.Location = new System.Drawing.Point(0, 0);
            this.lstResponseLog.Margin = new System.Windows.Forms.Padding(4);
            this.lstResponseLog.Name = "lstResponseLog";
            this.lstResponseLog.Size = new System.Drawing.Size(1172, 514);
            this.lstResponseLog.TabIndex = 10;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 536);
            this.Controls.Add(this.lstResponseLog);
            this.Controls.Add(this.statusBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(650, 300);
            this.Name = "TestForm";
            this.Text = "Syrah Client";
            this.Load += new System.EventHandler(this.TestForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestForm_FormClosing);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer statusTimer;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ListBox lstResponseLog;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;

    }
}

