﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Net;

using NigelBrowne.Syrah.Common.Exceptions;

namespace NigelBrowne.Syrah.Common.Comm {
    /// <summary>
    /// The communication server components of Syrah, Nigel Browne's Masters Thesis project.
    /// </summary>
    /// <remarks>This class implements an HTTP server over SSL to service the Syrah client requests.</remarks>
    public class CommServer {
        protected HttpListener _serverListener;
        protected bool _useSsl = false;
        protected int _portNumber = 8080;

        protected MessageHandlerDelegate MessageHandler;
        protected SyrahClientRequestHandler ClientRequestHandler;

        /// <summary>
        /// Default constructor
        /// </summary>
        public CommServer(MessageHandlerDelegate msgHandler, SyrahClientRequestHandler requestHandler, int portNumber, bool useSsl) {
            if (!HttpListener.IsSupported) {
                throw new SyrahGeneralException("Windows XP SP2 or Server 2003 is required to use the Syrah CommServer class.");
            }

            //Set the member vars
            _useSsl = useSsl;
            _portNumber = portNumber;
            MessageHandler = msgHandler;
            ClientRequestHandler = requestHandler;

            //Instantiate the server listener
            _serverListener = new HttpListener();

            //Set the listern prefix based on if we're using SSL or not
            if (!_useSsl) {
                _serverListener.Prefixes.Add("http://+:"+ _portNumber.ToString() +"/");
            } else {
                //This will cause an error in the HttpWebRequest query if a SSL certificate isn't 
                //set for the listener. The error will appear as an expection connection close.
                _serverListener.Prefixes.Add("https://+:"+ _portNumber.ToString() +"/");                
            }

        }


        /// <summary>
        /// Start the CommServer component. The server will start listening for and handling client requests.
        /// </summary>
        public void Start() {
            if (!_serverListener.IsListening) {
                MessageHandler(LogDetailLevel.Information, "Syrah CommServer starting to listen on port " + _portNumber.ToString() + ".");
                _serverListener.Start();

                //Initiate the async callback handler
                IAsyncResult result = _serverListener.BeginGetContext(new AsyncCallback(ListenerCallback), _serverListener);
            }
        }


        /// <summary>
        /// Stop the CommServer component. The server will no longer listen for client requests.
        /// </summary>
        public void Stop() {
            if (_serverListener.IsListening) {                
                _serverListener.Close();
                MessageHandler(LogDetailLevel.Information, "Syrah CommServer stopped listening on port " + _portNumber.ToString() + ".");
            }
        }


        /// <summary>
        /// Restart the CommServer component
        /// </summary>
        public void Restart() {
            if (_serverListener.IsListening) {
                _serverListener.Stop();
            }

            //Start the server
            _serverListener.Start();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        protected void ListenerCallback(IAsyncResult result) {
            HttpListener listener = (HttpListener)result.AsyncState;

            if (listener.IsListening) {
                // Call EndGetContext to complete the asynchronous operation.
                HttpListenerContext context = listener.EndGetContext(result);

                // *** Immediately set up the next context, not the cleanest way, but it'll work for now.
                ///todo: Investigate replacing this with a Thread Pool
                listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);

                //Hand off for processing
                ProcessListenerRequest(context);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected void ProcessListenerRequest(HttpListenerContext context) {
            //Notify that we've received a request
            MessageHandler(LogDetailLevel.Detail, 
                String.Format("Received request from {0} for \"{1}\". ", 
                context.Request.RemoteEndPoint.Address.ToString(), context.Request.RawUrl)
                );

            //Process the url
            string[] urlParts = context.Request.RawUrl.ToLower().Trim().Trim('/').Split('/');

            if (urlParts.Length > 0) {
                switch (urlParts[0]) {
                    case "syrah":
                        this.HandleSyrahClientRequest(context);
                        break;

                    case "status":
                        if (urlParts.Length == 1)
                            SendWebPage(context, "Status", GetStatusWebPage());
                        else
                            Send404(context);
                        break;

                    case "":
                        SendWebPage(context, "Home", GetHomeWebPage());
                        break;

                    default:
                        Send404(context);
                        break;
                }

            } else {
                SendWebPage(context, "Home", GetHomeWebPage());

            }

        }


        #region Webpage processing and sending
        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        protected void Send404(HttpListenerContext context) {
            context.Response.StatusCode = 404;
            context.Response.StatusDescription = "Resource " + context.Request.Url + " not found.";
            SendWebPage(context, "404", "Resource &quot;" + context.Request.Url + "&quot; does not exist or is not available.");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="output"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        protected void SendWebPage(HttpListenerContext context, string title, string content) {
            //Build the webpage to send
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(ApplyWebPageTemplate(title, content));

            //Get the output stream to use
            System.IO.Stream output = context.Response.OutputStream;

            // Get a response stream and write the response to it.
            context.Response.ContentLength64 = buffer.Length;

            //System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);

            // Close the output stream
            output.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        protected string ApplyWebPageTemplate(string title, string content) {
            return @"
                <!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                <html xmlns=""http://www.w3.org/1999/xhtml"" lang=""en"">

                    <head>
                        <title>Syrah Evolver Master Node - "+ title +@"</title>
                        <style>
                            body {
                                background: #369;
                                margin: 0;
                                padding: 0;
                            }
                            #wrapper {
                                width: 750px;
                                background: #fff;
                                border: 1px solid #000;
                                margin: 15px auto;
                                padding: 10px 15px 30px 15px;
                            }
                            .formTable th {
                                text-align: right;
                            }
                            .PageTitle {
                                border-bottom: 1px solid #000;
                            }
                        </style>
                    </head>
                    <body>
                        <div id=""wrapper"">
                            <h1 class=""PageTitle"">Syrah Evolver: "+ title +@"</h1>

                            "+ content +@"
                        </div>
                    </body>    
                </html>
            ";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string GetHomeWebPage() {
            return @"
                <p>
                    The Syrah Evolver is the research package developer by Nigel Browne
                    to facilitate the research for his Masters Thesis. Please direct questions
                    to nbrowne (at) acm.org.
                </p>
                <p>
                    If you have reached this page and do not know why, you are probably in the wrong place.
                </p>
            ";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string GetStatusWebPage() {
            return @"
                <table class=""formTable"">
                    <tr><th>Port: </th><td>" + _portNumber.ToString() +@"</td></tr>
                    <tr><th>SSL Enabled: </th><td>" + (_useSsl ? "Yes" : "No") + @"</td></tr>
                    <tr><th>Listening: </th><td>" + (_serverListener.IsListening ? "Yes" : "No") +@"</td></tr>
                </table>
            ";
        }
        #endregion

        #region Syrah Request Handling
        protected void HandleSyrahClientRequest(HttpListenerContext context) {
            //Get the response and request objects from the content
            HttpListenerResponse response = context.Response;
            HttpListenerRequest request = context.Request;

            //Get the request command and paramters 
            SyrahRequest syrahRequest = Utilities.UnpackFromReceive<SyrahRequest>(request.InputStream);
            syrahRequest.PayloadSize = request.ContentLength64;

            //Hand off the command processing to the callback
            SyrahResponse syrahResponse = ClientRequestHandler(syrahRequest);

            //Set the http status to ok
            response.StatusCode = 200;

            //Set the response content type
            response.ContentType = "application/syrah";

            //Get the output stream to use
            System.IO.Stream output = context.Response.OutputStream;

            //Pack the response object into the output stream
            Utilities.PackForSend(response.OutputStream, syrahResponse);

            //Set the length of the response
            //response.ContentLength64 = output.Length;

            response.Close();
        }
        #endregion
    }
}
