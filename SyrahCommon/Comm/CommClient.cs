﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace NigelBrowne.Syrah.Common.Comm {
    /// <summary>
    /// The communication client components of Syrah, Nigel Browne's Masters Thesis project.
    /// </summary>
    /// <remarks>This class allows the client components of Syrah to communicate with the server using a common framework and command set.</remarks>
    public class CommClient {
        protected string _serverAddress = "";
        protected int _serverPort = 8080;
        protected string _clientId;
        protected string _clientVersion = "";

        /// <summary>
        /// Get or set the client version
        /// </summary>
        public string ClientVersion {
            get { return _clientVersion; }
            set { _clientVersion = value; }
        }

        public string ClientId {
            get { return _clientId; }
            set { _clientId = value; }
        }
        public string ServerAddress {
            get { return _serverAddress; }
            set { _serverAddress = value; }
        }

        public int ServerPort {
            get { return _serverPort; }
            set { _serverPort = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="serverAddress"></param>
        /// <param name="serverPort"></param>
        public CommClient(string clientId, string clientVersion, string serverAddress, int serverPort) {
            _clientVersion = clientVersion;
            _clientId = clientId;
            _serverAddress = serverAddress;
            _serverPort = serverPort;
        }

        /// <summary>
        /// Synchronously sends a SyrahRequest to the server and waits for a SyrahResponse object, or fails.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SyrahResponse Send(SyrahRequest request) {
            string serverPath = "http://" + _serverAddress +":"+ _serverPort.ToString() +"/syrah/";

            //Add standard payload to the request
            request.Source = _clientId;
            request.ClientVersion = _clientVersion;
            request.Add("ClientIp", this.GetLocalIp());

            //Instantiate a new webclient request
            WebClient client = new WebClient();
            
            //Pack the request
            byte[] packedRequest = Utilities.PackObject(request);

            //Upload the packed request to the server
            byte[] packedResponse = client.UploadData(serverPath, packedRequest);

            //Unpack the response
            SyrahResponse response = Utilities.UnpackObject<SyrahResponse>(packedResponse);

            //Return the response
            return response;
        }
      

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string GetLocalIp() {
            string localIp = "";

            // Then using host name, get the IP address list..
            IPAddress[] ipAddresses = Dns.GetHostAddresses(Dns.GetHostName());
            
            if (ipAddresses.Length > 0) localIp = ipAddresses[0].ToString();

            return localIp;
        }
    }
}
