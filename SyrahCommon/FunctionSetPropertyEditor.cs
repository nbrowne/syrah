﻿using System;
using System.Collections.Generic;

using System.Text;
using System.ComponentModel;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common {
    public class FunctionSetPropertyEditor : System.Drawing.Design.UITypeEditor {

        private IWindowsFormsEditorService _wfes;
        private ListBox _listBox = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context) {
            //Tell the editor we're a dropdown editor
            return System.Drawing.Design.UITypeEditorEditStyle.DropDown;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, System.IServiceProvider provider, object value) {
            _wfes = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            CodonCollection returnCollection = (CodonCollection)value;

            if (_wfes != null) {
                //Create the control that will actually do the editing. This can be any standard Windows.Forms control or
                //a custom made user control
                _listBox = new ListBox();
                _listBox.SelectionMode = SelectionMode.MultiExtended;
                _listBox.Dock = DockStyle.Fill;

                //Here we attch an event handler to the list box to close the combo box containing
                //it when an item is selected
                _listBox.SelectedIndexChanged += new EventHandler(listBox_SelectedIndexChanged);

                //Add the items 
                CodonCollection selectedFunctions = (CodonCollection)value;
                CodonCollection availableFunctions = FunctionManager.GetAvailableFunctions();
                for (int i=0; i< availableFunctions.Length; i++) {
                    _listBox.Items.Add(availableFunctions[i]);
                    _listBox.SetSelected(i, selectedFunctions.Contains(availableFunctions[i]));
                }

                //Tell the propertygrid to show the listbox, this method is blocked until the editing is done
                _wfes.DropDownControl(_listBox);

                //Editing is now done, so update the collection and return it, which will set the grid value.
                returnCollection = new CodonCollection();
                foreach (int index in _listBox.SelectedIndices) {
                    returnCollection.Add((Codon)_listBox.Items[index]);
                }
            }

            return returnCollection;    
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e) {
            switch (_listBox.SelectionMode) {
                case SelectionMode.MultiExtended:
                case SelectionMode.MultiSimple:
                    break;
                default:
                    _wfes.CloseDropDown();
                    break;
            }
        }

    }
}

