﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace NigelBrowne.Syrah.Common.Client {
    [Serializable]
    public class LocalClientConfiguration {       
        public string ClientId;
        public string ServerAddress;
        public int ServerPort;


        /// <summary>
        /// 
        /// </summary>
        public LocalClientConfiguration() {
            ClientId = this.GenerateClientId();
            ServerAddress = "127.0.0.1";
            ServerPort = 8080;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string GenerateClientId() {
            return Dns.GetHostName();
        }
    }


    [Serializable] 
    public class ClientConfiguration {
        #region Member Variables
        protected string _clientId;
        protected int _islands;
        #endregion

        #region Properties
        public string ClientId { get { return _clientId; } }
        public int Islands { get { return _islands; } }
        #endregion


        //Load client configuration settings from the client data abstraction object
        public ClientConfiguration(Syrah.Common.Data.Client client) {
            _clientId = client.Name;
            _islands = client.Islands;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClientConfiguration() {
            _clientId = "";
            _islands = 1;
        }
    }
}
