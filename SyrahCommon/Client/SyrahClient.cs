﻿#define DONT_CATCH_ERRORS

using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.Client {
    public class SyrahClient {
        public const int SLEEPTIME_AFTER_ERROR = 60;
        protected string CLIENT_VERSION = "0.0.2";

        #region Fields
        protected bool _isRunning = false;
        
        protected ClientConfiguration _clientConfig = null;
        protected LocalClientConfiguration _localClientConfig = null;

        protected Common.Comm.CommClient _commManager;
        protected List<Common.EvolutionaryAlgorithms.EvolutionaryAlgorithm> _islandList;

        //Delegates
        protected MessageHandlerDelegate MessageHandler;
        #endregion

        #region Properties
        public bool IsRunning { get { return _isRunning; } }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        public SyrahClient(MessageHandlerDelegate msgHandler) {
            this.MessageHandler = msgHandler;

            //Notify that we're starting the initialization process.
            MessageHandler(LogDetailLevel.Detail, "Initializing Syrah client.");

            //Load the local settings
            _localClientConfig = Utilities.LoadClientConfiguration();

            _commManager = new Common.Comm.CommClient(_localClientConfig.ClientId, CLIENT_VERSION, _localClientConfig.ServerAddress, _localClientConfig.ServerPort);
            _islandList = new List<Common.EvolutionaryAlgorithms.EvolutionaryAlgorithm>();

            //Send notification that we're done
            MessageHandler(LogDetailLevel.Detail, "Initialized Syrah client, client id is '"+ _localClientConfig.ClientId +"'");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="problemId"></param>
        /// <param name="testCases"></param>
        public void TestCaseSampleRequestHandler(out List<TestCase> testCases) {
            testCases = new List<TestCase>();
            
            //Build the request
            SyrahRequest request = new SyrahRequest(_localClientConfig.ClientId, CommCommands.ClientSampleRequest);

            //Send the request and wait for the response
            SyrahResponse response = _commManager.Send(request);

            //If the server successfully responded, load the client settings
            if (response.Status == CommCommandStatus.Success) {
                testCases = (List<TestCase>)response["TestCases"];
                MessageHandler(LogDetailLevel.Information, "Received new test case sample from the server.");

            } else {
                MessageHandler(LogDetailLevel.Error, "Failure while requesting new test cases from the server. Error on the server was: " + response.Message);

            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="problemId"></param>
        /// <param name="emigrants"></param>
        /// <param name="immigrants"></param>
        public void MigrationRequestHandler(List<Chromosome> emigrants, out List<Chromosome> immigrants) {
            immigrants = new List<Chromosome>();

            //Build the request
            SyrahRequest request = new SyrahRequest(_localClientConfig.ClientId, CommCommands.ClientMigrationRequest);

            //Add the list of individuals that are being sent to the server
            request.Add("Emigrants", emigrants);

            //Send the request and wait for the response
            SyrahResponse response = _commManager.Send(request);

            //If the server successfully responded, load the client settings
            if (response.Status == CommCommandStatus.Success) {
                immigrants = (List<Chromosome>)response["Immigrants"];
                MessageHandler(LogDetailLevel.Information, "Received immigrants from the server.");

            } else {
                MessageHandler(LogDetailLevel.Error, "Failure while performing migration. Error on the server was: " + response.Message);

            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void Run() {
            if (!_isRunning) {
#if (!DONT_CATCH_ERRORS)
                try {
#endif
                    MessageHandler(LogDetailLevel.Information, "Starting Syrah client.");
                    _isRunning = true;                    

                    //Hand off the running
                    this.DoRun();

                    _isRunning = false;
#if (!DONT_CATCH_ERRORS)
                } catch (Exception err) {
                    _isRunning = false;
                    MessageHandler(LogDetailLevel.Error, "An error occured while running the Syrah Client in '" + err.Source + "'. The error was: " + err.Message);
                } 
#endif
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected void RequestSettingsAndUpdateClientInformation() {
            //Instantiate a new hardware interrogator object and query the hardware
            HardwareInterrogator clientHardware = new HardwareInterrogator();

            //Build the request
            SyrahRequest request = new SyrahRequest(_localClientConfig.ClientId, CommCommands.ClientSettingsRequest);
            request.Add("ClientHardware", clientHardware);

            //Send the request and wait for the response
            SyrahResponse response = _commManager.Send(request);

            //If the server successfully responded, load the client settings
            if (response.Status == CommCommandStatus.Success) {
                _clientConfig = (ClientConfiguration)response["ClientConfiguration"];
                MessageHandler(LogDetailLevel.Information, "Received client settings from server.");

            } else {
                MessageHandler(LogDetailLevel.Error, "Failure while requesting client settings from the server. Error on the server was: " + response.Message);

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop() {
            //Only attempt to stop if the system is running
            if (_isRunning) {
                MessageHandler(LogDetailLevel.Information, "Stopping Syrah client.");
                _isRunning = false;

                //Save the settings
                //Utilities.SaveClientConfiguration(_localClientConfig);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected void DoRun() {
            bool shutdownClientFlag = false;

            //Iterate and process
            while (_isRunning && !shutdownClientFlag) {
#if (!DONT_CATCH_ERRORS)
                try {
#endif
                    //IF we don't have settings yet, hand off the settings request to the client worker
                    if (_clientConfig == null) {
                        this.RequestSettingsAndUpdateClientInformation();

                    } else {
                        //We have our settings, so send that the client is available
                        SyrahResponse response = _commManager.Send(new SyrahRequest(_localClientConfig.ClientId, CommCommands.ClientAvailable));

                        if (response.Status == CommCommandStatus.Success) {
                            //Handle the response
                            switch (response.Command) {
                                case CommCommands.Run:
                                    this.RunHandler(response);
                                    break;

                                case CommCommands.Wait:
                                    MessageHandler(LogDetailLevel.Information, "Client was told to wait.");

                                    //wait the thread for 5 minutes. The thread periodically wakes up and processes shutdown requestes, etc
                                    //while waiting
                                    //todo: Client doesn't seem to wait
                                    this.DoWait(1 * 60);
                                    break;

                                case CommCommands.ShutdownClient:
                                    MessageHandler(LogDetailLevel.Information, "Client was told to shutdown.");
                                    shutdownClientFlag = true;
                                    this.Stop();
                                    break;
                            }

                        } else {
                            MessageHandler(LogDetailLevel.Error, "Failure while sending available status to server. Error on the server was: " + response.Message);
                            this.DoWait(SLEEPTIME_AFTER_ERROR);

                        }
                    }
#if (!DONT_CATCH_ERRORS)
                } catch (Exception err) {
                    string errorMessage = "";

                    //Override certain messages
                    if (err.Message == "Unable to connect to the remote server") 
                        errorMessage = "Syrah Server at '" + _localClientConfig.ServerAddress + ":" + _localClientConfig.ServerPort.ToString() + " could not be contacted.";

                    //If we haven't overridden the message with a custom one, generate a default.
                    if (errorMessage.Length == 0) {
                        errorMessage = err.Message.Trim();
                        if (errorMessage[errorMessage.Length - 1] != '.') errorMessage += ".";
                        if (err.InnerException != null) errorMessage += " " + err.InnerException.Message;
                        if (errorMessage[errorMessage.Length - 1] != '.') errorMessage += ".";
                    }

                    //errorMessage += " Waiting for " + SLEEPTIME_AFTER_ERROR.ToString() + " seconds before retrying.";

                    MessageHandler(LogDetailLevel.Error, errorMessage);
                    this.DoWait(SLEEPTIME_AFTER_ERROR);

                }
#endif
            } //End of execution loop
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="?"></param>
        protected void RunHandler(SyrahResponse response) {
            EvolutionaryAlgorithms.GEP.GepStatistics results = null;
            int problemId = (int)response["ProblemId"];
            string algorithmType = (string)response["AlgorithmType"];
            int runNumber = 1;
            int foldIndex = response.ContainsKey("FoldIndex") ? Convert.ToInt32(response["FoldIndex"]) : -1;

            //Notify
            MessageHandler(LogDetailLevel.Information, "Client was told to start a run.");

            //Load the run settings
            EvolutionaryAlgorithms.GEP.GepSettings settings = (EvolutionaryAlgorithms.GEP.GepSettings)response["AlgorithmSettings"];
            List<EvolutionaryAlgorithms.GEP.TestCase> testCases = (List<EvolutionaryAlgorithms.GEP.TestCase>)response["TestCases"];

            MessageHandler(LogDetailLevel.Detail, "Received run settings from server.");

            //Instantiate a GEP algorithm object
            EvolutionaryAlgorithms.GEP.GepAlgorithm algorithm = new EvolutionaryAlgorithms.GEP.GepAlgorithm();

            //Configure the GEP Algorithm
            algorithm.TestCaseSampleRequestHandler = this.TestCaseSampleRequestHandler;
            algorithm.MigrationRequestHandler = this.MigrationRequestHandler;

            //Run the algorithm and get the results
            MessageHandler(LogDetailLevel.Detail, "About to call algorithm's Run method for problem #" + problemId.ToString() + ".");
#if (!DONT_CATCH_ERRORS)
            try {
#endif
                results = algorithm.Run(settings, testCases);

#if (!DONT_CATCH_ERRORS)            
            } catch (Exception err) {
                MessageHandler(LogDetailLevel.Error, "EC algorithm run failed at '" + err.Source + "'. The error was '" + err.Message + "'.");
            }
#endif

            //If the run finished and we received results, send them to the server
            if (results != null) {
                MessageHandler(LogDetailLevel.Detail, "Run method completed, preparing to send data to the server.");

                //Build the request (really the response) for the server
                SyrahRequest request = new SyrahRequest(_localClientConfig.ClientId, CommCommands.RunFinished);
                request["ProblemId"] = problemId;
                request["RunNumber"] = runNumber;
                request["RunResults"] = results;
                request["AlgorithmType"] = algorithmType;
                request["FoldIndex"] = foldIndex;

                //Send the statistics back to the server
                MessageHandler(LogDetailLevel.Detail, "About to send run results to the server.");
                SyrahResponse resultsResponse = _commManager.Send(request);

                //Check the status of the response.
                if (resultsResponse.Status == CommCommandStatus.Success) {
                    MessageHandler(LogDetailLevel.Detail, "Server successfully received run results.");

                } else {
                    MessageHandler(LogDetailLevel.Detail, "Server was unable to process or receive the run results.");

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secondsToSleep"></param>
        protected void DoWait(int secondsToSleep) {
            double timeAsleep = 0;

            //We'll sleep for a bit and then process
            int sleepIncrement = 500;

            while (_isRunning && timeAsleep < secondsToSleep) {
                //Sleep for a bit, then unblock the process
                System.Threading.Thread.Sleep(sleepIncrement);

                //Update the time asleep, but convert the milliseconds to seconds.
                timeAsleep += (double)sleepIncrement / 1000D;
            }
        }
    } //End of SyrahClient class
}
