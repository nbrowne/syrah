﻿using System;
using System.Collections.Generic;

using System.Text;

namespace NigelBrowne.Syrah.Common {
    public class About {
        #region MemberVars
        private static string _sAuthor = "Nigel Browne - nbrowne@acm.org";
        private static string _sCompany = "Nigel Browne";
        private static string _sPackageTitle = "Syrah Evolver";
        private static string _sClientTitle = "Syrah Client";
        private static string _sMasterTitle = "Syrah Master";
        private static string _sCopyright = "Copyright ©  2008 - Nigel Browne. All rights reserved.";
        private static string _sClientDescription = @"
            Client application for the Syrah Distributed Evolver package.
        ";

        private static string _sMasterDescription = @"
            Master control application for the Syrah Distributed Evolver package.
        ";
        
        #endregion

        #region Properties
        public static string ClientDescription { get { return CleanText(_sClientDescription); } }
        public static string MasterDescription { get { return CleanText(_sMasterDescription); } }
        public static string Author { get { return CleanText(_sAuthor); } }
        public static string ClientTitle { get { return CleanText(_sClientTitle); } }
        public static string MasterTitle { get { return CleanText(_sMasterTitle); } }
        public static string Copyright { get { return CleanText(_sCopyright); } }
        public static string Company { get { return CleanText(_sCompany); } }
        public static string PackageTitle { get { return CleanText(_sPackageTitle); } }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        ///todo: Change this method to use RegEx to clean the strings.
        /// </remarks>
        /// <param name="text"></param>
        /// <returns></returns>
        private static string CleanText(string text) {            
            string sBuffer = "";
            
            //Split on each line and clean it. 
            foreach (string sLine in text.Split('\n')) {
                sBuffer += sLine.Trim();
            }

            return sBuffer;
        }
    }
}

