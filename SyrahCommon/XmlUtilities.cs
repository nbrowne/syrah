﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Reflection;
using System.CodeDom;
using System.ComponentModel;

namespace NigelBrowne.Syrah.Common {
    public class XmlUtilities {
        #region Specific Xml Serializing methods
        public static T DeserializeObject<T>(XmlReader reader) {
            //Instantiate a xml serializer for the deserialization process and the textreader to read the file.
            XmlSerializer xsSerializer = new XmlSerializer(typeof(T));

            //Deserialize the configuration object from the file
            T obj = (T)xsSerializer.Deserialize(reader);

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectToPack"></param>
        /// <returns></returns>
        public static void SerializeObject<T>(XmlWriter writer, object obj) {
            //Instantiate a xml serializer for the serialization process and the string writer to hold the xml
            XmlSerializer xsSerializer = new XmlSerializer(typeof(T));

            //Serial the object
            xsSerializer.Serialize(writer, obj);
        }


        public static void ListDictionaryXmlSeralizer<TKey, TValue>(XmlWriter writer, string name, List<Dictionary<TKey, TValue>> data) {
            //Write the start element
            writer.WriteStartElement(name);

            //Iteratve over the list items
            foreach (Dictionary<TKey, TValue> dictionaryItem in data) {
                XmlUtilities.DictionaryXmlSeralizer<TKey, TValue>(writer, "dictionary", dictionaryItem);
            }

            //End the current terminal tag
            writer.WriteEndElement();
        }


        /// <summary>
        /// Serializes a List<Dictionary<key, value> to an xml writer.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public static void DictionaryXmlSeralizer<TKey, TValue>(XmlWriter writer, string name, Dictionary<TKey, TValue> data) {
            //Write the start element
            writer.WriteStartElement(name);

            foreach(TKey keyValue in data.Keys) {
                //Start a new terminal tag
                writer.WriteStartElement("item");

                //Add the terminal codon as an attribute
                writer.WriteAttributeString("key", keyValue.ToString());

                //Write out the value
                writer.WriteValue(data[keyValue]);

                //End the current terminal tag
                writer.WriteEndElement();
            }

            //End the current terminal tag
            writer.WriteEndElement();
        }


        /// <summary>
        /// Serializes a List<> to an xml writer.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public static void ListXmlSeralizer<TValue>(XmlWriter writer, string name, List<TValue> data) {
            //Write the start element
            writer.WriteStartElement(name);

            foreach (TValue value in data) {
                //Start a new terminal tag
                writer.WriteStartElement("item");

                //Write out the value
                writer.WriteValue(value);

                //End the current terminal tag
                writer.WriteEndElement();
            }

            //End the current terminal tag
            writer.WriteEndElement();
        }

        /// <summary>
        /// Serializes a List<> to an xml writer.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public static void ListListXmlSeralizer<TValue>(XmlWriter writer, string name, List<List<TValue>> data) {
            //Write the start element
            writer.WriteStartElement(name);

            foreach (List<TValue> sublist in data) {
                writer.WriteStartElement("list");

                foreach (TValue value in sublist) {
                    //Start a new terminal tag
                    writer.WriteStartElement("item");

                    //Write out the value
                    writer.WriteValue(value);

                    //End the current terminal tag
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }

            //End the current terminal tag
            writer.WriteEndElement();
        }
        #endregion

        #region Generic xml serializing methods
        public static T GenericXmlDeserializer<T>(XmlReader reader) {
            T obj = default(T);

            return obj;
        }

        public static void GenericXmlSerializer(XmlWriter writer, object obj) {
            XmlUtilities.GenericXmlSerializer(writer, null, obj);
        }
        public static void GenericXmlSerializer(XmlWriter writer, string name, object obj) {
            Type objType = obj.GetType();

            string typeName = objType.Name.ToLower();
            if (typeName.IndexOf('`') > 0) typeName = typeName.Substring(0, typeName.IndexOf('`'));
            
            string elementName = name != null ? name : typeName;

            //Create the start tag
            writer.WriteStartElement(elementName);

            //Iterate or output
            if (objType.GetInterface("IEnumerable") != null || objType.GetInterface("IEnumerable`1")!=null) {
                //Add attributes to help with extraction

                MethodInfo method = objType.GetMethod("GetEnumerator");
                object objEnumerator = method.Invoke(obj, null);                
                
                while (!(bool)objEnumerator.GetType().GetMethod("MoveNext").Invoke(objEnumerator, null)) {
                    object item = objEnumerator.GetType().GetProperty("Current").GetValue(objEnumerator, null);
                    
                    GenericXmlSerializer(writer, item);
                }

            } else {
                //Serial the object
                XmlSerializer xsSerializer = new XmlSerializer(objType);
                xsSerializer.Serialize(writer, obj);
            }

            writer.WriteEndElement();
            
            
        } //End of generic class handling        
        #endregion
    }
}
