﻿using System;
using System.Collections.Generic;

using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace NigelBrowne.Syrah.Common {
    public class Utilities {
        //Seed number from http://www.c-sharpcorner.com/UploadFile/rmcochran/random07172006175425PM/random.aspx
        static Random _oRandom = new Random(Convert.ToInt32(DateTime.Now.Ticks & 0x000000007FFFFFFF));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="config"></param>
        public static void SaveClientConfiguration(Client.LocalClientConfiguration config) {
            SaveObjectToXmlFile<Client.LocalClientConfiguration>("SyrahClient.xml", config);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Client.LocalClientConfiguration LoadClientConfiguration() {
            return LoadObjectFromXmlFile<Client.LocalClientConfiguration>("SyrahClient.xml");
        }


        /// <summary>
        /// Returns the smaller of two values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        public static int GetSmallerValue(int value1, int value2) {
            return value1 <= value2 ? value1 : value2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        public static void SaveObjectToXmlFile<T>(string path, T obj) {
            //Instantiate a xml serializer for the serialization process and the textwriter to write the file.
            XmlSerializer xsSerializer = new XmlSerializer(typeof(T));
            TextWriter oWriter = new StreamWriter(path);

            //Serial the object
            xsSerializer.Serialize(oWriter, obj);

            //Flush and close the writer object to ensure the data gets writen and the clean up is done.
            oWriter.Flush();
            oWriter.Close();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T LoadObjectFromXmlFile<T>(string path) {
            T obj = default(T);

            //If a config file exists, then load the object, otherwise create a new one
            if (File.Exists(path)) {
                //Instantiate a xml serializer for the deserialization process and the textreader to read the file.
                XmlSerializer xsSerializer = new XmlSerializer(typeof(T));
                TextReader oReader = new StreamReader(path);

                //Deserialize the configuration object from the file
                obj = (T)xsSerializer.Deserialize(oReader);

                //Close the reader object to clean up
                oReader.Close();
            } else {
                throw new FileNotFoundException("Couldn't locate '" + path + "'. ");
            }

            return obj;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectToPack"></param>
        /// <returns></returns>
        public static String PackObjectXml<T>(object obj) {
            //Instantiate a xml serializer for the serialization process and the string writer to hold the xml
            XmlSerializer xsSerializer = new XmlSerializer(typeof(T));
            StringWriter writer = new StringWriter();

            //Serial the object
            xsSerializer.Serialize(writer, obj);

            //Get the xml to return
            string returnXml = writer.ToString();

            //Close the writer
            writer.Close();

            //Flush and close the writer object to ensure the data gets writen and the clean up is done.
            return returnXml;
        }

        public static T UnpackObjectXml<T>(string xmlSource) {
            //Instantiate a xml serializer for the deserialization process and the textreader to read the file.
            XmlSerializer xsSerializer = new XmlSerializer(typeof(T));
            StringReader oReader = new StringReader(xmlSource);

            //Deserialize the configuration object from the file
            T obj = (T)xsSerializer.Deserialize(oReader);

            //Close the reader object to clean up
            oReader.Close();

            return obj;
        }

        /// <summary>
        /// Packs an object into a byte array using a binary serialization formatter.
        /// </summary>
        /// <param name="objectToPack"></param>
        /// <returns></returns>
        public static byte[] PackObject(object objectToPack) {
            //Instantiate a new stream object
            MemoryStream stream = new MemoryStream();

            //Call the packing method and pack the data into the stream
            PackForSend(stream, objectToPack);

            //Return the byte array representing the packed object
            return stream.ToArray();
        }

        /// <summary>
        /// Unpacks a serialized binary object to its object representation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T UnpackObject<T>(byte[] source) {
            MemoryStream stream = new MemoryStream(source);
            return UnpackFromReceive<T>(stream);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializationStream"></param>
        /// <param name="obj"></param>
        public static void PackForSend(Stream serializationStream, object objectToPack) {
            BinaryFormatter formatter = new BinaryFormatter();            
            formatter.Serialize(serializationStream, objectToPack);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializationStream"></param>
        /// <returns></returns>
        public static T UnpackFromReceive<T>(Stream serializationStream) {            
            BinaryFormatter formatter = new BinaryFormatter();
            return (T)formatter.Deserialize(serializationStream);
        }

        /// <summary>
        /// Returns a random integer between lowerLimit and upperLimit (including the limits).
        /// </summary>
        /// <param name="lowerLimit"></param>
        /// <param name="upperLimit"></param>
        /// <returns></returns>
        public static int RandomInteger(int lowerLimit, int upperLimit) {
            //The Next method exludes the upperlimit, so we need to add one to it
            return _oRandom.Next(lowerLimit, upperLimit + 1);
        }


        /// <summary>
        /// Return a random probability (double between 0 and 1, inclusive)
        /// </summary>
        /// <returns></returns>
        public static double RandomProbability() {
            return _oRandom.NextDouble();
        }


        /// <summary>
        /// Generates a random double between lowerLimit and upperLimit inclusively (That is, 
        /// lower and upper are include in the possible range).
        /// </summary>
        /// <param name="lowerLimit"></param>
        /// <param name="upperLimit"></param>
        /// <returns>A random double between [lowerLimit, upperLimit]</returns>
        public static double RandomDoubleRange(double lowerLimit, double upperLimit) {
            return lowerLimit + _oRandom.NextDouble() * (upperLimit - lowerLimit);
        }


        /// <summary>
        /// Toss a coin, that is, the method will return a true or false boolean value with equal probability.
        /// </summary>
        /// <returns>A true or false boolean value with equal probability.</returns>
        public static bool CoinToss() {
            return RandomProbability() >= 0.5;
        }


        /// <summary>
        /// Returns a byte array from a string.
        /// </summary>
        /// <param name="buffer">The string object to convert</param>
        /// <returns>A byte array of the string.</returns>
        public static byte[] StringToByteArray(string buffer) {
            return Encoding.ASCII.GetBytes(buffer);
        }
    }
}
