﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.Comm;
using NigelBrowne.Syrah.Common.Data;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.Master {
    public class SyrahMaster {
        protected static string CONFIGURATION_FILE = "MasterConfiguration.xml";

        protected static int TotalNumberOfFolds = 10;
        protected static int NumberOfRunsPerFold = 30;
        protected bool _kFoldEnabled = false;

        protected int _activeProblemId = -1;
        protected int _currentFoldIndex = 1;
        protected int _runsInCurrentFold = 0;

        protected List<List<TestCase>> _foldTestCases = new List<List<TestCase>>();

        protected bool _isRunning = false;
        protected CommServer _commServer = null;
        protected MasterConfiguration _masterConfiguration = null;
        protected int _serverPort = 8080;

        protected static int _runCounter = 0;
        protected static int _maximumNumberOfRuns = 10;
        protected List<Chromosome> _migrationIndividuals = null;

        protected Problem ActiveProblem { get; set; }

        public MasterConfiguration Configuration {
            get { return _masterConfiguration; }
        }

        public bool KFoldEnabled {
            get { 
                return _kFoldEnabled;             
            }
            set {
                _kFoldEnabled = value;

                //Initialize the fold data
                if (_kFoldEnabled) {
                    _currentFoldIndex = 1;
                } else {
                    _currentFoldIndex = -1;
                }
            }
        }

        public int ActiveProblemId {
            get { return _activeProblemId; }
            set { 
                _activeProblemId = value;
                
                //Initialize the fold data
                if (_kFoldEnabled) {
                    _currentFoldIndex = 1;
                } else {
                    _currentFoldIndex = -1;
                }

                _runsInCurrentFold = 0;

                if (value != -1) {
                    this.ActiveProblem = new Problem(_activeProblemId);
                    
                    //Load the test cases for the folds
                    _foldTestCases = this.PartionTestCasesIntoFolds(this.ActiveProblem);

                    MessageHandler(LogDetailLevel.Information, "The active problem was set to '" + _activeProblemId.ToString() + " - '" + this.ActiveProblem.Title + "'.");

                } else {
                    MessageHandler(LogDetailLevel.Information, "The active problem was set to 'None' and processing is currently disabled.");
                }
            }
        }

        //Delegates
        protected MessageHandlerDelegate MessageHandler;
        protected ClientRunCompletedDelegate ClientRunCompletedHandler;
        protected ClientRunStartedDelegate ClientRunStartedHandler;

        /// <summary>
        /// Instantiate a new SyrahMaster object.
        /// </summary>
        /// <param name="msgHandler"></param>
        public SyrahMaster(MessageHandlerDelegate msgHandler, ClientRunStartedDelegate startOfRunHandler, ClientRunCompletedDelegate endOfRunHandler) {
            //Set the message handler callback
            MessageHandler = msgHandler;

            //Set the client run callbacks
            ClientRunCompletedHandler = endOfRunHandler;
            ClientRunStartedHandler = startOfRunHandler;

            //Notify that we're starting the initialization process.
            MessageHandler(LogDetailLevel.Detail, "Initializing Syrah master.");

            //Instantiate the communication server
            _commServer = new CommServer(MessageHandler, new SyrahClientRequestHandler(ClientRequestHandler), _serverPort, false);

            //Load the settings from the master config file
            _masterConfiguration = Utilities.LoadObjectFromXmlFile<MasterConfiguration>(CONFIGURATION_FILE);

            //Initialize the pool for our migration individuals
            _migrationIndividuals = new List<Chromosome>();

            //Send notification that we're done
            MessageHandler(LogDetailLevel.Detail, "Initialized Syrah master.");
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start() {
            if (!_isRunning) {
                MessageHandler(LogDetailLevel.Information, "Starting Syrah master.");
                _commServer.Start();
                _isRunning = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop() {
            //Only attempt to stop if the system is running
            if (_isRunning) {
                MessageHandler(LogDetailLevel.Information, "Stopping Syrah master.");
                _commServer.Stop();
                _isRunning = false;                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Data.Problem> GetProblemList() {
            List<Data.Problem> problemList = null;
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);

            try {
                Data.ProblemCollection problems = new ProblemCollection(dataAccess);
                problemList = problems.GetCollection();

            } finally {
                dataAccess.Close();
            }

            return problemList;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Data.ProblemResult> GetProblemRuns(ProblemResultCollectionFilter filter) {
            List<Data.ProblemResult> problemRuns = null;
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);

            try {
                Data.ProblemResultCollection collection = new ProblemResultCollection(dataAccess);

                problemRuns = collection.GetList(filter);

            } finally {
                dataAccess.Close();
            }

            return problemRuns;
        }


        /// <summary>
        /// Returns a list of the clients the master is aware of.
        /// </summary>
        /// <returns></returns>
        public SortedList<string, Data.Client> GetClientList() {
            SortedList<string, Data.Client> clientList = null;
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);

            try {
                Data.ClientCollection clients = new ClientCollection(dataAccess);
                clientList = clients.GetCollection();

            } finally {
                dataAccess.Close();
            }

            return clientList;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="problemId"></param>
        /// <returns></returns>
        public ProblemResult GetBestProblemResults(int problemId, out int totalRuns) {
            ProblemResult bestResults = null;
            totalRuns = 0;

            DataAccess dataAccess = new DataAccess();
            DataTable data = dataAccess.Query(dataAccess.CleanSql(@"
                select 
                    s.total_runs, 
                    r.* 
                from 
                    problem_results r
					join (select problem_id, count(id) as total_runs from problem_results group by problem_id) s on r.problem_id=s.problem_id
                where 
                    r.problem_id=" + problemId.ToString() + @" and r.best_fitness = 
	                    (select max(best_fitness) from problem_results where problem_id=" + problemId.ToString() + @")						
            "));

            if (data.Rows.Count > 0) {
                bestResults = new ProblemResult(data.Rows[0]);
                totalRuns = (int)data.Rows[0]["total_runs"];
            }

            return bestResults;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public SyrahResponse ClientRequestHandler(SyrahRequest request) {
            SyrahResponse response = new SyrahResponse();
            response.Command = request.Command;

            //Notify that we received a command
            MessageHandler(LogDetailLevel.Detail, 
                String.Format("Received command '{0}' from {1} with payload of {2} bytes.", 
                request.Command, request.Source, request.PayloadSize)
            );
            
            //try {
                switch (request.Command) {
                    case CommCommands.ClientMigrationRequest:
                        this.ClientMigrationRequestHandler(ref request, ref response);
                        break;

                    case CommCommands.ClientSampleRequest:
                        this.ClientSampleRequestHandler(ref request, ref response);
                        break;

                    case CommCommands.ClientSettingsRequest:
                        this.ClientSettingsRequestHandler(ref request, ref response);
                        break;

                    case CommCommands.ClientAvailable:
                        this.ClientAvailableRequestHandler(ref request, ref response);
                        break;

                    case CommCommands.RunFinished:
                        this.ClientRunFinishedHandler(ref request, ref response);
                        break;

                    default:
                        response.Status = CommCommandStatus.Failure;
                        break;
                }

            //} catch (Exception err) {
            //    response.Status = CommCommandStatus.Failure;
            //    response.Message = err.Message;
            //}            

            //Return the results of the command
            return response;
        }

        /// <summary>
        /// Handles a migration request from a client.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        private void ClientMigrationRequestHandler(ref SyrahRequest request, ref SyrahResponse response) {
            //Get the list of indivuduals set by the client
            List<Chromosome> individuals = (List<Chromosome>)request["Emigrants"];
            int numberOfIndividuals = individuals.Count;

            //Lock the list of individuals
            lock (_migrationIndividuals) {
                //Add the emmigrants to our list of individuals
                foreach (Chromosome individual in individuals) {
                    _migrationIndividuals.Add(individual);
                }

                //We only want to remove the and repopulate if we have 2x the number of individuals we need.
                //This means that the first migration request will get it's own chromosomes back. We do this so we
                //don't have to block the evolution.
                if (_migrationIndividuals.Count > individuals.Count * 2) {
                    //Reset the list so we can reuse it
                    individuals.Clear();

                    //Add random individuals until we have enough
                    for (int i = 0; i < numberOfIndividuals; i++) {
                        //Get the chromosome to add
                        int individualIndex = Utilities.RandomInteger(0, _migrationIndividuals.Count - 1);

                        //Add the individual to our list of immigrants
                        individuals.Add(_migrationIndividuals[individualIndex]);

                        //Now remove the individual from the pool of individuals, this ensures that the individuals are unique
                        _migrationIndividuals.RemoveAt(individualIndex);

                    }
                }
            }

            //Build the response
            response.Command = CommCommands.ClientMigrationRequest;
            response.Add("ProblemId", _activeProblemId);
            response.Add("Immigrants", individuals);
            response.Status = CommCommandStatus.Success;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        protected void ClientSampleRequestHandler(ref SyrahRequest request, ref SyrahResponse response) {
            //Open a new connection to the database
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);
            
            try {
                //Get the requested problem from the client
                int currentProblem = _activeProblemId;

                //Query the problem from the database
                Problem problem = new Problem();
                problem.LoadFromDatabase(dataAccess, currentProblem);

                //Get the sample size
                int sampleSize = problem.Settings.StochasticSampleSize;

                //Get the sample subset
                List<TestCase> sampleTestCases = GepAlgorithm.SelectTestCaseSample(problem.TestCases, sampleSize);

                //Set the response for the client to perform a run
                response.Command = CommCommands.ClientSampleRequest;
                response.Add("ProblemId", problem.Id);
                response.Add("TestCases", sampleTestCases);


                //Update the status of the command
                response.Status = CommCommandStatus.Success;

            } finally {
                dataAccess.Close();
            }
        } //End of ClientSampleRequestHandler


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        protected void ClientRunFinishedHandler(ref SyrahRequest request, ref SyrahResponse response) {
            //Open a new connection to the database
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);
            dataAccess.BeginTransaction();

            //try {
                //Get the algorithm type 
                string algorithmType = (string)request["AlgorithmType"];
                Data.ProblemResult runResults = null;

                //Set the default status as failure. 
                response.Status = CommCommandStatus.Failure;

                //Store the results of the run
                switch (algorithmType.ToLower().Trim()) {
                    case "gep":
                        EvolutionaryAlgorithms.GEP.GepStatistics gepResults = (EvolutionaryAlgorithms.GEP.GepStatistics)request["RunResults"];

                        //Instantiate a new run result object
                        runResults = new ProblemResult();
                        runResults.ClientName = request.Source;
                        runResults.ProblemId = (int)request["ProblemId"];
                        int clientFoldIndex = request.ContainsKey("FoldIndex") ? Convert.ToInt32(request["FoldIndex"]) : -1;

                        //todo:Max the run number from the client mean something.
                        runResults.RunNumber = _runCounter; // (int)request["RunNumber"];
                        
                        runResults.RunTime = gepResults.RunTime;
                        runResults.AverageGenerationTime = gepResults.AverageGenerationTime;
                        runResults.MaxGenerationTime = gepResults.MaxGenerationTime;
                        runResults.BestChromosome = gepResults.BestChromosome;
                        runResults.BestFitness = gepResults.BestFitness;
                        runResults.BestFitnessGeneration = gepResults.BestSolutionGeneration;
                        runResults.BestChromosomeText = gepResults.BestChromosome.ToString();
                        runResults.ResultsText = gepResults.ResultsText;
                        runResults.MaxPossibleFitness = gepResults.MaxPossibleFitness;
                        runResults.RunStatistics = gepResults;
                        runResults.FoldIndex = clientFoldIndex;
                        runResults.Save(dataAccess);

                        //Commit the changes
                        dataAccess.CommitTransaction();

                        //Update the status of the command                    
                        response.Status = CommCommandStatus.Success;

                        //Notify that we received the results
                        MessageHandler(LogDetailLevel.Information, "Received run results from client '" + request.Source + "'.");

                        //Fire the client run finished handler
                        ClientRunCompletedHandler(request.Source, runResults);
                        break;

                    default:
                        throw new ArgumentException("Unexpected algorithm type in SyrahMaster.ClientRunFinishedHandler.");
                }
            
            //} catch (Exception err) {
            //    dataAccess.RollbackTransaction();
            //    response.Status = CommCommandStatus.Failure;
            //    response.Message = err.Message;
            //    throw;

            //} finally {
                dataAccess.Close();
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        protected void ClientAvailableRequestHandler(ref SyrahRequest request, ref SyrahResponse response) {
            //Open a new connection to the database
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);
            dataAccess.BeginTransaction();

            try {
                if (this.ActiveProblemId!=-1) {
                    //Assign to a local, just incase _activeProblemId is updated during the method
                    int currentProblem = this.ActiveProblemId;

                    //Query the problem from the database
                    //Problem problem = new Problem();
                    //problem.LoadFromDatabase(dataAccess, currentProblem);
                    Problem problem = this.ActiveProblem;

                    //Get the sample subset
                    List<TestCase> testCases = null;

                    //Select the test cases depending on how the stochastic sample is enabled
                    if (problem.Settings.IsStochasticSamplingEnabled) {
                        throw new NotSupportedException("Support for stochastic sampling has been disabled while k-Fold is in use.");
                        //testCases = GepAlgorithm.SelectTestCaseSample(problem.TestCases, problem.Settings.StochasticSampleSize);

                    } else {
                        if (_kFoldEnabled) {
                            testCases = _foldTestCases[_currentFoldIndex - 1];
                        } else {
                            testCases = problem.TestCases;                            
                        }
                    }                    

                    //Set the response for the client to perform a run
                    response.Command = CommCommands.Run;
                    response.Add("ProblemId", problem.Id);
                    response.Add("AlgorithmType", problem.AlgorithmType);
                    response.Add("AlgorithmSettings", problem.Settings);
                    response.Add("TestCases", testCases);
                    response.Add("FoldIndex", _currentFoldIndex);

                    //Update the client table to indicate what it's doing
                    Data.Client client = new Common.Data.Client();
                    client.LoadFromDatabase(dataAccess, request.Source);
                    client.CurrentProblemId = currentProblem;
                    client.Save(dataAccess);

                    _runCounter++;

                    ///Increment the number of runs inthe current fold, but only if the recieved fold data is still running.
                    if (_kFoldEnabled) {
                        _runsInCurrentFold++;

                        //Check if we need to move to the next fold
                        if (_runsInCurrentFold > NumberOfRunsPerFold) {
                            MessageHandler(LogDetailLevel.Information, "Finished fold " + _currentFoldIndex.ToString() + " of " + TotalNumberOfFolds.ToString() + ".");
                            _currentFoldIndex++;
                            _runsInCurrentFold = 0;
                        }

                        //Check if we have enough folds
                        if (_currentFoldIndex > TotalNumberOfFolds) {
                            this.ActiveProblemId = -1;
                            MessageHandler(LogDetailLevel.Information, "Finished all runs for all folds, terminating.");
                        }
                    }

                    //Fire the run started handler
                    ClientRunStartedHandler(request.Source, problem);

                } else {
                    response.Command = CommCommands.Wait;
                }

                //Update the status of the command
                response.Status = CommCommandStatus.Success;

                //Commit changes
                dataAccess.CommitTransaction();

            } catch {
                dataAccess.RollbackTransaction();
                throw;

            } finally {
                dataAccess.Close();
            }
        } //End of ClientAvailableRequestHandler


        /// <summary>
        /// 
        /// </summary>
        protected void ClientSettingsRequestHandler(ref SyrahRequest request, ref SyrahResponse response) {
            bool isNewClient = false;

            //Open a new connection to the database
            DataAccess dataAccess = new DataAccess(_masterConfiguration.DbConnection);
            dataAccess.BeginTransaction();

            try {
                //Extract the client hardware from the request
                HardwareInterrogator clientHardware = (HardwareInterrogator )request["ClientHardware"];

                //Query the client object from the database
                Data.Client client = new Data.Client(request.Source);

                //If the client doesn't exist in the database, then add it before continuing
                if (!client.LoadedFromDatabase) {
                    client.Name = request.Source;
                    client.Islands = 1;

                    client.LastSeen = DateTime.Now;
                    client.Version = request.ClientVersion;                    
                    client.IpAddress = (String)request["ClientIp"];
                    client.ComputerName = clientHardware.ComputerName;
                    client.OsMemory = clientHardware.TotalMemory;
                    client.OperatingSystem = clientHardware.OperatingSystem;
                    client.ProcessorCount = clientHardware.ProcessorCount;

                    client.ProcessorDetails = "";
                    foreach (ProcessorInformation pi in clientHardware.Processors) {
                        if (client.ProcessorDetails.Length > 0) client.ProcessorDetails += ", ";
                        client.ProcessorDetails = pi.Description + " @ " + pi.ClockSpeed + "MHz";
                    }

                    client.Save();
                    isNewClient = true;
                }

                //Get the client configuration and add it to the response
                Client.ClientConfiguration clientConfig = new Client.ClientConfiguration(client);
                response.Add("ClientConfiguration", clientConfig);

                //If we haven't added a new client above, update the client data
                if (!isNewClient) {
                    //Update the time the client was seen
                    client.LastSeen = DateTime.Now;
                    client.IpAddress = (String)request["ClientIp"];
                    client.LastSeen = DateTime.Now;
                    client.Version = request.ClientVersion;
                    client.IpAddress = (String)request["ClientIp"];
                    client.ComputerName = clientHardware.ComputerName;
                    client.OsMemory = clientHardware.TotalMemory;
                    client.OperatingSystem = clientHardware.OperatingSystem;
                    client.ProcessorCount = clientHardware.ProcessorCount;

                    client.ProcessorDetails = "";
                    foreach (ProcessorInformation pi in clientHardware.Processors) {
                        if (client.ProcessorDetails.Length > 0) client.ProcessorDetails += ", ";
                        client.ProcessorDetails = pi.Description + " @ " + pi.ClockSpeed + " MHz";
                    }
                    client.Save(dataAccess);
                }

                //Commit the database changes
                dataAccess.CommitTransaction();

                //Update the status of the command
                response.Status = CommCommandStatus.Success;

            } catch {
                dataAccess.RollbackTransaction();
                throw;

            } finally {
                dataAccess.Close();
            }
        }//End of ClientAvailableRequestHandler


        /// <summary>
        /// 
        /// </summary>
        /// <param name="activeProblem"></param>
        /// <returns></returns>
        private List<List<TestCase>> PartionTestCasesIntoFolds(Problem problem) {
            int validationIndex = 0;

            //Define the return
            List<List<TestCase>> returnList = new List<List<TestCase>>();

            //Initialize the master list
            for (int i = 0; i < TotalNumberOfFolds; i++) {
                returnList.Add(new List<TestCase>());
            }

            //Iterate over the master list of test cases
            for (int i = 0; i < problem.TestCases.Count; i++) {
                TestCase testCase = problem.TestCases[i];                

                ///Iterate over the return list adding the current test case to all of the
                ///partitions EXCEPT the one equal to the current validationIndex
                for (int j = 0; j < returnList.Count; j++) {
                    if (j != validationIndex) {
                        returnList[j].Add(testCase);
                    } else {
                        ///We could potential build a List<List<TestCases>> and population it here to 
                        ///store the validation sets
                    }
                }

                //Increment the validation index and reset as required
                validationIndex++;
                if (validationIndex >= TotalNumberOfFolds) //validationIndex is 0-indexed and TotalNumberOfFolds is 1-indexed
                    validationIndex = 0;
            }

            //Return the list of test cases
            return returnList;
        }
    }
}
