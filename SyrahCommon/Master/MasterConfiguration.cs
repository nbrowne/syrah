﻿using System;
using System.Collections.Generic;

using System.Text;

namespace NigelBrowne.Syrah.Common.Master {
    public class MasterConfiguration {
        public string DbConnection = "";
        public int ClientViewerRefreshInterval = 0;

        public MasterConfiguration() {
        }
    }
}
