﻿using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common;
using NigelBrowne.Syrah.Common.Data;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.ProblemBuilder {
    /// <summary>
    /// Worker class to build and store problems and test sets.
    /// </summary>
    public class ProblemBuilder {
        /// <summary>
        /// 
        /// </summary>
        public ProblemBuilder() {
        }


        #region Test Case Import Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<TestCase> ImportTestCasesFromFile(string path) {
            return ProblemBuilder.ImportTestCasesFromText(System.IO.File.ReadAllText(path));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static List<TestCase> ImportTestCasesFromText(string text) {
            int lineCounter = 0;
            List<Codon> variables = new List<Codon>();
            List<TestCase> testCases = new List<TestCase>();
            List<String> inputs, outputs;

            //Iterate over each line in the buffer            
            foreach (string line in text.Split('\n')) {
                string cleanedLine = ProblemBuilder.CleanImportLine(line);

                //Check for data before proceeding
                if (cleanedLine.Length > 0) {
                    //Every data line should have an equal sign in it, to seperate the dependant vars from the independants.
                    if (cleanedLine.IndexOf('=') == -1) throw new ArgumentException("Line " + lineCounter.ToString() + " of the data file is a data line without an '=' symbol. Please correct.");

                    //Get the inputs and outputs
                    ProblemBuilder.ParseInputsAndOutputs(cleanedLine, out inputs, out outputs);

                    if (lineCounter == 0) {
                        variables = ProblemBuilder.ParseVariablesFromLine(inputs, outputs);

                    } else {
                        testCases.Add(ProblemBuilder.ParseTestCaseFromLine(variables, inputs, outputs));
                    }                   
                }
                
                lineCounter++;
            } //End of line iterator

            return testCases;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineBuffer"></param>
        /// <returns></returns>
        private static string CleanImportLine(string lineBuffer) {
            //Clean the line
            string formattedLine = lineBuffer.Trim();

            //Remove any comments
            if (formattedLine.IndexOf('#') >= 0) formattedLine = formattedLine.Substring(0, formattedLine.IndexOf('#')).Trim();

            return formattedLine;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineBuffer"></param>
        /// <param name="values"></param>
        /// <param name="results"></param>
        private static void ParseInputsAndOutputs(string lineBuffer, out List<String> inputs, out List<String> outputs) {            
            string[] inputAndOutputArray = lineBuffer.Split('=');

            //Initialize the returns
            inputs = new List<string>();
            outputs = new List<string>();

            //Add the inputs
            foreach (string element in inputAndOutputArray[0].Split(',')) {
                inputs.Add(element.Trim().ToLower());
            }

            //Add the outputs
            foreach (string element in inputAndOutputArray[1].Split(',')) {
                outputs.Add(element.Trim());
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineBuffer"></param>
        /// <returns></returns>
        private static List<Codon> ParseVariablesFromLine(List<string> inputs, List<string> outputs) {
            List<Codon> variables = new List<Codon>();
            
            //Get the list of available functions
            List<Codon> availableFunctions = FunctionManager.GetAvailableFunctions();

            //Convert the left side elements to our variables
            foreach (string element in inputs) {
                Codon variableCodon = new Codon(element);

                //Check if the variable matches a function
                if (availableFunctions.Contains(variableCodon)) throw new ArgumentException("Cannot import data file because the variable '" + element + "' matches a function codon value. Please select another name for this variable.");

                //Add the new codon
                variables.Add(variableCodon);
            }

            //Return the list of variables
            return variables;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineBuffer"></param>
        /// <returns></returns>
        private static TestCase ParseTestCaseFromLine(List<Codon> variables, List<string> inputs, List<string> outputs) {
            //Throw a fit if the variables and inputs don't match
            if (variables.Count != inputs.Count) throw new ArgumentException("Couldn't parse import line because the number of variables didn't match the number of inputs.");

            //Instantiate the return test case
            TestCase testCase = new TestCase();

            //todo: Change this to support multiple outputs
            testCase.ExpectedValue = double.Parse(outputs[0]);

            //Add the inputs
            for (int i = 0; i < variables.Count; i++) {
                testCase.TerminalValues.Add(variables[i], double.Parse(inputs[i]));
            }

            //Return the test case
            return testCase;
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cases"></param>
        /// <returns></returns>
        public static string ExportTestCasesToText(List<TestCase> cases) {
            //Get the terminals
            string terminals = "";
            if (cases.Count > 0) {
                foreach (Codon terminal in cases[0].TerminalValues.Keys) {
                    if (terminals.Length > 0) terminals += ", ";
                    terminals += terminal.ToString();
                }
            }

            string exportBuffer =
                "Test cases: " + cases.Count.ToString() + ", " + Environment.NewLine +
                "Terminals: " + terminals;

            return exportBuffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cases"></param>
        /// <returns></returns>
        private static string ExportTestCasesToTextOld(List<TestCase> cases) {
            string buffer = "";
            string variables = "";
            bool firstLoop = true;

            foreach(TestCase testCase in cases) {
                foreach (Codon terminal in testCase.TerminalValues.Keys) {
                    if (firstLoop) {
                        if (variables.Length > 0) variables += ", ";
                        variables += terminal.ToString();
                    }
                                                         
                    buffer += testCase.TerminalValues[terminal];
                }

                if (firstLoop) {
                    buffer = variables + "\r\n"+
                            "----------------------------------------\r\n"+ 
                            buffer;                    
                }

                buffer += " \t= " + testCase.ExpectedValue.ToString() +"\r\n";
                firstLoop = false;
            }


            return buffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Problem> GetProblemList(DataAccess dataAccess) {
            List<Problem> problemList = null;

            try {
                ProblemCollection problems = new ProblemCollection(dataAccess);
                ProblemCollectionFilter filter = new ProblemCollectionFilter();
                filter.OrderBy = "id desc";
                problemList = problems.GetCollection(filter);

            } finally {
                dataAccess.Close();
            }

            return problemList;
        }
    } //End of problem builder class
} 
