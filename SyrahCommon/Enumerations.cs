﻿/***********************************************************************
 * Enumerations.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Stores the enumerations used by the application
 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;

namespace NigelBrowne.Syrah.Common {
    public enum CommCommands {
        Unknown,
        ClientSettingsRequest,
        ClientAvailable,
        ClientSampleRequest,    //Client is requesting a new sample for testing/validation
        ClientMigrationRequest, //Client is requesting a migration event
        Wait,
        Run,
        RunFinished,
        ShutdownClient
    }
    public enum CommCommandStatus {
        NotSet,
        Success,
        Failure
    }
    public enum LogDetailLevel {
        Detail,
        Information,
        Warning,
        Error
    }

    public enum GeneType {
        Normal,
        Homeotic
    }

    public enum FitnessEvaluationMethod {
        PrecisionAndSelectionRange,
        MeanSquaredError,
        HitsWithPenalty,
        
        //Define the parameter optimization methods
        PoFerreira84 = 20,
        PoFerreira87,
        PoFerreira88,
        DeJong1,
        DeJong2,
        DeJong3,
        DeJong4,
        DeJong5,

        //Define parameter optimization start and end
        PoStart = PoFerreira84,
        PoEnd = DeJong5
    }

    /// <summary>
    /// Represents the different types of constants that can be used by the GEP-RNC algorithm.
    /// </summary>
    public enum RncConstantsType {
        Rational,
        Integer
    }

    /// <summary>
    /// Represents the parts of a GEP gene
    /// </summary>
    public enum GeneParts {
        Unknown,
        Head,
        Tail,
        Dc,      
    }

    /// <summary>
    /// Represents the different methods that the stochastic sampling algorithm can use to determine when to request a 
    /// new sample.
    /// </summary>
    public enum StochasticSampleTriggerMethod {
        Generations,
        Fitness
    }


    /// <summary>
    /// Represents the method used to trigger the use of parsimony pressure during a run.
    /// </summary>
    public enum ParsimonyPressureTriggerMethod {
        Generations,
        Fitness,
        Size
    }

    /// <summary>
    /// The migration method/topology
    /// </summary>
    public enum MigrationMethod {
        Automatic
    } 

}
