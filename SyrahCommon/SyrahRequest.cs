﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Runtime.Serialization;

namespace NigelBrowne.Syrah.Common {
    /// <summary>
    /// Stores the parameters associated with a Syrah command.
    /// </summary>
    [Serializable]
    public class SyrahRequest : Dictionary<string, object> {
        protected string _commandMessage = "";
        protected string _source = "";
        protected long _payloadSize = 0;
        protected string _clientVersion = "";
        protected CommCommands _command = CommCommands.Unknown;


        /// <summary>
        /// Get or set the client version
        /// </summary>
        public string ClientVersion {
            get { return _clientVersion; }
            set { _clientVersion = value; }
        }


        /// <summary>
        /// Get or set the size of the request payload.
        /// </summary>
        public long PayloadSize {
            get { return _payloadSize; }
            set { _payloadSize = value; }
        }


        /// <summary>
        /// Client name that the command originated from.
        /// </summary>
        public string Source {
            get { return _source; }
            set { _source = value; }
        }


        /// <summary>
        /// Get or set the Syrah command the client wishes to execute.
        /// </summary>
        public CommCommands Command { 
            get { return _command; }
            set { _command = value; }
        }


        /// <summary>
        /// Gets or sets the text message associated with this command result.
        /// </summary>
        public string Message {
            get { return _commandMessage; }
            set { _commandMessage = value; }
        }


        /// <summary>
        /// Default constructor
        /// </summary>
        public SyrahRequest() {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="source"></param>
        public SyrahRequest(string source, CommCommands command) : this() {
            _source = source;
            _command = command;
        }


        /// <summary>
        /// Deserialization constructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected SyrahRequest(SerializationInfo info, StreamingContext context)
            : base(info, context) {

            //Load the deserialized values
            _commandMessage = info.GetString("CommandMessage");
            _source = info.GetString("Source");
            _command = (CommCommands)info.GetValue("Command", typeof(CommCommands));
            _clientVersion = info.GetString("ClientVersion");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context) {
            //Let the parent class do its streaming first
            base.GetObjectData(info, context);

            //Add the objects for this class now
            info.AddValue("CommandMessage", _commandMessage);
            info.AddValue("Source", _source);
            info.AddValue("Command", _command, typeof(CommCommands));
            info.AddValue("ClientVersion", _clientVersion);
        }
    }
}
