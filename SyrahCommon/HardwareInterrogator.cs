﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Management;
using System.Runtime.Serialization;


namespace NigelBrowne.Syrah.Common {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ProcessorInformation {
        protected string _description = "";
        protected uint _clockSpeed = 0;

        /// <summary>
        /// Description of the CPU
        /// </summary>
        public string Description { get { return _description; }} 

        /// <summary>
        /// Maximum clock speed of the processor in MHz.
        /// </summary>
        public uint ClockSpeed { get { return _clockSpeed; } }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="description"></param>
        /// <param name="clockSpeed"></param>
        public ProcessorInformation(string description, uint clockSpeed) {
            _description = description;
            _clockSpeed = clockSpeed;
        }
  }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class HardwareInterrogator {  
        [NonSerialized]
        protected int PROCESSORTYPE_CPU = 3;

        protected int _processorCount = 0;
        protected List<ProcessorInformation> _processorInformationList = null;
        protected string _operatingSystem = "Unknown";
        protected string _computerName = "Unknown";
        protected int _visibleMemory = 0; // in MB

        /// <summary>
        /// The number of processors (CPUs) the system contains.
        /// </summary>
        public int ProcessorCount { get { return _processorCount; }} 

        /// <summary>
        /// Information about each processor in the system.
        /// </summary>
        public List<ProcessorInformation>Processors { get { return _processorInformationList; }}

        /// <summary>
        /// Get the operating system name.
        /// </summary>
        public string OperatingSystem { get { return _operatingSystem; }}

        /// <summary>
        /// Get the name of the computer
        /// </summary>
        public string ComputerName { get { return _computerName; } }

        //Get the total amount of memory visible to the operatoring system, in MB.
        public int TotalMemory { get { return _visibleMemory; } }


        /// <summary>
        /// Queries the hardware on instantiation.
        /// </summary>
        public HardwareInterrogator() {
            _processorInformationList = new List<ProcessorInformation>();
            
             //Get the processor and system information
            this.LoadProcessorInformation();
            this.LoadSystemInformation();
        }


        /// <summary>
        /// Load the system information into the member variables. This is a worker method.
        /// </summary>
        protected void LoadSystemInformation() {
            //Instantiate the management object and get the processor information
            ManagementClass mc = new ManagementClass("Win32_OperatingSystem");
            ManagementObjectCollection moc = mc.GetInstances();            
          
            //Iterate over each active os, since there should only be one, we'll bail after the first is done
            foreach (ManagementObject mo in moc) {
                //Load the member vars
                _operatingSystem = (string)mo.GetPropertyValue("Caption");
                _computerName = (string)mo.GetPropertyValue("CSName");

                //Convert from KB to MB
                _visibleMemory = (int)((ulong)mo.GetPropertyValue("TotalVisibleMemorySize") / (ulong)1024);

                //Break out of the loop, since we should only EVER have one active OS.
                break;
            }
        }


        /// <summary>
        /// Load the processor information into the object's member variables. This is a worker method.
        /// </summary>
        protected void LoadProcessorInformation() {
            //Instantiate the management object and get the processor information
            ManagementClass mc = new ManagementClass("Win32_Processor");
            ManagementObjectCollection moc = mc.GetInstances();

            //Load the number of processors on the system
            _processorCount = moc.Count;

            //Iterate over each processor
            foreach (ManagementObject mo in moc) {
                int processorType = (ushort)mo.Properties["ProcessorType"].Value;

                //Only process CPUs
                if (processorType == PROCESSORTYPE_CPU) {
                    _processorInformationList.Add(
                        new ProcessorInformation(
                            ((String)mo.GetPropertyValue("Name")).Trim(), 
                            (uint)mo.GetPropertyValue("MaxClockSpeed")
                        )
                    );                                      
                }
            }
        } //End of AddProcessorInformation(...)
    }
}
