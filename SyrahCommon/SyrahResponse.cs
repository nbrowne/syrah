﻿using System;
using System.Collections.Generic;
using System.Collections;

using System.Text;
using System.Runtime.Serialization;

namespace NigelBrowne.Syrah.Common {
    /// <summary>
    /// Stores the results of a Syrah command request.
    /// </summary>
    [Serializable]
    public class SyrahResponse : Dictionary<string, object> {
        protected string _commandMessage = "";
        protected CommCommandStatus _commandStatus = CommCommandStatus.NotSet;
        protected CommCommands _command = CommCommands.Unknown;
        
        /// <summary>
        /// Gets or sets the text message associated with this command result.
        /// </summary>
        public string Message {
            get { return _commandMessage; }
            set { _commandMessage = value; }
        }


        /// <summary>
        /// Get or set the result status of the command execution.
        /// </summary>
        public CommCommandStatus Status {
            get { return _commandStatus; }
            set { _commandStatus = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public CommCommands Command { 
            get { return _command; }
            set { _command = value; }
        }


        /// <summary>
        /// Default constructor
        /// </summary>
        public SyrahResponse() {            
        }


        /// <summary>
        /// Instantiate a CommandResult object and set the status
        /// </summary>
        /// <param name="status">Command result status.</param>
        /// <param name="message">A string message associated with the command result and status.</param>
        public SyrahResponse(CommCommands command, CommCommandStatus status, string message) {
            _command = command;
            _commandStatus = status;
            _commandMessage = message;
        }

        protected SyrahResponse(SerializationInfo info, StreamingContext context)
            : base(info, context) {

            //Get the SyrahRequest specific values
            _command = (CommCommands)info.GetValue("Command", typeof(CommCommands));
            _commandStatus = (CommCommandStatus)info.GetValue("Status", typeof(CommCommandStatus));
            _commandMessage = info.GetString("CommandMessage");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context) {
            //Let the parent class do its streaming first
            base.GetObjectData(info, context);

            //Add the SyrahRequest specific values
            info.AddValue("Command", _command, typeof(CommCommands));
            info.AddValue("CommandMessage", _commandMessage);
            info.AddValue("Status", _commandStatus, typeof(CommCommandStatus));
        }
    } 
}
