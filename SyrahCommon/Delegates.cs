﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Net;

using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common {
    /// <summary>
    /// Handles the completion of a client run, this is fired by the SyrahMaster class and is intended to by 
    /// handled by the master application's user interface.
    /// </summary>
    /// <param name="clientName">Name of the client</param>
    /// <param name="statistics">Run statistics</param>
    public delegate void ClientRunCompletedDelegate(string clientName, Common.Data.ProblemResult results);


    /// <summary>
    /// 
    /// </summary>
    /// <param name="clientName"></param>
    public delegate void ClientRunStartedDelegate(string clientName, Data.Problem problem);

    /// <summary>
    /// Handles output messages from the algorithm. Normally,
    /// the implementation of this method will send the ouput in 'text' to
    /// the screen and/or a file.
    /// </summary>
    /// <param name="text"></param>
    public delegate void MessageHandlerDelegate(LogDetailLevel level, string text);


    /// <summary>
    /// 
    /// </summary>
    /// <param name="command"></param>
    /// <param name="?"></param>
    /// <returns></returns>
    public delegate SyrahResponse SyrahClientRequestHandler(SyrahRequest request);


    /// <summary>
    /// Fired when the GEP algorithm requires an new sample of test cases. This is used by the 
    /// stochastic sampling algorithm.
    /// </summary>
    /// <param name="problemId"></param>
    /// <param name="testCases"></param>
    public delegate void TestCaseSampleRequestDelegate(out List<TestCase> testCases);


    /// <summary>
    /// Fired when the GEP algorithm needs to perform a migration
    /// </summary>
    /// <param name="problemId"></param>
    /// <param name="emigrants">Those chromosomes leave the island.</param>
    /// <param name="immigrants">Those chromosomes coming to the island.</param>
    public delegate void MigrationRequestDelegate(List<Chromosome> emigrants, out List<Chromosome> immigrants);
}
