using System; 
using System.Collections.Generic; 
using System.Data; 
using System.ComponentModel;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.Data { 
    #region Collection Class - This is used to create a collection of entity objects
    /// <summary> 
    /// Collection class for a collection of 'problem_results' table records and/or entities. 
    /// </summary> 
    public class ProblemResultCollection {

        protected DataAccess _dataAccess = null;
        public DataAccess DbAccess {
            get { return _dataAccess; }
            set { _dataAccess = value; }
        }

        /// <summary> 
        /// Default constructor.
        /// </summary> 
        public ProblemResultCollection() {
        }

        /// <summary> 
        /// Default constructor.
        /// </summary> 
        public ProblemResultCollection(DataAccess dataAccess) {
			_dataAccess = dataAccess;
        }

        /// <summary> 
        /// Get all records from the problem_results table.
        /// </summary> 
        public DataTable GetTable() {
            return this.GetTable(null);
        }


        /// <summary> 
        /// Get a filtered set of records from the problem_results table.
        /// </summary> 
        /// <param name="filter">
        /// The ProblemResultCollectionFilter filter object to use to build the where statement.
        /// </param>
        public DataTable GetTable(ProblemResultCollectionFilter filter) {
            //Build the query to get the data
            string sql = BuildQuery(filter);           

            //Query the database
            DataTable returnData = _dataAccess.Query(sql);

            //Return the query results
            return returnData;
        }

        
        /// <summary> 
        /// Get a collection of all ProblemResult objects in the database.
        /// </summary> 
        public SortedList<string, ProblemResult> GetCollection() {
            return this.GetCollection(null);
        }


        /// <summary> 
        /// Get a filtered collection of ProblemResult objects
        /// </summary> 
        /// <param name="filter">
        /// The ProblemResultCollectionFilter filter object to use to build the where statement.
        /// </param>
        public SortedList<string, ProblemResult> GetCollection(ProblemResultCollectionFilter filter) {
            SortedList<string, ProblemResult> returnList = new SortedList<string, ProblemResult>();
            DataTable oData = this.GetTable(filter);

            //Iterate over each item in the data table, instantiate a ProblemResult object 
            //from it and then add it to our collection
            foreach(DataRow oRow in oData.Rows) {
                returnList.Add(oRow["id"].ToString(), new ProblemResult(oRow));
            }

            //Return the object collection that we've built
            return returnList;
        }

        public List<ProblemResult> GetList(ProblemResultCollectionFilter filter) {
            List<ProblemResult> returnList = new  List<ProblemResult>();
            DataTable oData = this.GetTable(filter);

            //Iterate over each item in the data table, instantiate a ProblemResult object 
            //from it and then add it to our collection
            foreach (DataRow oRow in oData.Rows) {
                returnList.Add(new ProblemResult(oRow));
            }

            //Return the object collection that we've built
            return returnList;
        }
        /// <summary> 
        /// Build the sql query to retrieve the entities from the database.
        /// </summary> 
        /// <param name="filter">
        /// The ProblemResultCollectionFilter filter object to use to build the where statement.
        /// </param>
        protected string BuildQuery(ProblemResultCollectionFilter filter) {
            string topStatement = "";
            if (filter != null && filter.TopN.Length > 0) topStatement = filter.TopN;

            string sqlStatement = @"
				select 
                    " + topStatement + @"
					main.id,
					main.problem_id,
					main.run_number,
					main.client_name,
                    main.max_possible_fitness,
					main.best_fitness,
					main.best_fitness_generation,
					main.run_time,
					main.average_generation_time,
					main.max_generation_time,
					main.best_chromosome,
                    --main.best_chromosome_xml,
                    main.best_chromosome_text,
                    main.results_text,
                    main.run_statistics,
                    fold_index,
					detail_1.title                    
				from problem_results main 
				left join problems detail_1 on detail_1.id=main.problem_id";
            string whereStatement = "";

            //If we've been given a filter object, then build the where statement
            if (filter!=null) {
                whereStatement = this.BuildWhereStatement(filter);
            }

            //Append the where statement to the select
            sqlStatement += whereStatement;

            //Return the query statement
            return sqlStatement;
        }


        /// <summary> 
        /// Build the where statement based on a filter.
        /// </summary> 
        /// <param name="filter">
        /// The ProblemResultCollectionFilter filter object to use to build the where statement.
        /// </param>
        protected string BuildWhereStatement(ProblemResultCollectionFilter filter) {
            string whereStatement = "";
            
			if (filter.IsIdSet) {
				whereStatement += " and main.id="+ filter.Id.ToString() +" ";
			}
			if (filter.IsProblemIdSet) {
				whereStatement += " and problem_id="+ filter.ProblemId.ToString() +" ";
			}
			if (filter.IsRunNumberSet) {
				whereStatement += " and run_number="+ filter.RunNumber.ToString() +" ";
			}
			if (filter.IsClientNameSet) {
				whereStatement += " and client_name='"+ filter.ClientName +"' ";
			}
			if (filter.IsBestFitnessSet) {
				whereStatement += " and best_fitness="+ filter.BestFitness.ToString() +" ";
			}
			if (filter.IsBestFitnessGenerationSet) {
				whereStatement += " and best_fitness_generation="+ filter.BestFitnessGeneration.ToString() +" ";
			}
			if (filter.IsRunTimeSet) {
				whereStatement += " and run_time="+ filter.RunTime.ToString() +" ";
			}
			if (filter.IsAverageGenerationTimeSet) {
				whereStatement += " and average_generation_time="+ filter.AverageGenerationTime.ToString() +" ";
			}
			if (filter.IsMaxGenerationTimeSet) {
				whereStatement += " and max_generation_time="+ filter.MaxGenerationTime.ToString() +" ";
			}
			if (filter.IsBestChromosomeSet) {
				whereStatement += " and best_chromosome="+ filter.BestChromosome +" ";
			}
			if (filter.IsTitleSet) {
				whereStatement += " and title='"+ filter.Title +"' ";
			}

            //If there are items in the where statement, finsh it
            if (whereStatement.Length>0) {
                whereStatement = " where "+ whereStatement.Substring(4).Trim();
            }

            //append the order by 
            if (filter.OrderBy.Length > 0) {
                whereStatement += " order by  " + filter.OrderBy.Trim();
            }	

            //Return the where statement we've built
            return whereStatement;
        }
    }
    #endregion

    #region Filter Class - This is used to filter a ProblemResultCollection object
    /// <summary> 
    /// A class for filtering a ProblemResultCollection object
    /// </summary> 
    public class ProblemResultCollectionFilter {
        
		protected int _id = int.MinValue;
		protected bool _isIdSet = false;
		protected int _problemId = int.MinValue;
		protected bool _isProblemIdSet = false;
		protected int _runNumber = int.MinValue;
		protected bool _isRunNumberSet = false;
		protected string _clientName = "";
		protected bool _isClientNameSet = false;
		protected float _bestFitness = float.MinValue;
		protected bool _isBestFitnessSet = false;
		protected int _bestFitnessGeneration = int.MinValue;
		protected bool _isBestFitnessGenerationSet = false;
		protected int _runTime = int.MinValue;
		protected bool _isRunTimeSet = false;
		protected int _averageGenerationTime = int.MinValue;
		protected bool _isAverageGenerationTimeSet = false;
		protected int _maxGenerationTime = int.MinValue;
		protected bool _isMaxGenerationTimeSet = false;
		protected byte[] _bestChromosome;
		protected bool _isBestChromosomeSet = false;
		protected string _title = "";
		protected bool _isTitleSet = false;
        protected string _orderby = "";

        public string TopN { get; set; }
        
        public string OrderBy {
            get { return _orderby; }
            set { _orderby = value; }
        }
        
		public int Id {
			get { return _id; }
			set { _id=value; _isIdSet=true; }
		}
		public bool IsIdSet {
			get { return _isIdSet; }
		}

		public int ProblemId {
			get { return _problemId; }
			set { _problemId=value; _isProblemIdSet=true; }
		}
		public bool IsProblemIdSet {
			get { return _isProblemIdSet; }
		}

		public int RunNumber {
			get { return _runNumber; }
			set { _runNumber=value; _isRunNumberSet=true; }
		}
		public bool IsRunNumberSet {
			get { return _isRunNumberSet; }
		}

		public string ClientName {
			get { return _clientName; }
			set { _clientName=value; _isClientNameSet=true; }
		}
		public bool IsClientNameSet {
			get { return _isClientNameSet; }
		}

		public float BestFitness {
			get { return _bestFitness; }
			set { _bestFitness=value; _isBestFitnessSet=true; }
		}
		public bool IsBestFitnessSet {
			get { return _isBestFitnessSet; }
		}

		public int BestFitnessGeneration {
			get { return _bestFitnessGeneration; }
			set { _bestFitnessGeneration=value; _isBestFitnessGenerationSet=true; }
		}
		public bool IsBestFitnessGenerationSet {
			get { return _isBestFitnessGenerationSet; }
		}

		public int RunTime {
			get { return _runTime; }
			set { _runTime=value; _isRunTimeSet=true; }
		}
		public bool IsRunTimeSet {
			get { return _isRunTimeSet; }
		}

		public int AverageGenerationTime {
			get { return _averageGenerationTime; }
			set { _averageGenerationTime=value; _isAverageGenerationTimeSet=true; }
		}
		public bool IsAverageGenerationTimeSet {
			get { return _isAverageGenerationTimeSet; }
		}

		public int MaxGenerationTime {
			get { return _maxGenerationTime; }
			set { _maxGenerationTime=value; _isMaxGenerationTimeSet=true; }
		}
		public bool IsMaxGenerationTimeSet {
			get { return _isMaxGenerationTimeSet; }
		}

		public byte[] BestChromosome {
			get { return _bestChromosome; }
			set { _bestChromosome=value; _isBestChromosomeSet=true; }
		}
		public bool IsBestChromosomeSet {
			get { return _isBestChromosomeSet; }
		}

		public string Title {
			get { return _title; }
			set { _title=value; _isTitleSet=true; }
		}
		public bool IsTitleSet {
			get { return _isTitleSet; }
		}


        /// <summary> 
        /// Instantiate an empty ProblemResultCollectionFilter object
        /// </summary> 
        public ProblemResultCollectionFilter() {
            this.TopN = "";
        }
    }
    #endregion

    #region Entity Class - This is used to represent the problem_results table in the database.
    /// <summary> 
    /// Table interface class for the 'problem_results' table. 
    /// </summary> 
    public class ProblemResult {         		

        #region Member Variables
            protected Dictionary<string, bool> _propertyNullStateDictionary;	
            protected bool _loadedFromDatabase = false;		
            
			protected int _id; 
			protected int _problemId; 
			protected int _runNumber; 
			protected string _clientName; 			//Nullable
            protected double _maxPossibleFitness;
			protected double _bestFitness; 
			protected int _bestFitnessGeneration; 
			protected int _runTime; 
			protected int _averageGenerationTime; 
			protected int _maxGenerationTime; 
			protected Chromosome _bestChromosome;
            protected string _bestChromosomeText = "";
            protected string _resultsText = "";
			protected string _title;
            protected int _foldIndex = -1;
            protected EvolutionaryAlgorithms.GEP.GepStatistics _runStatistics;

        #endregion

        #region Properties
            
			public int Id {
				get { return _id; }
			}
            public EvolutionaryAlgorithms.GEP.GepStatistics RunStatistics {
                get { return _runStatistics; }
                set { _runStatistics = value; }
            }
			public int ProblemId {
				get { return _problemId; }
				set { _problemId=value; }
			}
			public int RunNumber {
				get { return _runNumber; }
				set { _runNumber=value; }
			}
			public string ClientName {
				get { return _clientName; }
				set { 
					_clientName=value; 
					_propertyNullStateDictionary["ClientName"] = false;
				}
			}
			public double BestFitness {
				get { return _bestFitness; }
				set { _bestFitness=value; }
			}
			public int BestFitnessGeneration {
				get { return _bestFitnessGeneration; }
				set { _bestFitnessGeneration=value; }
			}
			public int RunTime {
				get { return _runTime; }
				set { _runTime=value; }
			}
			public int AverageGenerationTime {
				get { return _averageGenerationTime; }
				set { _averageGenerationTime=value; }
			}
			public int MaxGenerationTime {
				get { return _maxGenerationTime; }
				set { _maxGenerationTime=value; }
			}
			public Chromosome BestChromosome {
				get { return _bestChromosome; }
				set { _bestChromosome=value; }
			}
			public string Title {
				get { return _title; }
			}
            public string BestChromosomeText {
                get { return _bestChromosomeText; }
                set { _bestChromosomeText = value; }
            }
            public string ResultsText {
                get { return _resultsText; }
                set { _resultsText = value; }
            }
            public double MaxPossibleFitness {
                get { return _maxPossibleFitness; }
                set { _maxPossibleFitness = value; }
            }
            public int FoldIndex {
                get { return _foldIndex; }
                set { _foldIndex = value; }
            }
            
            [ReadOnly(true)]
            [Browsable(false)]
            public bool LoadedFromDatabase { get { return _loadedFromDatabase; } }
        #endregion


        #region Constructors 
        /// <summary> 
        /// Default constructor, instaniates an empty object
        /// </summary>         
        public ProblemResult() { 
            this.InitializeNullableFieldsCollection();		
        } 
        
        
        /// <summary> 
        /// Instantiates the object and populates the object with the database data for the specified recordId
        /// </summary>         
        /// <param name="recordId">Id field value to match</param>
        public ProblemResult(int recordId) : this() { 
            LoadFromDatabase(recordId); 
        } 

        /// <summary> 
        /// Instantiates the object and populates the object with the database data for the specified recordId
        /// </summary>         
        /// <param name="recordId">Id field value to match</param>
        public ProblemResult(DataRow dataItem) : this() { 
            this.LoadFromDatabaseRow(dataItem);
        } 
        #endregion             

        public override string ToString() {
            double fitnessPercent = this.BestChromosome.Fitness / this.MaxPossibleFitness * 100.0;

            return _id.ToString() +" - "+ fitnessPercent.ToString("0.00000") +"%";
        }
     
        #region Null Fields Handling Methods - These are required because some database types are value types in C# (and not nullable)				
        /// <summary>
        /// Set a nullable property to null.
        /// </summary>
        /// <param name="propertyName"></param>				
        public void SetPropertyNull(string propertyName) {
            if (this.IsNullableProperty(propertyName)) {
                _propertyNullStateDictionary[propertyName] = true;
            }
        }
        
        /// <summary>
        /// Checks if a nullable property is currently null.
        /// </summary>
        /// <param name="propertyName">String name of the property to check.</param>		
        /// <returns>Returns true is the property is currently set to null, otherwise returns false.</returns> 		
        public bool IsPropertyNull(string propertyName) {
            return _propertyNullStateDictionary[propertyName];
        }		
        
        /// <summary>
        /// Indicates if the property specified in propertyName represents 
        /// a nullable field in the database.
        /// </summary>
        /// <param name="propertyName">String name of the property to check.</param>		
        /// <returns>Returns true is the field is nullable in the database, otherwise returns false.</returns>  
        public bool IsNullableProperty(string propertyName) {
            bool isNullable = false;			
            
			isNullable |= propertyName.Trim().ToLower()=="clientname";
			
            return isNullable;		
        }
        
        
        /// <summary>
        /// Initializes the items in the Dictionary that tracks the null state of the 
        /// object properties that represent nullable fields in the database.
        /// </summary>		
        protected void InitializeNullableFieldsCollection() {
            _propertyNullStateDictionary = new Dictionary<string, bool>();			
            
			_propertyNullStateDictionary.Add("ClientName", true);

        }
        #endregion
        
        
        /// <summary>
        /// Populate the fields of the object from the database. This method will
        /// create a new CDatabase object for it's communication with the database.
        /// </summary>
        /// <param name="recordId">Id field value to match.</param>
        public void LoadFromDatabase(int recordId) {
            DataAccess dataAccess = new DataAccess();
            
            try {
				this.LoadFromDatabase(dataAccess, recordId);
				
			} finally {
				dataAccess.Close();
            }
        }


        /// <summary>
        /// Populate the fields of the object from an item in the database.
        /// </summary>
        /// <param name="recordId">Id field value to match.</param>
        /// <param name="dataAccess">DataAccess object to use to communicate with the database.</param>
        public void LoadFromDatabase(DataAccess dataAccess, int recordId) {
            string sqlStatement = @"
				select 
					main.id,
					main.problem_id,
					main.run_number,
					main.client_name,
                    main.max_possible_fitness,
					main.best_fitness,
					main.best_fitness_generation,
					main.run_time,
					main.average_generation_time,
					main.max_generation_time,
					main.best_chromosome,
                    --main.best_chromosome_xml,
                    main.best_chromosome_text,
                    main.results_text,
                    main.run_statistics,
                    main.fold_index,
					detail_1.title
				from problem_results main 
				left join problems detail_1 on detail_1.id=main.problem_id
				where main.id=" + recordId.ToString() +@"
			";

            DataTable oData = dataAccess.Query(sqlStatement);    
            
            if (oData.Rows.Count==1) {
                //Reassign the row for readability
                DataRow oItem = oData.Rows[0];
                this.LoadFromDatabaseRow(oItem);
            }               
        }


        /// <summary>
        /// 
        /// </summary>    
        /// <param name="dataItem"></param>  
        protected void LoadFromDatabaseRow(DataRow dataItem) {
            //Load the data from the data row into the member variables
            this._id = (int)dataItem["id"];
			this._problemId = (int)dataItem["problem_id"];
			this._runNumber = (int)dataItem["run_number"];
			if (!dataItem["client_name"].Equals(System.DBNull.Value)) this._clientName = (string)dataItem["client_name"];
			this._bestFitness = (double)dataItem["best_fitness"];
			this._bestFitnessGeneration = (int)dataItem["best_fitness_generation"];
			this._runTime = (int)dataItem["run_time"];
			this._averageGenerationTime = (int)dataItem["average_generation_time"];
			this._maxGenerationTime = (int)dataItem["max_generation_time"];
            //todo: fix the chromosome unpacking from the database
            //this._bestChromosome = new Chromosome();//Utilities.UnpackObject<Chromosome>((byte[])dataItem["best_chromosome"]);
            this._bestChromosome = Utilities.UnpackObject<Chromosome>((byte[])dataItem["best_chromosome"]);
            this._bestChromosomeText = (string)dataItem["best_chromosome_text"];
            this._resultsText = (string)dataItem["results_text"];			
            this._maxPossibleFitness = (double)dataItem["max_possible_fitness"];
            this._runStatistics = Utilities.UnpackObjectXml<EvolutionaryAlgorithms.GEP.GepStatistics>((string)dataItem["run_statistics"]);
            this._foldIndex = (int)dataItem["fold_index"];
            _loadedFromDatabase = true;
        }


        /// <summary>
        /// Checks the object's parameter values for validity. This method will throw an exception if  
        /// one of the parameter does not validate.
        /// </summary>        
        public void ValidateParameters() {
            
        }     
        
        
        #region Save Functions
         /// <summary>
        ///  Save the current settings to the database. This will instanitate a new CDataAcess object
        /// and use the new object to communicate with the database.
        /// </summary>
        public void Save() {
            DataAccess dataAccess = new DataAccess();
            this.Save(dataAccess);
            dataAccess.Close();
        }
        
        
        /// <summary>
        /// Save the current settings to the database using the supplied DataAccess object.
        /// </summary>
        /// <param name="dataAccess">DataAccess object to use to communicate with the database.</param>
        public void Save(DataAccess dataAccess) {
            string sqlStatement = "";

            //Check that the params are good
            this.ValidateParameters();

            //Build the sql parameters
            List<System.Data.SqlClient.SqlParameter> parameters = new List<System.Data.SqlClient.SqlParameter>();
            parameters.Add(new System.Data.SqlClient.SqlParameter("@bestChromosome", Utilities.PackObject(_bestChromosome)));
            //parameters.Add(new System.Data.SqlClient.SqlParameter("@bestChromosomeXml", Utilities.PackObjectXml<EvolutionaryAlgorithms.GEP.Chromosome>(_bestChromosome)));
            parameters.Add(new System.Data.SqlClient.SqlParameter("@runStatistics", Utilities.PackObjectXml<EvolutionaryAlgorithms.GEP.GepStatistics>(_runStatistics)));

            //Check if the current record is a new record or an update
            if (!_loadedFromDatabase) {
                //New Record
                sqlStatement = @"
					insert into problem_results(
						problem_id, run_number, client_name, best_fitness, best_fitness_generation, run_time, 
                        average_generation_time, max_generation_time, best_chromosome, 
                        --best_chromosome_xml, 
                        best_chromosome_text,
                        results_text, max_possible_fitness, run_statistics, fold_index
					) values (
						"+ _problemId.ToString() +@", 
						"+ _runNumber.ToString() +@", 
						"+ this.GetNullablePropertyValue("ClientName", "'"+ _clientName.Replace("'", "''") +@"'") +@", 
						"+ _bestFitness.ToString() +@", 
						"+ _bestFitnessGeneration.ToString() +@", 
						"+ _runTime.ToString() +@", 
						"+ _averageGenerationTime.ToString() +@", 
						"+ _maxGenerationTime.ToString() +@", 
						@bestChromosome,
                        --@bestChromosomeXml,
                        '"+ _bestChromosomeText +@"',
                        '"+ _resultsText +@"',
                        "+ _maxPossibleFitness.ToString() + @",
                        @runStatistics,
                        "+ _foldIndex.ToString() +@"
					)
				";

                dataAccess.ExecuteSQL(sqlStatement, parameters);
                this._id = dataAccess.GetLastIdentity("problem_results");
                
                _loadedFromDatabase = true;
            }
            else {
                //Update existing
                sqlStatement = @"
					update problem_results set
						problem_id = "+ _problemId.ToString() +@", 
						run_number = "+ _runNumber.ToString() +@", 
						client_name = "+ this.GetNullablePropertyValue("ClientName", "'"+ _clientName.Replace("'", "''") +@"'") +@", 
						best_fitness = "+ _bestFitness.ToString() +@", 
						best_fitness_generation = "+ _bestFitnessGeneration.ToString() +@", 
						run_time = "+ _runTime.ToString() +@", 
						average_generation_time = "+ _averageGenerationTime.ToString() +@", 
						max_generation_time = "+ _maxGenerationTime.ToString() +@", 
						best_chromosome = @bestChromosome,
                        best_chromosome_text = '"+ _bestChromosomeText +@"',
                        results_text = '"+ _resultsText +@"',
                        max_possible_fitness = " + _maxPossibleFitness.ToString() + @",
                        run_statistics = @runStatistics,
                        fold_index = "+ _foldIndex.ToString() +@"
					where Id=" + this._id.ToString() +@"
				";

                dataAccess.ExecuteSQL(sqlStatement, parameters);
            }
        }
        
        
        /// <summary>
        /// Return a string containing 'null' if the null state is set for the requested propertyName,
        /// otherwise the method will return the supplied property value string.
        /// </summary>
        /// <param name="propertyName">String matching the name of one of the classes properties that can be null in the db.</param>
        /// <param name="propertyValue">String representation of the value of the property to use if it isn't null.</param>
        /// <returns>Returns the string representing the value the to be sent to the database for the requested nullable property.</returns>        
        protected string GetNullablePropertyValue(string propertyName, string propertyValue) {
            return _propertyNullStateDictionary[propertyName] ? "null" : propertyValue;
        }        
        #endregion
    } 
    #endregion // End of entity class region
}     