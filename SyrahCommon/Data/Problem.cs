using System; 
using System.Collections.Generic; 
using System.Data; 
using System.ComponentModel;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.Data { 
    #region Collection Class - This is used to create a collection of entity objects
    /// <summary> 
    /// Collection class for a collection of 'problems' table records and/or entities. 
    /// </summary> 
    public class ProblemCollection {

        protected DataAccess _dataAccess = null;
        public DataAccess DbAccess {
            get { return _dataAccess; }
            set { _dataAccess = value; }
        }

        /// <summary> 
        /// Default constructor.
        /// </summary> 
        public ProblemCollection() {
        }

        /// <summary> 
        /// Default constructor.
        /// </summary> 
        public ProblemCollection(DataAccess dataAccess) {
			_dataAccess = dataAccess;
        }

        /// <summary> 
        /// Get all records from the problems table.
        /// </summary> 
        public DataTable GetTable() {
            return this.GetTable(null);
        }


        /// <summary> 
        /// Get a filtered set of records from the problems table.
        /// </summary> 
        /// <param name="filter">
        /// The ProblemCollectionFilter filter object to use to build the where statement.
        /// </param>
        public DataTable GetTable(ProblemCollectionFilter filter) {
            //Build the query to get the data
            string sql = BuildQuery(filter);           

            //Query the database
            DataTable returnData = _dataAccess.Query(sql);

            //Return the query results
            return returnData;
        }

        
        /// <summary> 
        /// Get a collection of all Problem objects in the database.
        /// </summary> 
        public List<Problem> GetCollection() {
            return this.GetCollection(null);
        }


        /// <summary> 
        /// Get a filtered collection of Problem objects
        /// </summary> 
        /// <param name="filter">
        /// The ProblemCollectionFilter filter object to use to build the where statement.
        /// </param>
        public List<Problem> GetCollection(ProblemCollectionFilter filter) {
            List<Problem> returnList = new List<Problem>();
            DataTable oData = this.GetTable(filter);

            //Iterate over each item in the data table, instantiate a Problem object 
            //from it and then add it to our collection
            foreach(DataRow oRow in oData.Rows) {
                returnList.Add(new Problem(oRow));
            }

            //Return the object collection that we've built
            return returnList;
        }


        /// <summary> 
        /// Build the sql query to retrieve the entities from the database.
        /// </summary> 
        /// <param name="filter">
        /// The ProblemCollectionFilter filter object to use to build the where statement.
        /// </param>
        protected string BuildQuery(ProblemCollectionFilter filter) {
            string sqlStatement = @"
				select 
					main.id,
					main.title,
					main.settings,
					main.test_cases,
                    main.notes
				from problems main";
            string whereStatement = "";
            string orderByStatement = "";

            //If we've been given a filter object, then build the where statement
            if (filter!=null) {
                whereStatement = this.BuildWhereStatement(filter);
                orderByStatement = filter.OrderBy.Length>0 ? " order by "+ filter.OrderBy : "";
            }

            //Append the where statement to the select
            sqlStatement += whereStatement + " " + orderByStatement;

            //Return the query statement
            return sqlStatement;
        }


        /// <summary> 
        /// Build the where statement based on a filter.
        /// </summary> 
        /// <param name="filter">
        /// The ProblemCollectionFilter filter object to use to build the where statement.
        /// </param>
        protected string BuildWhereStatement(ProblemCollectionFilter filter) {
            string whereStatement = "";
            
			if (filter.IsIdSet) {
				whereStatement += " and id="+ filter.Id.ToString() +" ";
			}
			if (filter.IsTitleSet) {
				whereStatement += " and title='"+ filter.Title +"' ";
			}
			if (filter.IsSettingsSet) {
				whereStatement += " and settings="+ filter.Settings +" ";
			}
			if (filter.IsTestCasesSet) {
				whereStatement += " and test_cases="+ filter.TestCases +" ";
			}

            //If there are items in the where statement, finsh it
            if (whereStatement.Length>0) {
                whereStatement = " where "+ whereStatement.Substring(4).Trim();
            }	

            //Return the where statement we've built
            return whereStatement;
        }
    }
    #endregion

    #region Filter Class - This is used to filter a ProblemCollection object
    /// <summary> 
    /// A class for filtering a ProblemCollection object
    /// </summary> 
    public class ProblemCollectionFilter {
        
		protected int _id = int.MinValue;
		protected bool _isIdSet = false;
		protected string _title = "";
		protected bool _isTitleSet = false;
		protected byte[] _settings;
		protected bool _isSettingsSet = false;
		protected byte[] _testCases;
		protected bool _isTestCasesSet = false;
        protected string _orderBy = "";


        public string OrderBy {
            get { return _orderBy; }
            set { _orderBy = value; }
        }
        
		public int Id {
			get { return _id; }
			set { _id=value; _isIdSet=true; }
		}
		public bool IsIdSet {
			get { return _isIdSet; }
		}

		public string Title {
			get { return _title; }
			set { _title=value; _isTitleSet=true; }
		}
		public bool IsTitleSet {
			get { return _isTitleSet; }
		}

		public byte[] Settings {
			get { return _settings; }
			set { _settings=value; _isSettingsSet=true; }
		}
		public bool IsSettingsSet {
			get { return _isSettingsSet; }
		}

		public byte[] TestCases {
			get { return _testCases; }
			set { _testCases=value; _isTestCasesSet=true; }
		}
		public bool IsTestCasesSet {
			get { return _isTestCasesSet; }
		}


        /// <summary> 
        /// Instantiate an empty ProblemCollectionFilter object
        /// </summary> 
        public ProblemCollectionFilter() {
        }
    }
    #endregion

    #region Entity Class - This is used to represent the problems table in the database.
    /// <summary> 
    /// Table interface class for the 'problems' table. 
    /// </summary> 
    public class Problem {         		

        #region Member Variables
            protected Dictionary<string, bool> _propertyNullStateDictionary;	
            protected bool _loadedFromDatabase = false;

            protected string _notes = "";
			protected int _id = -1; 
			protected string _title = ""; 
			protected GepSettings _settings = new GepSettings(); 			//Nullable
            protected List<TestCase> _testCaseList = new List<TestCase>();

        #endregion

        #region Properties
            public string AlgorithmType { 
                get { return "GEP"; }
            }
			public int Id {
				get { return _id; }
			}
			public string Title {
				get { return _title; }
				set { _title=value; }
			}
            public string Notes {
                get { return _notes; }
                set { _notes = value; }
            }
			public GepSettings Settings {
				get { return _settings; }
				set { _settings=value; }
			}
			public List<TestCase> TestCases {
				get { return _testCaseList; }
				set { _testCaseList = value; }
			}

            
            [ReadOnly(true)]
            [Browsable(false)]
            public bool LoadedFromDatabase { get { return _loadedFromDatabase; } }
        #endregion


        #region Constructors 
        /// <summary> 
        /// Default constructor, instaniates an empty object
        /// </summary>         
        public Problem() { 
            this.InitializeNullableFieldsCollection();
        } 
        
        
        /// <summary> 
        /// Instantiates the object and populates the object with the database data for the specified recordId
        /// </summary>         
        /// <param name="recordId">Id field value to match</param>
        public Problem(int recordId) : this() { 
            LoadFromDatabase(recordId); 
        } 

        /// <summary> 
        /// Instantiates the object and populates the object with the database data for the specified recordId
        /// </summary>         
        /// <param name="recordId">Id field value to match</param>
        public Problem(DataRow dataItem) : this() { 
            this.LoadFromDatabaseRow(dataItem);
        } 
        #endregion             
     
        public override string ToString() {
            return "Problem #" + this.Id + " - " + this.Title;
        }

        #region Null Fields Handling Methods - These are required because some database types are value types in C# (and not nullable)				
        /// <summary>
        /// Set a nullable property to null.
        /// </summary>
        /// <param name="propertyName"></param>				
        public void SetPropertyNull(string propertyName) {
            if (this.IsNullableProperty(propertyName)) {
                _propertyNullStateDictionary[propertyName] = true;
            }
        }
        
        /// <summary>
        /// Checks if a nullable property is currently null.
        /// </summary>
        /// <param name="propertyName">String name of the property to check.</param>		
        /// <returns>Returns true is the property is currently set to null, otherwise returns false.</returns> 		
        public bool IsPropertyNull(string propertyName) {
            return _propertyNullStateDictionary[propertyName];
        }		
        
        /// <summary>
        /// Indicates if the property specified in propertyName represents 
        /// a nullable field in the database.
        /// </summary>
        /// <param name="propertyName">String name of the property to check.</param>		
        /// <returns>Returns true is the field is nullable in the database, otherwise returns false.</returns>  
        public bool IsNullableProperty(string propertyName) {
            bool isNullable = false;			
           			
            return isNullable;		
        }
        
        
        /// <summary>
        /// Initializes the items in the Dictionary that tracks the null state of the 
        /// object properties that represent nullable fields in the database.
        /// </summary>		
        protected void InitializeNullableFieldsCollection() {
            _propertyNullStateDictionary = new Dictionary<string, bool>();			
        }
        #endregion
        
        
        /// <summary>
        /// Populate the fields of the object from the database. This method will
        /// create a new CDatabase object for it's communication with the database.
        /// </summary>
        /// <param name="recordId">Id field value to match.</param>
        public void LoadFromDatabase(int recordId) {
            DataAccess dataAccess = new DataAccess();
            
            try {
				this.LoadFromDatabase(dataAccess, recordId);
				
			} finally {
				dataAccess.Close();
            }
        }


        /// <summary>
        /// Populate the fields of the object from an item in the database.
        /// </summary>
        /// <param name="recordId">Id field value to match.</param>
        /// <param name="dataAccess">DataAccess object to use to communicate with the database.</param>
        public void LoadFromDatabase(DataAccess dataAccess, int recordId) {
            string sqlStatement = @"
				select 
					main.id,
					main.title,
					main.settings,
					main.test_cases,
                    main.notes
				from problems main 
				where main.id="+ recordId.ToString() +@"
			";

            DataTable oData = dataAccess.Query(sqlStatement);    
            
            if (oData.Rows.Count==1) {
                //Reassign the row for readability
                DataRow oItem = oData.Rows[0];
                this.LoadFromDatabaseRow(oItem);
            }               
        }


        /// <summary>
        /// 
        /// </summary>    
        /// <param name="dataItem"></param>  
        protected void LoadFromDatabaseRow(DataRow dataItem) {
            //Load the data from the data row into the member variables
            this._id = (int)dataItem["id"];
			this._title = (string)dataItem["title"];
            this._notes = (string)dataItem["notes"];

            this._settings = Utilities.UnpackObjectXml<GepSettings>((string)dataItem["settings"]);                        
            this._testCaseList = Utilities.UnpackObjectXml<List<TestCase>>((string)dataItem["test_cases"]);
                        
            _loadedFromDatabase = true;
        }


        /// <summary>
        /// Checks the object's parameter values for validity. This method will throw an exception if  
        /// one of the parameter does not validate.
        /// </summary>        
        public void ValidateParameters() {
            
        }     
        
        
        #region Save Functions
         /// <summary>
        ///  Save the current settings to the database. This will instanitate a new CDataAcess object
        /// and use the new object to communicate with the database.
        /// </summary>
        public void Save() {
            DataAccess dataAccess = new DataAccess();
            this.Save(dataAccess);
            dataAccess.Close();
        }
        
        
        /// <summary>
        /// Save the current settings to the database using the supplied DataAccess object.
        /// </summary>
        /// <param name="dataAccess">DataAccess object to use to communicate with the database.</param>
        public void Save(DataAccess dataAccess) {
            string sqlStatement = "";

            //Check that the params are good
            this.ValidateParameters();

            //Build teh sql parameters
            List<System.Data.SqlClient.SqlParameter> parameters = new List<System.Data.SqlClient.SqlParameter>();
            parameters.Add(new System.Data.SqlClient.SqlParameter("@problemSettings", Utilities.PackObjectXml<GepSettings>(_settings)));
            parameters.Add(new System.Data.SqlClient.SqlParameter("@problemTests", Utilities.PackObjectXml<List<TestCase>>(_testCaseList)));

            //Check if the current record is a new record or an update
            if (!_loadedFromDatabase) {
                //New Record
                sqlStatement = @"
					insert into problems(
						title, settings, test_cases, notes
					) values (
						'"+ _title.Replace("'", "''") + @"', 
						@problemSettings,
                        @problemTests,
                        '"+ _notes +@"'
					)
				";

                dataAccess.ExecuteSQL(sqlStatement, parameters);
                this._id = dataAccess.GetLastIdentity("problems");
                
                _loadedFromDatabase = true;
            }
            else {
                //Update existing
                sqlStatement = @"
					update problems set
						title = '"+ _title.Replace("'", "''") +@"', 
                        settings = @problemSettings,
                        test_cases = @problemTests,
                        notes = '"+ _notes +@"'
                    where Id=" + this._id.ToString() + @"
				";

                dataAccess.ExecuteSQL(sqlStatement, parameters);
            }
        }
        
        
        /// <summary>
        /// Return a string containing 'null' if the null state is set for the requested propertyName,
        /// otherwise the method will return the supplied property value string.
        /// </summary>
        /// <param name="propertyName">String matching the name of one of the classes properties that can be null in the db.</param>
        /// <param name="propertyValue">String representation of the value of the property to use if it isn't null.</param>
        /// <returns>Returns the string representing the value the to be sent to the database for the requested nullable property.</returns>        
        protected string GetNullablePropertyValue(string propertyName, string propertyValue) {
            return _propertyNullStateDictionary[propertyName] ? "null" : propertyValue;
        }        
        #endregion
    } 
    #endregion // End of entity class region
}     