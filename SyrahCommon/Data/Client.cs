using System; 
using System.Collections.Generic; 
using System.Data;
using System.ComponentModel;

namespace NigelBrowne.Syrah.Common.Data { 
    #region Collection Class - This is used to create a collection of entity objects
    /// <summary> 
    /// Collection class for a collection of 'clients' table records and/or entities. 
    /// </summary> 
    public class ClientCollection {

        protected DataAccess _dataAccess = null;
        public DataAccess DbAccess {
            get { return _dataAccess; }
            set { _dataAccess = value; }
        }

        /// <summary> 
        /// Default constructor.
        /// </summary> 
        public ClientCollection() {
        }

        /// <summary> 
        /// Default constructor.
        /// </summary> 
        public ClientCollection(DataAccess dataAccess) {
			_dataAccess = dataAccess;
        }

        /// <summary> 
        /// Get all records from the clients table.
        /// </summary> 
        public DataTable GetTable() {
            return this.GetTable(null);
        }


        /// <summary> 
        /// Get a filtered set of records from the clients table.
        /// </summary> 
        /// <param name="filter">
        /// The ClientCollectionFilter filter object to use to build the where statement.
        /// </param>
        public DataTable GetTable(ClientCollectionFilter filter) {
            //Build the query to get the data
            string sql = BuildQuery(filter);           

            //Query the database
            DataTable returnData = _dataAccess.Query(sql);

            //Return the query results
            return returnData;
        }

        
        /// <summary> 
        /// Get a collection of all Client objects in the database.
        /// </summary> 
        public SortedList<string, Client> GetCollection() {
            return this.GetCollection(null);
        }


        /// <summary> 
        /// Get a filtered collection of Client objects
        /// </summary> 
        /// <param name="filter">
        /// The ClientCollectionFilter filter object to use to build the where statement.
        /// </param>
        public SortedList<string, Client> GetCollection(ClientCollectionFilter filter) {
            SortedList<string, Client> returnList = new SortedList<string, Client>();
            DataTable oData = this.GetTable(filter);

            //Iterate over each item in the data table, instantiate a Client object 
            //from it and then add it to our collection
            foreach(DataRow oRow in oData.Rows) {
                returnList.Add(oRow["name"].ToString(), new Client(oRow));
            }

            //Return the object collection that we've built
            return returnList;
        }


        /// <summary> 
        /// Build the sql query to retrieve the entities from the database.
        /// </summary> 
        /// <param name="filter">
        /// The ClientCollectionFilter filter object to use to build the where statement.
        /// </param>
        protected string BuildQuery(ClientCollectionFilter filter) {
            string sqlStatement = @"
				select 
					main.name,
					main.last_seen,
					main.islands,
					main.version,
					main.ip_address,
					main.processor_count,
					main.processor_details,
					main.operating_system,
					main.computer_name,
					main.os_memory,
                    main.current_problem_id,
                    p.title as problem_title
				from clients main 
                left join problems p on p.id=main.current_problem_id";
            string whereStatement = "";

            //If we've been given a filter object, then build the where statement
            if (filter!=null) {
                whereStatement = this.BuildWhereStatement(filter);
            }

            //Append the where statement to the select
            sqlStatement += whereStatement;

            //Return the query statement
            return sqlStatement;
        }


        /// <summary> 
        /// Build the where statement based on a filter.
        /// </summary> 
        /// <param name="filter">
        /// The ClientCollectionFilter filter object to use to build the where statement.
        /// </param>
        protected string BuildWhereStatement(ClientCollectionFilter filter) {
            string whereStatement = "";
            
			if (filter.IsNameSet) {
				whereStatement += " and name='"+ filter.Name +"' ";
			}
			if (filter.IsLastSeenStartSet) {
				whereStatement += " and last_seen>='"+ filter.LastSeenStart.ToString(DataAccess.NativeDateFormat) +"' ";
			}
			if (filter.IsLastSeenEndSet) {
				whereStatement += " and last_seen<='"+ filter.LastSeenEnd.ToString(DataAccess.NativeDateFormat) +"' ";
			}
			if (filter.IsIslandsSet) {
				whereStatement += " and islands="+ filter.Islands.ToString() +" ";
			}
			if (filter.IsVersionSet) {
				whereStatement += " and version='"+ filter.Version +"' ";
			}
			if (filter.IsIpAddressSet) {
				whereStatement += " and ip_address='"+ filter.IpAddress +"' ";
			}
			if (filter.IsProcessorCountSet) {
				whereStatement += " and processor_count="+ filter.ProcessorCount.ToString() +" ";
			}
			if (filter.IsProcessorDetailsSet) {
				whereStatement += " and processor_details='"+ filter.ProcessorDetails +"' ";
			}
			if (filter.IsOperatingSystemSet) {
				whereStatement += " and operating_system='"+ filter.OperatingSystem +"' ";
			}
			if (filter.IsComputerNameSet) {
				whereStatement += " and computer_name='"+ filter.ComputerName +"' ";
			}
			if (filter.IsOsMemorySet) {
				whereStatement += " and os_memory="+ filter.OsMemory.ToString() +" ";
			}

            //If there are items in the where statement, finsh it
            if (whereStatement.Length>0) {
                whereStatement = " where "+ whereStatement.Substring(4).Trim();
            }	

            //Return the where statement we've built
            return whereStatement;
        }
    }
    #endregion

    #region Filter Class - This is used to filter a ClientCollection object
    /// <summary> 
    /// A class for filtering a ClientCollection object
    /// </summary> 
    public class ClientCollectionFilter {
        
		protected string _name = "";
		protected bool _isNameSet = false;
		protected DateTime _lastSeenStart = DateTime.MinValue;
		protected bool _isLastSeenStartSet = false;
		protected DateTime _lastSeenEnd = DateTime.MinValue;
		protected bool _isLastSeenEndSet = false;
		protected int _islands = int.MinValue;
		protected bool _isIslandsSet = false;
		protected string _version = "";
		protected bool _isVersionSet = false;
		protected string _ipAddress = "";
		protected bool _isIpAddressSet = false;
		protected int _processorCount = int.MinValue;
		protected bool _isProcessorCountSet = false;
		protected string _processorDetails = "";
		protected bool _isProcessorDetailsSet = false;
		protected string _operatingSystem = "";
		protected bool _isOperatingSystemSet = false;
		protected string _computerName = "";
		protected bool _isComputerNameSet = false;
		protected int _osMemory = int.MinValue;
		protected bool _isOsMemorySet = false;

        
		public string Name {
			get { return _name; }
			set { _name=value; _isNameSet=true; }
		}
		public bool IsNameSet {
			get { return _isNameSet; }
		}

		public DateTime LastSeenStart {
			get { return _lastSeenStart; }
			set { _lastSeenStart=value; _isLastSeenStartSet=true; }
		}
		public bool IsLastSeenStartSet {
			get { return _isLastSeenStartSet; }
		}

		public DateTime LastSeenEnd {
			get { return _lastSeenEnd; }
			set { _lastSeenEnd=value; _isLastSeenEndSet=true; }
		}
		public bool IsLastSeenEndSet {
			get { return _isLastSeenEndSet; }
		}

		public int Islands {
			get { return _islands; }
			set { _islands=value; _isIslandsSet=true; }
		}
		public bool IsIslandsSet {
			get { return _isIslandsSet; }
		}

		public string Version {
			get { return _version; }
			set { _version=value; _isVersionSet=true; }
		}
		public bool IsVersionSet {
			get { return _isVersionSet; }
		}

		public string IpAddress {
			get { return _ipAddress; }
			set { _ipAddress=value; _isIpAddressSet=true; }
		}
		public bool IsIpAddressSet {
			get { return _isIpAddressSet; }
		}

		public int ProcessorCount {
			get { return _processorCount; }
			set { _processorCount=value; _isProcessorCountSet=true; }
		}
		public bool IsProcessorCountSet {
			get { return _isProcessorCountSet; }
		}

		public string ProcessorDetails {
			get { return _processorDetails; }
			set { _processorDetails=value; _isProcessorDetailsSet=true; }
		}
		public bool IsProcessorDetailsSet {
			get { return _isProcessorDetailsSet; }
		}

		public string OperatingSystem {
			get { return _operatingSystem; }
			set { _operatingSystem=value; _isOperatingSystemSet=true; }
		}
		public bool IsOperatingSystemSet {
			get { return _isOperatingSystemSet; }
		}

		public string ComputerName {
			get { return _computerName; }
			set { _computerName=value; _isComputerNameSet=true; }
		}
		public bool IsComputerNameSet {
			get { return _isComputerNameSet; }
		}

		public int OsMemory {
			get { return _osMemory; }
			set { _osMemory=value; _isOsMemorySet=true; }
		}
		public bool IsOsMemorySet {
			get { return _isOsMemorySet; }
		}


        /// <summary> 
        /// Instantiate an empty ClientCollectionFilter object
        /// </summary> 
        public ClientCollectionFilter() {
        }
    }
    #endregion

    #region Entity Class - This is used to represent the clients table in the database.
    /// <summary> 
    /// Table interface class for the 'clients' table. 
    /// </summary> 
    public class Client {         		

        #region Member Variables
            protected Dictionary<string, bool> _propertyNullStateDictionary;	
            protected bool _loadedFromDatabase = false;		
            
			protected string _name; 
			protected DateTime _lastSeen; 
			protected int _islands; 
			protected string _version; 
			protected string _ipAddress; 
			protected int _processorCount; 
			protected string _processorDetails; 
			protected string _operatingSystem; 
			protected string _computerName; 
			protected int _osMemory;
            protected int _currentProblemId;
            protected string _currentProblemTitle;

        #endregion

        #region Properties
            
			public string Name {
				get { return _name; }
				set { _name=value; }
			}
			public DateTime LastSeen {
				get { return _lastSeen; }
				set { _lastSeen=value; }
			}
			public int Islands {
				get { return _islands; }
				set { _islands=value; }
			}
			public string Version {
				get { return _version; }
				set { _version=value; }
			}
			public string IpAddress {
				get { return _ipAddress; }
				set { _ipAddress=value; }
			}
			public int ProcessorCount {
				get { return _processorCount; }
				set { _processorCount=value; }
			}
			public string ProcessorDetails {
				get { return _processorDetails; }
				set { _processorDetails=value; }
			}
			public string OperatingSystem {
				get { return _operatingSystem; }
				set { _operatingSystem=value; }
			}
			public string ComputerName {
				get { return _computerName; }
				set { _computerName=value; }
			}
			public int OsMemory {
				get { return _osMemory; }
				set { _osMemory=value; }
			}
            public string CurrentProblemTitle {
                get { return _currentProblemTitle; }
                set { _currentProblemTitle  = value; }
            }
            public int CurrentProblemId {
                get { return _currentProblemId; }
                set { _currentProblemId = value; }
            }


            [ReadOnly(true)]
            [Browsable(false)]
            public bool LoadedFromDatabase { get { return _loadedFromDatabase; } }
        #endregion


        #region Constructors 
        /// <summary> 
        /// Default constructor, instaniates an empty object
        /// </summary>         
        public Client() { 
            this.InitializeNullableFieldsCollection();		
        } 
        
        
        /// <summary> 
        /// Instantiates the object and populates the object with the database data for the specified recordId
        /// </summary>         
        /// <param name="recordId">Id field value to match</param>
        public Client(string recordId) : this() { 
            LoadFromDatabase(recordId); 
        } 

        /// <summary> 
        /// Instantiates the object and populates the object with the database data for the specified recordId
        /// </summary>         
        /// <param name="recordId">Id field value to match</param>
        public Client(DataRow dataItem) : this() { 
            this.LoadFromDatabaseRow(dataItem);
        } 
        #endregion             
     
        #region Null Fields Handling Methods - These are required because some database types are value types in C# (and not nullable)				
        /// <summary>
        /// Set a nullable property to null.
        /// </summary>
        /// <param name="propertyName"></param>				
        public void SetPropertyNull(string propertyName) {
            if (this.IsNullableProperty(propertyName)) {
                _propertyNullStateDictionary[propertyName] = true;
            }
        }
        
        /// <summary>
        /// Checks if a nullable property is currently null.
        /// </summary>
        /// <param name="propertyName">String name of the property to check.</param>		
        /// <returns>Returns true is the property is currently set to null, otherwise returns false.</returns> 		
        public bool IsPropertyNull(string propertyName) {
            return _propertyNullStateDictionary[propertyName];
        }		
        
        /// <summary>
        /// Indicates if the property specified in propertyName represents 
        /// a nullable field in the database.
        /// </summary>
        /// <param name="propertyName">String name of the property to check.</param>		
        /// <returns>Returns true is the field is nullable in the database, otherwise returns false.</returns>  
        public bool IsNullableProperty(string propertyName) {
            bool isNullable = false;

            isNullable |= propertyName.Trim().ToLower() == "currentproblemid";
			
            return isNullable;		
        }
        
        
        /// <summary>
        /// Initializes the items in the Dictionary that tracks the null state of the 
        /// object properties that represent nullable fields in the database.
        /// </summary>		
        protected void InitializeNullableFieldsCollection() {
            _propertyNullStateDictionary = new Dictionary<string, bool>();
            _propertyNullStateDictionary.Add("CurrentProblemId", true);

        }
        #endregion
        
        
        /// <summary>
        /// Populate the fields of the object from the database. This method will
        /// create a new CDatabase object for it's communication with the database.
        /// </summary>
        /// <param name="recordId">Id field value to match.</param>
        public void LoadFromDatabase(string recordId) {
            DataAccess dataAccess = new DataAccess();
            
            try {
				this.LoadFromDatabase(dataAccess, recordId);
				
			} finally {
				dataAccess.Close();
            }
        }


        /// <summary>
        /// Populate the fields of the object from an item in the database.
        /// </summary>
        /// <param name="recordId">Id field value to match.</param>
        /// <param name="dataAccess">DataAccess object to use to communicate with the database.</param>
        public void LoadFromDatabase(DataAccess dataAccess, string recordId) {
            string sqlStatement = @"
				select 
					main.name,
					main.last_seen,
					main.islands,
					main.version,
					main.ip_address,
					main.processor_count,
					main.processor_details,
					main.operating_system,
					main.computer_name,
					main.os_memory,
                    main.current_problem_id,
                    p.title as problem_title
				from clients main 
                left join problems p on p.id=main.current_problem_id
				where main.name='"+ recordId +@"'
			";

            DataTable oData = dataAccess.Query(sqlStatement);    
            
            if (oData.Rows.Count==1) {
                //Reassign the row for readability
                DataRow oItem = oData.Rows[0];
                this.LoadFromDatabaseRow(oItem);
            }               
        }


        /// <summary>
        /// 
        /// </summary>    
        /// <param name="dataItem"></param>  
        protected void LoadFromDatabaseRow(DataRow dataItem) {
            //Load the data from the data row into the member variables
            this._name = (string)dataItem["name"];
			this._lastSeen = DateTime.Parse(dataItem["last_seen"].ToString());
			this._islands = (byte)dataItem["islands"];
			this._version = (string)dataItem["version"];
			this._ipAddress = (string)dataItem["ip_address"];
			this._processorCount = (int)dataItem["processor_count"];
			this._processorDetails = (string)dataItem["processor_details"];
			this._operatingSystem = (string)dataItem["operating_system"];
			this._computerName = (string)dataItem["computer_name"];
			this._osMemory = (int)dataItem["os_memory"];

            if (!dataItem["current_problem_id"].Equals(System.DBNull.Value)) this._currentProblemId = (int)dataItem["current_problem_id"];
            if (!dataItem["problem_title"].Equals(System.DBNull.Value)) this._currentProblemTitle = (string)dataItem["problem_title"];

            _loadedFromDatabase = true;
        }


        /// <summary>
        /// Checks the object's parameter values for validity. This method will throw an exception if  
        /// one of the parameter does not validate.
        /// </summary>        
        public void ValidateParameters() {
            
        }     
        
        
        #region Save Functions
         /// <summary>
        ///  Save the current settings to the database. This will instanitate a new CDataAcess object
        /// and use the new object to communicate with the database.
        /// </summary>
        public void Save() {
            DataAccess dataAccess = new DataAccess();
            this.Save(dataAccess);
            dataAccess.Close();
        }
        
        
        /// <summary>
        /// Save the current settings to the database using the supplied DataAccess object.
        /// </summary>
        /// <param name="dataAccess">DataAccess object to use to communicate with the database.</param>
        public void Save(DataAccess dataAccess) {
            string sqlStatement = "";

            //Check that the params are good
            this.ValidateParameters();

            //Check if the current record is a new record or an update
            if (!_loadedFromDatabase) {
                //New Record
                sqlStatement = @"
					insert into clients(
						name, last_seen, islands, version, ip_address, processor_count, processor_details, operating_system, computer_name, os_memory, current_problem_id
					) values (
						'"+ _name.Replace("'", "''") +@"', 
						'"+ _lastSeen.ToString(DataAccess.NativeDateFormat) +@"', 
						"+ _islands.ToString() +@", 
						'"+ _version.Replace("'", "''") +@"', 
						'"+ _ipAddress.Replace("'", "''") +@"', 
						"+ _processorCount.ToString() +@", 
						'"+ _processorDetails.Replace("'", "''") +@"', 
						'"+ _operatingSystem.Replace("'", "''") +@"', 
						'"+ _computerName.Replace("'", "''") +@"', 
						"+ _osMemory.ToString() +@",
                        " + this.GetNullablePropertyValue("CurrentProblemId", _currentProblemId.ToString()) + @"
					)
				";

                dataAccess.ExecuteSQL(sqlStatement);
                
                
                _loadedFromDatabase = true;
            }
            else {
                //Update existing
                sqlStatement = @"
					update clients set
						name = '"+ _name.Replace("'", "''") +@"', 
						last_seen = '"+ _lastSeen.ToString(DataAccess.NativeDateFormat) +@"', 
						islands = "+ _islands.ToString() +@", 
						version = '"+ _version.Replace("'", "''") +@"', 
						ip_address = '"+ _ipAddress.Replace("'", "''") +@"', 
						processor_count = "+ _processorCount.ToString() +@", 
						processor_details = '"+ _processorDetails.Replace("'", "''") +@"', 
						operating_system = '"+ _operatingSystem.Replace("'", "''") +@"', 
						computer_name = '"+ _computerName.Replace("'", "''") +@"', 
						os_memory = "+ _osMemory.ToString() +@",
                        current_problem_id = " + this.GetNullablePropertyValue("CurrentProblemId",  _currentProblemId.ToString()) + @"
					where Name='"+ this._name +@"'
				";                

                dataAccess.ExecuteSQL(sqlStatement);
            }
        }
        
        
        /// <summary>
        /// Return a string containing 'null' if the null state is set for the requested propertyName,
        /// otherwise the method will return the supplied property value string.
        /// </summary>
        /// <param name="propertyName">String matching the name of one of the classes properties that can be null in the db.</param>
        /// <param name="propertyValue">String representation of the value of the property to use if it isn't null.</param>
        /// <returns>Returns the string representing the value the to be sent to the database for the requested nullable property.</returns>        
        protected string GetNullablePropertyValue(string propertyName, string propertyValue) {
            return _propertyNullStateDictionary[propertyName] ? "null" : propertyValue;
        }        
        #endregion
    } 
    #endregion // End of entity class region
}     