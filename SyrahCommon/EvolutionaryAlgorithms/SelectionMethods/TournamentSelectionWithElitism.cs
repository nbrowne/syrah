﻿/***********************************************************************
 * TournamentSelectionWithElitism.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Implements the roulette wheel selection with elitism. The clone of the
 * elite chromosome (best fitness) is added first, into position 0 of
 * the population array.
 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.SelectionMethods {
    public class TournamentSelectionWithElitism : ISelectionMethod {
        protected int _tounamentSize = 2;

        public int TournamentSize {
            get { return _tounamentSize; }
            set { _tounamentSize = value; }
        }


        /// <summary>
        /// Default constructor
        /// </summary>
        public TournamentSelectionWithElitism() {
        }


        /// <summary>
        /// Initialize class with a defined tournament size
        /// </summary>
        /// <param name="tournamentSize"></param>
        public TournamentSelectionWithElitism(int tournamentSize) {
            _tounamentSize = tournamentSize;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourcePopulation"></param>
        /// <param name="newPopulationSize"></param>
        /// <returns></returns>
        public List<Chromosome> ApplySelection(List<Chromosome> sourcePopulation, int newPopulationSize) {
            double maxFitness = double.MinValue;
            int indexOfBest = -1;
            int bestPlayerIndex = -1;
            double bestPlayerFitness = -1;
            int playerIndex = 0;
            double playerFitness = 0;
            int populationIndex = 0;
            int tournamentIndex = 0;

            //Instantiate the return list
            List<Chromosome> newPopulation = new List<Chromosome>(newPopulationSize);

            //Calculate the fitness sum and the index of the best individual.
            for (populationIndex = 0; populationIndex < sourcePopulation.Count; populationIndex++) {
                //Check if this is the best individual
                if (sourcePopulation[populationIndex].Fitness > maxFitness || maxFitness == double.MinValue) {
                    maxFitness = sourcePopulation[populationIndex].Fitness;
                    indexOfBest = populationIndex;
                }
            }

            //Add the best
            newPopulation.Add(sourcePopulation[indexOfBest]);


            //Run tournaments with random players until we fill the new population. Index starting at one since
            //we've already added an individual.
            for (populationIndex = 1; populationIndex < newPopulationSize; populationIndex++) {
                bestPlayerIndex = -1;

                //Add the players to the player list
                for (tournamentIndex = 0; tournamentIndex < _tounamentSize; tournamentIndex++) {
                    //Select a random player
                    playerIndex = Utilities.RandomInteger(0, sourcePopulation.Count - 1);
                    playerFitness = sourcePopulation[playerIndex].Fitness;

                    //If the current player is the best so far in this tournament record
                    if (bestPlayerIndex == -1 || playerFitness > bestPlayerFitness) {
                        bestPlayerFitness = playerFitness;
                        bestPlayerIndex = playerIndex;
                    }
                }

                //Add the best player from the tournament
                newPopulation.Add(sourcePopulation[bestPlayerIndex].Copy());
            }


            return newPopulation;
        } //End of ApplySeleciton
    }
}
