﻿/***********************************************************************
 * RouletteWheelWithElitism.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Implements the roulette wheel selection with elitism. The clone of the
 * elite chromosome (best fitness) is added first, into position 0 of
 * the population array.
 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.SelectionMethods {
    public class RouletteWheelWithElitism : ISelectionMethod {
        /// <summary>
        /// Default constructor
        /// </summary>
        public RouletteWheelWithElitism() {
        }


        /// <summary>
        /// Selects an individual from the population based on proportional fitness and returns its index.
        /// The best individual from the sourcePopulation is added first (index 0), then the remaining 
        /// population is selected.
        /// </summary>
        /// <remarks>
        /// This algorithm uses a linear search for finding the indivdual. Not the most effective. The protected
        /// method below is more complicated, but uses a binary search to find the individual. Look into using this later.
        /// </remarks>
        /// <param name="sourcePopulation"></param>
        /// <param name="newPopulationSize"></param>
        /// <returns></returns>
        public List<Chromosome> ApplySelection(List<Chromosome> sourcePopulation, int newPopulationSize) {
            double fitnessSum = 0;
            double maxFitness = double.MinValue;
            int indexOfBest = -1;
            double lineSegmentMax = 0;

            //Instantiate the return list
            List<Chromosome> newPopulation = new List<Chromosome>(newPopulationSize);

            //Calculate the fitness sum and the index of the best individual.
            for(int i=0; i< sourcePopulation.Count; i++) {
                fitnessSum += sourcePopulation[i].Fitness;

                //Check if this is the best individual
                if (sourcePopulation[i].Fitness > maxFitness || maxFitness == double.MinValue) {
                    maxFitness = sourcePopulation[i].Fitness;
                    indexOfBest = i;
                }
            }

            if (fitnessSum == 0) {
                throw new Exceptions.SyrahGeneralException("The fitness sum in RouletteWheelWithElitism.ApplySelection() is zero. This shouldn't happen.");
            }

            //Iterate over the population and set the proportional fitness, this maps the 
            //chromosome's fitness onto a [0,1] line segment
            for (int chromosomeIndex = 0; chromosomeIndex < newPopulationSize; chromosomeIndex++) {
                lineSegmentMax += sourcePopulation[chromosomeIndex].Fitness / fitnessSum;
                sourcePopulation[chromosomeIndex].ProportionalFitness = lineSegmentMax;
            }

            //Add the best
            newPopulation.Add(sourcePopulation[indexOfBest]);

            //Spin the roulette wheel until we fill the new population. Index starting at one since
            //we've already added an individual.
            for (int populationIndex = 1; populationIndex < newPopulationSize; populationIndex++) {
                ///Get the value [0, 1] to select the new individual from. We always use [0, 1] because
                ///0 is the start of our line segment and the last individual on line is set to 
                ///lineSegmentMax / fitnessSum, which for the last individual is always one.
                double rouletteValue = Utilities.RandomDoubleRange(0, 1);

                //Find the indivdual and add it
                for (int i = 0; i < sourcePopulation.Count; i++) {
                    if (rouletteValue <= sourcePopulation[i].ProportionalFitness) {
                        //Add the individual to the new population.
                        newPopulation.Add(sourcePopulation[i].Copy());
                        break;
                    }
                }
            }

            //Return the newly selected population
            return newPopulation;
        }


        /// <summary>
        /// Selects an individual from the population based on proportional fitness and returns its index.
        /// </summary>
        /// <remarks>
        /// This method uses a binary search to search for the individual and assumes that the population is sorted by
        /// fitness.
        /// 
        /// This method is not currently used by the selection algorithm.
        /// </remarks>
        /// <param name="fitnessProportion">The target fitness proportion to search for.</param>
        /// <param name="population">The sorted population list to search.</param>
        /// <returns></returns>
        protected int SelectIndividualFromPopulation(double fitnessProportion, List<Chromosome> population) {
            int leftIndex = 0;
            int rightIndex = population.Count - 1;
            int target = 0;

            //Initialize the return to the zero index, this should be the most fit individual, incase we 
            //screw up and can't find the correct index.
            int returnIndex = 0;

            //While the interval is non-empty
            while (leftIndex <= rightIndex) {
                //Get the midpoint target index
                target = (leftIndex + rightIndex) / 2;

                //Load the fitness proportions into friendly locals
                double upperFitnessProportion = population[target].ProportionalFitness;
                double lowerFitnessProportion = (target > 0 ? population[target - 1].ProportionalFitness : -0.1);

                //Test if we were fortunate and hit the correct index
                if (lowerFitnessProportion < fitnessProportion && fitnessProportion <= upperFitnessProportion) {
                    returnIndex = target;
                    break;

                }

                //Check if the target is to the right or the left and update the limits
                if (fitnessProportion > upperFitnessProportion) {
                    leftIndex = target + 1;
                } else {
                    rightIndex = target - 1;
                }
            }

            return returnIndex;
        }
    }
}
