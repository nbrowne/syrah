﻿/***********************************************************************
 * SelectionMethodBase.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.

 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.SelectionMethods {
    public interface ISelectionMethod {
        List<Chromosome> ApplySelection(List<Chromosome> sourcePopulation, int newPopulationSize);
    }
}
