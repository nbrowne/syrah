﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    [Serializable]
    public class Gene : IXmlSerializable {
        public readonly int CLASS_VERSION = 2;

        [NonSerialized]
        protected Phenotype _genePhenoType;

        public int TailLength {
            get { return this.GeneCodons.Count - this.HeadLength; }
        }

        public bool UseRnc { get; set; }

        public int HeadLength { get; set; }
        public int MaxArity { get; set; }
        
        
        public Phenotype GenePhenotype {
            get { return _genePhenoType; }
            set { _genePhenoType = value; }
        }

        public bool IsPhenotypeBuilt { get; set; }

        /// <summary>
        /// Get or set the gene codons
        /// </summary>
        public List<Codon> GeneCodons { get; set; }

        /// <summary>
        /// Get or set the Dc domain codons
        /// </summary>
        public List<Codon> DcDomain { get; set; }

        /// <summary>
        /// Get and set the RNC values.
        /// </summary>
        public List<double> RandomNumericalConstants { get; set; }


        /// <summary>
        /// Gets the length of the normal portion of the gene, the head and tail.
        /// </summary>
        public int Length { get { return this.GeneCodons.Count; } }

        public int TotalLength { 
            get { return this.GeneCodons.Count + this.DcDomain.Count; }
        }

        public Codon this[int index] {
            get { return this.GeneCodons[index]; }
            set {
                this.ResetPhenotype();

                this.GeneCodons[index] = value;
            }
        }

        public Gene() {
            this.IsPhenotypeBuilt = false;
        }


        public Gene(int maxArity, int headLength, bool useRnc, int numberOfRncConstants) {            
            this.HeadLength = headLength;
            this.MaxArity = maxArity;
            this.UseRnc = useRnc;

            this.IsPhenotypeBuilt = false;
            this.GenePhenotype = new Phenotype();                        
            this.GeneCodons = new List<Codon>();
            this.DcDomain = new List<Codon>();

            int geneLength = this.HeadLength + GepAlgorithm.CalculateTailLength(this.HeadLength, this.MaxArity);

            //Initialize an empty gene
            for (int i = 0; i < geneLength; i++) {
                GeneCodons.Add(new Codon());
            }

            //If the rnc is used, initialize the Dc Domain
            if (this.UseRnc) {
                //Initialize the dc domain
                for (int i = 0; i < this.TailLength; i++) {
                    this.DcDomain.Add(new Codon());
                }

                this.RandomNumericalConstants = new List<double>(numberOfRncConstants);

                //Add the uninitialized constants
                for (int i = 0; i < numberOfRncConstants; i++) {
                    this.RandomNumericalConstants.Add(0);
                }
            }
        }

        public List<Codon> CutEnd(int startIndex) {
            return this.CutRange(startIndex, this.GeneCodons.Count - startIndex);
        }


        public List<Codon> CutRange(int startIndex, int count) {
            List<Codon> cutList = new List<Codon>();

            //First copy the codons to the list
            for (int codonIndex = startIndex; codonIndex < startIndex + count; codonIndex++) {
                cutList.Add(this.GeneCodons[codonIndex]);
            }

            //Now remove the codons from the chromosome
            this.GeneCodons.RemoveRange(startIndex, count);

            //Return the list of genes cut from the chromosome.
            return cutList;
        }


        public void InsertCodons(int index, List<Codon> codons) {
            this.GeneCodons.InsertRange(index, codons);
        }

        public void AppendCodons(List<Codon> codons) {
            this.GeneCodons.AddRange(codons);
        }

        public void AppendCodons(Codon codon) {
            this.GeneCodons.Add(codon);
        }

        public List<Codon> CutDcEnd(int startIndex) {
            return this.CutDcRange(startIndex, this.DcDomain.Count - startIndex);
        }


        public List<Codon> CutDcRange(int startIndex, int count) {
            List<Codon> cutList = new List<Codon>();

            //First copy the codons to the list
            for (int codonIndex = startIndex; codonIndex < startIndex + count; codonIndex++) {
                cutList.Add(this.DcDomain[codonIndex]);
            }

            //Now remove the codons from the chromosome
            this.DcDomain.RemoveRange(startIndex, count);

            //Return the list of genes cut from the chromosome.
            return cutList;
        }


        public void InsertDcCodons(int index, List<Codon> codons) {
            this.DcDomain.InsertRange(index, codons);
        }

        public void AppendDcCodons(List<Codon> codons) {
            this.DcDomain.AddRange(codons);
        }

        public void AppendDcCodons(Codon codon) {
            this.DcDomain.Add(codon);
        }

        /// <summary>
        /// Gets the gene part at the index supplied. This behaves as if 
        /// the head is followed by the tail, followed immediately by the Dc domain
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public GeneParts GetGenePartAtIndex(int index) {
            GeneParts genePart = GeneParts.Unknown;

            if (index < this.HeadLength) {
                genePart = GeneParts.Head;
            } else if (index < this.Length) {
                genePart = GeneParts.Tail;
            } else if (index < this.TotalLength) {
                genePart = GeneParts.Dc;
            } else {
                throw new ArgumentOutOfRangeException("The supplied index is not in the gene.");
            }


            return genePart;
        }


        public void CopyTo(out Gene gene) {
            int rncConstants = this.RandomNumericalConstants != null ? this.RandomNumericalConstants.Count : 0;
            gene = new Gene(this.MaxArity, this.HeadLength, this.UseRnc, rncConstants);
            this.IsPhenotypeBuilt = false;
            this.GenePhenotype = new Phenotype();
            
            //Copy the codons
            for (int i = 0; i < this.Length; i++) {
                gene[i] = this[i].Copy();
            }

            //Copy the dc codons
            for (int i = 0; i < this.DcDomain.Count; i++) {
                gene.DcDomain[i] = this.DcDomain[i].Copy();
            }

            gene.RandomNumericalConstants = new List<double>(rncConstants);

            //Copy the rnc arrays            
            if (this.RandomNumericalConstants != null) {
                foreach (double rncConstant in this.RandomNumericalConstants) {
                    gene.RandomNumericalConstants.Add(rncConstant);
                }
            }
        }


        public Gene Copy() {
            Gene geneCopy = null;
            this.CopyTo(out geneCopy);
            return geneCopy;
        }


        /// <summary>
        /// Gets if the supplied index in in the gene's head
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool IsInHead(int index) {
            return index < this.HeadLength;
        }


        /// <summary>
        /// Get the length of the ORF of a gene in this chromosome.
        /// </summary>
        /// <param name="codonIndex"></param>
        /// <returns></returns>
        public int GetOpenReadFrameLength(int geneIndex) {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Reset the phenotype of the gene
        /// </summary>
        public void ResetPhenotype() {
            this.GenePhenotype = new Phenotype();
            this.IsPhenotypeBuilt = false;
        }


        public override string ToString() {
            string buffer = "";

            foreach (Codon codon in this.GeneCodons) {
                if (buffer.Length > 0) buffer += ".";
                buffer += codon.ToString();
            }

            if (this.UseRnc) {
                foreach (Codon codon in this.DcDomain) {
                    if (buffer.Length > 0) buffer += ".";
                    buffer += codon.ToString();
                }

                buffer += "[";
                foreach (double rncConstant in this.RandomNumericalConstants) {
                    if (buffer[buffer.Length - 1] != '[') buffer += ", ";
                    buffer += rncConstant.ToString();
                }
                buffer += "]";
            }

            return buffer;
        }

        /// <summary>
        /// Control how this class is serialized to xml.
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="writer"></param>
        public void WriteXml(XmlWriter writer) {
            //Add a version code to the test value
            writer.WriteAttributeString("XmlVersion", CLASS_VERSION.ToString());

            writer.WriteElementString("UseRnc", this.UseRnc.ToString());
            writer.WriteElementString("HeadLength", this.HeadLength.ToString());
            writer.WriteElementString("MaxArity", this.MaxArity.ToString());

            XmlUtilities.SerializeObject<List<Codon>>(writer, this.GeneCodons);
            XmlUtilities.SerializeObject<List<Codon>>(writer, this.DcDomain);
            XmlUtilities.SerializeObject<List<double>>(writer, this.RandomNumericalConstants);
        }


        /// <summary>
        /// Control how this class is deserialized from xml
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader) {
            //Read the version code of this testcase
            int version = Int32.Parse(reader.GetAttribute("XmlVersion"));

            //Read past the testcase container
            reader.Read();

            this.UseRnc = reader.ReadElementContentAsString().ToLower() == "true";
            this.HeadLength = Int32.Parse(reader.ReadElementContentAsString());
            this.MaxArity = Int32.Parse(reader.ReadElementContentAsString());

            if (version==1) 
                this.IsPhenotypeBuilt = reader.ReadElementContentAsString().ToLower() == "true";

            //read the codon list
            this.GeneCodons = XmlUtilities.DeserializeObject<List<Codon>>(reader);
            this.DcDomain = XmlUtilities.DeserializeObject<List<Codon>>(reader);
            this.RandomNumericalConstants = XmlUtilities.DeserializeObject<List<double>>(reader);

            //Read past the GepStatistics end tag, otherwise the deserialization process won't continue to the 
            //element.  
            reader.Read();
        }

        /// <summary>
        /// Returns a null schema, this method is required by the IXmlSerializable interface.
        /// </summary>
        /// <remarks>
        /// This method isn't actually required for what we're doing
        /// </remarks>
        /// <returns></returns>
        public XmlSchema GetSchema() {
            return (null);
        }
    } //End of gene class
}
