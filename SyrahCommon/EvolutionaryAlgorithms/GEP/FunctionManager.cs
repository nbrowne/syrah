﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.CodeDom;
using System.CodeDom.Compiler;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    public delegate double EvaluateDelegate(List<double> parameters);

    public enum FunctionType {
        Operator,
        Call
    }


    /// <summary>
    /// 
    /// </summary>
    public class FunctionDetail {
        public Codon FunctionCodon = null;
        public int Arity = 0;
        public string Description = "";
        public FunctionType Type = FunctionType.Operator;
        public string Code = "";
        public EvaluateDelegate Evaluate;
        public string FunctionName = "";

        public FunctionDetail(Codon codon, int arity, string description, FunctionType type, string functionName, string code, EvaluateDelegate eval) {
            this.FunctionCodon = codon;
            this.Arity = arity;
            this.Description = description;
            this.Type = type;
            this.Code = code;
            this.Evaluate = eval;
            this.FunctionName = functionName;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public  class FunctionManager {
        protected static Dictionary<Codon, FunctionDetail> _functionCollection;

        /// <summary>
        /// Get the number of functions stored in the function manager
        /// </summary>
        public static int Count { 
            get {
                return _functionCollection.Count; 
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codon"></param>
        /// <param name="function"></param>
        /// <returns></returns>
        public static bool TryGetFunction(Codon codon, out FunctionDetail function) {
            return _functionCollection.TryGetValue(codon, out function);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="function"></param>
        /// <returns></returns>
        public static FunctionDetail GetFunction(Codon function) {
            FunctionDetail value;
            return _functionCollection.TryGetValue(function, out value) ? value : null;
        }


        /// <summary>
        /// Returns a collection of the available functions.
        /// </summary>
        /// <returns></returns>
        public static CodonCollection GetAvailableFunctions() {
            CodonCollection returnCollection = new CodonCollection();

            foreach (Codon function in _functionCollection.Keys) {
                returnCollection.Add(function);
            }

            return returnCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        static FunctionManager() {
            _functionCollection = new Dictionary<Codon, FunctionDetail>();

            //Add the single operand mathmatical functions
            FunctionManager.AddFunction("Q", 1, "Square Root", FunctionType.Call, "Sqrt", "Math.Sqrt(p0);", delegate(List<double> p) { return Math.Sqrt(p[0]); });
            FunctionManager.AddFunction("E", 1, "Exponential", FunctionType.Call, "Exp", "Math.Exp(p0);", delegate(List<double> p) { return Math.Exp(p[0]); });
            FunctionManager.AddFunction("S", 1, "Sin", FunctionType.Call, "Sin", "Math.Sin(p0);", delegate(List<double> p) { return Math.Sin(p[0]); });
            FunctionManager.AddFunction("C", 1, "Cos", FunctionType.Call, "Cos", "Math.Cos(p0);", delegate(List<double> p) { return Math.Cos(p[0]); });
            FunctionManager.AddFunction("T", 1, "Tan", FunctionType.Call, "Tan", "Math.Tan(p0);", delegate(List<double> p) { return Math.Tan(p[0]); });
            FunctionManager.AddFunction("L", 1, "Log", FunctionType.Call, "Log", "Math.Log(p0);", delegate(List<double> p) { return Math.Log(p[0]); });
            FunctionManager.AddFunction("Fl", 1, "Floor", FunctionType.Call, "Floor", "Math.Floor(p0);", delegate(List<double> p) { return Math.Floor(p[0]); });
            FunctionManager.AddFunction("Ce", 1, "Ceiling", FunctionType.Call, "Ceiling", "Math.Ceiling(p0);", delegate(List<double> p) { return Math.Ceiling(p[0]); });

            //Add the single operand logical functions
            FunctionManager.AddFunction("N", 1, "Not function", FunctionType.Call, "Not", "p0 <= 0 ? 1 : 0;", delegate(List<double> p) { return p[0] <= 0 ? 1 : 0; });

            //Add the two operand operator mathmatical functions
            FunctionManager.AddFunction("+", 2, "Addition", FunctionType.Operator, "", "+", delegate(List<double> p) { return p[0] + p[1]; });
            FunctionManager.AddFunction("-", 2, "Subtraction", FunctionType.Operator, "", "-", delegate(List<double> p) { return p[0] - p[1]; });
            FunctionManager.AddFunction("*", 2, "Multiplication", FunctionType.Operator, "", "*", delegate(List<double> p) { return p[0] * p[1]; });
            FunctionManager.AddFunction("/", 2, "Protected Division", FunctionType.Call, "Div", "p1 != 0 ? p0 / p1 : 1;", delegate(List<double> p) { return p[1] != 0 ? p[0] / p[1] : 1; });

            //Add the two operand logical functions
            FunctionManager.AddFunction("O", 2, "Or", FunctionType.Call, "Or", "p0 > 0 || p1 > 0 ? 1 : 0;", delegate(List<double> p) { return p[0] > 0 || p[1] > 0 ? 1 : 0; });
            FunctionManager.AddFunction("A", 2, "An", FunctionType.Call, "And", "p0 > 0 && p1 > 0 ? 1 : 0;", delegate(List<double> p) { return p[0] > 0 && p[1] > 0 ? 1 : 0; });
            FunctionManager.AddFunction("<", 2, "Less than", FunctionType.Call, "Lt", "p0 < p1 ? p0 : p1;", delegate(List<double> p) { return p[0] < p[1] ? p[0] : p[1]; });
            FunctionManager.AddFunction(">", 2, "Greater than", FunctionType.Call, "Gt", "p0 > p1 ? p0 : p1;", delegate(List<double> p) { return p[0] > p[1] ? p[0] : p[1]; });
            FunctionManager.AddFunction("<=", 2, "Less than or equal", FunctionType.Call, "Lte", "p0 <= p1 ? p0 : p1;", delegate(List<double> p) { return p[0] <= p[1] ? p[0] : p[1]; });
            FunctionManager.AddFunction(">=", 2, "Greater than or equal", FunctionType.Call, "Gte", "p0 >= p1 ? p0 : p1;", delegate(List<double> p) { return p[0] >= p[1] ? p[0] : p[1]; });
            FunctionManager.AddFunction("==", 2, "Equal", FunctionType.Call, "Eq", "p0 == p1 ? p0 : p1;", delegate(List<double> p) { return p[0] == p[1] ? p[0] : p[1]; });
            FunctionManager.AddFunction("!=", 2, "Not equal", FunctionType.Call, "Ne", "p0 != p1 ? p0 : p1;", delegate(List<double> p) { return p[0] != p[1] ? p[0] : p[1]; });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="arity"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        /// <param name="code"></param>
        protected static void AddFunction(string symbol, int arity, string description, FunctionType type, string functionName, string code, EvaluateDelegate eval) {
            //Create the codon
            Codon codon = new Codon(symbol);

            //Create the function detail
            FunctionDetail function = new FunctionDetail(codon, arity, description, type, functionName, code, eval);

            //Add the entry to the dictionary
            _functionCollection.Add(codon, function);
        }
    }
}
