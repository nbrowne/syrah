﻿using System;
using System.Collections.Generic;

using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    public class ConfusionMatrix {
        public int TruePositive = 0;
        public int TrueNegative = 0;
        public int FalsePositive = 0;
        public int FalseNegative = 0;
    }
}
