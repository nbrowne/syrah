﻿using System;
using System.Collections.Generic;

using System.Text;
using System.ComponentModel;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    [Serializable]
    public class CodonCollection : List<Codon>, ICustomTypeDescriptor {
        public int Length { get { return this.Count; } }
        public string DisplayName {
            get { return this.ToString(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        public CodonCollection Union(CodonCollection second) {
            CodonCollection returnCollection = new CodonCollection();

            foreach (Codon codon in this) returnCollection.Add(codon);
            foreach (Codon codon in second) returnCollection.Add(codon);

            return returnCollection;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            StringBuilder collectionString = new StringBuilder("{");

            foreach (Codon codon in this) {
                if (collectionString.Length > 1) collectionString.Append(", ");
                collectionString.Append(codon.ToString());
            }

            collectionString.Append("}");

            return collectionString.ToString();
        }



        #region ICustomTypeDescriptor Members

        public String GetClassName() {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes() {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName() {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter() {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent() {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty() {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType) {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes) {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents() {
            return TypeDescriptor.GetEvents(this, true);
        }

        public object GetPropertyOwner(PropertyDescriptor pd) {
            return this;
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes) {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties() {
            // Create a new collection object PropertyDescriptorCollection
            PropertyDescriptorCollection pds = new PropertyDescriptorCollection(null);

            // Iterate the list of codons
            for (int i = 0; i < this.Count; i++) {
                // For each item, create a property descriptor and add it to the return
                CodonCollectionPropertyDescriptor pd = new CodonCollectionPropertyDescriptor(this, i);
                pds.Add(pd);
            }
            return pds;
        }


        #endregion
    }
}
