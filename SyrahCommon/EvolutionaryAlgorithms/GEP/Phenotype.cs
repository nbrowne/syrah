﻿/***********************************************************************
 * Phenotype.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Implements the phenotype of the GEP chromosome, so that it is simpler
 * and cleaner to interface with in the chromsome.
 ***********************************************************************/


using System;
using System.Collections.Generic;

using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    /// <summary>
    /// The Phenotype representation of the GEP Chromosome.
    /// </summary>
    /// <remarks>
    /// This class is essentially a wrapper to a 2D array 
    /// of codon objects. This is a faster way to execute
    /// the phenotype than building a "real" tree in
    /// memory. This technique was suggested by C. Ferriera
    /// on her website.
    /// </remarks>
    [Serializable]
    public class Phenotype : List<List<Codon>> {
        int _currentRow=-1;

        /// <summary>
        /// Add a new codon to the current phenotype row.
        /// </summary>
        /// <param name="codon"></param>
        public void AddToCurrent(Codon codon) {
            this[_currentRow].Add(codon);
        }

        /// <summary>
        /// And a new row to the phenotype representation.
        /// </summary>
        public void AddRow() {
            this.Add(new List<Codon>());
            _currentRow++;
        }

        /// <summary>
        /// Reset and empty the phenotype.
        /// </summary>
        public void Reset() {
            _currentRow = 0;
        }
    }
}
