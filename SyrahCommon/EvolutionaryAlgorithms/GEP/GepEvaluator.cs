﻿/***********************************************************************
 * GepEvaluator.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Handles the evaluation of the GEP chromosomes
 ***********************************************************************/


using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    /// <summary>
    /// Evaluates a GEP chromosome
    /// </summary>
    public class GepEvaluator {
        public GepEvaluator() {

        }


        /// <summary>
        /// The rounding threshold for classifier hits.
        /// If the output values is <= HitRoundingThreshold, then the output will round to
        /// zero (miss), otherwise it will round to 1 (hit).
        /// </summary>
        public readonly double HitRoundingThreshold = 0.5;


        protected GepSettings _settings = null;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="settings"></param>
        public GepEvaluator(GepSettings settings) {
            _settings = settings;

            ///todo: When Syrah supports multiple outputs, the GepEvaluator will need to be updated to remove this validation rule.
            //Validate that we're using static link functions OR homeotic genes.
            if (!GepEvaluator.IsParamterOptimizationProblem(_settings.FitnessMethod) && !_settings.UseStaticLinkingFunction && _settings.HomeoticGenes == 0) {
                throw new Exceptions.SyrahGeneralException("You must enable either a static linking function or homeotic genes (ADFs).");
            }

            //Validate that a linking funciton is used if it is enabled
            if (_settings.UseStaticLinkingFunction && _settings.StaticLinkingFunction == char.MinValue) {
                throw new Exceptions.SyrahGeneralException("Static linking functions are enabled, but no linking function is defined.");
            }
        }

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetArity(Codon codon) {
            FunctionDetail function;
            int functionArity = 0;
            
            if (FunctionManager.TryGetFunction(codon, out function)) {
                functionArity = function.Arity;
            }

            return functionArity;
        }

        /// <summary>
        /// Scans a list of function codons and returns the maximum arity 
        /// (number of required parameters) found.
        /// </summary>
        /// <param name="functionList"></param>
        /// <returns></returns>
        public static int GetMaximumArity(CodonCollection functionList) {
            int maxArity = 0;

            foreach (Codon functionCodon in functionList) {
                int functionArity = GepEvaluator.GetArity(functionCodon);

                if (functionArity > maxArity) maxArity = functionArity;
            }

            return maxArity;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chromosome"></param>
        /// <returns></returns>
        public string EvaluateChromosomeToText(Chromosome chromosome, List<TestCase> testCases) {
            string buffer = "";
            this.EvaluateChromosome(chromosome, testCases, ref buffer);

            return buffer;
        }

        public double EvaluateSingleTestCase(Chromosome chromosome, TestCase testCase, GepSettings settings) {
            double chromosomeValue = 0;
            List<double> geneValues = null;
            List<Codon> terminals = new List<Codon>();

            //Build the chromosome's phenotype
            chromosome.BuildPhenotype();

            //Get the gene results
            this.EvaluateGenePhenotypes(chromosome, testCase.TerminalValues, out geneValues);

            //todo: Implement other evaluations. Currently only static linking function for symbolic regression is implemented.

            //If there are no homeotic genes, then use a static linking function to link the genes together
            if (settings.UseStaticLinkingFunction && settings.HomeoticGenes == 0) {
                //Initialize to the first value in the gene list
                chromosomeValue = geneValues[0];

                //todo: Implement other static linking functions. Currently on addition, subtraction and multiplication are done.
                //Iterate over the remaining gene values, linking them together
                for (int i = 1; i < geneValues.Count; i++) {
                    switch (_settings.StaticLinkingFunction) {
                        case '+':
                            chromosomeValue += geneValues[i];
                            break;

                        case '-':
                            chromosomeValue -= geneValues[i];
                            break;

                        case '*':
                            chromosomeValue *= geneValues[i];
                            break;
                    }
                }

                if (settings.FitnessMethod == FitnessEvaluationMethod.HitsWithPenalty) {
                    //Determine if the chromosome value is in the class or not.
                    bool chromosomeInClass = chromosomeValue >= this.HitRoundingThreshold;

                    chromosomeValue = chromosomeInClass ? 1.0 : 0.0;
                }

            } //End of static linking handler

            return chromosomeValue;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <remarks>We will maximize all optimization functions, to make fitness evaluation easier.</remarks>
        /// <returns></returns>
        protected IParameterOptimizationFunction GetFunctionEvaluatorByMethod(FitnessEvaluationMethod method) {
            IParameterOptimizationFunction function = null;

            switch (method) {
                case FitnessEvaluationMethod.PoFerreira84:
                    function = new Ferreira84();
                    break;

                case FitnessEvaluationMethod.PoFerreira87:
                    function = new Ferreira87();
                    break;

                case FitnessEvaluationMethod.PoFerreira88:
                    function = new Ferreira88();
                    break;

                case FitnessEvaluationMethod.DeJong1:
                    function = new DeJong1();
                    break;

                case FitnessEvaluationMethod.DeJong2:
                    function = new DeJong2();
                    break;

                case FitnessEvaluationMethod.DeJong3:
                    function = new DeJong3();
                    break;

                case FitnessEvaluationMethod.DeJong4:
                    function = new DeJong4();
                    break;

                case FitnessEvaluationMethod.DeJong5:
                    function = new DeJong5();
                    break;

                default:
                    throw new ArgumentOutOfRangeException("Invalid fitness evaluation method requested.");
            }

            return function;
        }


        /// <summary>
        /// Returns if the current fitness method is a parameter optimization problem
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static bool IsParamterOptimizationProblem(FitnessEvaluationMethod method) {
            return method >= FitnessEvaluationMethod.PoStart && method <= FitnessEvaluationMethod.PoEnd;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chromosome"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        public string EvaluateChromosomeForParameterOptimizationToText(Chromosome chromosome, FitnessEvaluationMethod method) {
            string buffer = "";

            this.EvaluateChromosomeForParameterOptimization(chromosome, method, ref buffer);
            return buffer;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="chromosome"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        public double EvaluateChromosomeForParameterOptimization(Chromosome chromosome, FitnessEvaluationMethod method) {
            string buffer = "";
            return this.EvaluateChromosomeForParameterOptimization(chromosome, method, ref buffer);
        }
        public double EvaluateChromosomeForParameterOptimization(Chromosome chromosome, FitnessEvaluationMethod method, ref string textResults ) {
            double result = 0;
            List<double> geneValues = null;

            //Validate the fitness method
            if (!IsParamterOptimizationProblem(method)) 
                throw new ArgumentOutOfRangeException("Fitness method, '" + method + "', is not a parameter optimization method.");            

            //Get the function
            IParameterOptimizationFunction function = this.GetFunctionEvaluatorByMethod(method);

            //Validate the gene count vs parameters
            if (chromosome.GeneCount != function.GetParameterCount()) {
                throw new ArgumentOutOfRangeException("Invalid number of genes for parameter optimization problems. Found " + chromosome.GeneCount.ToString() + " but require " + function.GetParameterCount().ToString() + ".");
            }

            //Build the chromosome's phenotype
            chromosome.BuildPhenotype();

            ///Get the gene results
            ///An empty dictionary is passed as the terminals, because there are no terminals
            ///in parameter optimization
            this.EvaluateGenePhenotypes(chromosome, new Dictionary<Codon, double>(), out geneValues);

            //Only evaluate if the parameters are in the range
            if (function.AreParametersInRange(geneValues)) {
                //Get the results of the current parameter values
                result = function.Evaluate(geneValues);
                textResults = result.ToString();
            } else {
                result = 0;
                textResults = "-";
            }

            //Finish building the text results
            textResults += " = f(";
            foreach (double value in geneValues) {
                textResults += value.ToString();
                if (geneValues.IndexOf(value) < geneValues.Count - 1) textResults += ", ";
            }
            textResults += ")";

            //Return the fitness measure
            return result;
        }

        /// <summary>
        /// Evaluate a chromosome and return its fitness. This method will also set the 
        /// Chromosome.Fitness property.
        /// </summary>
        /// <remarks>
        /// Verified that this functions correctly on Sept 18, 2008 at 1300.
        /// </remarks>
        /// <param name="chromosome"></param>
        /// <returns></returns>
        public double EvaluateChromosome(Chromosome chromosome, List<TestCase> testCases) {
            string buffer = " ";
            return this.EvaluateChromosome(chromosome, testCases, ref buffer);
        }
        public double EvaluateChromosome(Chromosome chromosome, List<TestCase> testCases, ref string textResults) {
            double chromosomeFitness = 0;
            List<double> geneValues = null;
            bool generateTextOutput = textResults.Length == 0;
            List<Codon> terminals = new List<Codon>();

            //Validate the fitness method
            if (IsParamterOptimizationProblem(_settings.FitnessMethod))
                throw new ArgumentOutOfRangeException("Fitness method, '" + _settings.FitnessMethod + "', is a parameter optimization method and cannot be evaluated by this method.");

            //Create a confusion matrix for the classifiers, we should find a better way to do this
            ConfusionMatrix outputConfusionMatrix = new ConfusionMatrix();

            //Initialize the text results return.
            textResults = "";
            textResults = "Expected Value, Evolved Value";
            foreach (Codon terminal in testCases[0].TerminalValues.Keys) {
                textResults += ", ";
                textResults += terminal.ToString();
                terminals.Add(terminal);
            }

            textResults += "\r\n";

            //Build the chromosome's phenotype
            chromosome.BuildPhenotype();

            //Iterate over all of the test cases
            foreach (TestCase testCase in testCases) {
                //Get the gene results
                this.EvaluateGenePhenotypes(chromosome, testCase.TerminalValues, out geneValues);
                
                //todo: Implement other evaluations. Currently only static linking function for symbolic regression is implemented.
                
                //If there are no homeotic genes, then use a static linking function to link the genes together
                if (_settings.UseStaticLinkingFunction && _settings.HomeoticGenes == 0) {
                    //Initialize to the first value in the gene list
                    double chromosomeValue = geneValues[0];

                    //todo: Implement other static linking functions. Currently on addition, subtraction and multiplication are done.
                    //Iterate over the remaining gene values, linking them together
                    for (int i = 1; i < geneValues.Count; i++) {
                        switch (_settings.StaticLinkingFunction) {
                            case '+':
                                chromosomeValue += geneValues[i];
                                break;

                            case '-':
                                chromosomeValue -= geneValues[i];
                                break;

                            case '*':
                                chromosomeValue *= geneValues[i];
                                break;
                        }
                    }

                    //Store the results as text
                    if (generateTextOutput) {
                        try {
                            //Append the expected value
                            textResults += testCase.ExpectedValue.ToString() + ", ";

                            //Append the chromosome value
                            textResults += chromosomeValue.ToString();

                            foreach (Codon terminal in terminals) {
                                textResults += ", ";
                                textResults += testCase.TerminalValues[terminal].ToString();
                            }

                        } finally {
                            textResults += "\r\n";
                        }                        
                    }

                    //Evaluate the fitness
                    switch (_settings.FitnessMethod) {
                        case FitnessEvaluationMethod.PrecisionAndSelectionRange:
                            chromosomeFitness += _settings.FitnessSelectionRangeValue - Math.Abs(chromosomeValue - testCase.ExpectedValue);
                            break;

                        case FitnessEvaluationMethod.MeanSquaredError:
                            chromosomeFitness += (chromosomeValue - testCase.ExpectedValue) * (chromosomeValue - testCase.ExpectedValue);
                            break;


                        case FitnessEvaluationMethod.HitsWithPenalty:
                            //Determine if the chromosome value is in the class or not.
                            bool chromosomeInClass = chromosomeValue >= this.HitRoundingThreshold;
                            bool expectedInClass = testCase.ExpectedValue >= this.HitRoundingThreshold;

                            //Update the confusion matrix for the output
                            if (chromosomeInClass  && expectedInClass ) outputConfusionMatrix.TruePositive++;
                            if (!chromosomeInClass && !expectedInClass) outputConfusionMatrix.TrueNegative++;
                            if (chromosomeInClass  && !expectedInClass) outputConfusionMatrix.FalsePositive++;
                            if (!chromosomeInClass && expectedInClass ) outputConfusionMatrix.FalseNegative++;
                            break;

                        default:
                            throw new NotImplementedException("The fitness evaluation method '" + _settings.FitnessMethod + "' is not implemented.");
                    }

                } //End of static linking handler
            }//End of testcase loop

            //Some fitness methods require an extra step to up them into a useful form, we do that here.
            switch (_settings.FitnessMethod) {
                case FitnessEvaluationMethod.MeanSquaredError:
                    //Divide by the number of test cases
                    chromosomeFitness = chromosomeFitness / testCases.Count;
                    
                    //Convert to a value that can be used by the roulette wheel
                    chromosomeFitness = _settings.FitnessSelectionRangeValue / (1 + chromosomeFitness);
                    break;

                case FitnessEvaluationMethod.HitsWithPenalty:
                    //The chromosome has now classified all of the test cases, so penalize any individuals which
                    //don't classify any of the TP or TN.
                    //The final fitness is the total number of classes correctly identified.
                    if (outputConfusionMatrix.TruePositive == 0 || outputConfusionMatrix.TrueNegative == 0) {
                        chromosomeFitness = 0;

                    } else {
                        chromosomeFitness = outputConfusionMatrix.TruePositive + outputConfusionMatrix.TrueNegative;
                    }
                    break;
            } 

            //Handle if the fitness gets out of hand.
            if (double.IsNaN(chromosomeFitness) || double.IsInfinity(chromosomeFitness)) {
                chromosomeFitness = 0;
            }

            return chromosomeFitness;
        }


        /// <summary>
        /// Evaluates a list of terminals using the individual's phenotype and 
        /// returns the value of each gene.
        /// </summary>
        /// <remarks>
        /// Verified that this functions correctly on Sept 18, 2008 at 1300.
        /// RNC algorithm implementation verified Nov 20, 2008.
        /// </remarks>
        /// <param name="terminalValues">A dictionary containing the terminal codons and thier corresponding values.</param>
        /// <param name="geneValues">The evaluated values of each gene in the chromosome, including homeotic genes.</param>
        protected bool EvaluateGenePhenotypes(Chromosome chromosome, Dictionary<Codon, double> terminalValues, out List<double> geneValues) {
            bool builtPhenotype = false;
            List<double> currentRowValues = null;
            List<double> previousRowValues = null;

            //If the phenotype hasn't been built yet, build it
            if (!chromosome.IsPhenotypeBuilt) {
                chromosome.BuildPhenotype();
                builtPhenotype = true;
            }

            //Initialize the gene values
            geneValues = new List<double>();

            //Iterate over each gene, evaluating it seperately
            //Iterate over each gene in the chromosome, building each phenotype seperately
            for (int geneIndex = 0; geneIndex < chromosome.Genes.Count; geneIndex++) {
                Phenotype genePhenotype = chromosome[geneIndex].GenePhenotype;
                int dcElementIndex = 0;

                //Start at the bottom and iterate over each row
                for (int rowIndex = genePhenotype.Count - 1; rowIndex >= 0; rowIndex--) {
                    currentRowValues = new List<double>();
                    int previousRowIndex = 0;

                    //Move left to right across each row, either subsituting terminals or evaluating functions
                    for (int codonIndex = 0; codonIndex < genePhenotype[rowIndex].Count; codonIndex++) {
                        //Assign the current codon to a local for clarity
                        Codon currentCodon = genePhenotype[rowIndex][codonIndex];

                        //Get the codon arity
                        int codonArity = GepEvaluator.GetArity(currentCodon);

                        //If we have a terminal (arity==0) then put it value into our row
                        //Terminals are the only case that we need to worry about the 
                        //homeotic or normal gene values. 
                        if (codonArity == 0) {
                            //Normal gene, so check if it's an RNC place holder
                            if (currentCodon == "?") {
                                //Get the index in the RNC array from the next Dc value
                                //RNC elements are 1 indexed, so we need to subtract one to get the real element index
                                //todo: verify this change in the evaluator for the RNC array
                                Codon dcElement = chromosome[geneIndex].DcDomain[dcElementIndex];
                                int rncElementIndex = Int32.Parse(dcElement.ToString()) - 1;

                                //Rnc place holder, grab the next rnc value
                                currentRowValues.Add(chromosome[geneIndex].RandomNumericalConstants[rncElementIndex]);

                                //Increment the Dc index, so the next time we see an RNC placeholder, we 
                                //use the next element in the Dc domain
                                dcElementIndex++;

                            } else {
                                //normal gene and normal terminal, just add the value of the terminal
                                currentRowValues.Add(terminalValues[currentCodon]);
                            }
                        }

                        //Now handle functions.
                        if (codonArity > 0) {
                            //Initialize the parameter list
                            List<double> functionParameters = new List<double>();

                            //Load the parameter values from the previous row
                            for (int i = 0; i < codonArity; i++) {
                                functionParameters.Add(previousRowValues[previousRowIndex]);
                                previousRowIndex++;
                            }

                            //Now evaluate the function
                            currentRowValues.Add(this.EvaluateCodon(currentCodon, functionParameters));
                        }
                    }

                    //We're about to move up a row, so update the previous row values. These will be the inputs to the 
                    //function in the current (next) row.
                    previousRowValues = currentRowValues;
                } //End of row iterator

                //Throw an error in usual states
                if (previousRowValues.Count != 1) throw new ArgumentException("Phenotype evaluation encountered an error. The gene ended up with more than one root value!");

                //Add the value of the root position to the gene value list
                geneValues.Add(previousRowValues[0]);

            } //End of gene iterator

            //Return if the phenotype was built before the evaluation
            return builtPhenotype;
        } //End of EvaluateGenePhenotypes method


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected double EvaluateCodon(Codon codon, List<double> parameters) {
            double value = 0;

            switch (codon.ToString()) {
                //SINGLE parameter functions
                case "Q":   //Square Root
                    value = Math.Sqrt(parameters[0]);
                    break;

                case "E":   //Exp
                    value = Math.Exp(parameters[0]);
                    break;

                case "S":   //Sin
                    value = Math.Sin(parameters[0]);
                    break;

                case "C":   //Cos
                    value = Math.Cos(parameters[0]);
                    break;

                case "T":   //Tan
                    value = Math.Tan(parameters[0]);
                    break;

                case "L":   //Log
                    value = Math.Log(parameters[0]);
                    break;

                case "Fl": //Floor
                    value = Math.Floor(parameters[0]);
                    break;

                case "Ce": //Ceiling
                    value = Math.Ceiling(parameters[0]);
                    break;

                //TWO parameter functions
                case "*":
                    value = parameters[0] * parameters[1];
                    break;

                case "/":   //Implement protected division
                    value = parameters[1] != 0 ? parameters[0] / parameters[1] : 1;
                    break;

                case "-":
                    value = parameters[0] - parameters[1];
                    break;

                case "+":
                    value = parameters[0] + parameters[1];
                    break;

                //Logical two parameter functions
                case "O":   //Or function, if either parameters are positive and non-zero, return 1 otherwise 0
                    value = parameters[0] > 0 || parameters[1] > 0 ? 1 : 0;
                    break;

                case "A":   //And function, if both parameters are positive and non-zero, return 1 otherwise 0
                    value = parameters[0] > 0 && parameters[1] > 0 ? 1 : 0;
                    break;

                case "N":   //not function
                    value = parameters[0] <= 0 ? 1 : 0;
                    break;

                case "<": //Less than, returns the lesser of two values
                    value = parameters[0] < parameters[1] ? parameters[0] : parameters[1];
                    break;

                case ">": //Greater than, returns the greater of two values
                    value = parameters[0] > parameters[1] ? parameters[0] : parameters[1];
                    break;

                case "<=": //Less than or equal
                    value = parameters[0] <= parameters[1] ? parameters[0] : parameters[1];
                    break;

                case ">=": //Greater than or equal
                    value = parameters[0] >= parameters[1] ? parameters[0] : parameters[1];
                    break;

                case "==": //equal
                    value = parameters[0] == parameters[1] ? parameters[0] : parameters[1];
                    break;

                case "!=": //not equal
                    value = parameters[0] != parameters[1] ? parameters[0] : parameters[1];
                    break;

                default:
                    throw new NotImplementedException("Codon function '"+ codon.ToString() +"' is not implemented in the GepEvaluator.EvaluateCodon(...) method.");
            }

            return value;
        } //End of EvaluateCodon method


    } //End of GepEvaluator class
}
