﻿/***********************************************************************
 * Codon.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Implements the Codon class used by the Gene Expression 
 * Programming algorithm. The codons are used to create a chromosome.
 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    /// <summary>
    /// Represents a single codon of a GEP chromosome. 
    /// Codon objects are immutable.
    /// </summary>
    [Serializable]
    public class Codon : IComparable {
        byte[] _data;

        /// <summary>
        /// Get the byte array value of the Codon.
        /// </summary>
        [Browsable(false)]
        public byte[] Value {
            get { return _data; }
            set { _data = value; }
        }


        /// <summary>
        /// Get or set the value of the codon as a string.
        /// </summary>
        [Browsable(true)]
        [Category("Functions and Terminals")]
        public string ValueAsString {
            get { return this.ToString(); }
            set { this.FromString(value); }
        }

        /// <summary>
        /// Instantiate an empty Codon object.
        /// </summary>
        public Codon() {
            _data = new byte[4];

            //Initialize the data array to null
            for (int i = 0; i < _data.Length; i++) {
                _data[i] = 0; 
            }
        }
        
        /// <summary>
        /// Instantiate a new Codon object from a string value.
        /// </summary>
        /// <param name="value"></param>
        public Codon(string value) : this() {
            this.FromString(value);
        }


        /// <summary>
        /// Instantiate a homeotic or constant terminal Codon
        /// </summary>
        /// <param name="value"></param>
        public Codon(int value) : this(value.ToString()) {}

        /// <summary>
        /// Instantiate a new Codon object from a character array
        /// </summary>
        /// <param name="value"></param>
        public Codon(char[] value) : this() {
            //Convert the value to a byte array for working with
            byte[] byteValue = Encoding.ASCII.GetBytes(value);

            //Validate length
            if (byteValue.Length > _data.Length) throw new ArgumentException("The codon value must be <=" + _data.Length.ToString() + ".");

            //Copy the value bytes.
            //This assumes the _data array has already been initialized in the default constructor
            for (int i = 0; i < byteValue.Length; i++) {
                _data[i] = byteValue[i];
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public Codon(byte[] value)
            : this() {

            //Validate length
            if (value.Length > _data.Length) throw new ArgumentException("The codon value must be <=" + _data.Length.ToString() + ".");

            //Copy the value bytes.
            //This assumes the _data array has already been initialized in the default constructor
            for (int i = 0; i < value.Length; i++) {
                _data[i] = value[i];
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        public void CopyTo(out Codon target) {
            target = new Codon(this.Value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Codon Copy() {
            Codon returnCodon = null;
            this.CopyTo(out returnCodon);
            return returnCodon;
        }

        public void FromString(string value) {
            //Convert the value to a byte array for working with
            byte[] byteValue = Encoding.ASCII.GetBytes(value);

            //Validate length
            if (byteValue.Length > _data.Length) throw new ArgumentException("The codon value must be <=" + _data.Length.ToString() + ".");

            //Copy the value bytes.
            //This assumes the _data array has already been initialized in the default constructor
            for (int i = 0; i < byteValue.Length; i++) {
                _data[i] = byteValue[i];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj) {
            int compareValue = int.MinValue;

            if (obj is Codon) {
                //Handle equality
                if (this == (Codon)obj) {
                    compareValue = 0;

                } else {
                    compareValue = this.ToString().CompareTo(((Codon)obj).ToString());
                }
            }

            return compareValue;
        }


        #region Static Methods
        /// <summary>
        /// Test two Codon objects for equality
        /// </summary>
        /// <param name="condon1"></param>
        /// <param name="condon2"></param>
        /// <returns></returns>
        public static bool operator ==(Codon codon1, Codon codon2) {
            return
                codon1.Value[0] == codon2.Value[0]
                && codon1.Value[1] == codon2.Value[1]
                && codon1.Value[2] == codon2.Value[2]
                && codon1.Value[3] == codon2.Value[3];
        }


        /// <summary>
        /// Test two Codon objects for inequality.
        /// </summary>
        /// <param name="condon1"></param>
        /// <param name="codon2"></param>
        /// <returns></returns>
        public static bool operator !=(Codon codon1, Codon codon2) {
            return !(codon1 == codon2);               
        }


        /// <summary>
        /// Implicit conversion from a char array to a codon
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static implicit operator Codon(char[] value) {
            return new Codon(value);
        }

        /// <summary>
        /// Implicit conversion from char to codon
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static implicit operator Codon(char value) {
            char[] valueArray = new char[1];
            valueArray[0] = value;

            return new Codon(valueArray);
        }


        /// <summary>
        /// Implicit conversion from string to codon
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static implicit operator Codon(string value) {
            return new Codon(value);
        }
        #endregion


        /// <summary>
        /// Returns a hashcode for this Codon object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() {
            return _data.ToString().GetHashCode();
        }


        /// <summary>
        /// Return a string representation of the Codon object.
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return Encoding.ASCII.GetString(_data).Trim('\0');
        }

        /// <summary>
        /// Returns if this instance of a Codon is equal to another object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) {
            if (!(obj is Codon)) {
                return false;
            } else {
                return this == (Codon)obj;
            }
        }       
    }
}
