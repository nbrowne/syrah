﻿/***********************************************************************
 * GepSettings.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Stores the settings for a GEP run or series.
 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;
using System.ComponentModel;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    [Serializable]
    public sealed class GepSettings {
        public readonly int CLASS_VERSION = 3;

        //Define the global controlling settings              
        private int _populationSize = 50;
        private int _numberOfRuns = 100;
        private int _maximumGenerations = 100;
        
        private double _twoPointCrossoverRate = 0.3;
        private double _onePointCrossoverRate = 0.3;
        private double _geneRecombinationRate = 0.0444;
        
        //Define the stochastic sampling settings
        private bool _isStochasticSamplingEnabled = false;          //Is stochastic sampling enabled
        private int _stochasticSampleSize = 500;                    //Size of each sample
        private double _stochasticSampleTrigger = 0.97;             //When the population fitness reaches this rate (out of 1), then send the new sample       
        private StochasticSampleTriggerMethod _stochasticSampleTriggerMethod = StochasticSampleTriggerMethod.Generations;

        //Define conventional gene settings
        private int _genes = 1;
        private double _mutationRate = 0.0444;
        private double _inversionRate = 0.1;
        private double _risRate = 0.1;
        private double _isRate = 0.1;
        private double _geneTranspositionRate = 0.1;

        //Define the homeotic gene settings
        private int _homeoticGenes = 0;
        private double _homeoticMutationRate = 0.0444;
        private double _homeoticInversionRate = 0.1;
        private double _homeoticRisRate = 0.1;
        private double _homeoticIsRate = 0.1;

        //Define if a static linker is to be used
        private char _staticLinkingFunction = '+';
        private bool _useStaticLinkingFunction = false;

        //Define the function lists
        private CodonCollection _functionList = new CodonCollection();
        private CodonCollection _terminalList = new CodonCollection();
        private CodonCollection _homeoticFunctionList = new CodonCollection();
        private CodonCollection _homeoticTerminalList = new CodonCollection();
        
        //Define the maximum fitness per case for "Precision and Selection Range" fitness
        private int _fitnessSelectionRangeValue = 1000;
        private double _fitnessSelectionPrecisionValue = 0.01;
        private FitnessEvaluationMethod _fitnessMethod = FitnessEvaluationMethod.PrecisionAndSelectionRange;

        //Define the special gene settings that require special accessors and settors
        private int _conventionalHeadLength = 0;
        private int _conventionalMaximumArity = 0;
        private int _conventionalTailLength = 0; //Do not directly set this value!

        //Define the special homeotic gene settings that require special accessors and settors
        private int _homeoticHeadLength = 0;
        private int _homeoticMaximumArity = 0;
        private int _homeoticTailLength = 0; //Do not directly set this value!

        //Define the RNC settings
        private bool _enableRnc = true;
        private int _numberOfRncConstants = 10;
        private RncConstantsType _rncConstantsType = RncConstantsType.Rational;
        private double _upperRncConstantRange = 10;
        private double _lowerRncConstantRange = -10;
        private double _rncInversionRate = 0.1;
        private double _rncTranspositionRate = 0.1;

        //Define the Self Adapation properties
        private bool _adaptiveChromosomeSizeEnabled = false;
        private double _adaptiveChromosomeSizeMutationRate = 0.1;
        private double _adaptiveChromosomeSizeMutationGrowShinkRatio = 0.7;

        private bool _parsimonyPressureEnabled = false;
        private double _parsimonyPressureValue = 1 / 5000;
        private ParsimonyPressureTriggerMethod _parsimonyPressureTrigger = ParsimonyPressureTriggerMethod.Fitness;
        private double _parsimonyPressureTriggerValue = 1.0;

        private bool _migrationEnabled = false;
        private double _migrationTriggerValue = 0;
        private int _migrationSize = 10;

        private bool _isAcsGeneEnabled = false;
        private bool _isHisTranspositionEnabled = false;
        private double _acsGeneMutationRate = 0.1;
        private double _hisTranspositionRate = 0.1;
        private int _hisHeadLengthMinimum = 5;
        private int _hisHeadLengthMaximum = 15;
        private int _acsGeneCountMinimum = 1;
        private int _acsGeneCountMaximum = 10;

        #region Public Properties
        /// <summary>
        /// Get or set if the adaptive chromosome sizing is enabled. This will cause the number of 
        /// genes in the chromosomes to change during a run.
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("Acs-Gene Enabled")]
        public bool IsAcsGeneEnabled { 
            get { return _isAcsGeneEnabled; }
            set { _isAcsGeneEnabled = value;  } 
        }


        [Category("2. Adaptive Settings")]
        [DisplayName("HIS Transposition Enabled")]
        public bool IsHisTranspositionEnabled {
            get { return _isHisTranspositionEnabled; }
            set { _isHisTranspositionEnabled = value; }
        }

        /// <summary>
        /// Get or set if the mutation rate for the adaptive chromosome sizing. This will
        /// change the number of individuals in a population that are selected to have their size mutated.
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("ACS-Gene - Mutation Rate")]
        public double AcsGeneMutationRate {
            get { return _acsGeneMutationRate; }
            set { _acsGeneMutationRate = value; }
        }

        /// <summary>
        /// Get or set if the mutation rate for the adaptive chromosome sizing. This will
        /// change the number of individuals in a population that are selected to have their size mutated.
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("HIS Transposition Rate")]
        [DefaultValue(0.1)]
        public double HisTranspositionRate {
            get { return _hisTranspositionRate; }
            set { _hisTranspositionRate = value; }
        }

        /// <summary>
        /// Gets the minimum head size that individuals are initialized with
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("HIS Transposition - Minimum Head Length")]
        public int HisHeadLengthMinimum {
            get { return _hisHeadLengthMinimum; }
            set { _hisHeadLengthMinimum = value; }
        }
        /// <summary>
        /// Gets the maximum head size that individuals are initialized with
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("HIS Transposition - Maximum Head Length")]
        public int HisHeadLengthMaximum {
            get { return _hisHeadLengthMaximum; }
            set { _hisHeadLengthMaximum = value; } 
        }

        /// <summary>
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("Minimum Number of Genes")]
        public int AcsGeneCountMinimum {
            get { return _acsGeneCountMinimum; }
            set { _acsGeneCountMinimum = value; }
        }

        /// <summary>
        /// </summary>
        [Category("2. Adaptive Settings")]
        [DisplayName("Maximum Number of Genes")]
        public int AcsGeneCountMaximum {
            get { return _acsGeneCountMaximum; }
            set { _acsGeneCountMaximum = value; }
        }


        /// <summary>
        /// Get or set if the RNC algorithm is enabled. This will enable the 
        /// generation of random constants by the GEP algorithm.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(true)]
        [DisplayName("RNC Enabled?")]
        public bool IsRncEnabled {
            get { return _enableRnc; }
            set { _enableRnc = value; }
        }


        /// <summary>
        /// Get or set the number of RNC constants generated per gene.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(10)]
        [DisplayName("Number of RNC Constants")]
        public int NumberOfRncConstants {
            get { return _numberOfRncConstants; }
            set { _numberOfRncConstants = value; }
        }


        /// <summary>
        /// Get or set the type of constants the RNC algorithm will generate.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(RncConstantsType.Rational)]
        [DisplayName("Type of RNC Constants")]
        public RncConstantsType RandomConstantsType {
            get { return _rncConstantsType; }
            set { _rncConstantsType = value; }
        }

        /// <summary>
        /// Get or set the upper range of the constants generated by the RNC algorithm.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(10)]
        [DisplayName("Upper limit for RNC Constants")]
        public double UpperRncConstantRange {
            get { return _upperRncConstantRange; }
            set { _upperRncConstantRange = value; }
        }

        /// <summary>
        /// Get or set the lower range of the constants generated by the RNC algorithm.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(-10)]
        [DisplayName("Lower limit for RNC Constants")]
        public double LowerRncConstantRange {
            get { return _lowerRncConstantRange; }
            set { _lowerRncConstantRange = value; }
        }

        /// <summary>
        /// Get or set the the inversion rate for the Dc (RNC) domain.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(0.1)]
        [DisplayName("RNC Inversion Rate")]
        public double RncInversionRate {
            get { return _rncInversionRate; }
            set { _rncInversionRate = value; }
        }

        /// <summary>
        /// Get or set the the transposition rate for the Dc (RNC) domain.
        /// </summary>
        [Category("3. RNC Settings")]
        [DefaultValue(0.1)]
        [DisplayName("RNC Transposition Rate")]
        public double RncTranspositionRate {
            get { return _rncTranspositionRate; }
            set { _rncTranspositionRate = value; }
        }

        /// <summary>
        /// Get or set the population size.
        /// </summary>
        [Category("1. Global Settings")]
        [DisplayName("Population Size")]
        public int PopulationSize {
            get { return _populationSize; }
            set { _populationSize = value; }
        }
        

        /// <summary>
        /// Get or set the maximum number of runs.
        /// </summary>
        [Category("1. Global Settings")]
        [DisplayName("Number of Runs")]
        public int NumberOfRuns {
            get { return _numberOfRuns; }
            set { _numberOfRuns = value; }
        }
        

        /// <summary>
        /// Get or set the maximum number of generations per run.
        /// </summary>
        [Category("1. Global Settings")]
        [DisplayName("Maximum Number of Generations")]
        public int MaximumGenerations {
            get { return _maximumGenerations; }
            set { _maximumGenerations = value; }
        }

        /// <summary>
        /// Get or set the number of standard genes in a chromosome.
        /// </summary>  
        [Category("5. Chromosome Settings")]
        [DisplayName("Number of Genes")]
        public int Genes {
            get { return _genes; }
            set { _genes = value; }
        }

        /// <summary>
        /// Get or set the mutation rate used by the standard genetic operators.
        /// </summary>
        [Category("6. Standard Gene Genetic Operators")]
        [DisplayName("Mutation Rate")]
        public double MutationRate {
            get { return _mutationRate; }
            set { _mutationRate = value; }
        }

        /// <summary>
        /// Get or set the inversion rate used by the standard genetic operators.
        /// </summary>
        [Category("6. Standard Gene Genetic Operators")]
        [DisplayName("Inversion Rate")]
        public double InversionRate {
            get { return _inversionRate; }
            set { _inversionRate = value; }
        }

        /// <summary>
        /// Get or set the root insertion sequence rate used by the standard genetic operators.
        /// </summary>
        [Category("6. Standard Gene Genetic Operators")]
        [DisplayName("Ris Rate")]
        public double RisRate {
            get { return _risRate; }
            set { _risRate = value; }
        }

        /// <summary>
        /// Get or set the insertion sequence transposition rate used by the standard genetic operators.
        /// </summary>
        [Category("6. Standard Gene Genetic Operators")]
        [DisplayName("IS Rate")]
        public double IsRate {
            get { return _isRate; }
            set { _isRate = value; }
        }
        
        /// <summary>
        /// Get or set the gene transposition rate used by the standard genetic operators.
        /// </summary>
        [Category("6. Standard Gene Genetic Operators")]
        [DisplayName("Gene Transposition Rate")]
        public double GeneTranspositionRate {
            get { return _geneTranspositionRate; }
            set { _geneTranspositionRate = value; }
        }


        /// <summary>
        /// Get or set number of homeotic genes in a chromosome.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [Browsable(false)]
        [DisplayName("Number of Homeotic Genes")]
        public int HomeoticGenes {
            get { return _homeoticGenes; }
            set { _homeoticGenes = value; }
        }

        /// <summary>
        /// Get or set the mutation rate used by the homeotic genetic operators
        /// </summary>
        [Category("Homeotic Genetic Operators")]
        [Browsable(false)]
        [DisplayName("Homeotic Mutation Rate")]
        public double HomeoticMutationRate {
            get { return _homeoticMutationRate; }
            set { _homeoticMutationRate = value; }
        }

        /// <summary>
        /// Get or set the inversion Rate used by the homeotic genetic operators
        /// </summary>
        [Category("Homeotic Genetic Operators")]
        [Browsable(false)]
        [DisplayName("Homeotic Inversion Rate")]
        public double HomeoticInversionRate {
            get { return _homeoticInversionRate; }
            set { _homeoticInversionRate = value; }
        }

        /// <summary>
        /// Get or set the RIS Rate used by the homeotic genetic operators
        /// </summary>
        [Category("Homeotic Genetic Operators")]
        [Browsable(false)]
        [DisplayName("Homeotic RIS Rate")]
        public double HomeoticRisRate {
            get { return _homeoticRisRate; }
            set { _homeoticRisRate = value; }
        }
        

        /// <summary>
        /// Get or set the IS Rate used by the homeotic genetic operators
        /// </summary>
        [Category("Homeotic Genetic Operators")]
        [Browsable(false)]
        [DisplayName("Homeotic IS Rate")]
        public double HomeoticIsRate {
            get { return _homeoticIsRate; }
            set { _homeoticIsRate = value; }
        }

 
        /// <summary>
        /// Get or set the static linking function
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Static Linking Function")]
        public char StaticLinkingFunction {
            get { return _staticLinkingFunction; }
            set { _staticLinkingFunction = value; }
        }
        
        /// <summary>
        /// Get or set if a static linking function should be used
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Use a static linking function?")]
        public bool UseStaticLinkingFunction {
            get { return _useStaticLinkingFunction; }
            set { _useStaticLinkingFunction = value; }
        }

        /// <summary>
        /// Get or set the two point recombination rate used by the genetic operators.
        /// </summary>
        [Category("7. Recombination Settings")]
        [DisplayName("Two Point Recombination Rate")]
        public double TwoPointCrossoverRate {
            get { return _twoPointCrossoverRate; }
            set { _twoPointCrossoverRate = value; }
        }

        /// <summary>
        /// Get or set the one point recombination rate used by the genetic operators.
        /// </summary>        
        [Category("7. Recombination Settings")]
        [DisplayName("One Point Recombination Rate")]
        public double OnePointCrossoverRate {
            get { return _onePointCrossoverRate; }
            set { _onePointCrossoverRate = value; }
        }
        
        /// <summary>
        /// Get or set the gene recombination rate used by the genetic operators.
        /// </summary>
        [Category("7. Recombination Settings")]
        [DisplayName("Gene Recombination Rate")]
        public double GeneRecombinationRate {
            get { return _geneRecombinationRate; }
            set { _geneRecombinationRate = value; }
        }

        /// <summary>
        /// Get or set the list of functions used by the homeotic genes
        /// </summary>
        [Category("1. Global Settings")]
        [Browsable(false)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Homeotic Function List")]
        public CodonCollection HomeoticFunctionList {
            get { return _homeoticFunctionList; }
            set { _homeoticFunctionList = value; }
        }

        /// <summary>
        /// Get or set the list of terminals used in the homeotic genes
        /// </summary>
        [Category("1. Global Settings")]
        [Browsable(false)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Homeotic Terminal List")]
        public CodonCollection HomeoticTerminalList {
            get { return _homeoticTerminalList; }
            set { _homeoticTerminalList = value; }
        }

        /// <summary>
        /// Get or set the list of functions used in the standard genes
        /// </summary>
        [Category("1. Global Settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [EditorAttribute(typeof(FunctionSetPropertyEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Function List")]
        public CodonCollection FunctionList {
            get { return _functionList; }
            set { _functionList = value; }
        }
        

        /// <summary>
        /// Get or set the list of terminals used in the standard genes.
        /// </summary>
        [Category("1. Global Settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Terminal List")]
        public CodonCollection TerminalList {
            get { return _terminalList; }
            set { _terminalList = value; }
        }


        /// <summary>
        /// Get or set the maximum fitness value for each fitness case
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Fitness Selection Range")]
        public int FitnessSelectionRangeValue {
            get { return _fitnessSelectionRangeValue; }
            set { _fitnessSelectionRangeValue = value; }
        }

        /// <summary>
        /// Get or set best fitness the algorithm will search for.
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Fitness Selection Precision")]
        public double FitnessSelectionPrecisionValue {
            get { return _fitnessSelectionPrecisionValue; }
            set { _fitnessSelectionPrecisionValue = value; }
        }

        
        /// <summary>
        /// Get or set the fitness evaluation method
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("FitnessMethod")]
        public FitnessEvaluationMethod FitnessMethod {
            get { return _fitnessMethod; }
            set { _fitnessMethod = value; }
        }               

        /// <summary>
        /// The length of the head of the "normal" genes.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [DisplayName("Head Length")]
        public int HeadLength {
            get { return _conventionalHeadLength; }
            set {
                _conventionalHeadLength = value;
                _conventionalTailLength = GepAlgorithm.CalculateTailLength(_conventionalHeadLength, _conventionalMaximumArity);
            }
        }


        /// <summary>
        /// Get the length of the gene's tail. This is a calculated value based on the 
        /// length of the gene's head.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [DisplayName("Tail Length")]
        public int TailLength {
            get { return _conventionalTailLength = GepAlgorithm.CalculateTailLength(_conventionalHeadLength, GepEvaluator.GetMaximumArity(this.FunctionList)); }
        }


        /// <summary>
        /// Get the maximum arity (number of parameters) from the functions to be used in the evolution.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [DisplayName("Maximum Function Arity")]
        public int MaximumArity {
            get { return GepEvaluator.GetMaximumArity(this.FunctionList); }
        }


        /// <summary>
        /// The length of the head of the gene.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [Browsable(false)]
        [DisplayName("Homeotic Head Length")]
        public int HomeoticHeadLength {
            get { return _homeoticHeadLength; }
            set {
                _homeoticHeadLength = value;
                _homeoticTailLength = GepAlgorithm.CalculateTailLength(_homeoticHeadLength, _homeoticMaximumArity);
            }
        }


        /// <summary>
        /// Get the length of the gene's tail. This is a calculated value based on the 
        /// length of the gene's head.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [DisplayName("Homeotic Tail Length")]
        public int HomeoticTailLength {
            get { return GepAlgorithm.CalculateTailLength(_homeoticHeadLength, GepEvaluator.GetMaximumArity(this.HomeoticFunctionList)); }
        }


        /// <summary>
        /// Get the maximum arity (number of parameters) from the functions to be used in the evolution.
        /// </summary>
        [Category("5. Chromosome Settings")]
        [DisplayName("Homeotic Maximum Function Arity")]
        public int HomeoticMaximumArity {
            get { return GepEvaluator.GetMaximumArity(this.HomeoticFunctionList); }
        }


        /// <summary>
        /// Is stochastic sampling enabled
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Stochastic Sampling is Enabled")]
        public bool IsStochasticSamplingEnabled {
            get { return _isStochasticSamplingEnabled; }
            set { _isStochasticSamplingEnabled = value; }
        }


        /// <summary>
        /// Get or set the trigger method for the stochastic sampling algorithm to request a new sample.
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Stochastic Sample Method")]
        public StochasticSampleTriggerMethod StochasticSampleMethod {
            get { return _stochasticSampleTriggerMethod; }
            set { _stochasticSampleTriggerMethod = value; }
        }

        /// <summary>
        /// Size of each sample in the stochastic sampling algorithm
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Stochastic Sample Size")]
        public int StochasticSampleSize {
            get { return _stochasticSampleSize; }
            set { _stochasticSampleSize = value; }
        }


        /// <summary>
        /// When the population fitness reaches this rate (out of 1), then send the new sample
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Stochastic Sample Fitness Trigger")]
        public double StochasticSampleTrigger {
            get { return _stochasticSampleTrigger; }
            set { _stochasticSampleTrigger = value; }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Parsimony Pressure Enabled?")]
        public bool ParsimonyPressureEnabled {
            get { return _parsimonyPressureEnabled; }
            set { _parsimonyPressureEnabled = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Parsimony Pressure Value")]
        public double ParsimonyPressureValue {
            get { return _parsimonyPressureValue; }
            set { _parsimonyPressureValue = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Parsimony Pressure Trigger")]
        public ParsimonyPressureTriggerMethod ParsimonyPressureTrigger {
            get { return _parsimonyPressureTrigger; }
            set { _parsimonyPressureTrigger = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        [Category("4. Evaluation")]
        [DisplayName("Parsimony Pressure Trigger Value")]
        public double ParsimonyPressureTriggerValue {
            get { return _parsimonyPressureTriggerValue; }
            set { _parsimonyPressureTriggerValue = value; }
        }


        /// <summary>
        /// Is migration enabled?
        /// </summary>
        [Category("8. Migration")]
        [DisplayName("Migration Enabled")]
        public bool MigrationEnabled {
            get { return _migrationEnabled; }
            set { _migrationEnabled = value; }
        }
   

        /// <summary>
        /// Get or set the setpoint value used by the Migration Trigger Method
        /// </summary>
        [Category("8. Migration")]
        [DisplayName("Migration Interval")]
        public double MigrationInterval {
            get { return _migrationTriggerValue; }
            set { _migrationTriggerValue = value; }
        }


        /// <summary>
        /// Get or set the number of individuals to emigrate/immigrate
        /// </summary>
        [Category("8. Migration")]
        [DisplayName("Migration Selection Individuals")]
        public int MigrationSize {
            get { return _migrationSize; }
            set { _migrationSize = value; }
        }

        public GepSettings() {
            //Set the acs defaults
            this.HisHeadLengthMaximum = 30;
            this.HisHeadLengthMinimum = 5;
            this.AcsGeneCountMinimum = 1;
            this.AcsGeneCountMaximum = 10;
        }
    }
}
