﻿/***********************************************************************
 * GepAlgorithm.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Implements the core Gene Expression Programming algorithm.
 ***********************************************************************/


using System;
using System.Collections.Generic;

using System.Text;
using NigelBrowne.Syrah.Common.Exceptions;
using NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.SelectionMethods;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    public class GepAlgorithm : EvolutionaryAlgorithm {
        public const int HIS_TRANSPOSITION_MAX_CUT_LENGTH = 3;

        protected GepSettings _settings = null;
        protected List<TestCase> _testCases = null;
        protected CodonCollection _headCodonSelectionBag = null;
        protected bool _abortRunFlag = false;

        public TestCaseSampleRequestDelegate TestCaseSampleRequestHandler = null;
        public MigrationRequestDelegate MigrationRequestHandler = null;

        protected List<SerializableDictionary<int, int>> _generationGeneCountMutations = null;
        protected int _currentGenerationIndex = 0;


        /// <summary>
        /// 
        /// </summary>
        public GepAlgorithm()
            : base() {           
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        public GepAlgorithm(GepSettings settings, List<TestCase> testCases)
            : base() {
            _settings = settings;
            _testCases = testCases;
        }


        /// <summary>
        /// Calculates the length of a gene's tail based on the head length and the maximum arity of the 
        /// functions used.
        /// </summary>
        /// <param name="headLength"></param>
        /// <returns></returns>
        public static int CalculateTailLength(int headLength, int maxArity) {
            return headLength * (maxArity - 1) + 1;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="testCases"></param>
        /// <param name="sampleSize"></param>
        /// <returns></returns>
        public static List<TestCase> SelectTestCaseSample(List<TestCase> testCases, int sampleSize) {
            int sanityCheckCount = testCases.Count * 10;

            List<int> sampleIndexes = new List<int>();
            List<TestCase> sampleCases = new List<TestCase>();

            //Collect the subset members
            for (int i = 0; i < sanityCheckCount; i++) {
                int testCaseIndex = Utilities.RandomInteger(0, testCases.Count - 1);

                //Need unique chromosomes, so check if the index already exists
                if (!sampleIndexes.Contains(testCaseIndex)) {
                    //Doesn't already exist, so add it
                    sampleIndexes.Add(testCaseIndex);
                    sampleCases.Add(testCases[testCaseIndex]);
                }

                //Break if we have enough members
                if (sampleIndexes.Count >= sampleSize) {
                    break;
                }
            }

            return sampleCases;
        }

        /// <summary>
        /// Run the GEP algorithm using the global settings.
        /// </summary>
        public override void Run() {
            if (_settings == null) throw new ArgumentNullException("GepSettings have not be set for this run.");

            //Call the real Run method. The base.Run() method is called from there.
            this.Run(_settings, _testCases);
        }

        /// <summary>
        /// Aborts the execution of the run.
        /// </summary>
        public void Abort() {
            _abortRunFlag = true;
        }

        /// <summary>
        /// Run the GEP algorithm with specific settings.
        /// </summary>     
        /// <param name="settings">GepSettings object to use the settings from.</param>
        public GepStatistics Run(GepSettings settings, List<TestCase> initialTestCases) {
            GepStatistics statistics = new GepStatistics();
            GepEvaluator gepEvaluator = new GepEvaluator(settings);            
            DateTime runStartTime = DateTime.Now;
            List<TestCase> testCases = initialTestCases;
            float maxPossibleFitness = this.GetMaxPossibleFitness(settings, testCases);
            int generationCounter = 0;
            double fitnessMinimum = double.MaxValue; //Use by parameter optimization
            double bestPrecision = 1;

            //Reset the global counters
            _generationGeneCountMutations = new List<SerializableDictionary<int, int>>();
            _currentGenerationIndex = 0;

            //Set the selection method
            //ISelectionMethod selectionMethod = new SelectionMethods.RouletteWheelWithElitism();
            ISelectionMethod selectionMethod = new SelectionMethods.TournamentSelectionWithElitism();

            //Let the parent class do anything it needs to
            base.Run();

            //Set the member variables
            _settings = settings;
            _testCases = testCases;

            //Initialize the population storage
            List<Chromosome> population = null;

            //Validate settings for PO
            if (GepEvaluator.IsParamterOptimizationProblem(settings.FitnessMethod)) {
                //Require RNC
                if (!settings.IsRncEnabled)
                    throw new ArgumentException("Cannot execute parameter optimization because RNC is not enabled.");

                //Disallow ACS
                if (settings.IsAcsGeneEnabled)
                    throw new ArgumentException("Cannot execute parameter optimization because ACS is enabled.");

                //Disallow terminals
                if (settings.TerminalList.Length != 0)
                    throw new ArgumentException("Cannot execute parameter optimization because there are terminals defined. An empty terminal set is required.");

            } else {
                //Validate that we have terminals for non-po runs
                if (_settings.TerminalList.Length == 0) 
                    throw new SyrahGeneralException("No terminals were defined! Run can not continue!");                
            }

            //If the RNC algorithm is enabled and we don't have the '?' codon in our terminal list, add it
            this.AddRncTerminalIfRequired();

            //Build the bag of functions and terminals that the chromosome initialization and mutation routines will use.
            //We need to do this after the addition of the RNC '?' terminal, so that it is included if required.
            _headCodonSelectionBag = this.GetHeadCodonSelectionBag(_settings.FunctionList, _settings.TerminalList);

            //Initialize the population
            this.InitializePopulation(ref population);
            
            //Enter the generational run loop
            for (int generationIndex = 0; generationIndex < _settings.MaximumGenerations; generationIndex++) {
                _currentGenerationIndex = generationIndex;
                generationCounter++;
                DateTime generationStartTime = DateTime.Now;
                statistics.BestGenerationFitness.Add(double.MinValue);
                double averageGenerationFitness = 0;
                double bestGenerationFitness = 0;
                int bestGenerationGeneCount = 0;
                int bestGenerationTotalLength = 0;

                //Add a new generational list to the statistics to track the gene counts / chromosome
                statistics.ChromosomeGenerationGeneCount.Add(new SerializableDictionary<int, int>());
                statistics.ChromosomeGenerationGeneFitness.Add(new SerializableDictionary<int, double>());

                //Add a new generational list to the statistics to track the chromosome lengths
                statistics.ChromosomeGenerationTotalLengthCount.Add(new SerializableDictionary<int, int>());
                statistics.ChromosomeGenerationTotalLengthFitness.Add(new SerializableDictionary<int, double>());

                _generationGeneCountMutations.Add(new SerializableDictionary<int, int>());

                //Execute and evaluate each individual in the population
                foreach (Chromosome chromosome in population) {
                    //Handle different problem type evalations
                    if (GepEvaluator.IsParamterOptimizationProblem(settings.FitnessMethod)) {
                        chromosome.Fitness = gepEvaluator.EvaluateChromosomeForParameterOptimization(chromosome, settings.FitnessMethod);

                        //Check if we have a new fitness minimum
                        if (chromosome.Fitness < fitnessMinimum) fitnessMinimum = chromosome.Fitness;

                    } else {
                        chromosome.Fitness = gepEvaluator.EvaluateChromosome(chromosome, testCases);
                    }
                    averageGenerationFitness += chromosome.Fitness;

                    //Record the gene count per chromosome into the statistics
                    if (statistics.ChromosomeGenerationGeneCount[generationIndex].ContainsKey(chromosome.Genes.Count)) {
                        //gene count exists, so just incrememnt it
                        statistics.ChromosomeGenerationGeneCount[generationIndex][chromosome.Genes.Count]++;

                        statistics.ChromosomeGenerationGeneFitness[generationIndex][chromosome.Genes.Count] += (double)chromosome.Fitness;

                    } else {
                        //Gene count doesn't exist, so add it
                        statistics.ChromosomeGenerationGeneCount[generationIndex].Add(chromosome.Genes.Count, 1);

                        //Add the fitness to the sum
                        statistics.ChromosomeGenerationGeneFitness[generationIndex].Add(chromosome.Genes.Count, chromosome.Fitness);
                    }

                    //Record the chromosome length per chromosome into the statistics
                    if (statistics.ChromosomeGenerationTotalLengthCount[generationIndex].ContainsKey(chromosome.Length)) {
                        //gene count exists, so just incrememnt it
                        statistics.ChromosomeGenerationTotalLengthCount[generationIndex][chromosome.Length]++;

                        statistics.ChromosomeGenerationTotalLengthFitness[generationIndex][chromosome.Length] += (double)chromosome.Fitness;

                    } else {
                        //Gene count doesn't exist, so add it
                        statistics.ChromosomeGenerationTotalLengthCount[generationIndex].Add(chromosome.Length, 1);

                        //Add the fitness to the sum
                        statistics.ChromosomeGenerationTotalLengthFitness[generationIndex].Add(chromosome.Length, chromosome.Fitness);
                    }

                    //If the current fitness is better than the best in the generation, update the statistics
                    if (statistics.BestGenerationFitness[generationIndex] < chromosome.Fitness) {
                        statistics.BestGenerationFitness[generationIndex] = chromosome.Fitness;
                        bestGenerationFitness = chromosome.Fitness;
                        bestGenerationGeneCount = chromosome.Genes.Count;
                        bestGenerationTotalLength = chromosome.Length;
                    }

                    //First time through, we need to assign the first chromosome to the best
                    //All other times, we only want to record if the current is better than the best
                    if (statistics.BestChromosome == null || chromosome.Fitness > statistics.BestFitness) {
                        statistics.BestChromosome = chromosome.Copy();
                        statistics.BestFitness = (float)chromosome.Fitness;
                        statistics.BestSolutionGeneration = generationIndex;
                    }

                    //If the abort flag has been set, then break out of the population iterator
                    if (_abortRunFlag)
                        break;
                }                

                //Update the generation statistics that require additional work
                List<int> listOfGeneCountKeys = new List<int>();
                foreach (int key in statistics.ChromosomeGenerationGeneFitness[generationIndex].Keys) {
                    listOfGeneCountKeys.Add(key);
                }
                foreach (int key in listOfGeneCountKeys) {
                    double fitnessSum = statistics.ChromosomeGenerationGeneFitness[generationIndex][key];
                    int fitnessIndividuals = statistics.ChromosomeGenerationGeneCount[generationIndex][key];

                    //Calculate the average fitness for each generation by chromosome size
                    statistics.ChromosomeGenerationGeneFitness[generationIndex][key] = fitnessSum / (double)fitnessIndividuals;
                }

                //Update the generation statistics that require additional work
                List<int> listCountKeys = new List<int>();
                foreach (int key in statistics.ChromosomeGenerationTotalLengthFitness[generationIndex].Keys) {
                    listCountKeys.Add(key);
                }
                foreach (int key in listCountKeys) {
                    double fitnessSum = statistics.ChromosomeGenerationTotalLengthFitness[generationIndex][key];
                    int fitnessIndividuals = statistics.ChromosomeGenerationTotalLengthCount[generationIndex][key];

                    //Calculate the average fitness for each generation by chromosome size
                    statistics.ChromosomeGenerationTotalLengthFitness[generationIndex][key] = fitnessSum / (double)fitnessIndividuals;
                }

                //If the abort flag has been set, then break out of the main generation loop
                if (_abortRunFlag)
                    break;

                //Check if our current generation has met the precision requirements and break out
                //of the evolution loop if we have
                if (!GepEvaluator.IsParamterOptimizationProblem(settings.FitnessMethod)) {
                    bestPrecision = 1 - (statistics.BestChromosome.Fitness / maxPossibleFitness);
                    if (bestPrecision <= settings.FitnessSelectionPrecisionValue) {
                        break;
                    }
                }

                //Check if the best is good enough to trigger a new sample request, if stochastic sampling is enabled
                double bestFitnessPercentage = statistics.BestChromosome.Fitness / maxPossibleFitness;

                //todo: Implement the parsimony pressure code here... We need it in this loop so we have access to all the fitness values, we then need to modify them all before selection.
                //if (settings.ParsimonyPressureEnabled && this.ParsimonyPressureTriggerReached()) {
                //    throw new NotImplementedException("Parismony pressure is not fully implemented.");

                //    //Alter the fitness value for each individual
                //    foreach (Chromosome individual in population) {
                //        double parsimonyFactor = 1;
                //        individual.Fitness = individual.Fitness * parsimonyFactor;
                //    }
                //}

                //Pass the migration off to the migration handler
                this.MigrationHandler(generationCounter, ref population);

                //Pass the stochastic sampling off to the handler
                this.HandleStochasticSampling(generationCounter, bestFitnessPercentage, ref maxPossibleFitness, ref testCases, ref statistics);

                //As per CF 2006 pg 299, we check if fitness min is <=0 and update all fitness values based on that
                if (fitnessMinimum <= 0) {
                    double updateValue = Math.Abs(fitnessMinimum) + 1;
                    foreach (Chromosome chromosome in population) {
                        chromosome.Fitness += updateValue;
                    }
                }

                //Select the new population
                population = selectionMethod.ApplySelection(population, population.Count);

                //Remove the best chrosome while we're applying the operators. We will readd it later.
                //It is a requirement for the GEP algorithm that 1 clone of the best chromosome does
                //not get modified, this is simple elitism.
                Chromosome bestGenerationChromosome = population[0];
                population.RemoveAt(0);

                //Apply the genetic operators
                this.ApplyMutation(ref population);
                this.ApplyAcsGeneMutation(ref population, bestGenerationFitness, bestGenerationGeneCount);
                this.ApplyHisTransposition(ref population, bestGenerationFitness, bestGenerationGeneCount);
                this.ApplyInversion(ref population);
                if (_settings.IsRncEnabled) this.ApplyRncInversion(ref population);
                this.ApplyIsTransposition(ref population);
                this.ApplyRisTransposition(ref population);
                this.ApplyGeneTransposition(ref population);
                if (_settings.IsRncEnabled) this.ApplyRncTransposition(ref population);
                this.ApplyOnePointRecombination(ref population);
                this.ApplyTwoPointRecombination(ref population);
                this.ApplyGeneRecombination(ref population);

                //Now that all of the operators have been applied, put the clone of the best back into the population
                population.Add(bestGenerationChromosome);

                //Get the generation time
                int generationTimeInSeconds = (int)DateTime.Now.Subtract(generationStartTime).TotalMilliseconds;

                //Add the current time to the average, we'll divide it later
                statistics.AverageGenerationTime += generationTimeInSeconds;

                //Store the average generation fitness
                statistics.AverageGenerationFitness.Add(averageGenerationFitness / population.Count);

                //Get the max generation time
                if (statistics.MaxGenerationTime == -1) {
                    statistics.MaxGenerationTime = generationTimeInSeconds;
                } else if (generationTimeInSeconds > statistics.MaxGenerationTime) {
                    statistics.MaxGenerationTime = generationTimeInSeconds;
                }

                //If the abort flag has been set, then break out of the main generation loop
                if (_abortRunFlag)
                    break;
            } //End of the generational run loop

            //Set the run time
            statistics.RunTime = (int)DateTime.Now.Subtract(runStartTime).TotalMilliseconds;

            //calculate the averages
            statistics.AverageGenerationTime = statistics.AverageGenerationTime / generationCounter;

            //Regenerate the results of the best
            if (GepEvaluator.IsParamterOptimizationProblem(settings.FitnessMethod)) {
                statistics.ResultsText = gepEvaluator.EvaluateChromosomeForParameterOptimizationToText(statistics.BestChromosome, settings.FitnessMethod);

            } else {
                //Non PO problem
                statistics.ResultsText = gepEvaluator.EvaluateChromosomeToText(statistics.BestChromosome, testCases);
            }

            //Set the maximum fitness
            statistics.MaxPossibleFitness = maxPossibleFitness;

            //Update the global counters
            statistics.GenerationGeneCountMutations = this._generationGeneCountMutations;

            ///If we're doing a parameter optimziation, update the fitness value in case it was.            
            ///modified by the fmin check
            ///We don't need to update any of ther other statistics because they are set before
            ///the values are updated 
            if (GepEvaluator.IsParamterOptimizationProblem(settings.FitnessMethod)) {
                statistics.BestChromosome.Fitness = gepEvaluator.EvaluateChromosomeForParameterOptimization(statistics.BestChromosome, settings.FitnessMethod);
            }
            //Return the run stats
            return statistics;
        } // End of Run Method



        /// <summary>
        /// 
        /// </summary>
        protected void AddRncTerminalIfRequired() {
            if (_settings.IsRncEnabled) {
                Codon rncCodon = new Codon("?");

                //If the list of terminals doesn't contain the RNC placeholder, we need to add it
                if (!_settings.TerminalList.Contains(rncCodon)) {
                    //Create the new terminal array that will store the codons
                    CodonCollection newTerminalArray = new CodonCollection();

                    //Add the RNC index place holder
                    newTerminalArray.Add(rncCodon);

                    //Add the existing terminals
                    for (int i = 0; i < _settings.TerminalList.Length; i++) {
                        newTerminalArray.Add(_settings.TerminalList[i]);
                    }

                    //Update the terminal list in the settings
                    _settings.TerminalList = newTerminalArray;
                }
            }
        }

        protected void MigrationHandler(int generationCounter, ref List<Chromosome> population) {
            List<int> selectionIndexes = new List<int>();
            List<Chromosome> immigrants = new List<Chromosome>();
            List<Chromosome> emigrants = new List<Chromosome>();

            //Check if migration is enabled and we're ready for a migration event.
            if (_settings.MigrationEnabled && generationCounter % _settings.MigrationInterval == 0) {
                //Get the individuals from the population. Individuals are removed from the population
                selectionIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.MigrationSize, true);

                //Add the individuals to the emigrant list
                foreach (int chromosomeIndex in selectionIndexes) {
                    emigrants.Add(population[chromosomeIndex]);
                }

                //Hand off to the communication layer and way for the immigrants to come back
                this.MigrationRequestHandler(emigrants, out immigrants);

                //Add the individuals to the population, replacing the emigrants. This works
                //because we receive the same number of individuals that we sent.
                int immigrantIndex = 0;
                foreach (int chromosomeIndex in selectionIndexes) {
                    //Update the chromosome at the index and increment the immigrant index
                    population[chromosomeIndex] = immigrants[immigrantIndex++];
                }
            }
        } //End of migration handler


        /// <summary>
        /// Create the list of codons available to the chromosome initialization and mutation routines
        /// from the provided function and terminal sets.
        /// </summary>
        /// <remarks>
        /// Verified working Nov 19, 2008
        /// 
        /// The selection of functions or terminals needs to be weighted as per http://www.gepsoft.com/faq.htm#s4t5.
        /// "
        /// During mutation, the same rules that are used to create the initial population apply, that is, 
        /// when the number of functions in the function set is smaller than the number of terminals, 
        /// for each mutation point in the heads, the probability of it mutating into a function is 1/2. 
        /// When functions outnumber terminals though, all elements (functions and terminals) are equally 
        /// weighted.
        /// "
        /// 
        /// We put these into a member list because it's faster to keep selecting from a bag than to perform
        /// the weighting each time.
        /// </remarks>
        /// <param name="functions"></param>
        /// <param name="terminals"></param>
        /// <returns></returns>
        protected CodonCollection GetHeadCodonSelectionBag(CodonCollection functions, CodonCollection terminals) {
            CodonCollection returnList = new CodonCollection();

            //Check if we have fewer function than terminals
            if (functions.Length < terminals.Length) {
                //Fewer functions, so they need to be duplicated in the bag.
                int functionIndex = 0;
                
                //For each terminal, add a terminal and a function
                foreach (Codon terminal in terminals) {
                    //Add the terminal
                    returnList.Add(terminal);

                    //Add the next function
                    returnList.Add(functions[functionIndex]);

                    //Increment the function index
                    functionIndex++;

                    //If we've reached the end of our functions, reset the index
                    if (functionIndex >= functions.Length) functionIndex = 0;
                }

            } else {
                //More functions, so all equally weighted
                returnList = functions.Union(terminals);
            }
            //Return the bag of codons
            return returnList;
        }

        /// <summary>
        /// Initialize the population
        /// </summary>
        /// <remarks>
        /// May 4/09 - Seems to work
        /// </remarks>
        protected void InitializePopulation(ref List<Chromosome> population) {
            //Initialize the population container
            population = new List<Chromosome>(_settings.PopulationSize);
            int conventionalMaxArity = _settings.MaximumArity;
            int homeoticMaxArity = _settings.HomeoticMaximumArity;

            //Initialize the population
            for (int i = 0; i < _settings.PopulationSize; i++) {
                int numberOfGenes = _settings.IsAcsGeneEnabled ? Utilities.RandomInteger(_settings.AcsGeneCountMinimum, _settings.AcsGeneCountMaximum) : _settings.Genes;
                int headSize = _settings.IsHisTranspositionEnabled ? Utilities.RandomInteger(_settings.HisHeadLengthMinimum, _settings.HisHeadLengthMaximum) : _settings.HeadLength;

                //Initialize the new entity
                Chromosome newChromosome = new Chromosome(numberOfGenes, _settings.HomeoticGenes, 
                    conventionalMaxArity, homeoticMaxArity, 
                    _settings.IsRncEnabled, _settings.RandomConstantsType, _settings.NumberOfRncConstants, 
                    _settings.LowerRncConstantRange, _settings.UpperRncConstantRange);

                //Create an initialize the new genes
                for (int geneIndex = 0; geneIndex < numberOfGenes; geneIndex++) {
                    //Create the new gene and then initialize it
                    Gene newGene = new Gene(_settings.MaximumArity, headSize, newChromosome.UseRnc, newChromosome.RncConstantsPerGene);
                    this.InitializeGene(ref newGene);

                    newChromosome.Genes.Add(newGene);
                } //End of normal gene iterator     

                //Add the new entity to the population
                population.Add(newChromosome);
            }
        } //End of InitializePopulation


        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// May 4/09 - Seems to work
        /// </remarks>
        /// <param name="chromosome"></param>
        /// <param name="codonIndex"></param>
        protected void InitializeGene(ref Gene gene) {
            int chromosomeIndex = 0;        

            //Load the head
            for (int i = 0; i < gene.HeadLength; i++) {
                //Select a head value from the bag. This saves a couple of calls by pulling for a bag.
                //The bag may or maynot be weighted, see this.GetHeadCodonSelectionBag() for details                    
                Codon newElement = _headCodonSelectionBag[Utilities.RandomInteger(0, _headCodonSelectionBag.Count - 1)];

                //Set the element
                gene[chromosomeIndex++] = newElement;
            }

            //Load the tail
            for (int i = 0; i < gene.TailLength; i++) {
                gene[chromosomeIndex++] = _settings.TerminalList[Utilities.RandomInteger(0, _settings.TerminalList.Length - 1)];
            }

            //Load the Dc domain after the tail
            if (_settings.IsRncEnabled) {
                for (int i = 0; i < gene.TailLength; i++) {
                    //Insert a random index from the RNC array into the Dc. The Rnc array indexes are 1 indexed.
                    gene.DcDomain[i] = new Codon(Utilities.RandomInteger(1, gene.RandomNumericalConstants.Count));
                }

                //Add the constants to the gene array
                for (int i = 0; i < gene.RandomNumericalConstants.Count; i++) {
                    switch (_settings.RandomConstantsType) {
                        case RncConstantsType.Integer:
                            gene.RandomNumericalConstants[i] = Utilities.RandomInteger((int)_settings.LowerRncConstantRange, (int)_settings.UpperRncConstantRange);
                            break;

                        case RncConstantsType.Rational:
                            gene.RandomNumericalConstants[i] = Utilities.RandomDoubleRange(_settings.LowerRncConstantRange, _settings.UpperRncConstantRange);
                            break;

                        default:
                            throw new NotImplementedException("The RNC constant type '" + _settings.RandomConstantsType + "' is not implemented in Gene.InitializeRncData(...).");
                    }
                }
            } //End of IsRncEnabled check
        }


        #region Worker and Helper Methods
        /// <summary>
        /// Handles the stochastic sampling
        /// </summary>
        /// <param name="generation"></param>
        /// <param name="bestFitnessPercentage"></param>
        /// <param name="maxPossibleFitness"></param>
        /// <param name="testCases"></param>
        /// <param name="statistics"></param>
        protected void HandleStochasticSampling(int generation, double bestFitnessPercentage, ref float maxPossibleFitness, ref List<TestCase> testCases, ref GepStatistics statistics) {
            if (_settings.IsStochasticSamplingEnabled) {
                bool requestNewSample = false;

                //Check the fitness based trigger
                if (_settings.StochasticSampleMethod == StochasticSampleTriggerMethod.Fitness
                    && bestFitnessPercentage >= _settings.StochasticSampleTrigger) {
                    requestNewSample = true;
                }

                //Check the generation based trigger
                if (_settings.StochasticSampleMethod == StochasticSampleTriggerMethod.Generations
                    && generation % _settings.StochasticSampleTrigger == 0) {
                    requestNewSample = true;
                }

                //Process the sample request, if required.
                if (requestNewSample) {
                    //Request the new samples from the controller
                    this.TestCaseSampleRequestHandler(out testCases);

                    //Validate
                    if (testCases == null || testCases.Count == 0)
                        throw new ArgumentException("Error while receiving new test case samples for stochastic sampling.");

                    //Update the new max possible fitness using the new testcases
                    maxPossibleFitness = this.GetMaxPossibleFitness(_settings, testCases);

                    //Log that we requested new samples
                    statistics.StochasticSampleRequestGenerations.Add(generation);
                }
            }
        }

        /// <summary>
        /// Check if the parsimony pressure trigger has been reached.
        /// </summary>
        /// <returns></returns>
        protected bool ParsimonyPressureTriggerReached() {
            bool triggerReached = false;

            //Switch based on the trigger method
            switch (_settings.ParsimonyPressureTrigger) {
                case ParsimonyPressureTriggerMethod.Fitness:
                    break;

                case ParsimonyPressureTriggerMethod.Generations:
                    break;

                case ParsimonyPressureTriggerMethod.Size:
                    break;
            }

            return triggerReached;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="testCases"></param>
        /// <returns></returns>
        public float GetMaxPossibleFitness(GepSettings settings, List<TestCase> testCases) {
            float maxPossibleFitness = -1;

            if (GepEvaluator.IsParamterOptimizationProblem(settings.FitnessMethod)) {
                maxPossibleFitness = 1;

            } else {
                //Calculate what the max fitness should be
                switch (settings.FitnessMethod) {
                    case FitnessEvaluationMethod.PrecisionAndSelectionRange:
                        maxPossibleFitness = testCases.Count * settings.FitnessSelectionRangeValue;
                        break;

                    case FitnessEvaluationMethod.MeanSquaredError:
                        maxPossibleFitness = settings.FitnessSelectionRangeValue;
                        break;

                    case FitnessEvaluationMethod.HitsWithPenalty:
                        maxPossibleFitness = testCases.Count;
                        break;

                }
            }

            return maxPossibleFitness;
        }


        /// <summary>
        /// Generate a subset of the population. This is used when applying the genetic operators
        /// </summary>
        /// <param name="population"></param>
        /// <param name="selectionRate"></param>
        /// <param name="uniqueSelection"></param>
        /// <returns></returns>
        public static List<int> GetPopulationSubset(List<Chromosome> population, double selectionRate) {
            return GepAlgorithm.GetPopulationSubset(population, selectionRate, true);
        }
        public static List<int> GetPopulationSubset(List<Chromosome> population, int numberOfIndividuals, bool uniqueSelection) {
            return GepAlgorithm.GetPopulationSubset(population, numberOfIndividuals / population.Count, uniqueSelection);
        }
        public static List<int> GetPopulationSubset(List<Chromosome> population, double selectionRate, bool uniqueSelection) {
            int subsetSize = (int)Math.Round(population.Count * selectionRate);
            int sanityCheckCount = population.Count * 10;

            List<int> subsetPopulationIndexes = new List<int>();

            //Collect the subset members
            for (int i = 0; i < sanityCheckCount; i++) {
                int chromosomeIndex = Utilities.RandomInteger(0, population.Count - 1);

                //Handle selection methods
                if (uniqueSelection) {
                    //Need unique chromosomes, so check if the index already exists
                    if (!subsetPopulationIndexes.Contains(chromosomeIndex)) {
                        //Doesn't already exist, so add it
                        subsetPopulationIndexes.Add(chromosomeIndex);
                    }

                } else {
                    //We don't need to check for unique members, just add it
                    subsetPopulationIndexes.Add(chromosomeIndex);
                }

                //Break if we have enough members
                if (subsetPopulationIndexes.Count >= subsetSize) {
                    break;
                }
            }

            return subsetPopulationIndexes;
        }


        /// <summary>
        /// Calcuates the adaptive mutation rate based on an individual's fitness compared to the best fitness.
        /// </summary>
        /// <param name="chromosomeFitness"></param>
        /// <param name="bestGenerationFitness"></param>
        /// <returns></returns>
        protected double GetAdaptiveMutationRate(double chromosomeFitness, double bestGenerationFitness) {
            //Scale the mutation rate based on the fitness of the chromosome to a max of the setting value
            //Even with the scaling, it does not guarrentee that the worst chromosome will be mutated.
            double mutationProbability = (1 - chromosomeFitness / bestGenerationFitness);
            //mutationProbability *= _settings.AcsGeneMutationRate;

            //Set the min
            mutationProbability += 0.05;

            //Decay by the generations.
            //reaches 0 by the last 10% of the run
            double decayRate = 1 - (_currentGenerationIndex + (_settings.MaximumGenerations * 0.2)) / _settings.MaximumGenerations;
            mutationProbability *= decayRate;

            return mutationProbability;
        }


        #endregion

        #region GEP Genetic Operators
        protected void ApplyHisTransposition(ref List<Chromosome> population, double bestGenerationFitness, int bestGenerationSize) {
            if (_settings.IsHisTranspositionEnabled) {
                //Generate the subset
                List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.HisTranspositionRate);

                //Iterate over the members of the subset 
                foreach (int populationIndex in subsetPopulationIndexes) {
                    Chromosome selectedChromosome = population[populationIndex];
                    this.DoHisTransposition(ref selectedChromosome);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyAcsGeneMutation(ref List<Chromosome> population, double bestGenerationFitness, int bestGenerationSize) {
            if (_settings.IsAcsGeneEnabled) {
                //Attempt ACS mutation on all members of the population
                for (int populationIndex = 0; populationIndex < population.Count; populationIndex++) {
                    Chromosome selectedChromosome = population[populationIndex];
                    this.DoAcsGeneMutation(ref selectedChromosome, bestGenerationFitness, bestGenerationSize);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyMutation(ref List<Chromosome> population) {
            //Mutate all members of the population
            for (int populationIndex = 0; populationIndex < population.Count; populationIndex++) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoMutation(ref selectedChromosome);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyInversion(ref List<Chromosome> population) {
            //Generate the subset
            List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.InversionRate);

            //Iterate over the members of the subset 
            foreach(int populationIndex in subsetPopulationIndexes) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoInversion(ref selectedChromosome);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyRncInversion(ref List<Chromosome> population) {
            //Generate the subset
            List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.RncInversionRate);

            //Iterate over the members of the subset 
            foreach (int populationIndex in subsetPopulationIndexes) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoRncInversion(ref selectedChromosome);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyIsTransposition(ref List<Chromosome> population) {
            //Generate the subset
            List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.IsRate);

            //Iterate over the members of the subset 
            foreach(int populationIndex in subsetPopulationIndexes) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoIsTransposition(ref selectedChromosome);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyRisTransposition(ref List<Chromosome> population) {
            //Generate the subset
            List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.RisRate);

            //Iterate over the members of the subset 
            foreach(int populationIndex in subsetPopulationIndexes) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoRisTransposition(ref selectedChromosome);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyGeneTransposition(ref List<Chromosome> population) {
            //Generate the subset
            List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.GeneTranspositionRate);

            //Iterate over the members of the subset 
            foreach(int populationIndex in subsetPopulationIndexes) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoGeneTransposition(ref selectedChromosome);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyRncTransposition(ref List<Chromosome> population) {
            //Generate the subset
            List<int> subsetPopulationIndexes = GepAlgorithm.GetPopulationSubset(population, _settings.RncTranspositionRate);

            //Iterate over the members of the subset 
            foreach (int populationIndex in subsetPopulationIndexes) {
                Chromosome selectedChromosome = population[populationIndex];
                this.DoRncTransposition(ref selectedChromosome);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyOnePointRecombination(ref List<Chromosome> population) {
            int maxIndex = 0;

            //Get the subsets
            List<int> mainPopulationSubset = GepAlgorithm.GetPopulationSubset(population, _settings.OnePointCrossoverRate);
            List<int> subsetA = new List<int>();
            List<int> subsetB = new List<int>();

            //Divide into two sets, these subset will be mutually exlusive
            for (int i = 0; i < mainPopulationSubset.Count; i++) {
                if (i % 2 == 0)
                    subsetA.Add(mainPopulationSubset[i]);
                else
                    subsetB.Add(mainPopulationSubset[i]);                
            }

            //Get the max index of the population, if they aren't equal, one 
            //is probably off by one, because of the mod seperation above. We
            //want the lowest of the two, so we get an even match up
            maxIndex = subsetA.Count;
            if (subsetB.Count < maxIndex) maxIndex = subsetB.Count;

            //Crossover the two populations
            for (int crossoverIndex = 0; crossoverIndex < maxIndex; crossoverIndex++) {
                Chromosome selectedChromosomeA = population[subsetA[crossoverIndex]];
                Chromosome selectedChromosomeB = population[subsetB[crossoverIndex]];

                this.DoOnePointRecombination(ref selectedChromosomeA, ref selectedChromosomeB);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyTwoPointRecombination(ref List<Chromosome> population) {
            int maxIndex = 0;

            //Get the subsets
            List<int> mainPopulationSubset = GepAlgorithm.GetPopulationSubset(population, _settings.TwoPointCrossoverRate);
            List<int> subsetA = new List<int>();
            List<int> subsetB = new List<int>();

            //Divide into two sets, these subset will be mutually exlusive
            for (int i = 0; i < mainPopulationSubset.Count; i++) {
                if (i % 2 == 0)
                    subsetA.Add(mainPopulationSubset[i]);
                else
                    subsetB.Add(mainPopulationSubset[i]);
            }

            //Get the max index of the population, if they aren't equal, one 
            //is probably off by one, because of the mod seperation above. We
            //want the lowest of the two, so we get an even match up
            maxIndex = subsetA.Count;
            if (subsetB.Count < maxIndex) maxIndex = subsetB.Count;

            //Crossover the two populations
            for (int crossoverIndex = 0; crossoverIndex < maxIndex; crossoverIndex++) {
                Chromosome selectedChromosomeA = population[subsetA[crossoverIndex]];
                Chromosome selectedChromosomeB = population[subsetB[crossoverIndex]];

                this.DoTwoPointRecombination(ref selectedChromosomeA, ref selectedChromosomeB);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        protected void ApplyGeneRecombination(ref List<Chromosome> population) {
            int maxIndex = 0;

            //Get the subsets
            List<int> mainPopulationSubset = GepAlgorithm.GetPopulationSubset(population, _settings.GeneRecombinationRate);
            List<int> subsetA = new List<int>();
            List<int> subsetB = new List<int>();

            //Divide into two sets, these subset will be mutually exlusive
            for (int i = 0; i < mainPopulationSubset.Count; i++) {
                if (i % 2 == 0)
                    subsetA.Add(mainPopulationSubset[i]);
                else
                    subsetB.Add(mainPopulationSubset[i]);
            }

            //Get the max index of the population, if they aren't equal, one 
            //is probably off by one, because of the mod seperation above. We
            //want the lowest of the two, so we get an even match up
            maxIndex = subsetA.Count;
            if (subsetB.Count < maxIndex) maxIndex = subsetB.Count;

            //Crossover the two populations
            for (int crossoverIndex = 0; crossoverIndex < maxIndex; crossoverIndex++) {
                Chromosome selectedChromosomeA = population[subsetA[crossoverIndex]];
                Chromosome selectedChromosomeB = population[subsetB[crossoverIndex]];

                this.DoGeneRecombination(ref selectedChromosomeA, ref selectedChromosomeB);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <remarks> 
        /// Tested May 13, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        /// <param name="bestGenerationFitness"></param>
        /// <param name="bestGenerationGeneCount"></param>
        protected void DoHisTransposition(ref Chromosome chromosome) {            
            //Select the source and destination genes
            int sourceGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);
            int destinationGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Only proceed if the source and destination genes are different
            if (sourceGene != destinationGene && chromosome[sourceGene].HeadLength > 3) {
                //Store the old tail length
                int oldTailLength = chromosome[sourceGene].TailLength;
                int destinationOldTailLength = chromosome[destinationGene].TailLength;

                //Select the destination point in the head of the destination gene
                int destinationPoint = Utilities.RandomInteger(0, chromosome[destinationGene].HeadLength - 1);

                //Select the length of the cut
                int cutLength = Utilities.RandomInteger(1, 3);

                if (cutLength > chromosome[sourceGene].HeadLength)
                    return;

                //Select the start of the cut, that is less than the head length - cutLength (so that it is always in the head)
                int cutStart = Utilities.RandomInteger(0, chromosome[sourceGene].HeadLength - cutLength);

                //Verify that we have a big enough head
                if (cutStart + cutLength > chromosome[sourceGene].HeadLength)
                    return;

                //Cut the source codons and insert into the destination gene
                List<Codon> cutCodons = chromosome[sourceGene].CutRange(cutStart, cutLength);
                chromosome[destinationGene].InsertCodons(destinationPoint, cutCodons);

                //Update the head lengths
                chromosome[sourceGene].HeadLength -= cutLength;
                chromosome[destinationGene].HeadLength += cutLength;

                //Calculate the expected length of the new, shorter tail of gene 1
                int newTailLength = GepAlgorithm.CalculateTailLength(chromosome[sourceGene].HeadLength, chromosome.MaxArity);
                int destinationNewTailLength = GepAlgorithm.CalculateTailLength(chromosome[destinationGene].HeadLength, chromosome.MaxArity);

                //Adjust the tails and the dc domain if the tail lengths have changed
                if (oldTailLength != newTailLength) {
                    cutCodons = chromosome[sourceGene].CutEnd(chromosome[sourceGene].HeadLength + newTailLength);

                    int destinationTailChange = destinationNewTailLength - destinationOldTailLength;
                    int cutIndex = 0;
                    for (int i = 0; i < destinationTailChange; i++) {
                        chromosome[destinationGene].AppendCodons(cutCodons[cutIndex]);

                        cutIndex ++;
                        if (cutIndex >= cutCodons.Count) cutIndex = 0;
                    }

                    //If Rnc is enabled, update the dc domain
                    if (chromosome.UseRnc) {
                        cutCodons = chromosome[sourceGene].CutDcEnd(newTailLength);

                        cutIndex = 0;
                        for (int i = 0; i < destinationTailChange; i++) {
                            chromosome[destinationGene].AppendDcCodons(cutCodons[cutIndex]);

                            cutIndex++;
                            if (cutIndex >= cutCodons.Count) cutIndex = 0;
                        }
                    }
                } //End of tail length check

                /*
                if (chromosome[sourceGene].Length != chromosome[sourceGene].HeadLength + newTailLength) 
                    throw new Exception("Error in source gene length after HIS transposition.");

                if (chromosome[destinationGene].Length != chromosome[destinationGene].HeadLength + destinationNewTailLength) 
                    throw new Exception("Error in destination gene length after HIS transposition.");

                if (chromosome[sourceGene].DcDomain.Count != newTailLength)
                    throw new Exception("Error in source gene Dc length after HIS transposition.");

                if (chromosome[destinationGene].DcDomain.Count != destinationNewTailLength)
                    throw new Exception("Error in destination gene Dc length after HIS transposition.");
                 */
                
            } //End of source gene check
        }


        /// <summary>
        /// Mutates the number of genes within a chromosome.
        /// </summary>
        /// <remarks> 
        /// Unverified
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoAcsGeneMutation(ref Chromosome chromosome, double bestGenerationFitness, int bestGenerationSize) {
            //Get the mutation rate to use for the current chromosome
            double mutationProbability = this.GetAdaptiveMutationRate(chromosome.Fitness, bestGenerationFitness);
            
            bool doMutation = Utilities.RandomProbability() <= mutationProbability;

            if (doMutation) {
                //bool growChromosome = Utilities.RandomProbability() <= _settings.AdaptiveChromosomeSizeMutationGrowShinkRatio;

                if (!_generationGeneCountMutations[_currentGenerationIndex].ContainsKey(chromosome.Genes.Count)) {
                    _generationGeneCountMutations[_currentGenerationIndex].Add(chromosome.Genes.Count, 1);

                } else {
                    _generationGeneCountMutations[_currentGenerationIndex][chromosome.Genes.Count]++;
                }

                //Grow the chromosome only if we're smaller than the best
                //bool growChromosome = chromosome.Genes <= bestGenerationGeneCount;
                bool growChromosome = Utilities.CoinToss();

                //Handle the insertion or deletion of a gene
                if (growChromosome) {
                    //Grow the chromosome, start by choosing the insertion point. We alllow the system to choose a point
                    //after the chromosome because we want to allow insertion at the end of the chromosome.
                    int insertionIndex = Utilities.RandomInteger(0, chromosome.Genes.Count);

                    //todo: When inserting a new gene using the DoAcsGeneMutation, need to set the headlength using a better method
                    Gene insertionGene = new Gene(_settings.MaximumArity, _settings.HeadLength, chromosome.UseRnc, chromosome.RncConstantsPerGene);

                    //Initialize the gene (including any RNC values, etc)
                    this.InitializeGene(ref insertionGene);

                    //Insert the gene
                    chromosome.Genes.Insert(insertionIndex, insertionGene);
                } else {
                    //Verify that there are atleast two genes, otherwise we can't shrink.
                    if (chromosome.Genes.Count > 1) {
                        //Shink the chromosome, start by choosing the gene to delete
                        int deletionGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

                        //Delete the gene
                        chromosome.Genes.RemoveAt(deletionGene);
                    }

                } // End of grow/shink
            }
        } // End of DoAcsGeneMutation


        /// <summary>
        /// Perform a single point mutation on the supplied chromosome.
        /// </summary>
        /// <remarks>
        /// Tested and verified May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoMutation(ref Chromosome chromosome) {
            int mutationPoint = -1;
            Codon mutatedCodon = null;

            //Select the mutation gene
            int mutationGene = Utilities.RandomInteger(0, chromosome.Genes.Count -1);
            
            //Select the mutation point
            mutationPoint = Utilities.RandomInteger(0, chromosome[mutationGene].Length + chromosome[mutationGene].DcDomain.Count - 1);

            //Point is in the head or the tail
            if (mutationPoint < chromosome[mutationGene].Length) {
                //Get the mutated codon
                if (chromosome[mutationGene].IsInHead(mutationPoint)) {
                    //Select a head value from the bag. This saves a couple of calls by pulling for a bag.
                    //The bag may or maynot be weighted, see this.GetHeadCodonSelectionBag() for details                    
                    mutatedCodon = _headCodonSelectionBag[Utilities.RandomInteger(0, _headCodonSelectionBag.Count - 1)];

                } else {
                    //Point is in the tail, so we need a terminal
                    mutatedCodon = _settings.TerminalList[Utilities.RandomInteger(0, _settings.TerminalList.Length - 1)];
                }

                //Set the mutated codon
                chromosome[mutationGene][mutationPoint] = mutatedCodon;

            } else {
                //Point is in the Dc Domain
                //Recalculate the offset into the Dc domain
                mutationPoint -= chromosome[mutationGene].Length;

                //Mutation is in the Dc domain, so choose a new RNC index. RNC indexes are 1 based.
                mutatedCodon = new Codon(Utilities.RandomInteger(1, _settings.NumberOfRncConstants));

                //Set the mutated codon
                chromosome[mutationGene].DcDomain[mutationPoint] = mutatedCodon;
            }
        } //End of do mutation


        /// <summary>
        /// Perform inversion on the supplied chromosome. 
        /// The inversion point may only be selected from the head - CF book page 81.
        /// </summary>
        /// <remarks> 
        /// Tested May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoInversion(ref Chromosome chromosome) {
            int geneIndex = -1;
            int inversionStart = -1;
            int inversionStop = -1;

            //Handle inversion of the normal genes
            geneIndex = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Get the start point within the head
            inversionStart = Utilities.RandomInteger(0, chromosome[geneIndex].HeadLength - 1);

            //Select the stop location, this must also be inthe head
            inversionStop = Utilities.RandomInteger(inversionStart, chromosome[geneIndex].HeadLength - 1);

            //Assign to temp vars
            int startPoint = inversionStart;
            int stopPoint = inversionStop;

            //Only do the inversion if there is two or more codons in the inversion. Otherwise,
            //nothing is being accomplished by looping.
            if (inversionStop - inversionStart >= 1) {
                //Flip the start and end and move the indices toward each other
                while (startPoint <= stopPoint && startPoint >= 0 && stopPoint >= 0) {
                    Codon temporaryCodon = chromosome[geneIndex][startPoint];
                    chromosome[geneIndex][startPoint] = chromosome[geneIndex][stopPoint];
                    chromosome[geneIndex][stopPoint] = temporaryCodon;

                    //step the start and end along toward each other
                    startPoint++;
                    stopPoint--;
                }
            }
        }


        /// <summary>
        /// Perform inversion in the Dc domain, on the supplied chromosome. 
        /// </summary>
        /// <remarks> 
        /// Tested May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoRncInversion(ref Chromosome chromosome) {
            //If we were called for some reason and RNC isn't enabled, error
            if (!_settings.IsRncEnabled)
                throw new Common.Exceptions.SyrahGeneralException("GepAlgorithm.DoRncInversion(...) was called when the GEP-RNC Algorithm was not enabled.");

            //Handle inversion of the normal genes
            int geneIndex = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Get the start point within the Dc domain
            int inversionStart = Utilities.RandomInteger(0, chromosome[geneIndex].DcDomain.Count - 1);

            //Calculate the end of the inversion, it must be after the start and before the end of the Dc domain.
            int inversionStop = Utilities.RandomInteger(inversionStart, chromosome[geneIndex].DcDomain.Count - 1);

            //Assign to temp vars
            int startPoint = inversionStart;
            int stopPoint = inversionStop;

            //Only do the inversion if there is two or more codons in the inversion. Otherwise, nothing is being accomplished by looping.
            if (inversionStop - inversionStart >= 1) {
                //Flip the start and end and move the indices toward each other
                while (startPoint <= stopPoint && startPoint >= 0 && stopPoint >= 0) {
                    Codon temporaryCodon = chromosome[geneIndex].DcDomain[startPoint];
                    chromosome[geneIndex].DcDomain[startPoint] = chromosome[geneIndex].DcDomain[stopPoint];
                    chromosome[geneIndex].DcDomain[stopPoint] = temporaryCodon;

                    //step the start and end along toward each other
                    startPoint++;
                    stopPoint--;
                }
            }
        }


        /// <summary>
        /// Perform insertion sequence transposition on the provided chromosome.
        /// </summary>
        /// <remarks> 
        /// Test May 4, 2009
        /// todo: Update this code to only queue and select as many codons as will fit in the head, to be more efficient                
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoIsTransposition(ref Chromosome chromosome) {
            Queue<Codon> codonQueue = new Queue<Codon>();

            //Select the source gene
            int sourceGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Get the source points from any where in the head or the tail of the gene
            int sourceStart = Utilities.RandomInteger(0, chromosome[sourceGene].HeadLength - 1);
            int sourceEnd = Utilities.RandomInteger(sourceStart, chromosome[sourceGene].Length - 1);           

            //Select the target gene within the normal area
            int targetGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            if (chromosome[targetGene].HeadLength == 0) return;

            //Select the target location within the gene, excluding the root
            //Then calculat the end point, which is always the end of the head section of the target gene
            int targetStart = Utilities.RandomInteger(1, chromosome[targetGene].HeadLength - 1);
            int targetEnd = chromosome[targetGene].HeadLength - 1;

            //Copy the source
            for (int codonIndex = sourceStart; codonIndex <= sourceEnd; codonIndex++) {
                codonQueue.Enqueue(chromosome[sourceGene][codonIndex]);
            }

            //Insert the source
            for (int codonIndex = targetStart; codonIndex <= targetEnd; codonIndex++) {
                codonQueue.Enqueue(chromosome[targetGene][codonIndex]);
                chromosome[targetGene][codonIndex] = codonQueue.Dequeue();
            }
        }


        /// <summary>
        /// Perform root insertion sequence transposition on the supplied chromosome.
        /// </summary>
        /// <remarks> 
        /// Test May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoRisTransposition(ref Chromosome chromosome) {
            bool foundFunction = false;
            Queue<Codon> codonQueue = new Queue<Codon>();

            //Choose the source gene
            int sourceGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Normal gene, get the source start and end points. These must be in the head
            int sourceStart = Utilities.RandomInteger(0, chromosome[sourceGene].HeadLength - 1);
            int sourceEnd = Utilities.RandomInteger(sourceStart, chromosome[sourceGene].HeadLength - 1);

            //Now confirm if we have a function as the start, if not, keep indexing
            for (int i = sourceStart; i <= sourceEnd; i++) {
                if (GepEvaluator.GetArity(chromosome[sourceGene][i]) > 0) {
                    sourceStart = i;
                    foundFunction = true;
                    break;
                }
            }                    

            //If we found a function, then perform the RIS transposition, otherwise, fall out the bottom.
            if (foundFunction) {
                //Select the target gene 
                int targetGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

                //Calculate the target start and end. For RIS, it will always
                //be the start and end of the head section of the target gene
                int targetStart = 0;
                int targetEnd = chromosome[targetGene].HeadLength - 1;

                //Copy the source into the queue
                for (int codonIndex = sourceStart; codonIndex <= sourceEnd; codonIndex++) {
                    codonQueue.Enqueue(chromosome[sourceGene][codonIndex]);
                }

                //Insert the source into the target head's root
                for (int codonIndex = targetStart; codonIndex <= targetEnd; codonIndex++) {
                    codonQueue.Enqueue(chromosome[targetGene][codonIndex]);
                    chromosome[targetGene][codonIndex] = codonQueue.Dequeue();
                }
            }
        } // End of DoRisTransposition(...)


        /// <summary>
        /// Perform gene transposition on the supplied chromosome.
        /// </summary>
        /// <remarks> 
        /// Tested May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoGeneTransposition(ref Chromosome chromosome) {
            //Choose the source gene to transpose
            int sourceGene = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Remove the gene
            Gene transposedGene = chromosome[sourceGene];
            chromosome.Genes.RemoveAt(sourceGene);

            ///Select the site for the gene insertion, it can be at any gene boundary.
            ///We don't do the normal Count - 1 because we can to be able to append to hte end of the gene
            int insertionPoint = Utilities.RandomInteger(0, chromosome.Genes.Count);

            //Insert the gene
            chromosome.Genes.Insert(insertionPoint, transposedGene);
        }


        /// <summary>
        /// Perform tranposition in the Dc (RNC) domain
        /// </summary>
        /// <remarks> 
        /// Tested May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoRncTransposition(ref Chromosome chromosome) {
            //Instantiate the stack to store the codons while moving
            Stack<Codon> codonStack = new Stack<Codon>();

            //Throw an exception if this is being called and the RNC algorithm isn't enabled
            if (!_settings.IsRncEnabled)
                throw new SyrahGeneralException("The GEP-RNC algorithm is not enabled and the GepAlgorithm.DoRncTransposition was called.");

            //Randomly pick a gene
            int geneIndex = Utilities.RandomInteger(0, chromosome.Genes.Count - 1);

            //Get the source point and length within the Dc domain
            int sourceStart = Utilities.RandomInteger(0, chromosome[geneIndex].DcDomain.Count - 1);
            int sourceLength = Utilities.RandomInteger(1, chromosome[geneIndex].DcDomain.Count - sourceStart); //No -1 
            
            //Copy the source codons
            for (int codonIndex = sourceStart; codonIndex < sourceStart + sourceLength; codonIndex++) {
                //Push the codon onto the stack
                codonStack.Push(chromosome[geneIndex].DcDomain[codonIndex]);
            }

            //Delete the source codons
            chromosome[geneIndex].DcDomain.RemoveRange(sourceStart, sourceLength);

            //Select the target location within the remaining Dc elements
            //No minus 1, since we want to be able to insert at the end
            //todo: verify in DoRnTransposition that transpositionTarget works for boundary conditions
            int transpositionTarget = Utilities.RandomInteger(0, chromosome[geneIndex].DcDomain.Count);

            //Insert the source codons at the target location. 
            //Since the codons are in a stack, pop the stack and insert at the same spot.
            while (codonStack.Count > 0) {
                chromosome[geneIndex].DcDomain.Insert(transpositionTarget, codonStack.Pop());
            }            
        }
         

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chromosome1"></param>
        /// <param name="chromosome2"></param>
        /// <param name="codonIndex"></param>
        /// <remarks>
        /// Tested may 14, 2009
        /// The size of chromosome1 and chromosome2 remain the same since we are crossing the codons before each point
        /// </remarks>
        protected void MidGeneRecombinationWorker(ref Chromosome chromosome1, ref Chromosome chromosome2, int geneIndex, int crossoverPoint) {
            Gene shorterGene = null, longerGene = null;
            List<Codon> shorterGeneCut = null, longerGeneCut = null;

            //Reassign to shorter and longer 
            if (chromosome1[geneIndex].TotalLength < chromosome2[geneIndex].TotalLength) {
                shorterGene = chromosome1[geneIndex];
                longerGene = chromosome2[geneIndex];
            } else {
                shorterGene = chromosome2[geneIndex];
                longerGene = chromosome1[geneIndex];
            }

            GeneParts crossoverPart = shorterGene.GetGenePartAtIndex(crossoverPoint);

            //Handle if the crossover part is in the head
            if (crossoverPart == GeneParts.Head) {
                //Cut the head/tails
                shorterGeneCut = shorterGene.CutEnd(crossoverPoint);
                longerGeneCut = longerGene.CutEnd(crossoverPoint);
                
                //Append the cut heads
                shorterGene.AppendCodons(longerGeneCut);
                longerGene.AppendCodons(shorterGeneCut);

                //Flip the head lengths
                int tempHeadLength = shorterGene.HeadLength;
                shorterGene.HeadLength = longerGene.HeadLength;
                longerGene.HeadLength = tempHeadLength;

                //Swap the entire Dc domains, since they will then match the correct lengths
                if (shorterGene.UseRnc && longerGene.UseRnc) {
                    shorterGeneCut = shorterGene.DcDomain;
                    shorterGene.DcDomain = longerGene.DcDomain;
                    longerGene.DcDomain = shorterGeneCut;
                }
            }

            //Handle if the crossover point is in the tail
            if (crossoverPart == GeneParts.Tail) {
                //calcuate the offset from the start of the tail of the shortest gene.
                int tailOffset = crossoverPoint - shorterGene.HeadLength;
                
                //Calcuate the length of the overlapping section that will be exchanged.
                int tailCrossoverLength = shorterGene.Length - crossoverPoint;

                shorterGeneCut = shorterGene.CutRange(shorterGene.HeadLength + tailOffset, tailCrossoverLength);
                longerGeneCut = longerGene.CutRange(longerGene.HeadLength + tailOffset, tailCrossoverLength);

                //put the cut portions into the opposite individual
                shorterGene.InsertCodons(shorterGene.HeadLength + tailOffset, longerGeneCut);
                longerGene.InsertCodons(longerGene.HeadLength + tailOffset, shorterGeneCut);

                //Swap the dc domains up to the end of the shortest gene's dc domain. This will maintain the length
                if (shorterGene.UseRnc && longerGene.UseRnc) {
                    int overlappingDcLength = shorterGene.DcDomain.Count;
                    shorterGeneCut = shorterGene.CutDcRange(0, overlappingDcLength); //Cut all of the shorter gene
                    longerGeneCut = longerGene.CutDcRange(0, overlappingDcLength); //Cut the overlapping portion of the longer gene
                    
                    shorterGene.InsertDcCodons(0, longerGeneCut); //Insert the overlapping section from the longer gene
                    longerGene.InsertDcCodons(0, shorterGeneCut); //Insert the shorter gene's dc
                }
            }

            //If the crossover point was in the dc domain, handle it
            if (crossoverPart == GeneParts.Dc) {
                //calcuate the offset from the start of the dc of the shortest gene.
                int dcOffset = crossoverPoint - shorterGene.Length;

                //Calcuate the length of the overlapping section that will be exchanged.
                int dcCrossoverLength = shorterGene.DcDomain.Count - dcOffset;

                shorterGeneCut = shorterGene.CutDcRange(dcOffset, dcCrossoverLength);
                longerGeneCut = longerGene.CutDcRange(dcOffset, dcCrossoverLength);

                //put the cut portions into the opposite individual
                shorterGene.InsertDcCodons(dcOffset, longerGeneCut);
                longerGene.InsertDcCodons(dcOffset, shorterGeneCut);
            }
        }


        /// <summary>
        /// 
        /// </summary>        
        /// <param name="chromosome1"></param>
        /// <param name="chromosome2"></param>
        /// <param name="geneIndex"></param>
        /// <remarks>
        /// Tested May 14, 2009
        /// </remarks>
        protected void DoOnePointRecombinationWorker(ref Chromosome chromosome1, ref Chromosome chromosome2, int geneIndex) {
            //Get the crossover point
            int crossoverPoint = Utilities.RandomInteger(0, Utilities.GetSmallerValue(chromosome1[geneIndex].TotalLength, chromosome2[geneIndex].TotalLength) - 1);

            //Crossover the gene containing the crossover point
            this.MidGeneRecombinationWorker(ref chromosome1, ref chromosome2, geneIndex, crossoverPoint);

            //Cut the any genes remaining after the crossover gene
            List<Gene> cut1 = chromosome1.CutEndGenes(geneIndex + 1);
            List<Gene> cut2 = chromosome2.CutEndGenes(geneIndex + 1);

            //Now append the ends to the other chromosome
            chromosome1.AppendGenes(cut2);
            chromosome2.AppendGenes(cut1);    
        }

        /// <summary>
        /// Perform one point recombination (crossover) on the two supplied chromosomes. The 
        /// recombination is done in place, so the two chromosomes start as the parents and
        /// are modified to become the children.
        /// </summary>
        /// <remarks> 
        /// Tested May 14, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoOnePointRecombination(ref Chromosome chromosome1, ref Chromosome chromosome2) {
            //Pick the crossover gene
            int geneIndex = Utilities.RandomInteger(0, Utilities.GetSmallerValue(chromosome1.Genes.Count, chromosome2.Genes.Count) -1);

            //Hand off to the worker, it will randomly select a point in the target gene
            this.DoOnePointRecombinationWorker(ref chromosome1, ref chromosome2, geneIndex);     
        }


        /// <summary>
        /// Perform two point recombination (crossover) on the two supplied chromosomes. The 
        /// recombination is done in place, so the two chromosomes start as the parents and
        /// are modified to become the children.
        /// </summary>
        /// <remarks> 
        /// Tested may 14, 2009s
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoTwoPointRecombination(ref Chromosome chromosome1, ref Chromosome chromosome2) {
            int geneCount = Utilities.GetSmallerValue(chromosome1.Genes.Count, chromosome2.Genes.Count);
            int startGene = Utilities.RandomInteger(0, geneCount-1);
            int endGene = Utilities.RandomInteger(startGene, geneCount-1);

            //call the one point operator for the start gene, it will randomly select the cross over point
            this.DoOnePointRecombinationWorker(ref chromosome1, ref chromosome2, startGene);

            //call the one point operator again, but for the end gene
            this.DoOnePointRecombinationWorker(ref chromosome1, ref chromosome2, endGene);
        }

        /// <summary>
        /// Perform gene recombination on the supplied chromosomes. This transfers an entire gene
        /// between the two chromosomes.
        /// </summary>
        /// <remarks> 
        /// Tested May 4, 2009
        /// </remarks>
        /// <param name="chromosome"></param>
        protected void DoGeneRecombination(ref Chromosome chromosome1, ref Chromosome chromosome2) {
            int shortestGeneCount = Utilities.GetSmallerValue(chromosome1.Genes.Count, chromosome2.Genes.Count);

            //Get the gene for recombination
            int targetGene = Utilities.RandomInteger(0, shortestGeneCount - 1);

            Gene tempGene = chromosome1[targetGene];
            chromosome1[targetGene] = chromosome2[targetGene];
            chromosome2[targetGene] = tempGene;
        }
        #endregion
    }
}
