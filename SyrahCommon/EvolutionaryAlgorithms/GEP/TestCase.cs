﻿/***********************************************************************
 * TestCase.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Stores the test case data used by the GEP algorithm.
 ***********************************************************************/


using System;
using System.Collections.Generic;

using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    [Serializable]
    public class TestCase : IXmlSerializable {
        public readonly int CLASS_VERSION = 2;

        public Dictionary<Codon, double> TerminalValues = new Dictionary<Codon, double>();
        public double ExpectedValue = double.NaN;

        /// <summary>
        /// Default constructor
        /// </summary>
        public TestCase() {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expectedValue"></param>
        /// <param name="terminalValues"></param>
        public TestCase(double expectedValue, Dictionary<Codon, double> terminalValues) {
            this.TerminalValues = terminalValues;
            this.ExpectedValue = expectedValue;
        }


        /// <summary>
        /// Control how this class is serialized to xml.
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="writer"></param>
        public void WriteXml(XmlWriter writer) {
            //Add a version code to the test value
            writer.WriteAttributeString("version", CLASS_VERSION.ToString());

            //Add the Expected value element
            writer.WriteElementString("ExpectedValue", this.ExpectedValue.ToString());
            
            // Serialize each terminal this collection holds
            foreach (Codon terminal in this.TerminalValues.Keys) {
                //Start a new terminal tag
                writer.WriteStartElement("TerminalValue");

                //Add the terminal codon as an attribute
                writer.WriteAttributeString("terminal", terminal.ToString());

                //Write out the value
                writer.WriteValue(this.TerminalValues[terminal]);

                //End the current terminal tag
                writer.WriteEndElement();
            }
        }


        /// <summary>
        /// Control how this class is deserialized from xml
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader) {
            //Read the version code of this testcase
            string version = reader.GetAttribute("version");

            //Read past the testcase container
            reader.Read();

            //Read the expected value
            this.ExpectedValue = reader.ReadElementContentAsDouble();

            while (true) {
                if (reader.Name.ToLower() == "terminalvalue") {
                    string terminal = reader.GetAttribute("terminal");
                    double terminaValue = reader.ReadElementContentAsDouble();

                    this.TerminalValues.Add(new Codon(terminal), terminaValue);
                }

                //If we hit the test case end tag, we should exit
                if (reader.Name.ToLower() == "testcase") break;
            }

            //Read past the testcase end tag, otherwise the deserialization process won't continue to the 
            //next test case.
            reader.Read();
        }


        /// <summary>
        /// Returns a null schema, this method is required by the IXmlSerializable interface.
        /// </summary>
        /// <remarks>
        /// This method isn't actually required for what we're doing
        /// </remarks>
        /// <returns></returns>
        public XmlSchema GetSchema() {
            return (null);
        }

    }
}
