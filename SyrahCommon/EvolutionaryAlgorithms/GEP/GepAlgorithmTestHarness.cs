﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    public class GepAlgorithmTestHarness : GepAlgorithm {
        public System.Windows.Forms.ListBox OutputListBox { get; set; }

        public GepAlgorithmTestHarness() : base() {
            this.OutputListBox = null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        protected void WriteLog(string message) {
            if (OutputListBox != null) {
                OutputListBox.Items.Add(message);
            }
        }

        public override void Run() {
            throw new Exception("Do not call the run method directly from the test harness sub class.");

        }

        protected void InitializeSettings() {
            _settings = new GepSettings();
            _settings.UseStaticLinkingFunction = true;
            _settings.StaticLinkingFunction = '+';
            
            _settings.PopulationSize = 100;

            _settings.HeadLength = 5;
            _settings.HisHeadLengthMinimum = 5;
            _settings.HisHeadLengthMaximum = 10;
            _settings.Genes = 2;

            _settings.RandomConstantsType = RncConstantsType.Rational;
            _settings.NumberOfRncConstants = 10;
            _settings.UpperRncConstantRange = 10;
            _settings.LowerRncConstantRange = 0;
            _settings.IsRncEnabled = true;

            _settings.IsAcsGeneEnabled = true;
            _settings.IsHisTranspositionEnabled = true;
            
            _settings.TerminalList = new CodonCollection();
            _settings.TerminalList.Add(new Codon("a"));
            _settings.TerminalList.Add(new Codon("b"));
            _settings.TerminalList.Add(new Codon("c"));
            _settings.TerminalList.Add(new Codon("e"));
            _settings.TerminalList.Add(new Codon("f"));

            _settings.FunctionList.Add(new Codon("+"));
            _settings.FunctionList.Add(new Codon("-"));
            _settings.FunctionList.Add(new Codon("*"));
            _settings.FunctionList.Add(new Codon("/"));
            _settings.FunctionList.Add(new Codon("E"));
            _settings.FunctionList.Add(new Codon("S"));
        }

        /// <summary>
        /// 
        /// </summary>
        public void RunTest() {
            this.WriteLog("Starting test.");
            int numberOfTestRuns = 500;

            for (int j = 1; j <=  numberOfTestRuns ; j++) {
                WriteLog("Run " + j.ToString() + "/" + numberOfTestRuns.ToString());

                this.InitializeSettings();
                List<Chromosome> population = new List<Chromosome>();
                this.AddRncTerminalIfRequired();
                _headCodonSelectionBag = this.GetHeadCodonSelectionBag(_settings.FunctionList, _settings.TerminalList);
                this.InitializePopulation(ref population);

                for (int i = 0; i < population.Count; i++) {
                    Chromosome chromosome = population[i];
                    Chromosome chromo2 = population[population.Count - 1 - i];

                    this.DoMutation(ref chromosome); //Tested
                    chromosome.Validate();

                    this.DoInversion(ref chromosome); //Tested                    
                    chromosome.Validate();

                    this.DoIsTransposition(ref chromosome); //Tested
                    chromosome.Validate();

                    this.DoRisTransposition(ref chromosome); //Tested
                    chromosome.Validate();

                    this.DoGeneTransposition(ref chromosome); //Tested
                    chromosome.Validate();

                    this.DoHisTransposition(ref chromosome);
                    chromosome.Validate();

                    if (chromosome.UseRnc) this.DoRncInversion(ref chromosome); //Tested
                    chromosome.Validate();

                    if (chromosome.UseRnc) this.DoRncTransposition(ref chromosome); //Tested
                    chromosome.Validate();                    

                    this.DoOnePointRecombination(ref chromosome, ref chromo2); //Tested
                    chromosome.Validate();
                    chromo2.Validate();

                    this.DoTwoPointRecombination(ref chromosome, ref chromo2); //Tested
                    chromosome.Validate();
                    chromo2.Validate();

                    this.DoGeneRecombination(ref chromosome, ref chromo2); //Tested
                    chromosome.Validate();
                    chromo2.Validate();
                }
            }

            this.WriteLog("End of test.");
        }


        protected void Inds() {
            Chromosome chromosome1 = new Chromosome(1, 0, 2, 0, _settings.IsRncEnabled, _settings.RandomConstantsType, _settings.NumberOfRncConstants, _settings.LowerRncConstantRange, _settings.UpperRncConstantRange);
            chromosome1[1][0] = new Codon("*");
            chromosome1[1][1] = new Codon("-");
            chromosome1[1][2] = new Codon("+");
            chromosome1[1][3] = new Codon("a");
            chromosome1[1][4] = new Codon("/");
            chromosome1[1][5] = new Codon("b");
            chromosome1[1][6] = new Codon("c");
            chromosome1[1][7] = new Codon("e");
            chromosome1[1][8] = new Codon("S");
            chromosome1[1][9] = new Codon("f");
            chromosome1[1].DcDomain[0] = new Codon("1");
            chromosome1[1].DcDomain[0] = new Codon("1");
            chromosome1[1].DcDomain[0] = new Codon("1");
            chromosome1[1].DcDomain[0] = new Codon("1");
            chromosome1[1].DcDomain[0] = new Codon("1");

            Chromosome chromosome2 = new Chromosome(1, 0, 2, 0, _settings.IsRncEnabled, _settings.RandomConstantsType, _settings.NumberOfRncConstants, _settings.LowerRncConstantRange, _settings.UpperRncConstantRange);
            chromosome2[1][0] = new Codon("-");
            chromosome2[1][1] = new Codon("*");
            chromosome2[1][2] = new Codon("*");
            chromosome2[1][3] = new Codon("l");
            chromosome2[1][4] = new Codon("-");
            chromosome2[1][5] = new Codon("m");
            chromosome2[1][6] = new Codon("n");
            chromosome2[1][7] = new Codon("o");
            chromosome2[1][8] = new Codon("E");
            chromosome2[1][9] = new Codon("p");
        }
    }
}
