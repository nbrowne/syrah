﻿/***********************************************************************
 * Chromosome.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Implements the Chromosome class used by the Gene Expression 
 * Programming algorithm. A Chromosome object represents an individual
 * in a GEP population. 
 ***********************************************************************/

using System;
using System.Collections.Generic;

using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    /// <summary>
    /// Represents a GEP chromosome
    /// </summary>
    [Serializable]
    public class Chromosome : IComparable<Chromosome>, IXmlSerializable {
        public readonly int CLASS_VERSION = 2;

        protected int _numberOfGenes = 1;
        protected int _numberOfHomeoticGenes = 0;
        protected int _maxArity = 0;
        protected int _homeoticMaxArity = 0;

        protected double _fitness = 0;
        protected double _proportionalFitness = 0;

        //Rnc member vars
        protected bool _useRnc = false;
        protected RncConstantsType _rncConstantType = RncConstantsType.Rational;
        protected int _rncConstantsPerGene = 0;
        protected double _rncLowerLimit = 0;
        protected double _rncUpperLimit = 0;

        //Chromosome storage.
        protected List<Gene> _genes;

        public List<Gene> Genes {
            get { return _genes; }
            set { _genes = value; }
        }

        public List<Gene> HomeoticGenes { get; protected set; }

        #region Public Properties
        /// <summary>
        /// Get if this chromosome is using RNC
        /// </summary>
        public bool UseRnc {
            get { return _useRnc; }
        }


        /// <summary>
        /// Gets the total length of the gene, including the Dc domains.
        /// </summary>
        public int Length {
            get {
                int length = 0;

                foreach (Gene gene in this.Genes) {
                    length += gene.TotalLength;
                }

                return length;
            }
        }

        /// <summary>
        /// Get the number of RNC constantsPerGene
        /// </summary>
        public int RncConstantsPerGene {
            get { return _rncConstantsPerGene; }
        }


        /// <summary>
        /// Get the lower limit for the RNC constant values.
        /// </summary>
        public double RncLowerLimit {
            get { return _rncLowerLimit; }
        }


        /// <summary>
        /// Get the upper limit of the RNC constant values.
        /// </summary>
        public double RncUpperLimit {
            get { return _rncUpperLimit; }
        }


        /// <summary>
        /// Get or set the Random numerical constants used by the RNC algorithm
        /// </summary>
        //Hack: Commented out this code for now.
        //public List<List<double>> RandomNumericalConstants {
        //    get { return _geneRandomNumericalConstants; }
        //    set { _geneRandomNumericalConstants = value; }
        //}

        /// <summary>
        /// Returns the status of the phenotype construction.
        /// </summary>
        /// <remarks>
        /// Iterates over all of the genes checking their phenotype statuses.
        /// </remarks>
        public bool IsPhenotypeBuilt {
            get {
                bool isBuilt = true;
                
                foreach (Gene gene in this.Genes) {
                    isBuilt &= gene.IsPhenotypeBuilt;
                }

                return isBuilt; }
        }

                
        /// <summary>
        /// Get or set the fitness of this chromosome.
        /// </summary>
        public double Fitness {
            get { return _fitness; }
            set { _fitness = value; }
        }


        /// <summary>
        /// Get or set the proportionalized fitness of this chromosome.
        /// </summary>
        public double ProportionalFitness {
            get { return _proportionalFitness; }
            set { _proportionalFitness = value; }
        }


        /// <summary>
        /// Get or set a gene in the chromosome
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Gene this[int index] {
            get {return Genes[index]; }
            set {
                //Chromosome is changing, reset the phenotype
                this.ResetPhenotype(); 
                
                //Set the chromosome value
                Genes[index] = value;
            }
        }       


        /// <summary>
        /// Get the number of normal genes encoded in this chromosome.
        /// </summary>
        public int GeneCount {
            get { return Genes.Count; }
        }

        /// <summary>
        /// Get the number of homeotic genes encoded in this chromosome.
        /// </summary>
        public int HomeoticGeneCount {
            get { return this.HomeoticGenes.Count; }
        }

        /// <summary>
        /// Get the maximum function arity this chromosome supports.
        /// </summary>
        public int MaxArity {
            get { return _maxArity; }
        }
        
 
        /// <summary>
        /// Get the maximum function arity this chromosome supports.
        /// </summary>
        public int HomeoticMaxArity {
            get { return _homeoticMaxArity; }
        }
        #endregion

        /// <summary>
        /// This is only for xml (de)serialization. Don't use it in code, if you want the 
        /// chromosome to be valid.
        /// </summary>
        public Chromosome() {
        }

        /// <summary>
        /// Instantiate a new chromosome object.
        /// </summary>
        /// <param name="numberOfGenes"></param>
        public Chromosome(int numberOfGenes, int numberOfHomeoticGenes, int maxArity, int homeoticMaxArity, 
            bool useRnc, RncConstantsType rncConstantType, int rncConstantsPerGene, double rncLowerLimit, double rncUpperLimit) {

            //Copy parameters to members
            _numberOfGenes = numberOfGenes;
            _numberOfHomeoticGenes = numberOfHomeoticGenes;
            _maxArity = maxArity;

            _useRnc = useRnc;
            _rncConstantType = rncConstantType;
            _rncConstantsPerGene = rncConstantsPerGene;
            _rncLowerLimit = rncLowerLimit;
            _rncUpperLimit = rncUpperLimit;

            _homeoticMaxArity = homeoticMaxArity;            
            
            //Instantiate the codon storage
            this.Genes = new List<Gene>(numberOfGenes);
        }


        /// <summary>
        /// Returns a string representation of the chromosome.
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            string buffer = "";

            foreach (Gene gene in this.Genes) {
                buffer += gene.ToString();
            }

            return buffer;
        }

        /// <summary>
        /// Implementation of the CompareTo(...) method required by the IComparable interface.
        /// Allows generic lists, etc to be sorted on fitness
        /// </summary>
        /// <param name="chromosome"></param>
        /// <returns></returns>
        public int CompareTo(Chromosome chromosome) {
            return Fitness.CompareTo(chromosome.Fitness);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        public void CopyTo(out Chromosome target) {
            //Instantiate new chromosome with same settings
            target = new Chromosome(_numberOfGenes, _numberOfHomeoticGenes, _maxArity, 
                _homeoticMaxArity, _useRnc, _rncConstantType, _rncConstantsPerGene, _rncLowerLimit, RncUpperLimit);

            //Copy chromosome
            for(int i=0; i<Genes.Count; i++) {
                target.Genes.Add(Genes[i].Copy());
            }            

            target.Fitness = this.Fitness;
            target.ProportionalFitness = this.ProportionalFitness;
        }

        /// <summary>
        /// Copy this chromosome and return the new one.
        /// </summary>
        /// <returns></returns>
        public Chromosome Copy() {
            Chromosome chromosomeCopy = null;
            this.CopyTo(out chromosomeCopy);
            return chromosomeCopy;
        }


        public List<Gene> CutEndGenes(int startIndex) {
            return this.CutGeneRange(startIndex, this.Genes.Count - startIndex);
        }


        public List<Gene> CutGeneRange(int startIndex, int count) {
            List<Gene> cutList = new List<Gene>();

            //First copy the codons to the list
            for (int geneIndex = startIndex; geneIndex < startIndex + count; geneIndex++) {
                cutList.Add(this.Genes[geneIndex]);
            }

            //Now remove the codons from the chromosome
            this.Genes.RemoveRange(startIndex, count);

            //Return the list of genes cut from the chromosome.
            return cutList;
        }


        public void AppendGenes(List<Gene> genes) {
            this.Genes.AddRange(genes);
        }

        /// <summary>
        /// Builts the phenotype representation of this chromosome and stores it
        /// within the individual for later evaluation.
        /// </summary>
        public void BuildPhenotype() {
            //Iterate over each gene in the chromosome, building each phenotype seperately
            for (int geneIndex = 0; geneIndex < this.Genes.Count; geneIndex++) {
                Phenotype genePhenotype = new Phenotype();

                //Configure the gene starting values.
                //These will force the loop to add a new row for the first iteration and add a 
                //single codon. The single codon's arity will cause the loop to continue
                int currentRowArity = 1;
                int lastRowArity = 0;
                int operandCount = 0;
                bool functionInRow = true;

                ///Iterate over the codons in the gene
                foreach (Codon geneCodon in this[geneIndex].GeneCodons) {
                    //If we've filled the row, add a new one
                    if (operandCount >= lastRowArity) {
                        //If we had a row of all terminals, we don't need to process anymore
                        if (!functionInRow) break;

                        //Update for the next row
                        lastRowArity = currentRowArity;
                        currentRowArity = 0;
                        operandCount = 0;
                        functionInRow = false;
                        genePhenotype.AddRow();
                    }

                    //Increment the operands on this row
                    operandCount++;

                    //If the current codon is a function, add it's arity to the current arity count
                    int codonArity = GepEvaluator.GetArity(geneCodon);
                    if (codonArity > 0) {
                        currentRowArity += codonArity;
                        functionInRow = true;
                    }

                    //Add the codon to the running phenotype
                    genePhenotype.AddToCurrent(geneCodon);
                }

                //Set the newly built phenotype for the gene
                this[geneIndex].GenePhenotype = genePhenotype;
                this[geneIndex].IsPhenotypeBuilt = true;
            }
        }

        /// <summary>
        /// Reset the Phenotype of the chromosome. Call this whenever the chromosome changes.
        /// </summary>
        public void ResetPhenotype() {
            foreach (Gene gene in this.Genes) {
                gene.ResetPhenotype();
            }
        }


        #region IXmlSerializable Methods
        /// <summary>
        /// Control how this class is serialized to xml.
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="writer"></param>
        public void WriteXml(XmlWriter writer) {
            //Add a version code to the test value
            writer.WriteAttributeString("XmlVersion", CLASS_VERSION.ToString());

            writer.WriteElementString("numberOfGenes", _numberOfGenes.ToString());
            writer.WriteElementString("numberOfHomeoticGenes", _numberOfHomeoticGenes.ToString());
            writer.WriteElementString("maxArity", _maxArity.ToString());
            writer.WriteElementString("homeoticMaxArity", _homeoticMaxArity.ToString());

            writer.WriteElementString("fitness", _fitness.ToString());
            writer.WriteElementString("proportionalFitness", _proportionalFitness.ToString());

            writer.WriteElementString("useRnc", _useRnc.ToString());
            writer.WriteElementString("rncConstantType", _rncConstantType.ToString());
            writer.WriteElementString("rncConstantsPerGene", _rncConstantsPerGene.ToString());
            writer.WriteElementString("rncLowerLimit", _rncLowerLimit.ToString());
            writer.WriteElementString("rncUpperLimit", _rncUpperLimit.ToString());            

            XmlUtilities.SerializeObject<List<Gene>>(writer, _genes);
        }


        /// <summary>
        /// Control how this class is deserialized from xml
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader) {
            //Read the version code of this testcase
            int version = Int32.Parse(reader.GetAttribute("XmlVersion"));

            //Read past the testcase container
            reader.Read();

            _numberOfGenes = reader.ReadElementContentAsInt();
            _numberOfHomeoticGenes = reader.ReadElementContentAsInt();
            _maxArity  = reader.ReadElementContentAsInt();
            _homeoticMaxArity  = reader.ReadElementContentAsInt();

             _fitness = reader.ReadElementContentAsDouble();
            _proportionalFitness = reader.ReadElementContentAsDouble();

            _useRnc = reader.ReadElementContentAsString().ToLower()=="true";
            _rncConstantType = (RncConstantsType)Enum.Parse(typeof(RncConstantsType), reader.ReadElementContentAsString(), true);
            _rncConstantsPerGene = reader.ReadElementContentAsInt();
            _rncLowerLimit = reader.ReadElementContentAsDouble();
            _rncUpperLimit  = reader.ReadElementContentAsDouble();            

            //read the codon list
            this.Genes = XmlUtilities.DeserializeObject<List<Gene>>(reader);

            //Read past the GepStatistics end tag, otherwise the deserialization process won't continue to the 
            //element.  
            reader.Read();
        }


        public void Validate() {
            string reason = "";

            if (!this.IsValid(out reason)) {
                throw new Exception("Chromosome is invalid. Reason: "+ reason);
            }
        }

        public bool IsValid(out string reason) {
            reason = "";

            foreach (Gene gene in this.Genes) {
                int calculatedTailLength = GepAlgorithm.CalculateTailLength(gene.HeadLength, gene.MaxArity);

                if (gene.TailLength < calculatedTailLength) {
                    reason += "Tail length of gene " + this.Genes.IndexOf(gene).ToString() + " is too short. ";
                }

                if (gene.TailLength > calculatedTailLength) {
                    reason += "Tail length of gene " + this.Genes.IndexOf(gene).ToString() + " is too long. ";
                }

                if (gene.UseRnc && gene.DcDomain.Count < calculatedTailLength) {
                    reason += "Dc domain of gene " + this.Genes.IndexOf(gene).ToString() + " is too short. ";
                }

                if (gene.UseRnc && gene.DcDomain.Count > calculatedTailLength) {
                    reason += "Dc domain of gene " + this.Genes.IndexOf(gene).ToString() + " is too long. ";
                }
            }

            return reason.Length == 0;
        }

        /// <summary>
        /// Returns a null schema, this method is required by the IXmlSerializable interface.
        /// </summary>
        /// <remarks>
        /// This method isn't actually required for what we're doing
        /// </remarks>
        /// <returns></returns>
        public XmlSchema GetSchema() {
            return (null);
        }
        #endregion
        
    } //End of Chromosome class

} //End of namespace
 