﻿/***********************************************************************
 * GepStatistics.cs
 * Syrah Distributed Evolutionary System
 * (C) 2008 Nigel P.A. Browne
 * All rights reserved.
 * 
 * Stores the statistics and data gathered during a GEP run.
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;

using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    [Serializable]
    public class GepStatistics : IXmlSerializable {
        public readonly int CLASS_VERSION = 3;

        //Solution statistics
        public float BestFitness = 0;
        public int BestSolutionGeneration = -1;
        public Chromosome BestChromosome = null;
        
        //Timing statistics
        public int RunTime = -1;
        public int AverageGenerationTime = -1;
        public int MaxGenerationTime = -1;
        public string ResultsText = "";
        public float MaxPossibleFitness = 0;

        //Generational Statistics
        public List<double> AverageGenerationFitness = new List<double>();
        public List<double> BestGenerationFitness = new List<double>();

        public List<int> StochasticSampleRequestGenerations = new List<int>();

        /// <summary>
        /// For each generation, this record the number of occurances of each chromsome
        /// length, describe by the number of genes per chromosome.
        /// </summary>
        /// <remarks>
        /// This was implemented to record the effects of mutating the number of genes in
        /// each chromosome.
        /// </remarks>
        public List<SerializableDictionary<int, int>> ChromosomeGenerationGeneCount = new List<SerializableDictionary<int, int>>();
        
        public List<SerializableDictionary<int, int>> ChromosomeGenerationTotalLengthCount = new List<SerializableDictionary<int, int>>();

        /// <summary>
        /// For each generation, this record the average fitness of each chromsome
        /// length, describe by the number of genes per chromosome.
        /// </summary>
        /// <remarks>
        /// This was implemented to record the effects of mutating the number of genes in
        /// each chromosome.
        /// </remarks>
        public List<SerializableDictionary<int, double>> ChromosomeGenerationGeneFitness = new List<SerializableDictionary<int, double>>();
        public List<SerializableDictionary<int, double>> ChromosomeGenerationTotalLengthFitness = new List<SerializableDictionary<int, double>>();

        public List<SerializableDictionary<int, int>> GenerationGeneCountMutations = new List<SerializableDictionary<int, int>>();

        /// <summary>
        /// Default constructor
        /// </summary>
        public GepStatistics() {

        }


        /// <summary>
        /// Control how this class is serialized to xml.
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="writer"></param>
        public void WriteXml(XmlWriter writer) {
            //Add a version code to the test value
            writer.WriteAttributeString("XmlVersion", CLASS_VERSION.ToString());

            //Add the property elements
            writer.WriteElementString("BestFitness", this.BestFitness.ToString());            
            writer.WriteElementString("RunTime", this.RunTime.ToString());

            XmlUtilities.SerializeObject<Chromosome>(writer, BestChromosome);

            writer.WriteElementString("AverageGenerationTime", this.AverageGenerationTime.ToString());
            writer.WriteElementString("MaxGenerationTime", this.MaxGenerationTime.ToString());
            writer.WriteElementString("ResultsText", this.ResultsText);
            writer.WriteElementString("MaxPossibleFitness", this.MaxPossibleFitness.ToString());
                       
            //Write the lists
            XmlUtilities.SerializeObject<List<double>>(writer, AverageGenerationFitness);
            XmlUtilities.SerializeObject<List<double>>(writer, BestGenerationFitness);
            XmlUtilities.SerializeObject<List<int>>(writer, StochasticSampleRequestGenerations);
            XmlUtilities.SerializeObject<List<SerializableDictionary<int, int>>>(writer, ChromosomeGenerationGeneCount);
            XmlUtilities.SerializeObject<List<SerializableDictionary<int, double>>>(writer, ChromosomeGenerationGeneFitness);
            XmlUtilities.SerializeObject<List<SerializableDictionary<int, int>>>(writer, GenerationGeneCountMutations);

            XmlUtilities.SerializeObject<List<SerializableDictionary<int, int>>>(writer, ChromosomeGenerationTotalLengthCount);
            XmlUtilities.SerializeObject<List<SerializableDictionary<int, double>>>(writer, ChromosomeGenerationTotalLengthFitness);

        }


        /// <summary>
        /// Control how this class is deserialized from xml
        /// </summary>
        /// <remarks>
        /// This method is required by the IXmlSerializable interface.
        /// </remarks>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader) {
            //Read the version code of this testcase
            int version = Int32.Parse(reader.GetAttribute("XmlVersion"));
          
            //Read past the object container
            reader.Read();

            //Read the expected value
            this.BestFitness = reader.ReadElementContentAsFloat();
            this.RunTime = reader.ReadElementContentAsInt();
            
            //Get the chromosome
            this.BestChromosome = XmlUtilities.DeserializeObject<Chromosome>(reader);

            this.AverageGenerationTime = reader.ReadElementContentAsInt();
            this.MaxGenerationTime = reader.ReadElementContentAsInt();
            this.ResultsText = reader.ReadElementContentAsString();
            this.MaxPossibleFitness = reader.ReadElementContentAsFloat();
           
            AverageGenerationFitness = XmlUtilities.DeserializeObject<List<double>>(reader);
            BestGenerationFitness = XmlUtilities.DeserializeObject<List<double>>(reader);
            StochasticSampleRequestGenerations = XmlUtilities.DeserializeObject<List<int>>(reader);
            ChromosomeGenerationGeneCount = XmlUtilities.DeserializeObject<List<SerializableDictionary<int, int>>>(reader);
            ChromosomeGenerationGeneFitness = XmlUtilities.DeserializeObject<List<SerializableDictionary<int, double>>>(reader);
            GenerationGeneCountMutations = XmlUtilities.DeserializeObject<List<SerializableDictionary<int, int>>>(reader);

            if (version >= 3) {
                ChromosomeGenerationTotalLengthCount = XmlUtilities.DeserializeObject<List<SerializableDictionary<int, int>>>(reader);
                ChromosomeGenerationTotalLengthFitness = XmlUtilities.DeserializeObject<List<SerializableDictionary<int, double>>>(reader);
            }

            //Read past the GepStatistics end tag, otherwise the deserialization process won't continue to the element.
            reader.Read();
        }


        /// <summary>
        /// Returns a null schema, this method is required by the IXmlSerializable interface.
        /// </summary>
        /// <remarks>
        /// This method isn't actually required for what we're doing
        /// </remarks>
        /// <returns></returns>
        public XmlSchema GetSchema() {
            return (null);
        }
    }
}
