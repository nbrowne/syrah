﻿using System;
using System.Collections.Generic;

using System.Text;
using System.ComponentModel;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    [Serializable]
    public class CodonCollectionPropertyDescriptor : PropertyDescriptor {
        private AttributeCollection _attributeCollection = null;
        private CodonCollection _collection = null;
        private int _index = -1;

        public CodonCollectionPropertyDescriptor(CodonCollection codonCollection, int index)
            : base("#" + index.ToString(), null) {
            _collection = codonCollection;
            _index = index;
            _attributeCollection = new AttributeCollection(null);
        }

        public override AttributeCollection Attributes {
            get {
                return _attributeCollection;
            }
        }

        public override bool CanResetValue(object component) {
            return true;
        }

        public override Type ComponentType {
            get {
                return this._collection.GetType();
            }
        }

        public override string DisplayName {
            get {
                return this._collection[_index].ToString();
            }
        }

        public override string Description {
            get {
                Codon codon = this._collection[_index];
                string codonDescription = codon.ToString();

                FunctionDetail functionDetail = FunctionManager.GetFunction(codon);
                if (functionDetail != null) codonDescription = functionDetail.Description;

                return codonDescription;
            }
        }

        public override object GetValue(object component) {
            return this.Description;
        }

        public override bool IsReadOnly {
            get { return true; }
        }

        public override string Name {
            get { return "#" + _index.ToString(); }
        }

        public override Type PropertyType {
            get { return this._collection[_index].GetType(); }
        }

        public override void ResetValue(object component) { }

        public override bool ShouldSerializeValue(object component) {
            return true;
        }

        public override void SetValue(object component, object value) {
            // this.collection[index] = value;
        }

    }
}
