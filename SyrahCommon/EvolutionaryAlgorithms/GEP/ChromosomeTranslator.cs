﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.GEP {
    /// <summary>
    /// 
    /// </summary>
    public class ExpressionTreeNode {
        public List<ExpressionTreeNode> Children = null;

        protected Codon _nodeValue = null;
        protected int _arity = 0;

        public int Arity { get { return _arity; } }
        public Codon Value { get { return _nodeValue; } }
        public bool HasChildren { get { return this.Children.Count > 0; } }

        public ExpressionTreeNode(Codon value) {
            _nodeValue = value;
            _arity = GepEvaluator.GetArity(_nodeValue);
            
            this.Children = new List<ExpressionTreeNode>(_arity);
        }

        public override string ToString() {
            return _nodeValue.ToString();
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class ChromosomeTranslator {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// This function works by processing the chromosome from left to right and loading codons
        /// into each level (depth) of the tree. Queues are used for maintain the functions on the 
        /// previous level (depth), so that the operands can be added to the previous level's functions.
        /// </remarks>
        /// <param name="chromosome"></param>
        /// <param name="settings"></param>
        /// <returns>A list of expression tree root nodes, one for each gene in the chromosome.</returns>
        public static List<ExpressionTreeNode> ToExpressionTree(Chromosome chromosome, GepSettings settings) {
            List<ExpressionTreeNode> geneExpressionTrees = new List<ExpressionTreeNode>(chromosome.Genes.Count);

            //Iterate over each gene in the chromosome, building each phenotype seperately
            for (int geneIndex = 0; geneIndex < chromosome.Genes.Count; geneIndex++) {
                Queue<ExpressionTreeNode> currentOperatorQueue = new Queue<ExpressionTreeNode>();
                Queue<ExpressionTreeNode> previousOperatorQueue = new Queue<ExpressionTreeNode>();
                ExpressionTreeNode geneRoot = null;

                //Configure the gene starting values.
                //These will force the loop to add a new row for the first iteration and add a 
                //single codon. The single codon's arity will cause the loop to continue
                bool functionInTreeLevel = true;

                ///Iterate over the codons in the gene
                ///The first time through the loop the previousOperatorQueue will be empty
                ///and will force the initialization of everything, this gives us a nice,
                ///generic handling of the tree levels
                foreach (Codon geneCodon in chromosome[geneIndex].GeneCodons) {
                    ExpressionTreeNode currentNode = new ExpressionTreeNode(geneCodon);

                    //Make the current node the root if we won't have one
                    if (geneRoot == null) geneRoot = currentNode;

                    //If we've used all of the operators from the previous tree level, it's time to move to the 
                    //next tree level
                    if (previousOperatorQueue.Count == 0) {
                        //If we had a level of all terminals, we don't need to process anymore
                        if (!functionInTreeLevel) break;

                        //Update for the next row
                        functionInTreeLevel = false;

                        //Set the queues for the next level
                        previousOperatorQueue = currentOperatorQueue;
                        currentOperatorQueue = new Queue<ExpressionTreeNode>();
                    }

                    //Handle the operators and operands seperately
                    if (currentNode.Arity > 0) {
                        //Set that we found a function in this tree level
                        functionInTreeLevel = true;

                        //Add the operator node to the current operator stack
                        currentOperatorQueue.Enqueue(currentNode);
                    }

                    //add it to the next available child link on the previous row.
                    //We need to verify that there are items on the stack because of the generic handling of the first node
                    if (previousOperatorQueue.Count > 0) {
                        previousOperatorQueue.Peek().Children.Add(currentNode);
                        if (previousOperatorQueue.Peek().Children.Count == previousOperatorQueue.Peek().Arity) {
                            previousOperatorQueue.Dequeue();
                        }
                    }
                    
                }

                //Add the gene expression tree to our list
                geneExpressionTrees.Add(geneRoot);
            }

            return geneExpressionTrees;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="chromosome"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static string ToCSharp(Chromosome chromosome, GepSettings settings) {
            StringBuilder code = new StringBuilder();
            
            //Get the expression trees
            List<ExpressionTreeNode> geneExpressionTrees = ChromosomeTranslator.ToExpressionTree(chromosome, settings);
            
            code.AppendLine("using System;");
            code.AppendLine("using System.Collections.Generic;");
            code.AppendLine();
            code.AppendLine("namespace NigelBrowne.Syrah.Models {");
            code.AppendLine("\tpublic class SyrahModel {");

            if (settings.FitnessMethod == FitnessEvaluationMethod.HitsWithPenalty) {
                code.AppendLine("\t\t//Rounding threshold for the classifier.");
                code.AppendLine("\t\tprotected readonly double HitRoundingThreshold = 0.5;");
            }

            //Define the constant arrays for RNC algorithms
            if (chromosome.UseRnc) {
                code.AppendLine("\t\t#region RNC Arrays for Syrah Model");
                code.AppendLine("\t\tList<List<double>> _rncConstants = new List<List<double>> {");

                foreach(Gene gene in chromosome.Genes) {
                    code.AppendLine("\t\t\tnew List<double> {");
                    code.Append("\t\t\t\t");

                    foreach (double constant in gene.RandomNumericalConstants) {
                        code.Append(constant.ToString());
                        if (gene.RandomNumericalConstants.IndexOf(constant) < gene.RandomNumericalConstants.Count - 1) code.Append(", ");
                    }

                    code.AppendLine();
                    code.Append("\t\t\t}");
                    if (chromosome.Genes.IndexOf(gene) < chromosome.Genes.Count - 1)
                        code.Append(",");
                    code.AppendLine();
                }

                code.AppendLine("\t\t}; //End of constant definition");
                code.AppendLine("\t\t#endregion");
                code.AppendLine();
            }

            //Define the support methods
            code.AppendLine("\t\t#region Support Methods for Syrah Model");            

            //Iterate over the functions used and output the calls
            foreach (Codon functionCodon in settings.FunctionList) {
                FunctionDetail function = FunctionManager.GetFunction(functionCodon);

                //Only output the methods for calls (duh)
                if (function.Type == FunctionType.Call) {
                    code.Append("\t\tprotected double " + function.FunctionName + "(");

                    //Output the parameters
                    for (int i = 0; i < function.Arity; i++) {
                        if (i > 0) code.Append(", ");
                        code.Append("double p" + i.ToString());
                    }

                    code.AppendLine(") {");
                    code.AppendLine("\t\t\treturn " + function.Code);
                    code.AppendLine("\t\t}");
                    code.AppendLine();
                }
            }

            code.AppendLine("\t\t#endregion");
            code.AppendLine();

            code.AppendLine("\t\t//Automatically generated method by Syrah.");
            code.Append("\t\tdouble EvaluateModel(");

            //Add the method operands (ie, the terminals)
            foreach (Codon terminal in settings.TerminalList) {
                if (settings.TerminalList.IndexOf(terminal) > 0) code.Append(", ");
                code.Append("double " + terminal.ToString());
            }

            //End the method signature
            code.AppendLine(") {");

            //Define the return value
            code.AppendLine("\t\t\tdouble returnValue = ");

            Stack<string> geneCodeStack = new Stack<string>();

            //Iterate over each gene expression tree
            foreach (ExpressionTreeNode geneRoot in geneExpressionTrees) {
                int geneIndex = geneExpressionTrees.IndexOf(geneRoot);
                int rncDcIndex = 0;

                if (geneExpressionTrees.IndexOf(geneRoot) > 0) {
                    code.AppendLine("\t\t\t\t" + settings.StaticLinkingFunction.ToString() + " // Static linking function");
                    code.AppendLine();
                }

                //Add a comment that we're starting a new gene
                code.AppendLine("\t\t\t\t//Gene " + (geneIndex + 1).ToString() + " of " + geneExpressionTrees.Count.ToString());

                //Hand off to the worker
                ChromosomeTranslator.ExpressionTreeToCSharp(code, chromosome, geneIndex, ref rncDcIndex, geneRoot, 0, geneCodeStack);

                //Pop the code for this gene off the stack
                string geneCode = geneCodeStack.Pop();
                code.AppendLine("\t\t\t\t" + geneCode);
            }

            code.AppendLine("\t\t\t;");
            code.AppendLine();

            if (settings.FitnessMethod == FitnessEvaluationMethod.HitsWithPenalty) {
                code.AppendLine("\t\t//Force into a classifier range");
                code.AppendLine("\t\treturnValue = returnValue >= this.HitRoundingThreshold ? 1.0 : 0.0;");                
            }

            code.AppendLine("\t\t\treturn returnValue;");
            code.AppendLine("\t\t} //End of evaluate method.");
            code.AppendLine("\t} //End of Class");
            code.AppendLine("}");
            
            return code.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="root"></param>
        /// <param name="constants"></param>
        protected static void ExpressionTreeToCSharp(StringBuilder code, Chromosome chromosome, int geneIndex, ref int rncDcIndex, ExpressionTreeNode node, int level, Stack<String> codeStack) {
            string codeFragment = "";

            //if (node.HasChildren) {
            //    code.AppendLine("(");
            //    code.Append('\t', level + 2);
            //}
            
            //code.Append(" "+ node.Value.ToString() +" ");            

            
            foreach (ExpressionTreeNode child in node.Children) {
                ChromosomeTranslator.ExpressionTreeToCSharp(code, chromosome, geneIndex, ref rncDcIndex, child, level + 1, codeStack);

            }

            //If we're in a function node, pop the parameters off the stack and add to the code
            if (node.Arity > 0) {
                FunctionDetail functionDetail = FunctionManager.GetFunction(node.Value);

                //add the function name and starting bracket
                codeFragment += functionDetail.FunctionName +"(";                

                //Iterate over the parameters required for the current function. The parameters
                //are the existing code fragments on the stack.
                for (int i = 0; i < functionDetail.Arity; i++) {
                    //Get the parameter code from the stack
                    string parameterCode = codeStack.Pop();

                    //For function calls, we need to seperate the parameters with commas
                    if (i > 0 && functionDetail.Type == FunctionType.Call) {
                        codeFragment += ", ";
                    }

                    //Add the parameter code to the code fragment.
                    codeFragment += parameterCode;

                    //If we're using an operator, add the operator
                    if (i < functionDetail.Arity-1 && functionDetail.Type == FunctionType.Operator) {
                        codeFragment += " " + functionDetail.Code + " ";
                    } 
                } 

                //If the function is a call, then end the call with a closing bracket
                codeFragment += ")";                

                //Push the new code fragment onto the code stack
                codeStack.Push(codeFragment);
            } else {
                
                //Genereate different code for terminals and rnc values
                if (node.ToString() != "?") {
                    codeFragment = node.Value.ToString();
                } else {

                    int rncValue = Int32.Parse(chromosome[geneIndex].DcDomain[rncDcIndex].ToString());
                    codeFragment += "_rncConstants["+ geneIndex.ToString() +"]["+ rncValue.ToString() +"]";

                    //Incremement the Dc index for the next rnc request, this is part of the GEP algorithm
                    rncDcIndex++;                    
                }
                
                //Push the literal code fragment onto the stack
                codeStack.Push(codeFragment);
            }
        }

        /*
        public static string ToCSharp2(Chromosome chromosome, GepSettings settings) {
            string code = "";

            //We need the phenotype, so build it if required
            if (!chromosome.IsPhenotypeBuilt) {
                chromosome.BuildPhenotype();
            }

            //Iterate over each gene, evaluating it seperately
            //Iterate over each gene in the chromosome, building each phenotype seperately
            for (int codonIndex = 0; codonIndex < chromosome.TotalGeneCount; codonIndex++) {
                Phenotype genePhenotype = chromosome.GenePhenotypes[codonIndex];
                int dcElementIndex = 0;

                //Start at the bottom and iterate over each row
                for (int rowIndex = genePhenotype.Count - 1; rowIndex >= 0; rowIndex--) {
                    int previousRowIndex = 0;

                    //Move left to right across each row, either subsituting terminals or evaluating functions
                    for (int codonIndex = 0; codonIndex < genePhenotype[rowIndex].Count; codonIndex++) {
                        //Assign the current codon to a local for clarity
                        Codon currentCodon = genePhenotype[rowIndex][codonIndex];

                        //Get the codon arity
                        int codonArity = GepEvaluator.GetArity(currentCodon);

                        //If we have a terminal (arity==0) then put it value into our row
                        //Terminals are the only case that we need to worry about the 
                        //homeotic or normal gene values. 
                        if (codonArity == 0) {
                            //Check if we're handling a normal gene or homeotic. 
                            if (codonIndex < chromosome.Genes) {
                                //Normal gene, so check if it's an RNC place holder
                                if (currentCodon == "?") {
                                    //Get the index in the RNC array from the next Dc value
                                    //RNC elements are 1 indexed, so we need to subtract one to get the real element index
                                    Codon dcElement = chromosome.GetGeneDcElement(codonIndex, dcElementIndex);
                                    int rncElementIndex = Int32.Parse(dcElement.ToString()) - 1;

                                    //Rnc place holder, grab the next rnc value
                                    currentRowValues.Add(chromosome.RandomNumericalConstants[codonIndex][rncElementIndex]);

                                    //Increment the Dc index, so the next time we see an RNC placeholder, we 
                                    //use the next element in the Dc domain
                                    dcElementIndex++;

                                } else {
                                    //normal gene and normal terminal, just add the value of the terminal
                                    currentRowValues.Add(terminalValues[currentCodon]);
                                }

                            } else {
                                //Homeotic gene, get the index of the gene it references
                                //This is done by converting the value of the codon to a string
                                //and then converting it to an integer. Need to subtract one
                                //because the homeotic gene references are one-based and the 
                                //geneValue list is zero-based.
                                int geneReference = Int32.Parse(currentCodon.ToString()) - 1;

                                //Add the value of the previously evaluated gene
                                currentRowValues.Add(geneValues[geneReference]);
                            }
                        }

                        //Now handle functions.
                        if (codonArity > 0) {
                            //Initialize the parameter list
                            List<double> functionParameters = new List<double>();

                            //Load the parameter values from the previous row
                            for (int i = 0; i < codonArity; i++) {
                                functionParameters.Add(previousRowValues[previousRowIndex]);
                                previousRowIndex++;
                            }

                            //Now evaluate the function
                            currentRowValues.Add(this.EvaluateCodon(currentCodon, functionParameters));
                        }
                    }

                    //We're about to move up a row, so update the previous row values. These will be the inputs to the 
                    //function in the current (next) row.
                    previousRowValues = currentRowValues;
                } //End of row iterator

                //Throw an error in usual states
                if (previousRowValues.Count != 1) throw new ArgumentException("Phenotype evaluation encountered an error. The gene ended up with more than one root value!");

                //Add the value of the root position to the gene value list
                geneValues.Add(previousRowValues[0]);

            } //End of gene iterator

            return code;
        }
         * */
    }
}
