﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// Partially from http://www.cs.uwyo.edu/~wspears/functs/dejong.c
    /// </summary>
    public class DeJong5 : IParameterOptimizationFunction {
        public double Evaluate2(List<double> x) {
            double result = 0.0002;
            double[,] a = new double[,] {
                {-32.0,-16.0,0.0,16.0,32.0},
                {-32.0,-16.0,0.0,16.0,32.0}
                         };

            for (int j = 0; j < 25; j++) {
                double sum2 = 0;

                for (int i = 0; i < 2; i++) {
                    sum2 += Math.Pow(x[i] + a[i, j/5] , 6);
                }

                result += 1 / (j+1 + sum2);
            }

            return result * -1.0;
        }

        protected double GetA(double i) {
            return 16 * ((i % 5) - 2);
        }

        protected double GetB(double i) {
            return 16 * (Math.Floor(i/5) - 2);
        }

        public double Evaluate(List<double> inputs) {
            double result = 0;
            double x = inputs[0];
            double y = inputs[1];
            double sum = 0;

            for (int i = 0; i < 25; i++) {
                double temp = 1 + i + Math.Pow(x - GetA(i), 6) + Math.Pow(y - GetB(i), 6);
                sum += 1 / temp;
            }            

            result = 500 - (1 / (0.002 + sum));


            return result;
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            bool inRange = true;

            for (int i = 0; i < this.GetParameterCount(); i++) {
                inRange &= -65.536 <= parametersValues[i] && parametersValues[i] <= 65.536;
            }

            return inRange;
        }

        public int GetParameterCount() {
            return 2;
        }
    }
}
