﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    public interface IParameterOptimizationFunction {
        int GetParameterCount();
        double Evaluate(List<double> parameterValues);
        bool AreParametersInRange(List<double> parameterValues);
    }
}
