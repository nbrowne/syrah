﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// </summary>
    public class DeJong4 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double sum = 0;

            for (int i = 1; i <= this.GetParameterCount(); i++) {
                double noise = SimpleRNG.GetNormal();
                double power = Math.Pow(parameterValues[i - 1], 4);
                sum += i * power + noise;
            }

            return sum;
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            bool inRange = true;

            for (int i = 0; i < this.GetParameterCount(); i++) {
                inRange &= -1.28 <= parametersValues[i] && parametersValues[i] <= 1.28;
            }

            return inRange;
        }

        public int GetParameterCount() {
            return 30;
        }
    }
}
