﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// </summary>
    public class DeJong1 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double x1 = parameterValues[0];
            double x2 = parameterValues[1];
            double x3 = parameterValues[2];

            return x1 * x1 + x2 * x2 + x3 * x3;
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            bool inRange = true;

            for (int i = 0; i < this.GetParameterCount(); i++) {
                inRange &= -5.12 <= parametersValues[i] && parametersValues[i] <= 5.12;
            }

            return inRange;
        }

        public int GetParameterCount() {
            return 3;
        }
    }
}
