﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// </summary>
    public class DeJong2 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double x1 = parameterValues[0];
            double x2 = parameterValues[1];

            return 100.0 * Math.Pow(x1 * x1 - x2, 2) + Math.Pow(1 - x1, 2);
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            bool inRange = true;

            for (int i = 0; i < this.GetParameterCount(); i++) {
                inRange &= -2.048 <= parametersValues[i] && parametersValues[i] <= 2.048;
            }

            return inRange;
        }

        public int GetParameterCount() {
            return 2;
        }
    }
}
