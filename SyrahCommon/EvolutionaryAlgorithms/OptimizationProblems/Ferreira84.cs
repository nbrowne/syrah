﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// Implements the parameter optimization problem from C. Ferreira's 
    /// 2006 book #8.4.
    /// f(x0) >= f(x) for all x in [-1, 2]
    /// 
    /// This function was used in Michalewicz1996 to show how GAs work.
    /// </summary>
    public class Ferreira84 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double x = parameterValues[0];
            return x * Math.Sin(10 * Math.PI * x) + 1.0;
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            return -1 <= parametersValues[0] && parametersValues[0] <= 2; //[-1, 2]
        }

        public int GetParameterCount() {
            return 1;
        }
    }
}
