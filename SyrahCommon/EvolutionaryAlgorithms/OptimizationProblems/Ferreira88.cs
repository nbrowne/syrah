﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// Implements the parameter optimization problem from C. Ferreira's 
    /// 2006 book #8.8 (pg 331)
    /// 
    /// f(p1, p2, p3, p4, p5) = 0.5 + ...
    /// 
    /// 0 <= p1,...,p5 <= 10 
    /// 
    /// No known max for this function
    /// </summary>
    public class Ferreira88 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double p1 = parameterValues[0];
            double p2 = parameterValues[1];
            double p3 = parameterValues[2];
            double p4 = parameterValues[3];
            double p5 = parameterValues[4];
            

            return 0.5 + (Math.Sin(Math.Sqrt(p1*p1 + p2*p2)) - 1.25*Math.Cos(p3*p4*p5) ) / Math.Sqrt(1 + 0.001 * (p3*p3 + p4*p4));
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            bool inRange = true;

            for (int i = 0; i < 5; i++) {
                inRange &= 0 <= parametersValues[i] && parametersValues[i] <= 10;
            }

            return inRange;
        }

        public int GetParameterCount() {
            return 5;
        }
    }
}
