﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// </summary>
    public class DeJong3 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double sum = 0;

            for (int i = 0; i < this.GetParameterCount(); i++) {
                sum += (int)parameterValues[i];
            }

            return sum;
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            bool inRange = true;

            for (int i = 0; i < this.GetParameterCount(); i++) {
                inRange &= -5.12 <= parametersValues[i] && parametersValues[i] <= 5.12;
            }

            return inRange;
        }

        public int GetParameterCount() {
            return 5;
        }
    }
}
