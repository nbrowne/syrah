﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.EvolutionaryAlgorithms.OptimizationProblems {
    /// <summary>
    /// Implements the parameter optimization problem from C. Ferreira's 
    /// 2006 book #8.7
    /// f(x,y) = -x * sin(4x) - 1.1y * sin(2y)
    /// 0 <= x <= 10 and 0 <= y <= 10
    /// </summary>
    public class Ferreira87 : IParameterOptimizationFunction {
        public double Evaluate(List<double> parameterValues) {
            double x = parameterValues[0];
            double y = parameterValues[1];

            return -1 * x * Math.Sin(4.0 * x) - 1.1 * y * Math.Sin(2.0 * y);
        }

        public bool AreParametersInRange(List<double> parametersValues) {
            double x = parametersValues[0];
            double y = parametersValues[1];

            return 0 <= x && x <= 10 && 0 <= y && y <= 10;
        }

        public int GetParameterCount() {
            return 2;
        }
    }
}
