﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NigelBrowne.Syrah.Common.Exceptions {
    public class SyrahGeneralException : Exception {
        /// <summary>
        /// 
        /// </summary>
        public SyrahGeneralException()
            : base() {
        }

        
        /// <summary>
        /// 
        /// </summary>
        public SyrahGeneralException(string message, Exception innerException)
            : base(message, innerException) {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public SyrahGeneralException(string message)
            : base(message) {
        }

        
    }
}
